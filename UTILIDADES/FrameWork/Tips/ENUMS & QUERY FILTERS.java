		private static final int OLF_SUCCEED 			=	OLF_RETURN_CODE.OLF_RETURN_SUCCEED.jvsValue();
		private static final EnumTypeMessage MSG_INFO	=	EnumTypeMessage.INFO;
		private static final EnumTypeMessage MSG_ERROR	=	EnumTypeMessage.ERROR;
		private static final COL_TYPE_ENUM COL_INT		=	COL_TYPE_ENUM.COL_INT;
		private static final COL_TYPE_ENUM COL_CELL		=	COL_TYPE_ENUM.COL_CELL;
		private static final COL_TYPE_ENUM COL_STRING	=	COL_TYPE_ENUM.COL_STRING;
		private static final COL_TYPE_ENUM COL_DOUBLE	=	COL_TYPE_ENUM.COL_DOUBLE;

		// UDW
		private static final int LABEL				=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt();
		private static final int COMBOBOX			=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt();
		private static final int TEXTAREA			=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_TED_WIDGET.toInt();
		private static final int BUTTON				=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt();

		// TRAN_STATUS
		private static final int STATUS_NEW			=	TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt();			//2
		private static final int STATUS_VALIDATED	=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();		//3
		private static final int STATUS_PROPOSED	=	TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt();		//7
		private static final int STATUS_PENDING		=	TRAN_STATUS_ENUM.TRAN_STATUS_PENDING.toInt();		//1

		// TRAN_TYPE
		private static final int TRAN_TYPE_TRADING	=	TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();		//0
		private static final int TRAN_TYPE_HOLDING	=	TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt();		//2

		// BUY_SELL
		private static final int BUY_TYPE			=	BUY_SELL_ENUM.BUY.toInt();	//0
		private static final int SELL_TYPE			=	BUY_SELL_ENUM.SELL.toInt();	//1

		// TOOLSET
		private static final int TOOLSET_LOANDEP	=	TOOLSET_ENUM.LOANDEP_TOOLSET.toInt();			//6
		private static final int TOOLSET_EQUITY		=	TOOLSET_ENUM.EQUITY_TOOLSET.toInt();			//28
		private static final int TOOLSET_FX			=	TOOLSET_ENUM.FX_TOOLSET.toInt();				//9
		private static final int TOOLSET_CASH		=	TOOLSET_ENUM.CASH_TOOLSET.toInt();				//10
		private static final int TOOLSET_GENERIC_FU	=	TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt();	//50
		private static final int TOOLSET_FIN_FUT	=	TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt();			//41
		private static final int TOOLSET_BOND_FUT	=	TOOLSET_ENUM.BONDFUT_TOOLSET.toInt();			//34
		private static final int TOOLSET_BOND_FUT	=	TOOLSET_ENUM.BOND_TOOLSET.toInt();				//5

		// ASSET_TYPE
		private static final int ASSET_TRADING		=	ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt();		//2
		private static final int ASSET_SEC_INV_PYMT	=	ASSET_TYPE_ENUM.ASSET_TYPE_SEC_INV_PYMT.toInt()	//8

		// CFLOW_TYPE
		private static final int CFLOW_MOV_TRABAJADORES			=	EnumsUserCflowType.MX_CFLOW_TYPE_MOV_TRABAJADORES.toInt();		//266
		private static final int CFLOW_MARGIN_CALL_AIMS			=	EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt();		//271
		private static final int CFLOW_COMISIONES_SL			=	EnumsUserCflowType.MX_CFLOW_TYPE_COMISIONES_SL.toInt();			//2031
		private static final int CFLOW_COMISION_SALDO			=	EnumsUserCflowType.MX_CFLOW_TYPE_COMISION_SALDO.toInt();		//2017
		private static final int CFLOW_AIMS 					=	EnumsUserCflowType.MX_CFLOW_TYPE_AIMS.toInt();					//2018
		private static final int CFLOW_FINAL_PRINCIPAL			=	CFLOW_TYPE.FINAL_PRINCIPAL_CFLOW.toInt();						//26
		private static final int CFLOW_MOV_ACCIONES_AFORE_A2 	=	EnumsUserCflowType.MX_CFLOW_TYPE_MOV_ACCIONES_AFORE_A2.toInt();	//2030

		// INS_TYPE
		private static final int INS_TYPE_DEPO_CHEQUERA		=	EnumsInstrumentsMX.MX_DEPO_CHEQUERA.toInt();				//1000056
		private static final int INS_TYPE_EQT_CKDKCALL		=	EnumsInstrumentsMX.MX_EQT_CKDKCALL.toInt();					//1000043

		// PARTY - BU
		private static final int PARTY_BBVA_BANCOMER		=	EnumsParty.MX_BBVA_BANCOMER_BU.toInt();						//20045
		private static final int PARTY_BBVA_DER				=	EnumsParty.MX_BBVA_DER_BU.toInt();							//20720
		private static final int PARTY_GOLDMAN_SACH 		=	EnumsParty.MX_GOLDMAN_SACH_BU.toInt();						//20057
		private static final int PARTY_GOLDMAN_SACH_SL		=	EnumsParty.MX_GL_SL_BU.toInt();								//20728
		private static final int PARTY_HSBC_FUTURES			=	EnumsParty.MX_HSBC_FUTURES_BU.toInt();						//20061
		private static final int PARTY_SCOTIABANK			=	EnumsParty.MX_SCOTIA_BU.toInt();							//20080
		private static final int PARTY_BANAMEX				=	EnumsParty.MX_BANAMEX_BU.toInt();							//20037
		private static final int PARTY_HSBC 				=	EnumsParty.MX_HSBC_BU.toInt();								//20406
 		private static final int PARTY_SINDEVAL				=	EnumsParty.MX_SDINDEVAL_BU.toInt();							//20363

 		// PORTFOLIOS
 		private static final int SB1	=	EnumsPortfolios.MX_PORTFOLIO_SB1.toInt();
 		private static final int SB2	=	EnumsPortfolios.MX_PORTFOLIO_SB2.toInt();
 		private static final int SB3	=	EnumsPortfolios.MX_PORTFOLIO_SB3.toInt();
 		private static final int SB4	=	EnumsPortfolios.MX_PORTFOLIO_SB4.toInt();

 		// TRAN_INFO
		private static final String TRAN_INFO_HORA_CONCERTACION		=	EnumsTranInfoFields.MX_TRAN_HORA_CONCERTACION.toString();	//Trad Tran Hora de Concertacion
 		private static final String TRAN_INFO_TIPO_COMPRAVENTA		=	EnumsTranInfoFields.MX_TRAN_TIPO_COMPRA_VENTA.toString();	//Trad Tran Tipo Compra Venta

 		// PARTY_INFO
		private static final String PARTY_INFO_ID_CONSAR			=	EnumsPartyInfoFields.MX_PARTY_ID_CONSAR.toString();			//Id_Consar

		// USER TABLES
		private static final String USER_MX_TRASPASOS_ANI_CATALOGO	=	EnumsUserTables.USER_MX_TRASPASOS_ANI_CATALOGO.toString();