private void functionsExamples(){
	// Select
		tTablaUno.select(tTablaDos, "internal_rating", "columnaTablaDos LE $columnaTablaUno");

	// Select renombrando columa
		tTablaUno.select(tTablaDos, "internal_rating(ir)", "columnaTablaDos LE $columnaTablaUno");

	// Obtener formato Julian date dada una fecha string con casi cualquier formato soportado
		int iDate 	=	OCalendar.parseString(sDate);

	// Obtener tipo de fecha para consulta a la bd
		String sDateForDB 	=	OCalendar.formatJdForDbAccess(int iJulianDate);
}
//-----------------------------------------------------------------//
//-----------------------------------------------------------------//
//-----------------------------------------------------------------//

// Obtener resultados de query en una tabla
private Table getPortfolios(){
	
	Table tPortfolios = Util.NULL_TABLE;
	
	try {
		tPortfolios = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select ");
		sb.append("\n 	p.id_number as internal_portfolio");
		sb.append("\n 	,p.name ");
		sb.append("\n from portfolio p ");
		sb.append("\n where ");
		sb.append("\n 	p.portfolio_type = 0 ");
		sb.append("\n 	and p.restricted = 1 ");
		DBaseTable.execISql(tPortfolios, sb.toString());
	} catch (OException e) {
		_Log.printMsg(EnumTypeMessage.ERROR,
				"Unable to get the data from query on getPortfolios function."
				+ "Error: " + e.getMessage());
	}

	return tPortfolios;
}

private void sendMails() throws OException {
	
	int iResponse = Ask.okCancel("Desea enviar los emails?");
	
		if(iResponse==1){
		// Declare mail variables
			String sMails		=	"";
			String sSubject		=	"";
			String sMessage		=	"";
			String sBottomImage	=	"";
			boolean mailResult	=	false;
				
		// Setting mail content from user_configurable_variables
			try {
				
				sMails			=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "mails");
				sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "subject");
				sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "body");
				sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
			
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to load variables for send mail, "
						+e.getMessage());
			}
			
		// Format
			sSubject		=	sSubject.replace("$TODAY", sTodaySlashDDMMAAAA);
			sMessage		=	sMessage.replace("$TODAY", sTodaySlashDDMMAAAA);

		// Sending Mails
			mailResult = UtilAfore.sendMail(
					sMails,
					sSubject,
					sMessage,
					sBottomImage,
					sFullFileNameXLS
					);
			
			if (!mailResult){
				Ask.ok("No fue posible enviar el email");
			} else {
				Ask.ok("Email enviado exitosamente");
			}
		}
}

// Verificar diferencias entre estas funciones
//-----------------------------------------------------------------//
private void pushUserTableToDB(Table table) throws OException {
	//If Usertable exists on DB
		if (DBUserTable.userTableIsValid(table.getTableName()) == 1){					
			//Drop previously created usertable
			try {
				DBUserTable.drop(table);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at pushUserTablesToDB(), failed to drop previuos user table, "
						+ e.getMessage());
			}
	    }
		
	//Create the structure of a table on db from a memory table 
		try {
			DBUserTable.create(table);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at pushUserTablesToDB(), failed to create the structure of usertable on DB, "
					+ e.getMessage());
		}


	//Push the memory table(with loaded info) to the user table with same name
		try {
			DBUserTable.bcpIn(table);
		} catch (OException e) {

			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at pushUserTablesToDB(), failed to push usertable, "
					+ e.getMessage());
		}
}
private boolean updateUsertable(Table tUserTable){	
	String	sUserTableName	=	"";
	int		iRetVal			=	0;
	
	// Get UserTable Name
		try {
			sUserTableName = tUserTable.getTableName();
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to get UserTable name, on updateUsertable function. Error: "
							+ e.getMessage());
		}
	
	// Clear previous version of UserTable on DB
		try {
			DBUserTable.clear(tUserTable);
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to clear UserTable"+sUserTableName+", on updateUsertable function. Error: "
							+ e.getMessage());
		}
	
	// Push memory table to DB 
		try {
			iRetVal = DBUserTable.bcpIn(tUserTable);
		} catch (OException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
    if (iRetVal == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()){
    	return true;
    } else return false;
}
//-----------------------------------------------------------------//
private void generateReport() {
		
	// Select data to show on report
		try {
			tOutput.select(tData,
					"contraparte, "
					+ "fondo, "
					+ "monto, "
					+ "tasa, "
					+ "plazo, "
					+ "intereses, "
					+ "total_importe, "
					+ "cuenta ",
					"deal_tracking_num GT 0"
				);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to set selected columns into tOutput Table : "
				+ e.getMessage());
		}
				
	// Configure paths and filenames
		try {
			String sTemplatePath = Crystal.getRptDir();
			String sTemplateName = "ReporteChequerasBancos.rpt";
			sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
		
			String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "path");
			String sFileName = "Reportes_Chequeras_Bancos_Interno_"+sTodayAAAAMMDD;
			sFullFileNameXLS = sFilePath+sFileName+".xls";
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to set selected columns into tOutput Table : "
				+ e.getMessage());
		}
		
	//Exporting excel
		try {
			Crystal.tableExportCrystalReport(tOutput, 
					sFullTemplateName, 
					CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
					sFullFileNameXLS, 
					CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
					);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to export Excel document. "
				+e.getMessage());
		}
}
private void generarReporte() throws OException, IOException {
	
	String sSelectedPortfolio = tReturnt.scriptDataGetWidgetString("Portfolio");
	
	Table tOutput = Table.tableNew();
	tOutput.select(tReturnt, "*", "portfolio EQ " + Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sSelectedPortfolio));		

	Table tParameters = Table.tableNew("Parametros Crystal");
	int iNewRow = tParameters.addRow();
	
	tParameters.addCol("varPortfolio", COL_TYPE_ENUM.COL_STRING);
	tParameters.addCol("varFecha", COL_TYPE_ENUM.COL_DATE_TIME);		
	
	tParameters.setString("varPortfolio", iNewRow, sSelectedPortfolio);
	tParameters.setDateTime("varFecha", iNewRow, ODateTime.strToDateTime(sToday));
	
	// Getting configurable variables to get path for report files
	String sPath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_DWS_LSAR", "path");
	String sFullFilePath = sPath + "NombreArchivo.xls";
	String sFullTemplatePath = Crystal.getRptDir() + "\\" + "ReporteLSAR.rpt";
	
	//Exporting excel
	Crystal.tableExportCrystalReport(tOutput, 
			sFullTemplatePath, 
			CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
			sFullFilePath, 
			CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
			tParameters);
	
	//Opening Report
	SystemUtil.createProcess(sFullFilePath);
}
//-----------------------------------------------------------------//
// Correr Simulación Online
public void getIntraDaySIM(Table tTable, EnumsUserSimResultType SimResType, 
		int iJulianDate, String sColDestino, UTIL_Log LOG) throws OException{
      
		// Setting portfolio table
		String sPortafolioQuery = "SELECT id_number, name as Portfolio FROM portfolio";
		Table tPortfolio = Table.tableNew();
		DBaseTable.execISql(tPortfolio, sPortafolioQuery);
		
		
		for (int i =1; i<tPortfolio.getNumRows();i++){
		      Table tAuxSimResults = Util.NULL_TABLE;
		      
		      // Run simulation and get values
		      tAuxSimResults = SimResult.tableLoadSrun(tPortfolio.getInt("id_number", i), SIMULATION_RUN_TYPE.INTRA_DAY_SIM_TYPE, iJulianDate, 0);
		      Table tAuxTranResults = SimResult.getTranResults(tAuxSimResults);
		      
		      // Get col num of searched column
		      int iColSim = tAuxTranResults.getColNum("" + SimResType.toInt());
		      
		      // if the col is found
	            if (iColSim > 1){
	            	
	            	tAuxTranResults.setColName(iColSim, sColDestino);
	            	tTable.select(tAuxTranResults, sColDestino, "deal_num EQ $deal_tracking_num");
	            }
		}
}
// Corre una simulacion y obtiene los datos
private void runOnlineSimulation(){

	        if (tArgt.getColNum("QueryId")<1)
            	tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);

            // Run the simulation for trades queried
            Table tResList = Sim.createResultListForSim();
            
            SimResultType simResultTypeMTM = SimResultType.create(PFOLIO_RESULT_TYPE.PV_RESULT.toString());
            SimResultType simResultTypeCalcPrice = SimResultType.create(PFOLIO_RESULT_TYPE.CALC_PRICE_RESULT.toString());
            
            SimResult.addResultForSim(tResList, simResultTypeMTM);
            SimResult.addResultForSim(tResList, simResultTypeCalcPrice);

                  int iQuery = Query.tableQueryInsert(tAllTrades, "tran_num");

            // The QueryId is now set into argt to prepare for the execution of a simulation.
            tArgt.setInt("QueryId", 1, iQuery);

            tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
            Query.clear(iQuery);

            // Extract the results from the simulation table for further processing
            Table tRes = SimResult.getTranResults(tSimRes);
            
            tReturnt.select(tRes, Str.intToStr(PFOLIO_RESULT_TYPE.CALC_PRICE_RESULT.toInt()) + "(indep_price)", "deal_num EQ $deal_num and deal_leg EQ 0");
}
//-----------------------------------------------------------------//
// Obtiene los datos almacenados de simulaciones ejecutadas con anterioridad
public void getSimulation(Table tblOperaciones, int iDate) throws OException
    {
        Table tblPortfolios = Table.tableNew("tblPortfolios");
        Table tSimResult = Table.tableNew("tblSimResults");
        
        int iScenario = 1;

        tblPortfolios.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT);
        tblOperaciones.copyColDistinct("sig_internal_portfolio", tblPortfolios, "internal_portfolio");

        _Log.printMsg(MSG_INFO, "Obteniendo Sim Results por Portfolio\n");
        
        for(int i = 1; i <= tblPortfolios.getNumRows(); i++)
        {
            int iPortfolio = tblPortfolios.getInt("internal_portfolio", i);
            
            String sCurrentPortfolio = Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, iPortfolio);
            
            _Log.printMsg(MSG_INFO, "Obteniendo Sim Results desde " + sCurrentPortfolio + "\n");
            
            tSimResult = SimResult.tableLoadSrun(iPortfolio, SIMULATION_RUN_TYPE.INTRA_DAY_SIM_TYPE, iDate, 0);
                
            if (tSimResult.getNumRows() <= 0)
                _Log.printMsg(MSG_ERROR, "No existe simulacion INTRA DAY para el portfolio " + sCurrentPortfolio + "\n");
            
            Table tTranResults = SimResult.getTranResults(tSimResult, iScenario);
            
            if (tTranResults.getNumRows() > 0) 
            {               
                tblOperaciones.select(tTranResults,  PFOLIO_RESULT_TYPE.ACCRUED_INTEREST_RESULT.jvsValue() + "(sig_interes_devengado)," +
                                                     PFOLIO_RESULT_TYPE.DAILY_ACCRUED_INTEREST_RESULT.jvsValue() + "(sig_interes_devengado_daily)," +
                                                     EnumsUserSimResultType.BIP_SIM_ACCT_RESULT_INT_TOTALES.toInt() + "(sig_intereses_totales)," +
                                                     EnumsUserSimResultType.BIP_SIM_ACCT_RESULT_DEV_INT_DRIARIO.toInt() + "(sig_devengo_int_diario)", "deal_num EQ $sig_deal_num");
                tTranResults.destroy();
            }
            else
                _Log.printMsg(MSG_ERROR,"No existen resultados para la simulacion INTRA DAY - Tran Results.\n");
        }
        
        tblPortfolios.destroy();
    }

public void getSimulation2(){
	for (int i=1; i<= tPortfolios.getNumRows(); i++){
		int iRowPortfolioId = tPortfolios.getInt("id_number", i);
		
		// Get simultaion results
	    int iScenario = 1; // Market Price
	    Table tSimResult = SimResult.tableLoadSrun(iRowPortfolioId, SIMULATION_RUN_TYPE.INTRA_DAY_SIM_TYPE, iToday, 0);
	    Table tTranResults = SimResult.getTranResults(tSimResult, iScenario);
	    
	    // Paste Mtm where deal tracking num = deal num of simulation
	    tTraspasos.select(tTranResults, "0(mark_to_market)", "deal_num EQ $deal_tracking_num");
	    tTranResults.destroy();
	}
}