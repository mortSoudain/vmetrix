/*$Header: v 1.0, 31/Ene/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_Plantilla_UDW.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Enero 2018
 * Version:					1.0
 * Description:				Script diseñado para... (Descripción)
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;

import java.io.IOException;

import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;

public class MISC_Afore_Plantilla_UDW implements IScript {

	// Basic util variables
		private String		sScriptName			=	this.getClass().getSimpleName();
		private UTIL_Log	_Log				=	new UTIL_Log(sScriptName);
		private UTIL_Afore	UtilAfore			=	new UTIL_Afore();
	
	// UDW Variables
		ENUM_UDW_ACTION		callback_type;
		ENUM_UDW_EVENT		callback_event;
		private Table 		tblProperties		=	Util.NULL_TABLE;
		private Table 		tblGlobalProperties	=	Util.NULL_TABLE;
		private Table 		tDisplay			=	Util.NULL_TABLE;
		private Table 		tReturnt			=	Util.NULL_TABLE;
		private String 		sCellName			=	null;

	public void execute(IContainerContext context) throws OException {
		
		initializeScript();
		
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					setVisualElements();

				// Adding columns layout
					addLayoutColumns();
				
				// Setting initial data
					setDataOnTable(tDisplay);
	
				// Apply filter on start
					filterDealsBy(tDisplay);
				
				// FINALLY, Setting table properties
					setTableProperties();

				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					getCallBackInfo();

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						if(sCellName.equals("btnGenerarReporte")){

						}
						break;
					case UDW_COMBO_BOX_EVENT:							
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		_Log.markEndScript();
		
	}

	private void initializeScript() {
		try {
			_Log.markStartScript();
			callback_type	=	ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
	}

	private void getCallBackInfo() {
		try {
			callback_event		=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
			tReturnt			=	UserDataWorksheet.getData();
			sCellName			=	tReturnt.scriptDataGetCallbackName();
			
			//sSelectedPortfolio 	=	tReturnt.scriptDataGetWidgetString("filterFondo");
			//iSelectedPortfolio 	=	Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sSelectedPortfolio);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to get callback info,on getCallBackInfo function."
					+ "Error: " + e.getMessage());
		}
	}

	private void setDataOnTable(Table tDisplay2) {
		// TODO Auto-generated method stub
		
	}

	private void addLayoutColumns() {
		
		try {
			//tDisplay.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT);
			//tDisplay.addCol("llamadas_capital", COL_TYPE_ENUM.COL_DOUBLE);
			//tDisplay.addCol("liquidez_llamadas_capital", COL_TYPE_ENUM.COL_DOUBLE);
			//tDisplay.addCol("porcentaje_llamadas_capital", COL_TYPE_ENUM.COL_DOUBLE);
			//tDisplay.addCol("limite", COL_TYPE_ENUM.COL_DOUBLE);
			//tDisplay.addCol("uso", COL_TYPE_ENUM.COL_DOUBLE);
			//tDisplay.addCol("status", COL_TYPE_ENUM.COL_STRING);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to add some cols for layout on addLayoutColumns function."
					+ "Error: " + e.getMessage());
		}
		
	}

	private void setTableProperties() {
		
		try {
			
			// Initialize properties tables
				tblProperties			=	UserDataWorksheet.initProperties();
				tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
			// Setting col properties
				//UserDataWorksheet.setColReadOnly("titulos", tDisplay, tblProperties);
		
			// Setting UDW properties
				UserDataWorksheet.setDisableSave(tblGlobalProperties);
				UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
				UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
				UserDataWorksheet.setDisableReload(tblGlobalProperties);
				UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set some of the properties tables on setTableProperties function."
					+ "Error: " + e.getMessage());
		}
		
	}

	private void setVisualElements() {
		
		try {
			// Creating display table
				tDisplay = Table.tableNew();

			tDisplay.scriptDataAddWidget("Portfolio", 
				SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
				"x=100, y=20, h=25, w=130",
				"label="+tPortfolio.getString(1, iRow), tPortfolio);

			tDisplay.scriptDataAddWidget("btnGenerarReporte", 
				SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
				"x=1100, y=15, h=25, w=130", "label=Generar Reporte");
			
			// Add data worksheet
				tDisplay.scriptDataMoveListBox("top=110,left=5,right=5,bottom=5");
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set some of the visual elements on setVisualElements function."
					+ "Error: " + e.getMessage());
		}
			
	}


}
