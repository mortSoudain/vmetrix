/*$Header: v 1.0, 31/Ene/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_Plantilla.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Enero 2018
 * Version:					1.0
 * Description:				Script diseñado para... (Descripción)
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;

import com.afore.enums.EnumTypeMessage;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.apm_foundation.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.DATE_FORMAT;

public class MISC_Afore_Plantilla implements IScript {

	// Basic util variables
		private String		sScriptName		=	this.getClass().getSimpleName();
		private UTIL_Log	_Log			=	new UTIL_Log(sScriptName);
		private UTIL_Afore	UtilAfore 		=	new UTIL_Afore();

	// Time variables
		private int 		iToday			=	0;
		private String		sHour			=	"";
		private String		sTodayDefault	=	"";
		private ODateTime	oToday			=	null;

	// User
		private String		sUserName		=	"";
		private int			iUserId			=	0;

	// Configurable Variables
		private String		sConfVar 		=	"";
		
	// Generic table
		private Table		tTable			= Util.NULL_TABLE;

	// Used ENUMS
		(VER ARCHIVO CON ENUMS)

	// Specific script variables

	@Override
	public void execute(IContainerContext context) {

		initializeScript();
		
		_Log.markEndScript();

	}
	
	private void initializeScript() {

		_Log.markStartScript();

		try {

			iToday			=	OCalendar.today();
			sHour			=	Util.timeGetServerTimeHMS();
			sTodayDefault	=	OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			oToday			=	ODateTime.strToDateTime(sTodayDefault);

			sUserName		=	Ref.getUserName();
			
			sConfVar		=	UtilAfore.getVariableGlobal("FINDUR", "PROCESO", "VARIABLE");
			
			tTable			=	UtilAfore.getTable("select * from ab_tran");

			Table tInfo		=	Ref.getInfo();
			iUserId			=	tInfo.getInt("submitter_user_id", 1);
			if(iUserId==0){
				iUserId		=	Ref.getUserId();
			}
			
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
	}
}