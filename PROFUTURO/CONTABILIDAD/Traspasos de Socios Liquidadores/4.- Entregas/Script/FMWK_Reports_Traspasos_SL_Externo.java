/*$Header: v 1.0, 05/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Traspasos_SL_Externo.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Febrero 2018
 * Version:					1.0
 * Description:				Script que genera reportes de traspasos a los socios liquidadores 
 * 							y envía los archivos por email
 * 
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.afore.custom_reports;

import java.util.ArrayList;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserCflowType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.BUY_SELL_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;

public class FMWK_Reports_Traspasos_SL_Externo implements IScript {

	// Declare utils and general variables
	String sScriptName		=	this.getClass().getSimpleName();	   
	UTIL_Afore UtilAfore	=	new UTIL_Afore();
	UTIL_Log LOG			=	new UTIL_Log(sScriptName);
	
	// Declare time variables
	int iToday					=	0;
	String sHour				=	"";
	String sTodayDefault		=	"";
	String sTodayAAAAMMDD		=	"";
	String sTodaySlashDDMMAAAA	=	"";

	// Declare global tables
	Table tData		=	Util.NULL_TABLE;
	
	ArrayList<Boolean> bMailResults = new ArrayList<Boolean>();
	
	String sFullTemplateName	=	"";
	String sFullFileNameXLS		=	"";
	

	@Override
	public void execute(IContainerContext context) throws OException {
		
		LOG.markStartScript();
		
		// Initialize Global Variables
			initializeScript();
		
		// Set data on tData Global Table
			setTableData();
			
		//Generate reports and send mails
			generateReportAndSendMails();
	
		LOG.markEndScript();
	}
	

	private void initializeScript() {
		
		// Load time variables
			try {
				iToday	=	OCalendar.today();
				sHour	=	Util.timeGetServerTimeHMS();
				sTodayDefault = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
				sTodayAAAAMMDD = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601);
				sTodaySlashDDMMAAAA = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load system time variables : " + e.getMessage());
			}

		// Create tables
			try {
				tData	=	Table.tableNew("Data Table");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to initialize tData Table : " + e.getMessage());
			}
	}

	private void setTableData() {
		
		StringBuilder sQueryPrincipal = new StringBuilder();
		
		// Query filters
			int iBuySell_Buy				=	BUY_SELL_ENUM.BUY.toInt();
			int iToolset_Cash				=	TOOLSET_ENUM.CASH_TOOLSET.toInt();
			int iCflowType_MarginCallAims	=	EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt();
	
		//Setting Principal Query
			sQueryPrincipal
				.append("\n SELECT ")
				.append("\n 	--ab.deal_tracking_num, ")
				.append("\n 	ab.external_bunit, ")
				.append("\n 	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5) AS CONTRAPARTE, ")
				.append("\n 	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5) AS FONDO, ")
				.append("\n 	CASE WHEN ab.buy_sell = "+ iBuySell_Buy +" THEN 'DEPOSITO' ")
				.append("\n 	  ELSE 'RETIRO' ")
				.append("\n 	END AS TIPO_DE_MOVIMIENTO, ")
				.append("\n 	pa.party_agreement_name AS NUMERO_DE_CUENTA, ")
				.append("\n 	CAST(SUM(ab.position) AS FLOAT) AS MONTO, ")
				.append("\n 	ccy.NAME AS DIVISA ")
				.append("\n  ")
				.append("\n FROM ab_tran ab ")
				.append("\n  ")
				.append("\n 	--NUMERO DE CUENTA ")
				.append("\n 		LEFT JOIN party_agreement pa ")
				.append("\n 			ON(ab.internal_lentity  = pa.int_party_id ")
				.append("\n 			AND ab.external_lentity = pa.ext_party_id) ")
				.append("\n  ")
				.append("\n 	--FONDO ")
				.append("\n 		LEFT JOIN party_portfolio pp ")
				.append("\n 			ON(ab.internal_portfolio = pp.portfolio_id) ")
				.append("\n 		LEFT JOIN party portfolio_name ")
				.append("\n 			ON(pp.party_id= portfolio_name.PARTY_ID) ")
				.append("\n  ")
				.append("\n 	--DIVISA ")
				.append("\n 		LEFT JOIN currency ccy ")
				.append("\n 			ON (ab.currency = ccy.ID_NUMBER) ")
				.append("\n 	   ")
				.append("\n 	--EXTERNAL BUNIT ")
				.append("\n 		LEFT JOIN party external_party ")
				.append("\n 			ON(ab.EXTERNAL_BUNIT=external_party.PARTY_ID) ")
				.append("\n   ")
				.append("\n WHERE 1=1 ")
				.append("\n 	AND ab.toolset = "+ iToolset_Cash +" --cash ")
				.append("\n 	AND ab.cflow_type = "+ iCflowType_MarginCallAims +" --Margin Call AIMS ")
				.append("\n 	AND ab.trade_date = '"+sTodayDefault+"'")
				.append("\n GROUP BY ")
				.append("\n 	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5), ")
				.append("\n 	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5), ")
				.append("\n 	CASE WHEN ab.buy_sell = "+ iBuySell_Buy +" THEN 'DEPOSITO' ")
				.append("\n 		  ELSE 'RETIRO' END, ")
				.append("\n 	pa.party_agreement_name, ")
				.append("\n 	ab.external_bunit, ")
				.append("\n 	ccy.NAME ");
			
			// Load query into table
			try {
				DBaseTable.execISql(tData, sQueryPrincipal.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load principal query into tData Table : " + e.getMessage());
			}
		
	}


	private void generateReportAndSendMails() throws OException {
		
		int iResponse = Ask.okCancel("Desea enviar los emails?");
		boolean bAllMailSended = true;
		
		if(iResponse==1){
		
			// Select distinct counterparts
				Table tDistinctPartys = Table.tableNew();
				tDistinctPartys.select(tData, "DISTINCT, external_bunit", "external_bunit GT 0");
			
			// Generate and send a report for each counterparty
				for (int iRow = 1; iRow <= tDistinctPartys.getNumRows(); iRow++){
					int iParty = tDistinctPartys.getInt("external_bunit", iRow);
					generateReportAndSendMail(iParty);
				}
			
			// If one of the generated reports was not sended, set flag to false
				for (Boolean bResult : bMailResults){
					if(!bResult) bAllMailSended=false;
				}
			
			// Show a message dependent of flag status
				if(!bAllMailSended){
					Ask.ok("Error al enviar alguno de los emails. Revisar el LOG para mayor informacion.");
				} else {
					Ask.ok("Emails enviados exitosamente.");
				}
		}
	}

	private void generateReportAndSendMail(int iParty) {
		
		Table tOutput = Util.NULL_TABLE;
		String sParty = "";
				
		try {
			tOutput	=	Table.tableNew();
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to create tOutput Table : "
				+ e.getMessage());
		}
		
		try {
			sParty = Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iParty);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to get party name : "
					+ e.getMessage());
		}
		
		// Select data to show on report
			try {
				tOutput.select(tData,
						"contraparte, "
						+ "fondo, "
						+ "tipo_de_movimiento, "
						+ "numero_de_cuenta, "
						+ "monto, "
						+ "divisa",
						"external_bunit EQ "+iParty
					);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to set selected columns into tOutput Table : "
					+ e.getMessage());
			}
										
		// Configure paths and filenames
			try {
				String sTemplatePath = Crystal.getRptDir();
				String sTemplateName = "ReporteTraspasosSL.rpt";
				sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
			
				String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Externo", "path");
				String sFileName = "Reportes_Traspasos_SL_("+sParty+")_"+sTodayAAAAMMDD;
				sFullFileNameXLS = sFilePath+sFileName+".xls";
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to set paths, "+e.getMessage());
			}		
		
		//Exporting excel
			try {
				Crystal.tableExportCrystalReport(tOutput, 
						sFullTemplateName, 
						CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
						sFullFileNameXLS, 
						CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
						);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to export Excel document. "+e.getMessage());
			}

//-------------------- Sending mail
				
		// Declare mail variables
			String sMailsTotal		= ""; 
			String sMailsDefault	=	"";
			String sMailsParty		=	"";
			String sSubject		=	"";
			String sMessage		=	"";
			String sBottomImage	=	"";
			boolean mailResult	=	false;
				
		// Setting mail content from user_configurable_variables
			try {
				
				sMailsDefault	=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Externo", "mails");
				sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Externo", "subject");
				sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Externo", "body");
				sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
			
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to load variables for send mail, "+e.getMessage());
			}
			
		//Setting party mails
			try {
				sMailsParty = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Externo", "mails_"+sParty);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to load party mails, "+e.getMessage());
			}
		
		// Concatenate default mails plus party mails
			if (sMailsParty==null) sMailsParty="";
			sMailsTotal = sMailsDefault + ";" + sMailsParty;
		
			
		// Format
			sSubject		=	sSubject.replace("$TODAY", sTodaySlashDDMMAAAA);
			sSubject		=	sSubject.replace("$CONTRAPARTE", sParty);
			sMessage		=	sMessage.replace("$TODAY", sTodaySlashDDMMAAAA);

		// Sending Mails
			mailResult = UtilAfore.sendMail(
					sMailsTotal,
					sSubject,
					sMessage,
					sBottomImage,
					sFullFileNameXLS
					);
			bMailResults.add(mailResult);
			
			if (!mailResult){
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to send mail.");
			}
					
			try {
				tOutput.destroy();
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to destroy tOutput Table, "+e.getMessage());
			}				
	}
}
