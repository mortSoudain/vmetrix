/*$Header: v 1.0, 05/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Traspasos_SL_Interno.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Febrero 2018
 * Version:					1.0
 * Description:				Script que genera reportes de traspasos a los socios liquidadores 
 * 							y envía los archivos por email
 * 
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserCflowType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.BUY_SELL_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.TOOLSET_ENUM;

public class FMWK_Reports_Traspasos_SL_Interno implements IScript {
	
	// Declare utils and general variables
		String sScriptName				=	this.getClass().getSimpleName();	   
		UTIL_Afore UtilAfore			=	new UTIL_Afore();
		UTIL_Log LOG					=	new UTIL_Log(sScriptName);

	// Declare time variables
		int iToday						=	0;
		String sHour					=	"";
		String sTodayDefault			=	"";
		String sTodayAAAAMMDD			=	"";
		String sTodaySlashDDMMAAAA		=	"";
	
	// Declare global tables
		Table tData						=	Util.NULL_TABLE;
		Table tOutput					=	Util.NULL_TABLE;
	
	// Declare output FileNames
		String sFullRptTemplateName		=	"";
		String sFullFileNameXLS			=	"";
	
	@Override
	public void execute(IContainerContext context) {

		LOG.markStartScript();
		
			// Initialize Global Variables
				initializeScript();
			
			// Set data on tData Global Table
				setTableData();
				
			// Generate report
				generateReport();
				
			// Send mails
				sendMails();
	
		LOG.markEndScript();
	}

	private void initializeScript() {
		
		// Load time variables
		try {
			iToday	=	OCalendar.today();
			sHour	=	Util.timeGetServerTimeHMS();
			sTodayDefault = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			sTodayAAAAMMDD = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601);
			sTodaySlashDDMMAAAA = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to load system time variables : "
				+ e.getMessage());
		}
	
		// Create tables
		try {
			tData	=	Table.tableNew("Data Table");
			tOutput	=	Table.tableNew("Output Table");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
				"Unable to initialize global tables : "
				+ e.getMessage());
		}
	}
	
	private void setTableData() {
		
		StringBuilder sQueryPrincipal = new StringBuilder();
		
		// Query filters
			int iBuySell_Buy				=	BUY_SELL_ENUM.BUY.toInt();
			int iToolset_Cash				=	TOOLSET_ENUM.CASH_TOOLSET.toInt();
			int iCflowType_MarginCallAims	=	EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt();
	
		//Setting Principal Query
			sQueryPrincipal
				.append("\n SELECT ")
				.append("\n 	--ab.deal_tracking_num, ")
				.append("\n 	ab.external_bunit, ")
				.append("\n 	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5) AS CONTRAPARTE, ")
				.append("\n 	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5) AS FONDO, ")
				.append("\n 	CASE WHEN ab.buy_sell = "+ iBuySell_Buy +" THEN 'DEPOSITO' ")
				.append("\n 	  ELSE 'RETIRO' ")
				.append("\n 	END AS TIPO_DE_MOVIMIENTO, ")
				.append("\n 	pa.party_agreement_name AS NUMERO_DE_CUENTA, ")
				.append("\n 	CAST(SUM(ab.position) AS FLOAT) AS MONTO, ")
				.append("\n 	ccy.NAME AS DIVISA ")
				.append("\n  ")
				.append("\n FROM ab_tran ab ")
				.append("\n  ")
				.append("\n 	--NUMERO DE CUENTA ")
				.append("\n 		LEFT JOIN party_agreement pa ")
				.append("\n 			ON(ab.internal_lentity  = pa.int_party_id ")
				.append("\n 			AND ab.external_lentity = pa.ext_party_id) ")
				.append("\n  ")
				.append("\n 	--FONDO ")
				.append("\n 		LEFT JOIN party_portfolio pp ")
				.append("\n 			ON(ab.internal_portfolio = pp.portfolio_id) ")
				.append("\n 		LEFT JOIN party portfolio_name ")
				.append("\n 			ON(pp.party_id= portfolio_name.PARTY_ID) ")
				.append("\n  ")
				.append("\n 	--DIVISA ")
				.append("\n 		LEFT JOIN currency ccy ")
				.append("\n 			ON (ab.currency = ccy.ID_NUMBER) ")
				.append("\n 	   ")
				.append("\n 	--EXTERNAL BUNIT ")
				.append("\n 		LEFT JOIN party external_party ")
				.append("\n 			ON(ab.EXTERNAL_BUNIT=external_party.PARTY_ID) ")
				.append("\n   ")
				.append("\n WHERE 1=1 ")
				.append("\n 	AND ab.toolset = "+ iToolset_Cash +" --cash ")
				.append("\n 	AND ab.cflow_type = "+ iCflowType_MarginCallAims +" --Margin Call AIMS ")
				.append("\n 	AND ab.trade_date = '"+sTodayDefault+"'")
				.append("\n GROUP BY ")
				.append("\n 	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5), ")
				.append("\n 	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5), ")
				.append("\n 	CASE WHEN ab.buy_sell = "+ iBuySell_Buy +" THEN 'DEPOSITO' ")
				.append("\n 		  ELSE 'RETIRO' END, ")
				.append("\n 	pa.party_agreement_name, ")
				.append("\n 	ab.external_bunit, ")
				.append("\n 	ccy.NAME ");
			
			// Load query into table
			try {
				DBaseTable.execISql(tData, sQueryPrincipal.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to load principal query into tData Table : "
					+ e.getMessage());
			}	
	}
	
	private void generateReport() {
		
		// Select data to show on report
			try {
				tOutput.select(tData,
						"contraparte, "
						+ "fondo, "
						+ "tipo_de_movimiento, "
						+ "numero_de_cuenta, "
						+ "monto, "
						+ "divisa",
						"contraparte GT 0"
					);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to set selected columns into tOutput Table : "
					+ e.getMessage());
			}
					
		// Configure paths and filenames
			try {
				String sTemplatePath = Crystal.getRptDir();
				String sTemplateName = "ReporteTraspasosSL.rpt";
				sFullRptTemplateName = sTemplatePath+"\\"+sTemplateName;
			
				String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Interno", "path");
				String sFileName = "Reportes_Traspasos_SL_Interno_"+sTodayAAAAMMDD;
				sFullFileNameXLS = sFilePath+sFileName+".xls";
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to configure paths or filenames : "
					+ e.getMessage());
			}
			
		//Exporting excel
			try {
				Crystal.tableExportCrystalReport(tOutput, 
						sFullRptTemplateName, 
						CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
						sFullFileNameXLS, 
						CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
						);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to export Excel document. "
					+e.getMessage());
			}	
	}
	
	private void sendMails() {
		
		int iResponse = 0;
		
		try {
			iResponse = Ask.okCancel("Desea enviar los emails?");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to show message dialog asking send mails, "
					+e.getMessage());
		}
		
			if(iResponse==1){
			// Declare mail variables
				String sMails		=	"";
				String sSubject		=	"";
				String sMessage		=	"";
				String sBottomImage	=	"";
				boolean mailResult	=	false;
					
			// Setting mail content from user_configurable_variables
				try {
					
					sMails			=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Interno", "mails");
					sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Interno", "subject");
					sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Traspasos_SL_Interno", "body");
					sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
				
				} catch (OException e) {
					LOG.printMsg(EnumTypeMessage.ERROR,
							"Unable to load variables for send mail, "
							+e.getMessage());
				}
				
			// Format
				sSubject		=	sSubject.replace("$TODAY", sTodaySlashDDMMAAAA);
				sMessage		=	sMessage.replace("$TODAY", sTodaySlashDDMMAAAA);
	
			// Sending Mails
				mailResult = UtilAfore.sendMail(
						sMails,
						sSubject,
						sMessage,
						sBottomImage,
						sFullFileNameXLS
						);
			// Send message indicating failure or success
				
				try {
					if (!mailResult){
						Ask.ok("No fue posible enviar el email");
					} else {
						Ask.ok("Email enviado exitosamente");
					}
				} catch (OException e) {
					LOG.printMsg(EnumTypeMessage.ERROR,
							"Unable to show send mail response pop-up, "
							+e.getMessage());
				}
				

			}
	}	
}