SELECT
	--ab.deal_tracking_num,
	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5) AS CONTRAPARTE,
	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5) AS FONDO,
	CASE WHEN ab.buy_sell = 1 THEN 'DEPOSITO'
	  ELSE 'RETIRO'
	END AS TIPO_DE_MOVIMIENTO,
	pa.party_agreement_name AS NUMERO_DE_CUENTA,
	CAST(SUM(ab.position) AS FLOAT) AS MONTO,
	ccy.NAME AS DIVISA

FROM ab_tran ab

	--NUMERO DE CUENTA
		LEFT JOIN party_agreement pa
			ON(ab.internal_lentity  = pa.int_party_id
			AND ab.external_lentity = pa.ext_party_id)

	--FONDO
		LEFT JOIN party_portfolio pp
			ON(ab.internal_portfolio = pp.portfolio_id)
		LEFT JOIN party portfolio_name
			ON(pp.party_id= portfolio_name.PARTY_ID)

	--DIVISA
		LEFT JOIN currency ccy
			ON (ab.currency = ccy.ID_NUMBER)
	  
	--EXTERNAL BUNIT
		LEFT JOIN party external_party
			ON(ab.EXTERNAL_BUNIT=external_party.PARTY_ID)
 
WHERE 1=1
	AND ab.toolset = 10 --cash
	AND ab.cflow_type = 271 --Margin Call AIMS
	AND ab.trade_date = '18-Dec-2017'
GROUP BY
	SUBSTR (external_party.SHORT_NAME, 1, LENGTH(external_party.SHORT_NAME)-5),
	SUBSTR (portfolio_name.SHORT_NAME, 1, LENGTH(portfolio_name.SHORT_NAME)-5),
	CASE WHEN ab.buy_sell = 1 THEN 'DEPOSITO'
		  ELSE 'RETIRO' END,
	pa.party_agreement_name,
	ccy.NAME