/**$Header: v 1.0 2017/10/27 $*/
/*****************************************************************************
Script File Name:                FMWK_Reports_CustodiaConciliacionOperaciones.java

Report Name:                     Settlement Today Detail

Output File Name:                

Available RptMgr Outputs:        

Revision History:

 *********************************************************************************** */

package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsParty;
import com.afore.enums.EnumsPartyInfoFields;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.PluginCategory;
import com.olf.openjvs.PluginType;
import com.olf.openjvs.Ref;
import com.olf.openjvs.ScriptAttributes;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Transaction;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SCRIPT_CATEGORY_ENUM;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SCRIPT_TYPE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRANF_FIELD;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

@ScriptAttributes(allowNativeExceptions=false)
@PluginCategory(SCRIPT_CATEGORY_ENUM.SCRIPT_CAT_GENERIC)
@PluginType(SCRIPT_TYPE_ENUM.MAIN_SCRIPT)

public class FMWK_Reports_Movimientos_Custodio implements IScript
{   
    //Declare utils
    protected String sScriptName    =   this.getClass().getSimpleName();
    protected UTIL_Log _Log         =   new UTIL_Log(sScriptName);
    UTIL_Afore UtilAfore            =   new UTIL_Afore();
    
    ENUM_UDW_ACTION callback_type;
    ENUM_UDW_EVENT  callback_event;
    
    private Table tblProperties         =   Util.NULL_TABLE;
    private Table tblGlobalProperties   =   Util.NULL_TABLE;
    private Table tDisplay              =   Util.NULL_TABLE;
    private Table tReturnt              =   Util.NULL_TABLE;
    private String sCellName            =   null;

    private int iToday      =   0;
    private String sToday   =   null;
        
    private final String GENERAR_CASH        = "generar_cash";

    private int iRetVal;
    private final int SUCCEEDED = OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt();
    
    public void execute(IContainerContext context) throws OException
    {
        _Log.markStartScript();
                
        iToday = OCalendar.today();
                
        callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
        
        switch(callback_type)
        {
            case UDW_INIT_ACTION:
                
                // Adding visual elements
                    addVisualElements();
                
                // Setting information on iDisplay Table
                    setDataOnTable(tDisplay);
                
                // FINALLY, Setting table properties
                    setTableProperties();
                
                break;

            case UDW_EDIT_ACTION: 
                
                // Getting callback info
                callback_event  =   ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
                tReturnt        =   UserDataWorksheet.getData();
                sCellName       =   tReturnt.scriptDataGetCallbackName();

                switch(callback_event)
                {
                    case UDW_CUSTOM_BUTTON_EVENT:
                                                
                         if (sCellName.equals(GENERAR_CASH)){
                             genCashFlows();
                             setDataOnTable(tReturnt);
                         }
                        
                        break;
                default:
                    break;
                }
        default:
            break;
        }
        _Log.markEndScript();
    }

    private void addVisualElements() {
     try {
         tDisplay = Table.tableNew("Reporte Conciliacion Operaciones");
             tDisplay.addCol("fondo", COL_TYPE_ENUM.COL_STRING, "Fondo");
             tDisplay.addCol("moneda", COL_TYPE_ENUM.COL_STRING, "Moneda");
             tDisplay.addCol("importe", COL_TYPE_ENUM.COL_DOUBLE, "Importe");
             tDisplay.addCol("estado", COL_TYPE_ENUM.COL_STRING, "Estado");
             
             tDisplay.scriptDataAddWidget(GENERAR_CASH,  
                     SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
                     "x=50, y=20, h=25, w=150",
                     "label=Generar Cash");
             
             tDisplay.scriptDataMoveListBox("top=60,left=40,right=600,bottom=300");
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to add visual elements on addVisualElements function : "+e.getMessage());
        }   
    }

    private void setTableProperties() {
        try {
            tblProperties = UserDataWorksheet.initProperties();
            tblGlobalProperties = UserDataWorksheet.initGlobalProperties();
            
            UserDataWorksheet.setColReadOnly("fondo", tDisplay, tblProperties);
            UserDataWorksheet.setColReadOnly("moneda", tDisplay, tblProperties);
            UserDataWorksheet.setColReadOnly("importe", tDisplay, tblProperties);
            UserDataWorksheet.setColReadOnly("estado", tDisplay, tblProperties);
            
            UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);   
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to set table properties on setTableProperties function : "+e.getMessage());
        }
    }
    
    /**
     * 
     * @param tDisplay
     * @throws OException
     */
        private void setDataOnTable(Table tDisplayOtReturnt) throws OException
        {       
            tDisplayOtReturnt.clearDataRows();
            StringBuffer sbQuery = new StringBuffer(); 
            Table tTodaysTrans = Table.tableNew("Transacciones del dia");
            
            try
            {           
                sbQuery.append("\n SELECT ab.deal_tracking_num deal_num,")
                       .append("\n    ab.tran_num,")
                       .append("\n    ab.toolset,")
                       .append("\n    ab.asset_type,")
                       .append("\n    ab.reference,")
                       .append("\n    ab.internal_portfolio ,")
                       .append("\n    ab.internal_contact ,")
                       .append("\n    ab.external_bunit,")
                       .append("\n    ab.internal_bunit,")
                       .append("\n    ab.buy_sell,")
                       .append("\n    ab.trade_date,")
                       .append("\n    ab.settle_date,")
                       .append("\n    CASE WHEN ab.toolset = ").append(TOOLSET_ENUM.CASH_TOOLSET.jvsValue()).append(" THEN abti.value ELSE h.ticker END AS ticker,")
                       .append("\n    CASE WHEN ab.toolset = ").append(TOOLSET_ENUM.CASH_TOOLSET.jvsValue()).append(" THEN ht.isin ELSE h.isin END AS isin,")
                       .append("\n    ivfa.value fee_amount,")
                       .append("\n    CASE WHEN ab.toolset IN (").append(TOOLSET_ENUM.BOND_TOOLSET.toInt()).append(", ").append(TOOLSET_ENUM.EQUITY_TOOLSET.toInt()).append(") ").append(" THEN stad.settle_ccy ELSE p.currency END AS settle_ccy,")
                       .append("\n    stad.settle_fx,")
                       .append("\n    CASE WHEN ab.toolset IN (").append(TOOLSET_ENUM.BOND_TOOLSET.toInt()).append(", ")
                                                                 .append(TOOLSET_ENUM.CASH_TOOLSET.toInt()).append(") ").append(" THEN pivdeuint.value ELSE pivcapint.value END AS indeval_int,")
                       .append("\n    CASE WHEN ab.toolset IN (").append(TOOLSET_ENUM.BOND_TOOLSET.toInt()).append(", ")
                                                                 .append(TOOLSET_ENUM.CASH_TOOLSET.toInt()).append(") ").append(" THEN pivdeuext.value ELSE pivcapext.value END AS indeval_ext,")
                       .append("\n    CASE WHEN (stad.settle_ccy = ").append(EnumsCurrency.MX_CURRENCY_MXN.toInt()).append(" OR p.currency = ").append(EnumsCurrency.MX_CURRENCY_MXN.toInt()).append(") THEN 'INDEVAL' ELSE 'STATE STREET' END AS custodia,")
                       .append("\n    CASE WHEN stad.settle_ccy != ").append(EnumsCurrency.MX_CURRENCY_MXN.toInt()).append(" THEN pivintss.value END AS ss_int,")
                       .append("\n    h.ins_group_id")
                       .append("\n FROM ab_tran ab ")
                       .append("\n    LEFT JOIN header h ON (ab.ins_num = h.ins_num)")
                       .append("\n    LEFT JOIN security_tran_aux_data stad ON ab.tran_num = stad.tran_num")
                       .append("\n    LEFT JOIN parameter p ON (ab.ins_num = p.ins_num)")
                       .append("\n    LEFT JOIN party_info_view pivcapint ON (ab.internal_bunit = pivcapint.party_id AND pivcapint.type_id = ").append(EnumsPartyInfoFields.MX_CUENTA_INDEVAL_CAPITALES_INT.toInt()).append(")")
                       .append("\n    LEFT JOIN party_info_view pivdeuint ON (ab.internal_bunit = pivdeuint.party_id AND pivdeuint.type_id = ").append(EnumsPartyInfoFields.MX_CUENTA_INDEVAL_DEUDA_INT.toInt()).append(")")
                       .append("\n    LEFT JOIN party_info_view pivintss ON (ab.internal_bunit = pivintss.party_id AND pivintss.type_id = ").append(EnumsPartyInfoFields.MX_ACCOUNT_NUMBER_INT.toInt()).append(")")
                       .append("\n    LEFT JOIN party_info_view pivcapext ON (ab.external_bunit = pivcapext.party_id AND pivcapext.type_id = ").append(EnumsPartyInfoFields.MX_CUENTA_INDEVAL_CAPITALES_EXT.toInt()).append(")")
                       .append("\n    LEFT JOIN party_info_view pivdeuext ON (ab.external_bunit = pivdeuext.party_id AND pivdeuext.type_id = ").append(EnumsPartyInfoFields.MX_CUENTA_INDEVAL_DEUDA_EXT.toInt()).append(")")
                       .append("\n    LEFT JOIN ab_tran_info_view ivfa ON (ab.tran_num = ivfa.tran_num AND ivfa.type_id = ").append(EnumsTranInfoFields.MX_ALL_FEE_AMOUNT.toInt()).append(")")
                       .append("\n    LEFT JOIN ab_tran_info_view abti ON (ab.tran_num = abti.tran_num AND abti.type_name = '").append(EnumsTranInfoFields.MX_CASH_TICKER)
                                                                                            .append("' AND ab.toolset = ").append(TOOLSET_ENUM.CASH_TOOLSET.jvsValue()).append(" )")
                       .append("\n    LEFT JOIN ab_tran abt ON (abti.value = abt.reference AND abt.tran_status IN (").append(TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.jvsValue()).append(", ")
                                                                                                                      .append(TRAN_STATUS_ENUM.TRAN_STATUS_MATURED.jvsValue())
                                                                                 .append(") AND abt.tran_type = ").append(TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.jvsValue()).append(" )")
                       .append("\n    LEFT JOIN header ht ON (abt.tran_num = ht.tran_num )")
                       .append("\n WHERE ab.tran_status IN (").append(TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()).append(",")
                                                              .append(TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt()).append(")")
                                                              
                        .append("\n     AND (ab.toolset IN (5, 28) OR (ab.toolset = 10 AND ab.cflow_type = 2037) AND ab.external_bunit != 20363 ")
                        .append("\n     OR (ab.toolset = 10 AND ab.cflow_type IN (26,54,263,2025,2041,2042))) -- final principal,coupon payment, dividendo efectivo,derechos ckd, deposito custodio,retiro custodio  ")
                        
                        .append("\n     AND ab.ins_type != "+ EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt() +" ")

                       .append("\n    AND ab.settle_date = '").append(OCalendar.formatJdForDbAccess(OCalendar.today())).append("'")
                       .append("\n    AND (ab.reference NOT LIKE '%Conciliacion%' OR ab.trade_date != '").append(OCalendar.formatJdForDbAccess(OCalendar.today())).append("')")
                       .append("\n    AND ab.tran_type = ").append(TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt())
                       .append("\n    AND ab.asset_type IN (").append(ASSET_TYPE_ENUM.ASSET_TYPE_SEC_INV_PYMT.toInt()).append(", ")
                                                              .append(ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt()).append(")")
                       .append("\n ORDER BY ab.internal_portfolio ASC");
                            
                iRetVal = DBaseTable.execISql(tTodaysTrans, sbQuery.toString());
                
                if (iRetVal == SUCCEEDED)
                    _Log.printMsg(EnumTypeMessage.INFO, "Transacciones para el dia de hoy cargadas correctamente.");
                else    
                    _Log.printMsg(EnumTypeMessage.ERROR, "Error al cargar Transacciones para el dia de hoy.");
                
                // Set settle proceeds
                    getTransactionData(tTodaysTrans);
                
                // Set a resume table by portfolio and settle currency 
                    Table tShowInDisplay = Table.tableNew();
                    tShowInDisplay.select(tTodaysTrans, "DISTINCT, internal_portfolio, settle_ccy ", "deal_num GT 0 AND internal_portfolio GT 0");
                    tShowInDisplay.select(tTodaysTrans, "SUM, settle_proceeds", "internal_portfolio EQ $internal_portfolio AND settle_ccy EQ $settle_ccy");
                                
                // TODO: DEBUG
                    tTodaysTrans.viewTable();
                    tShowInDisplay.viewTable();
                    
                // Set resumed data on table tDisplay
                    String sFondo = "";
                    String sMonedaDePago = "";
                    String sEstado= "";
                    double dImporte = 0.0 ;
                    
                    int iFondo = 0;
                    int iMonedaDePago = 0;
                    int iNewRow = 0;
                
                for(int i=1; i<=tShowInDisplay.getNumRows(); i++){
                    
                    iFondo = tShowInDisplay.getInt("internal_portfolio", i);
                    iMonedaDePago = tShowInDisplay.getInt("settle_ccy", i);
                    dImporte = tShowInDisplay.getDouble("settle_proceeds", i);
                    
                    iNewRow = tDisplayOtReturnt.addRow();
                    
                    sFondo = Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, iFondo);
                    sMonedaDePago = Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, iMonedaDePago);
                    
                    if(dImporte<-0.1) sEstado = "Deposito";
                    else if(dImporte>0.1)sEstado = "Retiro";
                    else sEstado = "-";
                    
                    tDisplayOtReturnt.setString("fondo", iNewRow, sFondo);
                    tDisplayOtReturnt.setString("moneda", iNewRow, sMonedaDePago);
                    tDisplayOtReturnt.setDouble("importe", iNewRow, dImporte);
                    tDisplayOtReturnt.setString("estado", iNewRow, sEstado);
                }
            UserDataWorksheet.setRefreshDataTable(tDisplay);
            }
            catch (OException e)
            {
                _Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
            }
            finally
            {
                tTodaysTrans.destroy();
            }
        }
        
        /**
         * Usando el deal_num de las operaciones guardadas en la Table,
         * rescata el Settle Proceeds de operacion.
         * 
         * @param tDisplay
         * @throws OException
         */
        
        public void getTransactionData(Table tDisplay) throws OException
        {
            Transaction trnTran = null;
            String sSettleProceeds = "";
            String sSettleProceedsLimpio = "";
            double dSettleProceeds = 0.00;      
           
            tDisplay.addCol("settle_proceeds", COL_TYPE_ENUM.COL_DOUBLE);
            
            try
            {
                for (int i = 1; i <= tDisplay.getNumRows(); i++)
                {
                    trnTran = Transaction.retrieve(tDisplay.getInt("tran_num", i));
                                                             
                    if (tDisplay.getInt("toolset", i) == TOOLSET_ENUM.EQUITY_TOOLSET.jvsValue())
                    {
                        sSettleProceeds = trnTran.getField(TRANF_FIELD.TRANF_TRAN_INFO.toInt(), 0, EnumsTranInfoFields.MX_PROCEED_W_FEE.toString());
                        if (sSettleProceeds == null)
                            sSettleProceeds = trnTran.getField(TRANF_FIELD.TRANF_SETTLE_PROCEEDS.toInt());
                    }
                    else if (tDisplay.getInt("toolset", i) == TOOLSET_ENUM.CASH_TOOLSET.jvsValue())
                        sSettleProceeds = trnTran.getField(TRANF_FIELD.TRANF_POSITION.toInt());
                    else
                        sSettleProceeds = trnTran.getField(TRANF_FIELD.TRANF_SETTLE_PROCEEDS.toInt());
                    
                    if (sSettleProceeds == null) sSettleProceeds = "0.0";
                    
                    sSettleProceedsLimpio = sSettleProceeds.replace(",", "");
                    dSettleProceeds = Str.strToDouble(sSettleProceedsLimpio);
                    dSettleProceeds = Math.round(dSettleProceeds * 100.0) / 100.0;
                    tDisplay.setDouble("settle_proceeds", i, dSettleProceeds);

                }
            }
            catch(OException e)
            {
                _Log.printMsg(EnumTypeMessage.INFO, e.getMessage() + "\n");
            }
        }


    private void genCashFlows() {
        
        try {
            iRetVal = Ask.okCancel("Se generaran los Cash para las operaciones,\nDesea continuar?");
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to show dialog box on genCashFlows function :"+e.getMessage());
        }

        if (iRetVal == SUCCEEDED){
                String sPartyIndeval = EnumsParty.MX_SDINDEVAL_BU.toString();
                String sPartyStateStreet = EnumsParty.MX_STATESTREET_BU.toString();
                
                int iTranNumTemplateCash = 0;
                String sInternalBU = "";
                String sCflowType = "";
                String sInternalPortfolio = "";
                String sMonedaPago = "";
                double dImporteLiquidacion = 0.0;
                String sEstado = "";
                String sParty = "";

                int iRowNum = 0;
                
                try {
                    iRowNum = tReturnt.getNumRows();
                } catch (OException e) {
                    _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get rownums from tDisplay on genCashFlows function");
                }

                for (int iRow =1; iRow <=iRowNum; iRow++){
                    try {
                        sInternalPortfolio=tReturnt.getString("fondo", iRow);
                        sMonedaPago=tReturnt.getString("moneda", iRow);
                        dImporteLiquidacion=tReturnt.getDouble("importe", iRow);
                        sEstado=tReturnt.getString("estado", iRow);
                    } catch (OException e) {
                        _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get fondo, cuenta_custodia, importe_liquidacion o estado desde tDisplay");
                    }
                    
                    // Genera el cash solo cuando el estado es distinto de "-"
                    if (!sEstado.equals("-")){
                        //Format info to insert on transaction
                        iTranNumTemplateCash = getTranNumTemplateCashByCurrency(sMonedaPago); 
                        dImporteLiquidacion = dImporteLiquidacion *-1;
                        sCflowType = sEstado+" Custodio";
                        sInternalBU = getInternalBuByInternalPortfolio(sInternalPortfolio);
                        
                        if(sMonedaPago.equals(EnumsCurrency.MX_CURRENCY_MXN)) sParty = EnumsParty.MX_SDINDEVAL_BU.toString();
                        else sParty = EnumsParty.MX_STATESTREET_BU.toString();
                        
                        insertTransaction(iTranNumTemplateCash,sInternalPortfolio,sInternalBU,sCflowType,sParty,dImporteLiquidacion);

                    }
                }
        }
    }
    
    private String getInternalBuByInternalPortfolio(String sInternalPortfolio) {
        
        Table tInternalBunit = Util.NULL_TABLE;
        int iPortfolio = 0;
        String sInternalBunit = "";
        int iInternalBunit = 0;

        try {
            iPortfolio = Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sInternalPortfolio);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get id for selected portfolio on getInternalBuByInternalPortfolio fucntion : "+e.getMessage());
        }

        try {
            tInternalBunit = Table.tableNew("Internal Bunit");
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to initialize tInternalBunit Table on getInternalBuByInternalPortfolio fucntion : "+e.getMessage());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT party_id FROM party_portfolio WHERE portfolio_id = "+iPortfolio);
        try {
            DBaseTable.execISql(tInternalBunit, sb.toString());
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to execute query on getInternalBuByInternalPortfolio fucntion : "+e.getMessage());
        }
        
        try {
            iInternalBunit = tInternalBunit.getInt(1, 1);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get InternalBunit id on getInternalBuByInternalPortfolio fucntion : "+e.getMessage());
        }
 
        try {
            sInternalBunit = Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iInternalBunit);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get name for selected Bunit on getInternalBuByInternalPortfolio fucntion : "+e.getMessage());
        }

        return sInternalBunit;
    }
 
    private void insertTransaction(int iTemplate, String sInternalPortfolio,String sInternalBU, String sCflowType, String sExternalBU, double dAmount) {
        Transaction tranCash = null;
        String sUserName = "";
        String sDealNumCash = "";

        try {
            sUserName = Ref.getUserName();
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get UserName on insertTransaction fucntion : "+e.getMessage());
        }

        try {
            tranCash = Transaction.retrieveCopy(iTemplate);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get Template Deal on insertTransaction fucntion : "+e.getMessage());
        }
        
        try {
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_TRAN_TYPE.toInt(), 0, "", TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toString() );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_INTERNAL_BUNIT.toInt(), 0, "", sInternalBU );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_INTERNAL_PORTFOLIO.toInt(), 0, "", sInternalPortfolio );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_INTERNAL_CONTACT.toInt(), 0, "", sUserName );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_EXTERNAL_BUNIT.toInt(), 0, "", sExternalBU );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_SETTLE_DATE.toInt(), 0, "", "0d" );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_TRADE_DATE.toInt(), 0, "", "0d" );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_CFLOW_TYPE.toInt(), 0, "", sCflowType );
            iRetVal =  tranCash.setField(TRANF_FIELD.TRANF_POSITION.toInt(), 0, "", Str.doubleToStr(dAmount,2));
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to set some fields on new deal on insertTransaction fucntion : "+e.getMessage());
        }

        try {
            iRetVal =  tranCash.insertByStatus(TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to insert transaction on insertTransaction fucntion : "+e.getMessage());
        }
 
        try {
            sDealNumCash = tranCash.getField(TRANF_FIELD.TRANF_DEAL_TRACKING_NUM.toInt(),0);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get new transaction deal_num on insertTransaction fucntion : "+e.getMessage());
        }
 
        if (iRetVal != SUCCEEDED)
        {       
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to Insert New CASH.");
        }
 
        else
            _Log.printMsg(EnumTypeMessage.INFO, "New Cash Deal " + sDealNumCash + " Created Successfully.");        
 
    }
 
    public int getTranNumTemplateCashByCurrency(String sCurrency){
        
        int iCurrency = 0;
        Table tTemplate = Util.NULL_TABLE;
        int iTranNum = 0;
        
        try {
            tTemplate = Table.tableNew("Template");
        } catch (OException e1) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to create tTemplate Table");
        }

        try {
            iCurrency = Ref.getValue(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, sCurrency);
        } catch (OException e) {
 
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to create tTemplate Table");
 
        }

        StringBuffer sTemplate = new StringBuffer();
        sTemplate.append(" SELECT ab.tran_num, ab.reference, ab.currency \n")
                 .append(" FROM ab_tran ab \n")
                 .append(" WHERE ab.tran_type = ").append(TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.jvsValue()).append(" \n")
                 .append(" AND ab.tran_status = ").append(TRAN_STATUS_ENUM.TRAN_STATUS_TEMPLATE.jvsValue()).append(" \n")
                 .append(" AND ab.toolset = ").append(TOOLSET_ENUM.CASH_TOOLSET.jvsValue()).append(" \n")
                 .append(" AND ab.currency IN (").append(iCurrency).append(") \n");
        try {
            DBaseTable.execISql(tTemplate, sTemplate.toString());
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to poblate tTemplate Table");
        }

        try {
            iTranNum = tTemplate.getInt("tran_num", 1);
        } catch (OException e) {
            _Log.printMsg(EnumTypeMessage.ERROR, "Unable to get tran num of template");
        }
        return iTranNum;
    }       
}