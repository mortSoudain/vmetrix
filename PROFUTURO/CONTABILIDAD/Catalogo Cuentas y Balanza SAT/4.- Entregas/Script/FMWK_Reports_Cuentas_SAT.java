/*$Header: v 1.2, 02/Mar/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Cuentas_SAT.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Noviembre 2017
 * Version:					1.0
 * Description:				Script diseñado para la obtencion de las cuentas SAT.
 * 							
 * 							Ademas, el plugin genera un archivo XML y XLS con respecto
 * 							a la informacion de cuentas obtenida
 *                       
 * REVISION HISTORY
 * Date:                  	01/Feb/2018
 * Version/Autor:          	1.1 - Basthian Matthews Sanhueza Vmetrix
 * Description:            	Se incluye comportamiento para filtrar por todos los portafolios,
 * 							y generar un informe para cada uno por separado
 *                       
 * REVISION HISTORY
 * Date:                  	02/Mar/2018
 * Version/Autor:          	1.2 - Basthian Matthews Sanhueza Vmetrix
 * Description:            	Eliminada Subcuenta del reporte y pantalla.
 *                         
 ************************************************************************************/

package com.afore.custom_reports;

import java.io.IOException;

import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.SystemUtil;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;

public class FMWK_Reports_Cuentas_SAT implements IScript {

	UTIL_Afore UtilAfore = new UTIL_Afore();
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;
	
    private Table tblProperties			=	Util.NULL_TABLE;
    private Table tblGlobalProperties	=	Util.NULL_TABLE;
	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;

    //private String sSelectedStartDate	=	null;
    private String sSelectedEndDate		=	null;
    private String sSelectedPortfolio	=	null;
    private String sSettedVersion		=	null;
	private String sCellName			=	null;

	private int iToday		=	0;
	private String sToday	=	null;
        
	public void execute(IContainerContext context) throws OException {
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DEFAULT);

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
		
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
					
				// Setting data and tDisplay table structure
					getData();
					
				// Set table formats
					setTableFormat();
				
				// Setting table properties
					setTableProperties();
				
				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
				callback_event	=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
				tReturnt		=	UserDataWorksheet.getData();
				sCellName		=	tReturnt.scriptDataGetCallbackName();
				
				sSelectedPortfolio	=	tReturnt.scriptDataGetWidgetString("Portfolio");
								
				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						if(sCellName.equals("btnGenerarReporte")){
							try {
								generarReportes();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						break;
					default:
						break;
				}
				
				break;
			default:
				break;
		}
	}

	private void generarReportes() throws OException, IOException {
		
		if(sSelectedPortfolio.equals("TODOS")){
			
			String sPortafolioQuery = "SELECT name as portfolio FROM portfolio where portfolio_type =0 and restricted = 1";
			Table tPortfolio = Table.tableNew();
			DBaseTable.execISql(tPortfolio, sPortafolioQuery);
			
			String sPortfolio = "";
			
			for(int i=1; i<=tPortfolio.getNumRows(); i++){
				sPortfolio = tPortfolio.getString("portfolio", i);
				generarReporte(sPortfolio);
			}
		} else {
			generarReporte(sSelectedPortfolio);
		}
		
	}

	private void setTableFormat() throws OException {
		tDisplay.setColTitle("cod_agrup", "Codigo\nde Agrupacion");
		tDisplay.setColTitle("num_cta", "Numero\nde Cuenta");
		tDisplay.setColTitle("description", "Descripcion");
		tDisplay.setColTitle("sub_cta", "Sub Cuenta");
		tDisplay.setColTitle("nivel", "Nivel");
		tDisplay.setColTitle("naturaleza", "Naturaleza");
	}

	private void generarReporte(String sPortfolio) throws OException, IOException {
		
		// Getting today month and year
		sSelectedEndDate	=	tReturnt.scriptDataGetWidgetString("EndDate");
		int iEndDate = OCalendar.parseString(sSelectedEndDate);
		
		int iMonth = OCalendar.getMonth(iEndDate);
		int iYear = OCalendar.getYear(iEndDate);
		String sMonth = Str.intToStr(iMonth);
		String sYear = Str.intToStr(iYear);
		
		if(sMonth.length()==1){
			sMonth="0"+sMonth;
		}
		
		// Getting configurable variables to get path for report files
			String sPath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Cuentas_Balanza_SAT", "path");
		
		// Getting RFC from portfolio
			String sQueryRFC = 
					"\n SELECT a.irs_terminal_num as RFC "+
					"\n FROM party_address a "+
					"\n 	JOIN party p ON (a.party_id = p.party_id AND p.int_ext = 0) "+
					"\n 	JOIN portfolio on (p.default_portfolio_id = portfolio.id_number) "+
					"\n WHERE portfolio.name = '"+sPortfolio+"' "+
					"\n AND p.short_name LIKE 'PROF-%' ";
			Table tRFC = Table.tableNew();
			DBaseTable.execISql(tRFC, sQueryRFC);
			String sRFC = tRFC.getString(1, 1);
			
		// Getting setted version
			sSettedVersion	=	tReturnt.scriptDataGetWidgetString("Version");

		// Setting filenames
			String sFileName = sPath+sRFC+sYear+sMonth+"CT";
			String sFileExtensionXML = ".xml";
			String sFileExtensionXLS = ".xls";
		
		//Setting output table
			Table tOutput = Table.tableNew();
			tOutput.select(tReturnt, "cod_agrup,"
					+ "num_cta,"
					+ "description,"
					+ "sub_cta,"
					+ "nivel,"
					+ "naturaleza", "num_cta GT 0");
			
		// Generating String with xml format and saving file
			genXmlOutput(tOutput, sRFC, sMonth, sYear, sSettedVersion, sFileName+sFileExtensionXML);
		
		// Generating and opening XLS
			genXlsOutput(tOutput, sRFC, sMonth, sYear, sSettedVersion, sFileName+sFileExtensionXLS);

	}

	private void genXmlOutput(Table tOutput, String sRFC, String sMonth,
			String sYear, String sSettedVersion, String sFullFilePath) throws OException {
		
		String sVersionUnderscore = sSettedVersion.replace(".", "_");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<catalogocuentas:Catalogo Version=\""+sSettedVersion+"\" RFC=\""+sRFC+"\" Mes=\""+sMonth+"\" Anio=\""+sYear+"\" xmlns:catalogocuentas=\"http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/CatalogoCuentas\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/CatalogoCuentas/CatalogoCuentas_"+ sVersionUnderscore +".xsd\">\n");
		
		for (int i=1 ; i <= tOutput.getNumRows(); i++){
			sb.append("<catalogocuentas:Ctas ")
			.append("CodAgrup=\"").append(tOutput.getString("cod_agrup", i)).append("\" ")
			.append("NumCta=\"").append(tOutput.getString("num_cta", i)).append("\" ")
			.append("Desc=\"").append(tOutput.getString("description", i)).append("\" ")
			.append("Nivel=\"").append(tOutput.getString("nivel", i)).append("\" ")
			.append("Natur=\"").append(tOutput.getString("naturaleza", i)).append("\"/>\n");
		}
		
		sb.append("</catalogocuentas:Catalogo>\n");
		Str.printToFile(sFullFilePath, sb.toString());
		
	}

	private void genXlsOutput(Table tOutput, String sRFC, String sMonth,
			String sYear, String sSettedVersion, String sFullFilePath) throws OException {
		
		Table tParameters = Table.tableNew("Parametros Crystal");
		int iNewRow = tParameters.addRow();
		
		tParameters.addCol("varRFC", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varVersion", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varMes", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varAnio", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varFecha", COL_TYPE_ENUM.COL_DATE_TIME);
		tParameters.addCol("varGeneradoPor", COL_TYPE_ENUM.COL_STRING);
		
		tParameters.setString("varRFC", iNewRow, sRFC);
		tParameters.setString("varVersion", iNewRow, sSettedVersion);
		tParameters.setString("varMes", iNewRow, sMonth);
		tParameters.setString("varAnio",iNewRow, sYear);
		tParameters.setDateTime("varFecha", iNewRow, ODateTime.strToDateTime(sToday));
		tParameters.setString("varGeneradoPor", iNewRow, Ref.getUserName());
		
		String sTemplatesCrystalPath = Crystal.getRptDir();
		String sReportTemplateName = "ReporteCuentasSAT.rpt";
		String sFullTemplatePath = sTemplatesCrystalPath + "\\" + sReportTemplateName;
		
		//Exporting excel
		Crystal.tableExportCrystalReport(tOutput, 
				sFullTemplatePath, 
				CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
				sFullFilePath, 
				CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
				tParameters);
		
		//Opening Report
		SystemUtil.createProcess(sFullFilePath);
	}

	private void getData() throws OException {
		
		// Getting all data from user table
		Table tData = Table.tableNew();
		String sQuery = "\n SELECT COD_AGRUP, "+
				"\n NUM_CTA, "+
				"\n DESCRIPTION, "+
				"\n '' AS SUB_CTA, "+
				"\n NIVEL, "+
				"\n NATURALEZA "+
				"\n FROM USER_MX_CATALOGO_CUENTAS ";
		DBaseTable.execISql(tData, sQuery);		
		
		// Fetching data to the tDisplay table
		tDisplay.select(tData, "*", "num_cta GT 0");
	}

	private void setTableProperties() throws OException {
		
		// Initialize properties tables
			tblProperties			=	UserDataWorksheet.initProperties();
			tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
		
		// Setting col properties
			UserDataWorksheet.setColReadOnly("cod_agrup", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("num_cta", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("description", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("sub_cta", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("nivel", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("naturaleza", tDisplay, tblProperties);
			
		// Setting UDW properties
			UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
	}

	private void addVisualElements() throws OException {
		
		// Creating display table
			tDisplay = Table.tableNew();
		
		// Getting portfolio table for combobox
			String sPortafolioQuery = "SELECT name as portfolio FROM portfolio";
			Table tPortfolio = Table.tableNew();
			DBaseTable.execISql(tPortfolio, sPortafolioQuery);
			int iNewRow = tPortfolio.addRow();
			tPortfolio.setString("portfolio", iNewRow, "TODOS");
			
		// Setting version table for combobox
			Table tVersion = Table.tableNew();
			tVersion.addCol("Version", COL_TYPE_ENUM.COL_STRING);
			int iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.0");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.1");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.2");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.3");

		// Setting visual elements
			tDisplay.scriptDataAddWidget("lblPortfolio", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=50, y=17, h=20, w=50",
					"label=Portfolio:");
			
			tDisplay.scriptDataAddWidget("Portfolio", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
					"x=100, y=20, h=25, w=130",
					"label="+tPortfolio.getString(1, iNewRow), tPortfolio);
			
			tDisplay.scriptDataAddWidget("lblStartDate", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=250, y=17, h=20, w=50",
					"label=Fecha Ini:");
			
			tDisplay.scriptDataAddWidget("StartDate", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_CALENDAR_WIDGET.toInt(), 
					"x=300, y=20, h=25, w=130", 
					"label="+sToday);
			
			tDisplay.scriptDataAddWidget("lblEndDate", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=450, y=17, h=20, w=50",
					"label=Fecha Fin:");
			
			tDisplay.scriptDataAddWidget("EndDate", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_CALENDAR_WIDGET.toInt(), 
					"x=500, y=20, h=25, w=130",
					"label="+sToday);
			
			tDisplay.scriptDataAddWidget("lblVersion", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=650, y=17, h=20, w=50",
					"label=Version:");
			
			tDisplay.scriptDataAddWidget("Version", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
					"x=670, y=20, h=25, w=50",
					"label="+tVersion.getString(1, 1), tVersion);
			
			tDisplay.scriptDataAddWidget("btnGenerarReporte", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=1100, y=15, h=25, w=130", "label=Generar Reporte");
	
			tDisplay.scriptDataMoveListBox("top=45,left=40,right=50,bottom=40");
		// Clean memory
			tPortfolio.destroy();
	}
}