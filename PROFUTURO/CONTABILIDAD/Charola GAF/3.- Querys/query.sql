SELECT 
  ab.deal_tracking_num AS DEAL,
  ab.ins_type as INS_TYPE,
  ab.toolset AS TOOLSET,

  NVL(TRIM(tran_info_per_settlement_fee.value),0) AS PER_SETTLEMENT_FEE,
  NVL(TRIM(tran_info_sec_fee.value),0) AS SEC_FEE,
  NVL(TRIM(tran_info_per_share_fee.value),0) AS PER_SHARE_FEE,
  NVL(TRIM(tran_info_tax_fee.value),0) AS TAX_FEE,
  NVL(TRIM(SECURITY_TRAN_AUX_DATA.net_proceeds),0) AS NET_PROCEEDS,
  NVL(TRIM(ab.proceeds),0) AS PROCEEDS,
  NVL(TRIM(tran_info_proceeds_fee.value),0) AS PROCEEDS_FEE,
  NVL(TRIM(SECURITY_TRAN_AUX_DATA.settle_fx),0) AS SETTLE_FX,
  NVL(TRIM(tran_clean_price.value),0) AS CLEAN_PRICE,
  NVL(TRIM(tran_info_precio_sucio_mp.value),0) AS PRECIO_SUCIO_MP,
  ab.price,

  CAST(CASE 
    WHEN (fx_aux.c_amt IS NULL) THEN 0.0
    ELSE fx_aux.c_amt
  END AS FLOAT )as c_amt,


  SUBSTR (h.ticker, 1, INSTR(h.ticker, '_')-1) AS TIPO_VALOR,
  SUBSTR (h.ticker, INSTR(h.ticker, '_') + 1, INSTR(h.ticker, '_', -1) - INSTR(h.ticker, '_') - 1) AS EMISORA,
  SUBSTR (h.ticker, INSTR(h.ticker, '_', -1) + 1, LENGTH(h.ticker) - INSTR(h.ticker, '_', -1)) AS SERIE,
  CAST(ab.trade_date AS DATE) AS FECHA_DE_OPERACION,
  CAST(ab.settle_date AS DATE) AS FECHA_DE_LIQUIDACION,
  external_party.SHORT_NAME AS CLAVE_DEL_PROVEEDOR,
  external_party_info.VALUE AS CLAVE_CONSAR,
  ' ' AS PLAZO_DEL_REPORTO,
  ab.position AS TITULOS,

  NVL(TRIM(tran_pricing_yield.value),0) AS TASA_DE_RENDIMIENTO,

  CAST(
      CASE WHEN (ab.toolset = 9) THEN ab.price
      ELSE CASE WHEN (SECURITY_TRAN_AUX_DATA.settle_fx IS NULL) THEN 1.0
            ELSE CAST(SECURITY_TRAN_AUX_DATA.settle_fx AS FLOAT)
            END
  END AS FLOAT) AS TIPO_DE_CAMBIO,

  internal_party.short_name AS CLAVE_DE_PIZARRA,
  '0' AS IMPORTE_COMISION_BROKER,
  tran_info_medio_concertacion.VALUE AS MEDIO_DE_CONCERTACION,
  tran_info_sistema_concertacion.VALUE AS SISTEMA_DE_CONCERTACION,
  'N/A' AS FOLIO_POSICION,
  tran_info_tipo_postura.VALUE AS TIPO_DE_POSTURA,
  tran_info_banco_trabajo.VALUE AS BANCO_DE_TRABAJO,
  '0' AS CLASE_DE_ACTIVO,

  user_mx_catalogo_consar.consar_id AS DIVISA_DE_LIQUIDACION,

  tran_info_mercado_primario.VALUE AS MERCADO_PRIMARIO,

  CASE 
    WHEN (tran_hora_concertacion.value IS NOT NULL) THEN tran_hora_concertacion.value
    ELSE ab_tran_history.row_creation
  END AS HORA_DE_CONCERTACION,

  '0' AS IVA_COM_BROKER,
  ab.toolset,
  ab.buy_sell,
  ab.tran_num,
  portfolio.name as portfolio
FROM ab_tran ab

  JOIN header h
    ON (ab.ins_num = h.ins_num)
  LEFT JOIN party internal_party
    ON (ab.INTERNAL_BUNIT = internal_party.party_id)
  LEFT JOIN party external_party
    ON (ab.EXTERNAL_BUNIT = external_party.party_id)
  LEFT JOIN party_info_view external_party_info
    ON (ab.EXTERNAL_BUNIT = external_party_info.party_id
    AND external_party_info.type_id=20006) -- CLAVE CONSAR
    
  LEFT JOIN ab_tran_info_view tran_info_medio_concertacion
    ON (ab.TRAN_NUM = tran_info_medio_concertacion.TRAN_NUM
    AND tran_info_medio_concertacion.TYPE_ID=20051)--medio concertacion
  LEFT JOIN ab_tran_info_view tran_info_sistema_concertacion
    ON (ab.TRAN_NUM = tran_info_sistema_concertacion.TRAN_NUM
    AND tran_info_sistema_concertacion.TYPE_ID=20024)--sistema concertacion
  LEFT JOIN ab_tran_info_view tran_info_tipo_postura
    ON (ab.TRAN_NUM = tran_info_tipo_postura.TRAN_NUM
    AND tran_info_tipo_postura.TYPE_ID=20019)--tipo postura
  LEFT JOIN ab_tran_info_view tran_info_banco_trabajo
    ON (ab.TRAN_NUM = tran_info_banco_trabajo.TRAN_NUM
    AND tran_info_banco_trabajo.TYPE_ID=20007)--tipo postura
  LEFT JOIN ab_tran_info_view tran_info_mercado_primario
    ON (ab.TRAN_NUM = tran_info_mercado_primario.TRAN_NUM
    AND tran_info_mercado_primario.TYPE_ID=20029)--tipo postura
  LEFT JOIN ab_tran_info_view tran_clean_price
    ON (ab.TRAN_NUM = tran_clean_price.TRAN_NUM
    AND tran_clean_price.TYPE_ID=20162)--medio concertacion

  LEFT JOIN ab_tran_info_view tran_pricing_yield
    ON (ab.TRAN_NUM = tran_pricing_yield.TRAN_NUM
    AND tran_pricing_yield.TYPE_ID=20112)--pricing yield
    
  LEFT JOIN ab_tran_info_view tran_hora_concertacion
    ON (ab.TRAN_NUM = tran_hora_concertacion.TRAN_NUM
    AND tran_hora_concertacion.TYPE_ID=20052)--pricing yield
    
  left join SECURITY_TRAN_AUX_DATA
  on (ab.tran_num = SECURITY_TRAN_AUX_DATA.tran_num)
    
  LEFT JOIN currency c
    ON (ab.CURRENCY = c.ID_NUMBER)
  LEFT JOIN user_mx_catalogo_consar
    ON (c.NAME = user_mx_catalogo_consar.consar_value
    AND user_mx_catalogo_consar.consar_field='Divisa')

  LEFT JOIN 
  (
    SELECT tran_num, TO_CHAR(row_creation, 'HH24:MI:SS') as row_creation
      FROM(
          SELECT tran_num, row_creation,  
          ROW_NUMBER() OVER (PARTITION BY tran_num ORDER BY version_number ASC) AS num_row 
          FROM ab_tran_history  
          WHERE tran_status IN ( 2 )  
          AND update_type =  9
      ) tran_history 
    WHERE num_row = 1
  ) ab_tran_history on ab.tran_num = ab_tran_history.tran_num
  
  --Fees
  LEFT JOIN ab_tran_info_view tran_info_sec_fee
    ON (ab.TRAN_NUM = tran_info_sec_fee.TRAN_NUM
    AND tran_info_sec_fee.TYPE_ID=20034)
  LEFT JOIN ab_tran_info_view tran_info_per_share_fee
    ON (ab.TRAN_NUM = tran_info_per_share_fee.TRAN_NUM
    AND tran_info_per_share_fee.TYPE_ID=20036)
  LEFT JOIN ab_tran_info_view tran_info_proceeds_fee
    ON (ab.TRAN_NUM = tran_info_proceeds_fee.TRAN_NUM
    AND tran_info_proceeds_fee.TYPE_ID=20008)
  LEFT JOIN ab_tran_info_view tran_info_per_settlement_fee
    ON (ab.TRAN_NUM = tran_info_per_settlement_fee.TRAN_NUM
    AND tran_info_per_settlement_fee.TYPE_ID=20009)
  LEFT JOIN ab_tran_info_view tran_info_tax_fee
    ON (ab.TRAN_NUM = tran_info_tax_fee.TRAN_NUM
    AND tran_info_tax_fee.TYPE_ID=20012)

  LEFT JOIN ab_tran_info_view tran_info_precio_sucio_mp
    ON (ab.TRAN_NUM = tran_info_precio_sucio_mp.TRAN_NUM
    AND tran_info_precio_sucio_mp.TYPE_ID=20164)

  left join fx_tran_aux_data fx_aux
  on (ab.tran_num = fx_aux.tran_num)

  inner join portfolio
    on ab.internal_portfolio = portfolio.id_number
    and portfolio.portfolio_type = 0
    and portfolio.restricted = 1

WHERE 1=1
  AND ab.toolset in (5,9,28,34,41,50) --bond,fx,equity,bondfut,finfut,genericfut
  AND ab.tran_status in (2,3,22) --new,validated,closeout
  AND ab.ins_type not in (1000006) -- EQT-siefore
  
AND ab.trade_date = '04-Jan-2018'