  CASE WHEN(ab.ins_type in (40001,40008,1000005)) --EQT-COMMON,EQT-BASKET,EQT-FIBRA
    THEN NVL(TRIM(tran_info_per_settlement_fee.value),0)
    ELSE 
      CASE WHEN(ab.ins_type in (40004)) --EQT-ETF
        THEN NVL(TRIM(tran_info_per_settlement_fee.value),0)
              +NVL(TRIM(tran_info_sec_fee.value),0)
              +NVL(TRIM(tran_info_per_share_fee.value),0)
        ELSE 0
      END
  END AS IMPORTE_DE_COMISIONES,


    CASE WHEN(ab.ins_type in (40001,40008,1000005)) --EQT-COMMON,EQT-BASKET,EQT-FIBRA
      THEN NVL(TRIM(tran_info_tax_fee.value),0)
      ELSE 0
  END AS IVA,


  CASE WHEN(ab.toolset in (34,41,50)) THEN NVL(TRIM(SECURITY_TRAN_AUX_DATA.net_proceeds),0) --Futuros   
      WHEN(ab.toolset in (5)) THEN '0'-- NVL(TRIM(ab.proceeds),0) + NVL(TRIM(SECURITY_TRAN_AUX_DATA.settle_fx),0) -- Bonos
      WHEN(ab.toolset in (28)) THEN NVL(TRIM(tran_info_proceeds_fee.value),0) -- EQT
      ELSE 0
  END AS IMPORTE_NETO,
