/*$Header: v 1.1, 06/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Charola_GAF.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Enero 2018
 * Version:					1.0
 * Description:				Script diseñado para la regenracion del reporte de Charola GAF.
 * 							Se genera un archivo xls por fondo y se enviana  los mails
 * 							configurados en la user_configurable variables.
 *                       
 * REVISION HISTORY
 * Date:					Febrero 2018
 * Version/Autor:			1.1 - Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				- Se incluyen operaciones status new
 * 							- Se obtiene para todos los toolset la moneda desde le misma fuente
 * 							- Se cambia valor de precio unitario para equity. Anteriormente = Precio Sucio MP, Ahora = Price 
 * 							- Modificado formato de hora en hora de concertacion en query principal
 *                         
 ************************************************************************************/


package com.afore.custom_reports;

import java.util.ArrayList;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsPartyInfoFields;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Transaction;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRANF_FIELD;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;

public class FMWK_Reports_Charola_GAF implements IScript {
	
	// Declare utils and general variables
		String sScriptName		=	this.getClass().getSimpleName();	   
		UTIL_Afore UtilAfore	=	new UTIL_Afore();
		UTIL_Log LOG			=	new UTIL_Log(sScriptName);
		Transaction tranPointer;
	
	// Declare time variables
		int iToday					=	0;
		String sHour				=	"";
		String sTodayDefault		=	"";
		String sTodayAAAAMMDD		=	"";
		String sTodaySlashDDMMAAAA	=	"";
	
	// Declare global data table
		Table tData = Util.NULL_TABLE;
	
	// Declare output FileNames
	ArrayList<String> sFullFileNames = new ArrayList<String>();
	String sListOfFiles= "";

	@Override
	public void execute(IContainerContext context) throws OException {
		LOG.markStartScript();
		
		// Initialize Global Variables
			initializeScript();
		
		// Set data on tData Global Table
			setTableData();
			
		// Set fees
			setFeeFields();
			
		// format data
			formatData();
			
		// Generate reports and send mails for each portfolio
			generateReportsAndSendMails();
	
		LOG.markEndScript();	
	}
	
	private void initializeScript() {
		
		// Set time variables
			try {
				iToday	=	OCalendar.today();
				sHour	=	Util.timeGetServerTimeHMS();
				tData	=	Table.tableNew("Data Table");
				sTodayDefault = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
				sTodayAAAAMMDD = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601);
				sTodaySlashDDMMAAAA = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to load system time variables : "
						+ e.getMessage());
			}
	}
	
	private void setTableData() throws OException {
		
		Table tUserMxClaveMovimiento = Util.NULL_TABLE;
		
		StringBuilder sQueryPrincipal = new StringBuilder();
		
		// Query filters
			int iInsTypeEqtSiefore				=	EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt();//1000006
			
			int iToolsetEqt						=	TOOLSET_ENUM.EQUITY_TOOLSET.toInt();//28
			int iToolsetBond					=	TOOLSET_ENUM.BOND_TOOLSET.toInt();//5
			int iToolsetFx						=	TOOLSET_ENUM.FX_TOOLSET.toInt();//9
			int iToolsetBondFut					=	TOOLSET_ENUM.BONDFUT_TOOLSET.toInt();//34
			int iToolsetFinFut					=	TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt();//41
			int iToolsetGenericFut				=	TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt();//50
			
			int iTranStatusNew					=	TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt();//2
			int iTranStatusValidated			=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
			int iTranStatusCloseout				=	TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt();//22
			int iPartyInfoConsar				=	EnumsPartyInfoFields.MX_PARTY_ID_CONSAR.toInt();//20006

			String sUserMxCatalogoConsar		=	EnumsUserTables.USER_MX_CATALOGO_CONSAR.toString();//user_mx_catalogo_consar
			
			int iTranInfoMedioConcertacion		=	EnumsTranInfoFields.MX_TRAN_MEDIO_CONCERTACION.toInt();//20051
			int iTranInfoSistemaConcertacion	=	EnumsTranInfoFields.MX_TRAN_SISTEMA_CONCERTACION.toInt();//20024
			int iTranInfoTipoPostura			=	EnumsTranInfoFields.MX_ALL_TIPO_POSTURA.toInt();//20019
			int iTranInfoBancoTrabajo			=	EnumsTranInfoFields.MX_ALL_BANCO_TRABAJO.toInt();//20007
			int iTranInfoMercadoPrimario		=	EnumsTranInfoFields.MX_ALL_MERCADO_PRIMARIO.toInt();//20029
			int iTranInfoSecFee					=	EnumsTranInfoFields.MX_SEC_FEE.toInt();//20034
			int iTranInfoPerShareFee			=	EnumsTranInfoFields.MX_PER_SHARE_FEE.toInt();//20036
			int iTranInfoProceedsFee			=	EnumsTranInfoFields.MX_PROCEED_W_FEE.toInt();//20008
			int iTranInfoPerSettlementFee		=	EnumsTranInfoFields.MX_PER_SETTLEMENT_FEE.toInt();//20009
			int iTranInfoTaxFee					=	EnumsTranInfoFields.MX_TAX_FEE.toInt();//20012
			int iTranInfoCleanPrice				=	EnumsTranInfoFields.MX_TRAN_CLEAN_PRICE.toInt();//20162
			int iTranInfoPrecioSucioMP			=	EnumsTranInfoFields.MX_TRAN_PRECIO_SUCIO_MP.toInt();//20164
			int iTranInfoPricingYield			=	EnumsTranInfoFields.MX_BOND_YIELD.toInt();// 20112?
			int iTranInfoHoraConcertacion		=	EnumsTranInfoFields.MX_TRAN_HORA_CONCERTACION.toInt();//20052
			
		//Setting Principal Query
			sQueryPrincipal
			.append("\n SELECT  ")
			.append("\n   ab.deal_tracking_num AS DEAL, ")
			.append("\n   ab.ins_type as INS_TYPE, ")
			.append("\n  ")
			.append("\n   NVL(TRIM(tran_info_per_settlement_fee.value),0) AS PER_SETTLEMENT_FEE, ")
			.append("\n   NVL(TRIM(tran_info_sec_fee.value),0) AS SEC_FEE, ")
			.append("\n   NVL(TRIM(tran_info_per_share_fee.value),0) AS PER_SHARE_FEE, ")
			.append("\n   NVL(TRIM(tran_info_tax_fee.value),0) AS TAX_FEE, ")
			.append("\n   NVL(TRIM(SECURITY_TRAN_AUX_DATA.net_proceeds),0) AS NET_PROCEEDS, ")
			.append("\n   NVL(TRIM(ab.proceeds),0) AS PROCEEDS, ")
			.append("\n   NVL(TRIM(tran_info_proceeds_fee.value),0) AS PROCEEDS_FEE, ")
			.append("\n   NVL(TRIM(SECURITY_TRAN_AUX_DATA.settle_fx),0) AS SETTLE_FX, ")
			.append("\n   NVL(TRIM(tran_clean_price.value),0) AS CLEAN_PRICE, ")
			.append("\n   NVL(TRIM(tran_info_precio_sucio_mp.value),0) AS PRECIO_SUCIO_MP, ")
			.append("\n   ab.price, ")
			.append("\n  ")
			.append("\n   CAST(CASE  ")
			.append("\n     WHEN (fx_aux.c_amt IS NULL) THEN 0.0 ")
			.append("\n     ELSE fx_aux.c_amt ")
			.append("\n   END AS FLOAT )as c_amt, ")
			.append("\n  ")
			.append("\n   SUBSTR (h.ticker, 1, INSTR(h.ticker, '_')-1) AS TIPO_VALOR, ")
			.append("\n   SUBSTR (h.ticker, INSTR(h.ticker, '_') + 1, INSTR(h.ticker, '_', -1) - INSTR(h.ticker, '_') - 1) AS EMISORA, ")
			.append("\n   SUBSTR (h.ticker, INSTR(h.ticker, '_', -1) + 1, LENGTH(h.ticker) - INSTR(h.ticker, '_', -1)) AS SERIE, ")
			.append("\n   CAST(ab.trade_date AS DATE) AS FECHA_DE_OPERACION, ")
			.append("\n   CAST(ab.settle_date AS DATE) AS FECHA_DE_LIQUIDACION, ")
			.append("\n   external_party.SHORT_NAME AS CLAVE_DEL_PROVEEDOR, ")
			.append("\n   external_party_info.VALUE AS CLAVE_CONSAR, ")
			.append("\n   ' ' AS PLAZO_DEL_REPORTO, ")
			.append("\n   ab.position AS TITULOS, ")
			.append("\n  ")
			.append("\n   NVL(TRIM(tran_pricing_yield.value),0) AS TASA_DE_RENDIMIENTO, ")
			.append("\n  ")
			.append("\n   CAST( ")
			.append("\n       CASE WHEN (ab.toolset = "+ iToolsetFx +") THEN ab.price ")
			.append("\n       ELSE CASE WHEN (SECURITY_TRAN_AUX_DATA.settle_fx IS NULL) THEN 1.0 ")
			.append("\n             ELSE CAST(SECURITY_TRAN_AUX_DATA.settle_fx AS FLOAT) ")
			.append("\n             END ")
			.append("\n   END AS FLOAT) AS TIPO_DE_CAMBIO, ")
			.append("\n  ")
			.append("\n   internal_party.short_name AS CLAVE_DE_PIZARRA, ")
			.append("\n   '0' AS IMPORTE_COMISION_BROKER, ")
			.append("\n   tran_info_medio_concertacion.VALUE AS MEDIO_DE_CONCERTACION, ")
			.append("\n   tran_info_sistema_concertacion.VALUE AS SISTEMA_DE_CONCERTACION, ")
			.append("\n   'N/A' AS FOLIO_POSICION, ")
			.append("\n   tran_info_tipo_postura.VALUE AS TIPO_DE_POSTURA, ")
			.append("\n   tran_info_banco_trabajo.VALUE AS BANCO_DE_TRABAJO, ")
			.append("\n   '0' AS CLASE_DE_ACTIVO, ")
			.append("\n   ")
			.append("\n   user_mx_catalogo_consar.consar_id AS DIVISA_DE_LIQUIDACION, ")
			.append("\n   ")
			.append("\n   tran_info_mercado_primario.VALUE AS MERCADO_PRIMARIO, ")
			.append("\n  ")
			.append("\n   CASE  ")
			.append("\n     WHEN (tran_hora_concertacion.value IS NOT NULL) THEN tran_hora_concertacion.value ")
			.append("\n     ELSE ab_tran_history.row_creation ")
			.append("\n   END AS HORA_DE_CONCERTACION, ")
			.append("\n  ")
			.append("\n   '0' AS IVA_COM_BROKER, ")
			.append("\n   ab.toolset, ")
			.append("\n   ab.buy_sell, ")
			.append("\n   ab.tran_num, ")
			.append("\n   portfolio.name as portfolio ")
			.append("\n FROM ab_tran ab ")
			.append("\n  ")
			.append("\n   JOIN header h ")
			.append("\n     ON (ab.ins_num = h.ins_num) ")
			.append("\n   LEFT JOIN party internal_party ")
			.append("\n     ON (ab.INTERNAL_BUNIT = internal_party.party_id) ")
			.append("\n   LEFT JOIN party external_party ")
			.append("\n     ON (ab.EXTERNAL_BUNIT = external_party.party_id) ")
			.append("\n   LEFT JOIN party_info_view external_party_info ")
			.append("\n     ON (ab.EXTERNAL_BUNIT = external_party_info.party_id ")
			.append("\n     AND external_party_info.type_id="+ iPartyInfoConsar +") -- CLAVE CONSAR ")
			.append("\n      ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_medio_concertacion ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_medio_concertacion.TRAN_NUM ")
			.append("\n     AND tran_info_medio_concertacion.TYPE_ID="+ iTranInfoMedioConcertacion +")--medio concertacion ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_sistema_concertacion ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_sistema_concertacion.TRAN_NUM ")
			.append("\n     AND tran_info_sistema_concertacion.TYPE_ID="+ iTranInfoSistemaConcertacion +")--sistema concertacion ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_tipo_postura ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_tipo_postura.TRAN_NUM ")
			.append("\n     AND tran_info_tipo_postura.TYPE_ID="+ iTranInfoTipoPostura +")--tipo postura ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_banco_trabajo ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_banco_trabajo.TRAN_NUM ")
			.append("\n     AND tran_info_banco_trabajo.TYPE_ID="+ iTranInfoBancoTrabajo +")--tipo postura ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_mercado_primario ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_mercado_primario.TRAN_NUM ")
			.append("\n     AND tran_info_mercado_primario.TYPE_ID="+ iTranInfoMercadoPrimario +")--tipo postura ")
			.append("\n      ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_clean_price ")
			.append("\n     ON (ab.TRAN_NUM = tran_clean_price.TRAN_NUM ")
			.append("\n     AND tran_clean_price.TYPE_ID="+ iTranInfoCleanPrice +")--medio concertacion ")
			.append("\n  ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_pricing_yield ")
			.append("\n     ON (ab.TRAN_NUM = tran_pricing_yield.TRAN_NUM ")
			.append("\n     AND tran_pricing_yield.TYPE_ID="+ iTranInfoPricingYield +")--pricing yield ")
			.append("\n      ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_hora_concertacion ")
			.append("\n     ON (ab.TRAN_NUM = tran_hora_concertacion.TRAN_NUM ")
			.append("\n     AND tran_hora_concertacion.TYPE_ID="+ iTranInfoHoraConcertacion +")--pricing yield ")
			.append("\n      ")
			.append("\n   left join SECURITY_TRAN_AUX_DATA ")
			.append("\n   on (ab.tran_num = SECURITY_TRAN_AUX_DATA.tran_num) ")
			.append("\n      ")
			.append("\n   LEFT JOIN currency c ")
			.append("\n     ON (ab.CURRENCY = c.ID_NUMBER) ")
			.append("\n   LEFT JOIN user_mx_catalogo_consar ")
			.append("\n     ON (c.NAME = user_mx_catalogo_consar.consar_value ")
			.append("\n     AND user_mx_catalogo_consar.consar_field='Divisa') ")
			.append("\n  ")
			.append("\n   LEFT JOIN  ")
			.append("\n   ( ")
			.append("\n     SELECT tran_num, TO_CHAR(row_creation, 'HH24:MI:SS') as row_creation ")
			.append("\n       FROM( ")
			.append("\n           SELECT tran_num, row_creation,   ")
			.append("\n           ROW_NUMBER() OVER (PARTITION BY tran_num ORDER BY version_number ASC) AS num_row  ")
			.append("\n           FROM ab_tran_history   ")
			.append("\n           WHERE tran_status IN ( "+ iTranStatusNew +" )   ")
			.append("\n           AND update_type =  9 ")
			.append("\n       ) tran_history  ")
			.append("\n     WHERE num_row = 1 ")
			.append("\n   ) ab_tran_history on ab.tran_num = ab_tran_history.tran_num ")
			.append("\n    ")
			.append("\n   --Fees ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_sec_fee ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_sec_fee.TRAN_NUM ")
			.append("\n     AND tran_info_sec_fee.TYPE_ID="+ iTranInfoSecFee +") ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_per_share_fee ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_per_share_fee.TRAN_NUM ")
			.append("\n     AND tran_info_per_share_fee.TYPE_ID="+ iTranInfoPerShareFee +") ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_proceeds_fee ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_proceeds_fee.TRAN_NUM ")
			.append("\n     AND tran_info_proceeds_fee.TYPE_ID="+ iTranInfoProceedsFee +") ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_per_settlement_fee ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_per_settlement_fee.TRAN_NUM ")
			.append("\n     AND tran_info_per_settlement_fee.TYPE_ID="+ iTranInfoPerSettlementFee +") ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_tax_fee ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_tax_fee.TRAN_NUM ")
			.append("\n     AND tran_info_tax_fee.TYPE_ID="+ iTranInfoTaxFee +") ")
			.append("\n  ")
			.append("\n   LEFT JOIN ab_tran_info_view tran_info_precio_sucio_mp ")
			.append("\n     ON (ab.TRAN_NUM = tran_info_precio_sucio_mp.TRAN_NUM ")
			.append("\n     AND tran_info_precio_sucio_mp.TYPE_ID="+ iTranInfoPrecioSucioMP +") ")
			.append("\n  ")
			.append("\n   left join fx_tran_aux_data fx_aux ")
			.append("\n   on (ab.tran_num = fx_aux.tran_num) ")
			.append("\n  ")
			.append("\n   inner join portfolio ")
			.append("\n     on ab.internal_portfolio = portfolio.id_number ")
			.append("\n     and portfolio.portfolio_type = 0 ")
			.append("\n     and portfolio.restricted = 1 ")
			.append("\n  ")
			.append("\n WHERE 1=1 ")
			.append("\n   AND ab.toolset in ("+ iToolsetBond +","+ iToolsetFx +","+ iToolsetEqt +","+ iToolsetBondFut +","+ iToolsetFinFut +","+ iToolsetGenericFut +") --bond,fx,equity,bondfut,finfut,genericfut ")
			.append("\n   AND ab.tran_status in ("+ iTranStatusNew +","+ iTranStatusValidated +","+ iTranStatusCloseout +") --new,validated,closeout ")
			.append("\n   AND ab.ins_type not in ("+ iInsTypeEqtSiefore +") -- EQT-siefore ")
			.append("\n   AND ab.trade_date = '"+sTodayDefault+"'");

		// Setting data from query to table
			try {
				DBaseTable.execISql(tData, sQueryPrincipal.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load data into table tData: "+e.getMessage());
			}
					
		// Adding difference betweeen trade_date and settle_date
			tData.addCol("dias_movimiento", COL_TYPE_ENUM.COL_INT);
			int iIndexId = Ref.getValue(SHM_USR_TABLES_ENUM.INDEX_TABLE, "SPOT_UDI.MXN");
			
			for (int i = 1; i<=tData.getNumRows(); i++){
				
				int iTradeDate = tData.getInt("fecha_de_operacion", i);
				int iSettleDate = tData.getInt("fecha_de_liquidacion", i);			
				
				int iDiffBetweenDates = OCalendar.numGBDBetweenForIndex(iTradeDate,iSettleDate, iIndexId);
				
				if(iDiffBetweenDates>=8) iDiffBetweenDates=8; // Si la cantidad de dias es mayor a 8, informar 8
				
				int iToolset = tData.getInt("toolset", i);
				if (iToolset == iToolsetFx) iDiffBetweenDates=0; // Si el toolset es fx, diferencia de deias es de 0
				
				tData.setInt("dias_movimiento", i, iDiffBetweenDates);
			}
        
		// Loading user table that contains clave_movimiento info
			try {
				tUserMxClaveMovimiento = Table.tableNew();
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create table: "+e.getMessage());
			}
			StringBuilder sQueryUserMxClaveMovimiento = new StringBuilder();
			
			sQueryUserMxClaveMovimiento
			.append("\n SELECT  ")
			.append("\n  	clave_movimiento,  ")
			.append("\n  	concepto_movimiento,  ")
			.append("\n  	toolset, ")
			.append("\n  	buy_sell,  ")
			.append("\n  	dias_movimiento  ")
			.append("\n  FROM user_mx_claves_movimiento ");
			
			try {
				DBaseTable.execISql(tUserMxClaveMovimiento, sQueryUserMxClaveMovimiento.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load data into table user_mx_claves_movimiento: "+e.getMessage());
			}
			
		// Matching data table with clave_movimiento
			tData.select(tUserMxClaveMovimiento, "clave_movimiento",
					"toolset EQ $toolset "
					+ "AND buy_sell EQ $buy_sell "
					+ "AND dias_movimiento EQ $dias_movimiento");

	}


	private void setFeeFields() throws OException {
		
		int iInsType				=	0;
		int iToolset				=	0;
		int iTranNum				= 	0;
		
		String sPerSettlementFee	=	"";
		String sSecFee				=	"";
		String sPerShareFee			=	"";
		String sTaxFee				=	"";
		String sNetProceeds			=	"";
		String sProceeds			=	"";
		String sProceedsFee			=	"";
		String sSettleFx			=	"";
		String sSettleProceeds		=	"";
		String sCleanPrice			=	"";
		String sPrecioSucioMP		=	"";
		
		Double dPerSettlementFee	=	0.0;
		Double dSecFee				=	0.0;
		Double dPerShareFee			=	0.0;
		Double dTaxFee				=	0.0;
		Double dNetProceeds			=	0.0;
		Double dProceeds			=	0.0;
		Double dProceedsFee			=	0.0;
		Double dSettleFx			=	0.0;
		Double dSettleProceeds		=	0.0;
		Double dCleanPrice			=	0.0;
		Double dPrecioSucioMP		=	0.0;
		Double dPrice				=	0.0;
		Double dCAmount				=	0.0;

		Double dImporteDeComisiones	=	0.0;
		Double dIva					=	0.0;
		Double dImporteNeto			=	0.0;
		Double dPrecioUnitario		=	0.0;
		
		int iInsTypeBondFut = EnumsInstrumentsMX.MX_BOND_FUT.toInt();
		int iInsTypeEqtCommon = EnumsInstrumentsMX.MX_EQT_COMMON.toInt();
		int iInsTypeEqtBasket = EnumsInstrumentsMX.MX_EQT_BASKET.toInt();
		int iInsTypeEqtFibra = EnumsInstrumentsMX.MX_EQT_FIBRA.toInt();
		int iInsTypeEqtEtf = EnumsInstrumentsMX.MX_EQT_ETF.toInt();
		
		int iToolsetBondFut = TOOLSET_ENUM.BONDFUT_TOOLSET.toInt();
		int iToolsetFinFut = TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt();
		int iToolsetGenericFut = TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt();
		int iToolsetBond = TOOLSET_ENUM.BOND_TOOLSET.toInt();
		int iToolsetEquity = TOOLSET_ENUM.EQUITY_TOOLSET.toInt();
		int iToolsetFx = TOOLSET_ENUM.FX_TOOLSET.toInt();
		

		
		tData.addCol("importe_de_comisiones", COL_TYPE_ENUM.COL_DOUBLE);
		tData.addCol("iva", COL_TYPE_ENUM.COL_DOUBLE);
		tData.addCol("importe_neto", COL_TYPE_ENUM.COL_DOUBLE);
		tData.addCol("precio_unitario", COL_TYPE_ENUM.COL_DOUBLE);
	
		for (int iRow = 1; iRow<= tData.getNumRows(); iRow++){
						
			iInsType				=	0;
			iToolset				=	0;
			sPerSettlementFee		=	"";
			sSecFee					=	"";
			sPerShareFee			=	"";
			sTaxFee					=	"";
			sNetProceeds			=	"";
			sProceeds				=	"";
			sProceedsFee			=	"";
			sSettleFx				=	"";
			sSettleProceeds			=	"";
			sCleanPrice				=	"";
			sPrecioSucioMP			=	"";
			
			dPerSettlementFee		=	0.0;
			dSecFee					=	0.0;
			dPerShareFee			=	0.0;
			dTaxFee					=	0.0;
			dNetProceeds			=	0.0;
			dProceeds				=	0.0;
			dProceedsFee			=	0.0;
			dSettleFx				=	0.0;
			dSettleProceeds			=	0.0;
			dCleanPrice				=	0.0;
			dPrecioSucioMP			=	0.0;
			dPrice					=	0.0;
			dCAmount				= 	0.0;

			dImporteDeComisiones	=	0.0;
			dIva					=	0.0;
			dImporteNeto			=	0.0;
			dPrecioUnitario			=	0.0;
			
			iInsType = tData.getInt("ins_type", iRow);
			iToolset = tData.getInt("toolset", iRow);
			iTranNum = tData.getInt("tran_num", iRow);
			this.tranPointer = Transaction.retrieve(iTranNum);

			sPerSettlementFee = tData.getString("per_settlement_fee", iRow);
			sSecFee = tData.getString("sec_fee", iRow);
			sPerShareFee = tData.getString("per_share_fee", iRow);
			sTaxFee = tData.getString("tax_fee", iRow);
			sNetProceeds = tData.getString("net_proceeds", iRow);
			sProceeds = tData.getString("proceeds", iRow);
			sProceedsFee = tData.getString("proceeds_fee", iRow);
			sSettleFx = tData.getString("settle_fx", iRow);
			dCAmount = tData.getDouble("c_amt", iRow);
			sSettleProceeds = this.tranPointer.getField(TRANF_FIELD.TRANF_SETTLE_PROCEEDS.toInt());
			sSettleProceeds = sSettleProceeds==null? "0" :sSettleProceeds;
			
			sCleanPrice				=	tData.getString("clean_price", iRow);
			sPrecioSucioMP			=	tData.getString("precio_sucio_mp", iRow);
			dPrice					=	tData.getDouble("price", iRow);
			
			dPerSettlementFee	=	Str.strToDouble(sPerSettlementFee);
			dSecFee				=	Str.strToDouble(sSecFee);
			dPerShareFee		=	Str.strToDouble(sPerShareFee);
			dTaxFee				=	Str.strToDouble(sTaxFee);
			dNetProceeds		=	Str.strToDouble(sNetProceeds);
			dProceeds			=	Str.strToDouble(sProceeds);
			dProceedsFee		=	Str.strToDouble(sProceedsFee);
			dSettleFx			=	Str.strToDouble(sSettleFx);
			dSettleProceeds		=	Str.strToDouble(sSettleProceeds,1);
			dCleanPrice			=	Str.strToDouble(sCleanPrice);
			dPrecioSucioMP		=	Str.strToDouble(sPrecioSucioMP);
			
			//Calculating Precio Unitario
				if (iToolset == iToolsetBond){
					dPrecioUnitario = dCleanPrice*dSettleFx;
				} else if (iToolset == iToolsetEquity){
					dPrecioUnitario = dPrice;
				} else {
					if(iInsType==iInsTypeBondFut)
						dPrecioUnitario = dPrice*100;
					else dPrecioUnitario = dPrice;
				}
			
			// Calculating importe de comisiones 
				if(iInsType == iInsTypeEqtCommon ||
					iInsType == iInsTypeEqtBasket ||
					iInsType == iInsTypeEqtFibra){
					
					dImporteDeComisiones = dPerSettlementFee;
					
				} else if (iInsType == iInsTypeEqtEtf){
					dImporteDeComisiones = dPerSettlementFee + dSecFee + dPerShareFee;
				} else {
					dImporteDeComisiones = 0.0;
				}
	
			// Calculating iva
				if(iInsType == iInsTypeEqtCommon ||
					iInsType == iInsTypeEqtBasket ||
					iInsType == iInsTypeEqtFibra){
					
					dIva = dTaxFee;
				} else{
					dIva = 0.0;
				}
				
			// Calculating importe neto
				if(iToolset == iToolsetBondFut ||
					iToolset == iToolsetFinFut ||
					iToolset == iToolsetGenericFut){
					
					dImporteNeto = dProceeds;
				} else if (iToolset == iToolsetBond){
					dImporteNeto = dSettleProceeds;
				} else if (iToolset == iToolsetEquity){
					dImporteNeto = dProceedsFee;
					if (dImporteNeto == 0.00)
						dImporteNeto = dSettleProceeds;// [1.1]CS-26012018
				}  else if (iToolset == iToolsetFx){
					dImporteNeto = dCAmount; // [1.2] BM-30012018
				}
			tData.setDouble("importe_de_comisiones", iRow, dImporteDeComisiones);
			tData.setDouble("iva", iRow, dIva);
			tData.setDouble("importe_neto", iRow, dImporteNeto);
			tData.setDouble("precio_unitario", iRow, dPrecioUnitario);
						
		}

	}

	private void formatData() {
		try {
			tData.setColFormatAsDouble("importe_de_comisiones", 10, 2);
			tData.setColFormatAsDouble("iva", 10, 2);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set double format on tData table: "+e.getMessage());
		}
		
	}

	private void generateReportsAndSendMails() throws OException {
						
		// Select distinct portfolios from query
			Table tDistinctPortfolio = Table.tableNew("Distinct Portfolio");
			tDistinctPortfolio.select(tData, "DISTINCT, portfolio", "portfolio GT 0");
			
		//Generate one report for each portfolio
			for (int i=1; i<=tDistinctPortfolio.getNumRows();i++){
				String sPortfolio = tDistinctPortfolio.getString("portfolio", i);
				// Generate reports
				generateReports(sPortfolio);
			}
			
		//Generate string with output filenames separated by ;
			for (String sFile : sFullFileNames){
				sListOfFiles += sFile + ";";
			}
			
		// Send mails
			if(!sFullFileNames.isEmpty()){
				sendMails();				
			} else {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to send mails: not a single file was generated");
			}
		
	}




	private void generateReports(String sPortfolio) throws OException {
			
			//Getting data from tData global table
				Table tOutput = Table.tableNew("Output Table");
				
				tOutput.select(tData,
						"deal, "
						+ "tipo_valor, "
						+ "emisora, "
						+ "serie, "
						+ "fecha_de_operacion, "
						+ "fecha_de_liquidacion, "
						+ "clave_movimiento, "
						+ "clave_del_proveedor, "
						+ "clave_consar, "
						+ "plazo_del_reporto, "
						+ "titulos, "
						+ "tasa_de_rendimiento, "
						+ "precio_unitario, "
						+ "tipo_de_cambio, "				
						+ "importe_de_comisiones, "
						+ "clave_de_pizarra, "
						+ "importe_comision_broker, "
						+ "medio_de_concertacion, "
						+ "sistema_de_concertacion, "
						+ "folio_posicion, "
						+ "tipo_de_postura, "
						+ "banco_de_trabajo, "
						+ "clase_de_activo, "
						+ "divisa_de_liquidacion, "
						+ "mercado_primario, "
						+ "hora_de_concertacion, "
						+ "iva, "
						+ "importe_neto, "
						+ "iva_com_broker ",
						"deal GT 0 AND portfolio EQ "+sPortfolio
					);
		
		// Configure paths and filenames
			String sTemplatePath = Crystal.getRptDir();
			String sTemplateName = "ReporteCharolaGAF.rpt";
			String sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
		
			String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Charola_GAF", "path");
			String sFileName = "CHAROLA_GAF_"+sPortfolio+"_"+sTodayAAAAMMDD;
			String sFullFileNameXLS = sFilePath+sFileName+".xls";
			
		// Add the full file path to arraylist with full file output names(used latter to send mails)
			sFullFileNames.add(sFullFileNameXLS);
			
		//Exporting excel
			try {
				Crystal.tableExportCrystalReport(tOutput, 
						sFullTemplateName, 
						CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
						sFullFileNameXLS, 
						CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
						);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to export Excel document. "+e.getMessage());
			}
			
		//Destroying tOutput Table
			tOutput.destroy();
	}

	private void sendMails() throws OException {

		// Declare mail variables
			String sMails		=	"";
			String sSubject		=	"";
			String sMessage		=	"";
			String sBottomImage	=	"";
			boolean mailResult	=	false;
				
		// Setting mail content from user_configurable_variables
			try {
				
				sMails			=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Charola_GAF", "mails");
				sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Charola_GAF", "subject");
				sSubject		=	sSubject.replace("$fecha", sTodaySlashDDMMAAAA);
				sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Charola_GAF", "body");
				sMessage		=	sMessage.replace("$fecha", sTodaySlashDDMMAAAA);
				sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
			
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to load variables for send mail, "+e.getMessage());
			}

		// Sending Mails
			mailResult = UtilAfore.sendMail(
					sMails,
					sSubject,
					sMessage,
					sBottomImage,
					sListOfFiles
					);
			
			if (!mailResult){
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to send mail.");
			}		
	}
}
