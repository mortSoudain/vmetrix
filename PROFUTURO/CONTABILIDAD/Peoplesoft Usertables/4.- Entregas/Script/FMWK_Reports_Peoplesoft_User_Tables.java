/*$Header: v 1.0, 15/Nov/2017 $*/
/***********************************************************************************
 * File Name:              FMWK_Reports_Peoplesoft_User_Tables.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Noviembre 2017
 * Version:					1.0
 * Description:				Script diseñado para la generacion de usertables en el sistema,
 * 							para el uso de Peoplesoft.
 * 							
 * 							Genera tablas de usuario para los siguientes productos:
 * 								- Socios Liquidadores Egreso
 * 								- Socios Liquidadores Ingreso
 * 								- Comision por Salgo Egreso
 * 								- Comision por Saldo Ingreso
 * 								- Compra de Divisas Egreso
 * 								- Venta de Divisas Ingreso
 * 								- Movimiento Chequeras Egreso
 * 								- Movimiento Chequeras Ingreso
 *                       
 * REVISION HISTORY
 * Date:					Febrero 2018                   
 * Version/Autor:          	1.1 - Basthian Matthews Sanhueza - Vmetrix SpA
 * Description:            	Incluidas tablas de:
 *  						- Llamadas de Capital Egreso
 *							- Compra de Acciones Egreso/Ingreso
 *							- Venta de Acciones Egreso/Ingreso
 *                         
 ************************************************************************************/
package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCountry;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsParty;
import com.afore.enums.EnumsPartyInfoFields;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.enums.EnumsUserCflowType;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.BUY_SELL_ENUM;
import com.olf.openjvs.enums.CFLOW_TYPE;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Reports_Peoplesoft_User_Tables implements IScript {
	
	//Declare utils
		protected String sScriptName	=	this.getClass().getSimpleName();
		protected UTIL_Log _Log			=	new UTIL_Log(sScriptName);
	
	//Declare memory tables to use
		private Table tMasterUserTables = Util.NULL_TABLE;
		
	@Override
	public void execute(IContainerContext context) throws OException {
		
		_Log.markStartScript();
		
			//Initialize Master User Table
				initializeMasterUserTable();
			
			//Setting query strings
				setQuerysValues();
				
			//Set query return to memory user table
				setUserTablesQueryValues();
				
			//Adding last values to tables (visual rate)
				setUserTablesVisualRate();
				
			//Format user tables (format references)
				setColsFormat();
					
			//TODO:Debug
				//tMasterUserTables.viewTable();
								
			//Push memory user tables to DB
				pushUserTablesToDB();
				
			//Clean memory tables
				cleanMemory();
			
		_Log.markEndScript();
	}

	private void setColsFormat() throws OException {
		
		// Go through all user tables 
		for(int i=1;i<=tMasterUserTables.getNumRows();i++){
			Table iTable = tMasterUserTables.getTable("UserTable", i);
			
			//Get all col names
			for(int j=1;j<=iTable.getNumCols();j++){
				String sColName = iTable.getColName(j);
				
				//If the col name is one of the above, set reference name instead of key id
				switch(sColName){
					case "pf_status_cif"	:	iTable.setColFormatAsRef("pf_status_cif", SHM_USR_TABLES_ENUM.TRANS_STATUS_TABLE);break;
					case "pf_nom_ben"		:	iTable.setColFormatAsRef("pf_nom_ben", SHM_USR_TABLES_ENUM.PARTY_TABLE); break;
					case "currency_cd"		:	iTable.setColFormatAsRef("currency_cd", SHM_USR_TABLES_ENUM.CURRENCY_TABLE); break;
					case "cust_id"			:	iTable.setColFormatAsRef("cust_id", SHM_USR_TABLES_ENUM.PARTY_TABLE); break;
					default: break;
				}
			}
		}
	}

	private void initializeMasterUserTable() {
		
		//Initialize the master table that will contain all the other user tables 
		try {
			tMasterUserTables = Table.tableNew("MasterUserTable");
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at initializeMasterUserTable(), failed to create Master User Table, "
					+ e.getMessage());
		} 
		//Set the cols of the master user table
		finally{
			try {
				tMasterUserTables.addCol("Name", COL_TYPE_ENUM.COL_STRING);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at initializeMasterUserTable(), unable to create Name col on Master User Table, "
						+ e.getMessage());
			}
			try {
				tMasterUserTables.addCol("UserTableName", COL_TYPE_ENUM.COL_STRING);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at initializeMasterUserTable(), unable to create UserTableName col on Master User Table, "
						+ e.getMessage());
			}
			try {
				tMasterUserTables.addCol("UserTableQuery", COL_TYPE_ENUM.COL_STRING);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at initializeMasterUserTable(), unable to create UserTableQuery col on Master User Table, "
						+ e.getMessage());
			}
			try {
				tMasterUserTables.addCol("UserTable", COL_TYPE_ENUM.COL_TABLE);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at initializeMasterUserTable(), unable to create UserTable col on Master User Table, "
						+ e.getMessage());
			}
		}		
	}
	
	//Clean all the user tables in memory
	private void cleanMemory() throws OException {
		tMasterUserTables.destroy();	
	}
	
	
	private void setUserTablesVisualRate() {
		
		Table tVectorPreciosHist = Util.NULL_TABLE;
		
		try {

			//Get the user table with historical prices
			String sQueryUserVectorPreciosHist	= "SELECT * FROM " + EnumsUserTables.USER_MX_FIX_RATES.toString();
			tVectorPreciosHist = Table.tableNew();
			DBaseTable.execISql(tVectorPreciosHist, sQueryUserVectorPreciosHist);
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at setUserTablesVisualRate(), unable to create or populate table tVectorPreciosHist, "
					+ e.getMessage());
		}
		
		try {
			
			//Convert the datetime fecha col, to julian int date col, used for later comparison 
			tVectorPreciosHist.colConvertDateTimeToInt("fecha");
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at setUserTablesVisualRate(), unable to convert column Fecha(Datetime) to Int(Julian Date), "
					+ e.getMessage());
		}	
		

		try {
			
			//Adding visual_rate to tables that matches names below
			//Field must be calculated outside query because of last business day field needed
			for(int iRow =1; iRow<= tMasterUserTables.getNumRows();iRow++){
				
				String sTableName = tMasterUserTables.getString("UserTableName", iRow);
				
				if(sTableName.equals(EnumsUserTables.USER_MX_SLE_PF_SADFI_DETCIF.toString())||
						sTableName.equals(EnumsUserTables.USER_MX_SLI_PF_SADFI_ING.toString())||
						sTableName.equals(EnumsUserTables.USER_MX_MCE_PF_SADFI_DETCIF.toString())){
					
					addVisualRateToTable(tMasterUserTables.getTable("UserTable", iRow), tVectorPreciosHist);
					
				}
			}
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"OException at setUserTablesVisualRate(), failed to add visual_rate on one of the tables, "
					+ e.getMessage());
		}	
	
	}
	
	private void addVisualRateToTable(Table tTable, Table tVectorPreciosHist) throws OException {
		
		//Add col visual_rate to the given table
		tTable.addCol("visual_rate", COL_TYPE_ENUM.COL_DOUBLE);
		
		//Go through all deals on table
		for(int i=1;i<=tTable.getNumRows();i++){
			
			//Get currency name and datetime(getting last good business date)
			String sCurrency = Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, tTable.getInt("currency", i));
			int iDateTime = OCalendar.getLgbd(tTable.getInt("trade_date", i));
			
			if(sCurrency.equals("MXN")){
				tTable.setDouble("visual_rate", i, 1);
			}
			else {
				//If the currency is USD, change to FIX to perform search
				if(sCurrency.equals("USD")) sCurrency="FIX";
				
				//Auxiliar table used to get ticker and precio_sucio from the deal date
				Table tVectorPreciosHistSelectedDate = Table.tableNew();
				tVectorPreciosHistSelectedDate.select(tVectorPreciosHist, "ticker, fix_value", "fecha EQ "+iDateTime);
				
				// Go through auxiliar table, and get the one with ticker ending with currency name
				for(int j=1;j<=tVectorPreciosHistSelectedDate.getNumRows();j++){
					String sTicker = tVectorPreciosHistSelectedDate.getString("ticker", j);

					if(sTicker.startsWith("*C_") && sTicker.endsWith(sCurrency)){
						tTable.setDouble("visual_rate", i, tVectorPreciosHistSelectedDate.getDouble("fix_value", j));
					}
				}
				tVectorPreciosHistSelectedDate.destroy();
			}
		}
		
		// Delete cols added on querys used for calculate visual_rate
		tTable.delCol("currency");
		tTable.delCol("trade_date");

	}

	private void pushUserTablesToDB() throws OException{
		//Push all the user tables to the DB
		for(int iRow =1; iRow<= tMasterUserTables.getNumRows();iRow++){
			pushUserTableToDB(tMasterUserTables.getTable("UserTable", iRow));
		}
	}

	private void pushUserTableToDB(Table table) throws OException {
		
		//If Usertable exists on DB
			if (DBUserTable.userTableIsValid(table.getTableName()) == 1){					
				//Clean data (just for precaution because table was just created so no data should be there)
				try {
					DBUserTable.clear(table);
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"OException at pushUserTablesToDB(), failed to clear the table, "
							+ e.getMessage());
				}
		    } else {
		    	
		    	//Create the structure of a table on db from a memory table 
				try {
					DBUserTable.create(table);
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"OException at pushUserTablesToDB(), failed to create the structure of usertable on DB, "
							+ e.getMessage());
				}	
		    }
						
		//Push the memory table(with loaded info) to the user table with same name
			try {
				DBUserTable.bcpIn(table);
			} catch (OException e) {

				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at pushUserTablesToDB(), failed to push usertable, "
						+ e.getMessage());
			}
	}

	private void setUserTablesQueryValues() throws OException {
		
		//Initialize tables with usertable correlated name
		for(int iRow =1; iRow<= tMasterUserTables.getNumRows();iRow++){
			try{
				tMasterUserTables.setTable("UserTable", iRow, Table.tableNew(tMasterUserTables.getString("UserTableName", iRow)));
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at setUserTablesQueryValues(), failed to create memory table"+tMasterUserTables.getString("UserTable", iRow)+", "
						+ e.getMessage());
			}		
		}
		
		//Set return values from string querys to memory table
		for(int iRow =1; iRow<= tMasterUserTables.getNumRows();iRow++){
			try{
				DBaseTable.execISql(tMasterUserTables.getTable("UserTable", iRow),	tMasterUserTables.getString("UserTableQuery", iRow));				
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at setUserTablesQueryValues(), failed to populate memory table"+tMasterUserTables.getString("UserTable", iRow)+", "
						+ e.getMessage());
			}		
		}
	}

	private void setQuerysValues() throws OException {
		
			int iToday 							=	OCalendar.today();
			String sToday						=	OCalendar.formatJd(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);//dd-mm-yyyy
			
			//String sCatalogo8UserTable			=	EnumsUserTables.USER_MX_CATALOGO8.toString();// user_mx_catalogo8
			String sCatalogo13UserTable			=	EnumsUserTables.USER_MX_CATALOGO13.toString();// user_mx_catalogo13
			String sCatalogo14_1UserTable		=	EnumsUserTables.USER_MX_CATALOGO14_1.toString();// user_mx_catalogo14_1
			String sCatalogo14_2UserTable		=	EnumsUserTables.USER_MX_CATALOGO14_2.toString();// user_mx_catalogo14_2
			String sCuentasBancarias			=	EnumsUserTables.USER_MX_CUENTAS_BANCARIAS.toString();// user_mx_cuentas_bancarias
			
			int iCountryMexico					=	EnumsCountry.MX_COUNTRY_MEXICO.toInt();//20040
			int iCurrencyMXP					=	EnumsCurrency.MX_CURRENCY_MXN.toInt();//70
			
			int iInsTypeDepoChequeras			=	EnumsInstrumentsMX.MX_DEPO_CHEQUERA.toInt();//1000056
			int iInsTypeEqtCkdKcall				=	EnumsInstrumentsMX.MX_EQT_CKDKCALL.toInt();//1000043
			
			int iPartyBbvaBancomer				=	EnumsParty.MX_BBVA_BANCOMER_BU.toInt();//20045
			int iPartyBbvaDer					=	EnumsParty.MX_BBVA_DER_BU.toInt();//20720; 			
			int iPartyGoldmanSach				=	EnumsParty.MX_GOLDMAN_SACH_BU.toInt();//20057;
			int iPartyGS						=	EnumsParty.MX_GL_SL_BU.toInt(); //20728;
			int iPartyHsbcFutures				=	EnumsParty.MX_HSBC_FUTURES_BU.toInt();//20061;
			int iPartyScotiabank				=	EnumsParty.MX_SCOTIA_BU.toInt();//20080;
			int iPartyBanamex					=	EnumsParty.MX_BANAMEX_BU.toInt(); //20037;
			int iPartyHsbc						=	EnumsParty.MX_HSBC_BU.toInt(); //20406;
			int iPartySindeval					=	EnumsParty.MX_SDINDEVAL_BU.toInt();//20363;
			String sPartyBbvaBancomer			=	Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iPartyBbvaBancomer);//BBVA BANCOMER - BU
			
			String sTranTipoCompraVenta			=	EnumsTranInfoFields.MX_TRAN_TIPO_COMPRA_VENTA.toString();//Trad Tran Tipo Compra Venta
		
			int iTranTypeTrading				=	TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();//0
			int iTranStatusNew					=	TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt();//2
			int iTranStatusValidated			=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
			
			int iToolsetCash					=	TOOLSET_ENUM.CASH_TOOLSET.toInt();//10
			int iToolsetFx						=	TOOLSET_ENUM.FX_TOOLSET.toInt();//9
			int iToolsetLoanDep					=	TOOLSET_ENUM.LOANDEP_TOOLSET.toInt();//6
			int iToolsetEquity					=	TOOLSET_ENUM.EQUITY_TOOLSET.toInt();//28
			
			int iCflowTypeAims					=	EnumsUserCflowType.MX_CFLOW_TYPE_AIMS.toInt(); //2018
			int iCflowTypeMarginCallAims		=	EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt(); //271
			int iCflowTypeComisionPorSaldo		=	EnumsUserCflowType.MX_CFLOW_TYPE_COMISION_SALDO.toInt(); //2017
			int iCflowTypeFinalPrincipal		=	CFLOW_TYPE.FINAL_PRINCIPAL_CFLOW.toInt();//26
			int iCflowTypeAccionesAforeA2		=	EnumsUserCflowType.MX_CFLOW_TYPE_MOV_ACCIONES_AFORE_A2.toInt();//2030
			
			int iBuySell_buy					=	BUY_SELL_ENUM.BUY.toInt();//0
			int iBuySell_sell					=	BUY_SELL_ENUM.SELL.toInt();//1
			
			String sPartyInfoViewIdConsar		=	EnumsPartyInfoFields.MX_PARTY_ID_CONSAR.toString();//Id_Consar
			String sTranInfoViewHoraConcertacion=	EnumsTranInfoFields.MX_TRAN_HORA_CONCERTACION.toString();//Trad Tran Hora de Concertacion

			int iRow = 0;
			
			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Socios Liquidadores Egreso Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_SLE_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	'SAD' AS origin, "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS pf_tran_id, "+
							"\n 	ab.tran_status AS pf_status_cif, "+
							"\n 	ab.external_bunit AS pf_nom_ben, "+
							"\n 	CASE WHEN(pebunit_country.country = "+ iCountryMexico +") "+
							"\n 		THEN peiv.value "+
							"\n 		ELSE 'N/A' "+
							"\n 	END AS bank_cd, "+
							"\n 	CASE WHEN(pebunit_country.country = "+ iCountryMexico +") "+
							"\n 		THEN CASE WHEN(party_external.party_id = "+ iPartyBbvaDer +") -- PARTY EXTERNAL BBVA-DER "+
							"\n 				THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=17) "+
							"\n 						THEN SUBSTR(pibunit_account_number.account_number,1,17) "+
							"\n 						ELSE pibunit_account_number.account_number "+
							"\n 					END "+
							"\n 				ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18) "+
							"\n 						THEN SUBSTR(pebunit_account_number.account_number,1,17) "+
							"\n 						ELSE pebunit_account_number.account_number "+
							"\n 					END "+
							"\n 			END "+
							"\n 		ELSE '00000000000000000' "+
							"\n 	END AS bank_account_num, "+
							"\n 	CASE WHEN(pebunit_country.country = "+ iCountryMexico +") "+
							"\n 		THEN CASE WHEN(party_external.party_id = "+ iPartyBbvaDer +") -- PARTY EXTERNAL BBVA-DER "+
							"\n 			THEN SUBSTR(pibunit_account_number.account_number,18,18) "+
							"\n 			ELSE SUBSTR(pebunit_account_number.account_number,18,18) "+
							"\n 			END "+
							"\n 		ELSE '0' "+
							"\n 	END AS check_digit, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
							"\n 	ab.position AS pymnt_amt, "+
							"\n 	ab.currency AS currency_cd, "+
							"\n 	CASE WHEN(ab.currency = "+ iCurrencyMXP +")  "+
							"\n 		THEN '40' "+
							"\n 		ELSE 'N/A' "+
							"\n 	END AS pf_tipo_cuenta, "+
							"\n 	CASE WHEN(ab.currency = "+ iCurrencyMXP +") "+
							"\n 		THEN "+
							"\n 			CASE WHEN(party_external.party_id = "+ iPartyBbvaDer +") "+
							"\n 				THEN '15' "+
							"\n 				ELSE '07' "+
							"\n 			END "+
							"\n 		ELSE '13' "+
							"\n 	END AS pfcodtipopago, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt, "+
							"\n 	tran_info_hora.value AS op_start_time, "+
							"\n 	ab.internal_contact AS oprid_entered_by, "+
							"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
							"\n 	'SL' AS id_tipo_instru, "+
							"\n 	ab_tran_reviewed_by.personnel_id AS oprid_reviewed_by, "+
							"\n 	CASE  "+
							"\n 		WHEN(party_external.party_id IN ("+ iPartyGoldmanSach +","+ iPartyGS +")) THEN 'MTPRE' "+
							"\n 		WHEN(party_external.party_id = "+ iPartyHsbcFutures +") THEN 'MT103' "+
							"\n 		ELSE '' "+
							"\n 	END AS pf_dummy_field1 "+
							"\n FROM ab_tran ab "+
							"\n 	INNER JOIN party party_external "+
							"\n 		ON (ab.external_bunit = party_external.party_id) "+
							"\n 	LEFT JOIN party_info_view peiv "+
							"\n 		ON (ab.external_bunit = peiv.party_id "+
							"\n 			AND peiv.type_name = '"+ sPartyInfoViewIdConsar +"')  "+
							"\n 	LEFT JOIN party_address pebunit_country "+
							"\n 		ON (ab.external_bunit = pebunit_country.party_id)  "+
							"\n 	LEFT JOIN party_account pebunit_account "+
							"\n 		ON (ab.external_bunit = pebunit_account.party_id) "+
							"\n 	LEFT JOIN account pebunit_account_number "+
							"\n 		ON (pebunit_account.account_id = pebunit_account_number.account_id "+
							"\n 			AND pebunit_account_number.account_status = 1) "+
							"\n  "+
							"\n 	LEFT JOIN ab_tran_settle_view absettlev "+
							"\n 		ON (ab.deal_tracking_num = absettlev.deal_tracking_num "+
							"\n 			AND absettlev.int_account_id!=0 "+
							"\n 			AND absettlev.TRAN_STATUS="+ iTranStatusValidated +") "+
							"\n  "+
							"\n 	LEFT JOIN account pibunit_account_number "+
							"\n 		ON (pibunit_account_number.account_id = absettlev.int_account_id) "+
							"\n  "+
							"\n 	LEFT JOIN ab_tran_info_view tran_info_hora "+
							"\n 		ON (ab.tran_num = tran_info_hora.tran_num   "+
							"\n 			AND tran_info_hora.type_name = '"+ sTranInfoViewHoraConcertacion +"') "+
							"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
							"\n 				FROM ab_tran_history "+
							"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
							"\n 				            FROM ab_tran_history "+
							"\n 				            WHERE TRAN_STATUS= "+ iTranStatusValidated +" "+
							"\n 				            GROUP BY TRAN_NUM) AUX_TABLE  "+
							"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
							"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
							"\n 				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
							"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
							"\n 				FROM ab_tran_history "+
							"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
							"\n 				            FROM ab_tran_history "+
							"\n 				            WHERE TRAN_STATUS= "+ iTranStatusNew +" "+
							"\n 				            GROUP BY TRAN_NUM) AUX_TABLE  "+
							"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
							"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
							"\n 				ab_tran_reviewed_by ON AB.TRAN_NUM = ab_tran_reviewed_by.TRAN_NUM "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetCash +" -- cash "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND (ab.cflow_type = "+ iCflowTypeAims +" "+
							"\n 		OR ab.cflow_type = "+ iCflowTypeMarginCallAims +") -- AIMS o Margin Call AIMS "+
							"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
							"\n 	AND ab.tran_status = "+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);

			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Socios Liquidadores Egreso Detcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_SLE_PF_SADFI_DETCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT  "+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	'1' AS seqno,"+
			"\n 	CASE WHEN(pebunit_country.country = "+iCountryMexico+")"+
			"\n 		THEN 'DSLMXP'"+
			"\n 		ELSE 'DSLEXT'"+
			"\n 	END AS pf_tipo_afectacion,"+
			"\n 	tran_settle_view.settle_amount AS amt_sel,"+
			"\n 	'100' AS percentage,"+
			"\n 	ab.currency AS currency, -- para sacar visual rate"+
			"\n 	ab.trade_date as trade_date -- para sacar visual rate"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN party_address pebunit_country"+
			"\n 		ON (ab.external_bunit = pebunit_country.party_id) "+
			"\n 	LEFT JOIN ab_tran_settle_view tran_settle_view"+
			"\n 		ON (ab.tran_num = tran_settle_view.tran_num) "+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetCash +" -- cash"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND (ab.cflow_type = "+ iCflowTypeAims +""+
			"\n 		OR ab.cflow_type = "+ iCflowTypeMarginCallAims +") -- AIMS o Margin Call AIMS"+
			"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);

			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Socios Liquidadores Ingreso");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_SLI_PF_SADFI_ING.toString());
			tMasterUserTables.setString("UserTableQuery", iRow,
					"\n SELECT "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS item, "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	ab.position AS orig_item_amt, "+
							"\n 	ab.external_bunit AS cust_id, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt, "+
							"\n 	'CONT' AS pymnt_terms_cd, "+
							"\n 	ab.currency AS currency_cd, "+
							"\n 	external_party.short_name AS bank_cd, "+
							"\n 	CASE WHEN(ab.currency = "+ iCurrencyMXP +") THEN account_num_bancomer.bank_account_num "+
							"\n 		WHEN(ab.currency = 0) THEN account_num_statestreet.bank_account_num "+
							"\n 		ELSE '00000000000000000' "+
							"\n 	END AS bank_account_num, "+
							"\n 	'T005' AS pf_id_proceso, "+
							"\n 	'0' AS secno, "+
							"\n 	'N' AS status, "+
							"\n 	'SADFI' AS origin_id, "+
							"\n 	ab.currency AS currency, -- para visual rate "+
							"\n 	ab.trade_date AS trade_date --para visual rate "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN party internal_party "+
							"\n 		ON (ab.internal_bunit = internal_party.party_id) "+
							"\n 	LEFT JOIN party external_party "+
							"\n 		ON (ab.external_bunit = external_party.party_id) "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN "+sCatalogo13UserTable +" account_num_bancomer "+
							"\n 		ON (internal_party.short_name = account_num_bancomer.internal_bunit_name "+
							"\n 			AND account_num_bancomer.external_bunit_name = '"+ sPartyBbvaBancomer +"') "+
							"\n 	LEFT JOIN "+sCatalogo13UserTable +" account_num_statestreet "+
							"\n 		ON (internal_party.short_name = account_num_statestreet.internal_bunit_name "+
							"\n 			AND account_num_statestreet.external_bunit_name = 'STATESTREET - BU') "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetCash +" -- cash "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND (ab.cflow_type = "+ iCflowTypeAims +" "+
							"\n 		OR ab.cflow_type = "+ iCflowTypeMarginCallAims +") -- AIMS o Margin Call AIMS "+
							"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
							"\n 	AND ab.tran_status="+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Comision Por Saldo Egreso Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CSE_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	ab.tran_status AS pf_status_cif,"+
			"\n 	'PROFUTURO AFORE SA DE CV BU' AS pf_nom_ben,"+
			"\n 	'40012' AS bank_cd,"+
			"\n 	'01218000102979764' AS bank_account_num,"+
			"\n 	'1' AS check_digit,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,"+
			"\n 	'CXSAFORE' AS remit_vendor,"+
			"\n 	amount.monto_comision AS pymnt_amt,"+
			"\n 	ab.currency AS currency_cd,"+
			"\n 	CASE WHEN(ab.currency = "+iCurrencyMXP+") "+
			"\n 		THEN '40'"+
			"\n 		ELSE 'N/A'"+
			"\n 	END AS pf_tipo_cuenta,"+
			"\n 	'21' AS pfcodtipopago,"+
			"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,"+
			"\n 	tran_info_hora.value AS op_start_time,"+
			"\n 	ab.internal_contact AS oprid_entered_by,"+
			"\n 	ab_tran_approved_by.personnel_id AS op_approved_by"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN ab_tran_info_view tran_info_hora"+
			"\n 		ON (ab.tran_num = tran_info_hora.tran_num  "+
			"\n 			AND tran_info_hora.type_name = '"+ sTranInfoViewHoraConcertacion +"')"+
			"\n 	LEFT JOIN (	SELECT ab_tran_history.*"+
			"\n 				FROM ab_tran_history"+
			"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN"+
			"\n 				            FROM ab_tran_history"+
			"\n 				            WHERE TRAN_STATUS= "+iTranStatusValidated+""+
			"\n 				            GROUP BY TRAN_NUM) AUX_TABLE "+
			"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM "+
			"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)"+
			"\n 				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM"+
			"\n 	LEFT JOIN user_mx_comision_saldo_dia amount"+
			"\n 		ON (portfolio.name = amount.portfolio)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetCash +" -- cash"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.cflow_type = "+ iCflowTypeComisionPorSaldo +" -- comision por saldo"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Comision Por Saldo Egreso Detcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CSE_PF_SADFI_DETCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	'1' AS seqno,"+
			"\n 	'COMIS' AS pf_tipo_afectacion,"+
			"\n 	amount.monto_comision AS amt_sel,"+
			"\n 	'100' AS percentage,"+
			"\n 	'1' AS visual_rate"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN user_mx_comision_saldo_dia amount"+
			"\n 		ON (portfolio.name = amount.portfolio)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetCash +" -- cash"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.cflow_type = "+ iCflowTypeComisionPorSaldo +" -- comision por saldo"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Comision Por Saldo Ingreso");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CSI_PF_SADFI_ING.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS item,"+
			"\n 	'Profuturo AFORE SA de CV - BU' AS business_unit,"+
			"\n 	comision_saldo_dia.var AS orig_item_amt,"+
			"\n 	ab.internal_bunit AS cust_id,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,"+
			"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,"+
			"\n 	'CONT' AS pymnt_terms_cd,"+
			"\n 	ab.currency AS currency_cd,"+
			"\n 	'"+sPartyBbvaBancomer+"' AS bank_cd,"+
			"\n 	'764C' AS bank_account_num,"+
			"\n 	id_proceso.pf_id_proceso AS pf_id_proceso,"+
			"\n 	comision_saldo_dia.seqno AS seqno,"+
			"\n 	'N' AS status,"+
			"\n 	'SADFI' AS origin_id,"+
			"\n 	'1' AS viual_rate"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN party internal_party"+
			"\n 		ON (ab.internal_bunit = internal_party.party_id)"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN ("+
			"\n 			SELECT portfolio, var, descripcion, seqno"+
			"\n 			FROM ("+
			"\n 				SELECT portfolio, var_trabajadores var,'Trabajadores' Descripcion, '0' seqno"+
			"\n 				FROM user_mx_comision_saldo_dia"+
			"\n 				UNION"+
			"\n 				SELECT portfolio, var_afore var, 'Afore' Descripcion, '1' seqno"+
			"\n 				FROM user_mx_comision_saldo_dia"+
			"\n 			)"+
			"\n 		) comision_saldo_dia"+
			"\n 		ON portfolio.name = comision_saldo_dia.portfolio"+
			"\n 	LEFT JOIN "+ sCatalogo14_1UserTable +" id_proceso"+
			"\n 		ON (id_proceso.internal_bunit = internal_party.short_name"+
			"\n 			AND id_proceso.descripcion = comision_saldo_dia.descripcion)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetCash +" -- cash"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.cflow_type = "+ iCflowTypeComisionPorSaldo +" -- comision por saldo"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Compra de Divisas Egreso Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CDE_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	'SAD' AS origin, "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS pf_tran_id, "+
							"\n 	ab.tran_status AS pf_status_cif, "+
							"\n 	ab.external_bunit AS pf_nom_ben, "+
							"\n 	peiv.value AS bank_cd, "+
							"\n 	CASE WHEN(party_external.party_id = "+ iPartyBbvaBancomer +") -- PARTY EXTERNAL BANCOMER "+
							"\n 			THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=18) "+
							"\n 					THEN SUBSTR(pibunit_account_number.account_number,1,17) "+
							"\n 					ELSE pibunit_account_number.account_number "+
							"\n 				END "+
							"\n 			ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18) "+
							"\n 					THEN SUBSTR(pebunit_account_number.account_number,1,17) "+
							"\n 					ELSE pebunit_account_number.account_number "+
							"\n 				END "+
							"\n 		END AS bank_account_num, "+
							"\n  "+
							"\n 	CASE WHEN(party_external.party_id = "+ iPartyBbvaBancomer +") -- PARTY EXTERNAL BANCOMER "+
							"\n 		THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=18) "+
							"\n 				THEN SUBSTR(pibunit_account_number.account_number,18,18) "+
							"\n 				ELSE '0' "+
							"\n 			END "+
							"\n 		ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18) "+
							"\n 				THEN SUBSTR(pebunit_account_number.account_number,18,18) "+
							"\n 				ELSE '0' "+
							"\n 			END "+
							"\n 	END AS check_digit, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
							"\n 	fx_aux.c_amt AS pymnt_amt, "+
							"\n 	'MXN' AS currency_cd, "+
							"\n 	'40' AS pf_tipo_cuenta, "+
							"\n 	CASE WHEN(party_external.party_id = "+ iPartyBbvaBancomer +") "+
							"\n 		THEN '15' "+
							"\n 		ELSE '07' "+
							"\n 	END AS pfcodtipopago, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt, "+
							"\n 	tran_info_hora.value AS op_start_time, "+
							"\n 	ab.internal_contact AS oprid_entered_by, "+
							"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
							"\n 	'CPADV' AS id_tipo_instru, "+
							"\n 	'CPADV' AS id_subtipo_instru "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN party party_external "+
							"\n 		ON (ab.external_bunit = party_external.party_id) "+
							"\n 	LEFT JOIN party_account pebunit_account "+
							"\n 		ON (ab.external_bunit = pebunit_account.party_id) "+
							"\n 	LEFT JOIN account pebunit_account_number "+
							"\n 		ON (pebunit_account.account_id = pebunit_account_number.account_id "+
							"\n 			AND pebunit_account_number.account_status = 1) "+
							"\n  "+
							"\n 	LEFT JOIN ab_tran_settle_view absettlev "+
							"\n 		ON (ab.deal_tracking_num = absettlev.deal_tracking_num "+
							"\n 			AND absettlev.int_account_id!=0 "+
							"\n 			AND absettlev.TRAN_STATUS="+ iTranStatusValidated +") "+
							"\n  "+
							"\n 	LEFT JOIN account pibunit_account_number "+
							"\n 		ON (pibunit_account_number.account_id = absettlev.int_account_id) "+
							"\n  "+
							"\n 	LEFT JOIN ab_tran_info_view tran_info_hora "+
							"\n 		ON (ab.tran_num = tran_info_hora.tran_num   "+
							"\n 			AND tran_info_hora.type_name = '"+ sTranInfoViewHoraConcertacion +"') "+
							"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
							"\n 				FROM ab_tran_history "+
							"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
							"\n 				            FROM ab_tran_history "+
							"\n 				            WHERE TRAN_STATUS= "+ iTranStatusValidated +" "+
							"\n 				            GROUP BY TRAN_NUM) AUX_TABLE  "+
							"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
							"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
							"\n 				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
							"\n 	LEFT JOIN party_info_view peiv "+
							"\n 		ON (ab.external_bunit = peiv.party_id "+
							"\n 			AND peiv.type_name = '"+ sPartyInfoViewIdConsar +"') "+
							"\n 	LEFT JOIN currency curr_name "+
							"\n 		ON(ab.currency = curr_name.id_number) "+
							"\n 	LEFT JOIN fx_tran_aux_data fx_aux "+
							"\n 		ON fx_aux.tran_num = ab.tran_num "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetFx +" -- fx "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
							"\n 	AND ab.tran_status = "+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);

			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Compra de Divisas Egreso Detcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CDE_PF_SADFI_DETCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT	 "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	'SAD' AS origin, "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS pf_tran_id, "+
							"\n 	'1' AS seqno, "+
							"\n 	'CPADV' AS pf_tipo_afectacion, "+
							"\n 	fx_aux.c_amt AS amt_sel,  "+
							"\n 	'100' AS percentage, "+
							"\n 	'1' AS visual_rate  "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN fx_tran_aux_data fx_aux "+
							"\n 		ON fx_aux.tran_num = ab.tran_num "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetFx +" -- fx "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
							"\n 	AND ab.tran_status="+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"

			);

			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Compra de Divisas Ingreso");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CDE_PF_SADFI_ING.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS item,"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	ab.position AS orig_item_amt,"+
			"\n 	ab.external_bunit AS cust_id,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,"+
			"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,"+
			"\n 	'CONT' AS pymnt_terms_cd,"+
			"\n 	ab.currency AS currency_cd,"+
			"\n 	'STATESTREET - BU' AS bank_cd,"+
			"\n 	account_num.bank_account_num  AS bank_account_num,"+
			"\n 	id_proceso.pf_id_proceso AS pf_id_proceso,"+
			"\n 	'0' AS seqno,"+
			"\n 	'N' AS status,"+
			"\n 	ab.rate AS visual_rate,"+
			"\n 	'SADFI' AS origin_id"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN party internal_party"+
			"\n 		ON(ab.internal_bunit = internal_party.party_id)"+
			"\n 	LEFT JOIN currency curr_name"+
			"\n 		ON(ab.currency = curr_name.id_number)"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN "+ sCatalogo13UserTable +" account_num"+
			"\n 		ON (account_num.internal_bunit_name =  internal_party.short_name"+
			"\n 			AND account_num.external_bunit_name = 'STATESTREET - BU')"+
			"\n 	LEFT JOIN "+ sCatalogo14_2UserTable +" id_proceso"+
			"\n 		ON (curr_name.name = id_proceso.currency_name)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetFx +" -- fx"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);



			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Venta de Divisas Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VDI_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT	 "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	'SAD' AS origin, "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num) AS pf_tran_id, "+
							"\n 	ab.tran_status AS pf_status_cif, "+
							"\n 	ab.external_bunit AS pf_nom_ben, "+
							"\n 	'N/A' AS bank_cd, "+
							"\n 	'00000000000000000' AS bank_account_num, "+
							"\n 	'0' AS check_digit, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
							"\n 	ab.position AS pymnt_amt, "+
							"\n 	aux_data.ccy1 AS currency_cd,  "+
							"\n 	'N/A' AS pf_tipo_cuenta, "+
							"\n 	'13' AS pfcodtipopago, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt, "+
							"\n 	tran_info_hora.value AS op_start_time, "+
							"\n 	ab.internal_contact AS oprid_entered_by, "+
							"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
							"\n 	'MT202' as pf_dummy_field1 "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN fx_tran_aux_data aux_data "+
							"\n 		ON (ab.tran_num = aux_data.tran_num) "+
							"\n 	LEFT JOIN ab_tran_info_view tran_info_hora "+
							"\n 		ON (ab.tran_num = tran_info_hora.tran_num   "+
							"\n 		AND tran_info_hora.type_name = '"+ sTranInfoViewHoraConcertacion +"') "+
							"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
							"\n 				FROM ab_tran_history "+
							"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
							"\n 				            FROM ab_tran_history "+
							"\n 				            WHERE TRAN_STATUS= "+ iTranStatusValidated +" "+
							"\n 				            GROUP BY TRAN_NUM) AUX_TABLE  "+
							"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
							"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
							"\n 				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetFx +" -- fx "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
							"\n 	AND ab.tran_status="+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);



			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Venta de Divisas Detcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VDI_PF_SADFI_DETCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT	"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	'1' AS seqno,"+
			"\n 	'VNTDV' AS pf_tipo_afectacion,"+
			"\n 	ab.position AS amt_sel,"+
			"\n 	'100' AS percentage,"+
			"\n 	fx_aux.rate AS visual_rate"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN fx_tran_aux_data fx_aux"+
			"\n 		ON fx_aux.tran_num = ab.tran_num"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetFx +" -- fx"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Venta de Divisas Ingreso");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VDI_PF_SADFI_ING.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT  "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS item, "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	fx_aux.c_amt AS orig_item_amt, "+
							"\n 	ab.external_bunit AS cust_id, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt, "+
							"\n 	'CONT' AS pymnt_terms_cd, "+
							"\n 	currency.name AS currency_cd, "+
							"\n 	'12' AS bank_cd, "+
							"\n 	account_num.bank_account_num AS bank_account_num, "+
							"\n 	'T210' AS pf_id_proceso, "+
							"\n 	'0' AS seqno, "+
							"\n 	'1' AS visual_rate, "+
							"\n 	'SADFI' AS origin_id "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN fx_tran_aux_data fx_aux "+
							"\n 		ON fx_aux.tran_num = ab.tran_num "+
							"\n 	LEFT JOIN currency "+
							"\n 		ON (fx_aux.ccy2 = currency.id_number) "+
							"\n 	LEFT JOIN party internal_party "+
							"\n 		ON (ab.internal_bunit = internal_party.party_id) "+
							"\n 	LEFT JOIN party external_party "+
							"\n 		ON (ab.external_bunit = external_party.party_id) "+
							"\n 	LEFT JOIN "+ sCatalogo13UserTable +" account_num "+
							"\n 		ON (account_num.internal_bunit_name = internal_party.short_name "+
							"\n 			AND account_num.external_bunit_name = '"+ sPartyBbvaBancomer +"') "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetFx +" -- fx "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
							"\n 	AND ab.tran_status="+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"

			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Movimiento de Chequeras Egreso Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_MCE_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	ab.tran_status AS pf_status_cif,"+
			"\n 	ab.external_bunit AS pf_nom_ben,"+
			"\n 	CASE WHEN(pebunit_country.country = "+iCountryMexico+")"+
			"\n 		THEN peiv.value"+
			"\n 		ELSE 'N/A'"+
			"\n 	END AS bank_cd,"+
			"\n 	SUBSTR(account_num.bank_account_num,0,LENGTH(account_num.bank_account_num)-1)  AS bank_account_num,"+
			"\n 	SUBSTR(account_num.bank_account_num,LENGTH(account_num.bank_account_num)) AS check_digit,"+
			"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,"+
			"\n 	ab.position AS pymnt_amt,"+
			"\n 	'MXN' AS currency_cd,"+
			"\n 	'40' AS pf_tipo_cuenta,"+
			"\n 	CASE WHEN(ab.external_bunit = "+iPartyBbvaBancomer+")"+
			"\n 		THEN '15'"+
			"\n 		ELSE '07'"+
			"\n 	END AS pfcodtipopago,"+
			"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,"+
			"\n 	tran_info_hora.value AS op_start_time,"+
			"\n 	ab.internal_contact AS oprid_entered_by,"+
			"\n 	ab_tran_approved_by.personnel_id AS op_approved_by,"+
			"\n 	'CH' AS id_tipo_instru"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n 	LEFT JOIN party_address pebunit_country"+
			"\n 		ON (ab.external_bunit = pebunit_country.party_id)"+
			"\n 	LEFT JOIN party_info_view peiv"+
			"\n 		ON (ab.external_bunit = peiv.party_id"+
			"\n 			AND peiv.type_name = '"+ sPartyInfoViewIdConsar +"')"+
			"\n 	LEFT JOIN ab_tran_info_view tran_info_hora"+
			"\n 		ON (ab.tran_num = tran_info_hora.tran_num  "+
			"\n 			AND tran_info_hora.type_name = '"+ sTranInfoViewHoraConcertacion +"')"+
			"\n 	LEFT JOIN (	SELECT ab_tran_history.*"+
			"\n 				FROM ab_tran_history"+
			"\n 				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN"+
			"\n 				            FROM ab_tran_history"+
			"\n 				            WHERE TRAN_STATUS= "+iTranStatusValidated+""+
			"\n 				            GROUP BY TRAN_NUM) AUX_TABLE "+
			"\n 								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM "+
			"\n 								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)"+
			"\n 				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM"+
			"\n 	LEFT JOIN party internal_party"+
			"\n 		ON (ab.internal_bunit = internal_party.party_id)"+
			"\n 	LEFT JOIN party external_party"+
			"\n 		ON (ab.external_bunit = external_party.party_id)"+
			"\n 	LEFT JOIN "+ sCuentasBancarias +" account_num"+
			"\n 		ON (internal_party.short_name = account_num.internal_bunit_name"+
			"\n 			AND external_party.short_name = account_num.external_bunit_name)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetLoanDep +" -- LoanDep"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.ins_type="+iInsTypeDepoChequeras+" -- Depo-Chequeras"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);


			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Movimiento de Chequeras Egreso Detcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_MCE_PF_SADFI_DETCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
			"\n SELECT"+
			"\n 	ab.internal_bunit AS business_unit,"+
			"\n 	'SAD' AS origin,"+
			"\n 	CONCAT("+
			"\n 		CONCAT("+
			"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'),"+
			"\n 			portfolio.name	"+
			"\n 		),ab.tran_num"+
			"\n 	) AS pf_tran_id,"+
			"\n 	'1' AS seqno,"+
			"\n 	'INIVER' AS pf_tipo_afectacion,"+
			"\n 	ab.position as amt_sel,"+
			"\n 	'100' AS percentage,"+
			"\n 	ab.currency AS currency, -- para visual rate"+
			"\n 	ab.trade_date AS trade_date -- para visual rate"+
			"\n 	-- falta visual rate"+
			"\n FROM ab_tran ab"+
			"\n 	LEFT JOIN portfolio"+
			"\n 		ON (ab.internal_portfolio = portfolio.id_number)"+
			"\n WHERE"+
			"\n 	ab.toolset = "+ iToolsetLoanDep +" -- LoanDep"+
			"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading"+
			"\n 	AND ab.ins_type="+iInsTypeDepoChequeras+" -- Depo-Chequeras"+
			"\n		AND ab.trade_date = '"+sToday+"'"+
			"\n		AND ab.tran_status = "+ iTranStatusValidated +""
			);



			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Movimiento de Chequeras Ingreso");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_MCI_PF_SADFI_ING.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS item, "+
							"\n 	ab.internal_bunit AS business_unit, "+
							"\n 	CAST (CASE WHEN (ab.external_bunit="+ iPartyBbvaBancomer +") THEN profile.pymt "+
							"\n       WHEN (ab.external_bunit="+ iPartyScotiabank +") THEN profile.pymt+physcash.cflow "+
							"\n       WHEN (ab.external_bunit = "+ iPartyBanamex +" OR ab.external_bunit = "+ iPartyHsbc +") THEN ab2.var "+
							"\n       ELSE 0 "+
							"\n 	END AS FLOAT) AS orig_item_amt, "+
							"\n 	ab.external_bunit as cust_id, "+
							"\n 	TO_CHAR(ab.maturity_date, 'DD-MM-YYYY') AS asof_dt, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt, "+
							"\n 	'CONT' AS pymnt_terms_cd, "+
							"\n 	'MXN' AS currency_cd, "+
							"\n 	'"+ sPartyBbvaBancomer +"' AS bank_cd, "+
							"\n 	account_num.bank_account_num AS bank_account_num, "+
							"\n 	CASE WHEN (ab.external_bunit="+ iPartyBbvaBancomer +") THEN 'T009I' "+
							"\n       WHEN (ab.external_bunit="+ iPartyScotiabank +") THEN 'T009' "+
							"\n       WHEN (ab.external_bunit = "+ iPartyBanamex +" OR ab.external_bunit = "+ iPartyHsbc +") THEN ab2.pf_id_proceso "+
							"\n       ELSE '0' "+
							"\n 	END AS pf_id_proceso, "+
							"\n 	CASE WHEN (ab.external_bunit="+ iPartyBbvaBancomer +") THEN '0' "+
							"\n       WHEN (ab.external_bunit="+ iPartyScotiabank +") THEN '1' "+
							"\n       WHEN (ab.external_bunit = "+ iPartyBanamex +" OR ab.external_bunit = "+ iPartyHsbc +") THEN ab2.seqno "+
							"\n       ELSE '0' "+
							"\n 	END AS seqno, "+
							"\n 	'N' AS status, "+
							"\n 	'1' AS visual_rate, "+
							"\n 	'SADFI' AS origin_id "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN party internal_party "+
							"\n 		ON (ab.internal_bunit = internal_party.party_id) "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN "+ sCatalogo13UserTable +" account_num "+
							"\n 		ON (account_num.internal_bunit_name = internal_party.short_name "+
							"\n 			AND account_num.external_bunit_name = '"+ sPartyBbvaBancomer +"') "+
							"\n 	LEFT JOIN ( "+
							"\n 		SELECT ab.tran_num, ab.external_bunit, physcash.cflow var, 'T009' AS pf_id_proceso, '0' as seqno "+
							"\n 			FROM ab_tran ab JOIN physcash ON (ab.ins_num = physcash.ins_num AND physcash.cflow_type="+ iCflowTypeFinalPrincipal +") "+
							"\n 			WHERE (ab.external_bunit IN("+ iPartyBanamex +","+ iPartyHsbc +")) "+
							"\n     	UNION "+
							"\n     	SELECT ab.tran_num, ab.external_bunit, profile.pymt var, 'T009I' AS pf_id_proceso, '1' as seqno "+
							"\n 			FROM ab_tran ab JOIN profile ON (ab.ins_num=profile.ins_num) "+
							"\n 			WHERE (ab.external_bunit IN("+ iPartyBanamex +","+ iPartyHsbc +")) "+
							"\n 	) ab2 ON ab.tran_num=ab2.tran_num "+
							"\n 	LEFT JOIN physcash  "+
							"\n 		ON (ab.ins_num = physcash.ins_num AND physcash.cflow_type="+ iCflowTypeFinalPrincipal +") "+
							"\n 	LEFT JOIN profile "+
							"\n 		ON (ab.ins_num=profile.ins_num) "+
							"\n WHERE "+
							"\n 	ab.toolset = "+ iToolsetLoanDep +" -- LoanDep "+
							"\n 	AND ab.tran_type = "+ iTranTypeTrading +" -- trading "+
							"\n 	AND ab.ins_type="+ iInsTypeDepoChequeras +" -- Depo-Chequeras "+
							"\n 	AND ab.tran_status="+ iTranStatusValidated +" --Validated "+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);
			
			
			// Llamadas de Capital Egreso Hdrcif
			iRow = tMasterUserTables.addRow();
			tMasterUserTables.setString("Name", iRow, "Llamada de Capital Egreso Hdrcif");
			tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_LCE_PF_SADFI_HDRCIF.toString());
			tMasterUserTables.setString("UserTableQuery", iRow, 
					"\n SELECT "+
							"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
							"\n 	'SAD' AS origin, "+
							"\n 	CONCAT( "+
							"\n 		CONCAT( "+
							"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
							"\n 			portfolio.name	 "+
							"\n 		),ab.tran_num "+
							"\n 	) AS pf_tran_id, "+
							"\n 	ab.tran_status AS pf_status_cif, "+
							"\n 	'INDEVAL' AS pf_nom_ben, "+
							"\n 	'40012' AS bank_cd, "+
							"\n 	'01218000445541034' AS bank_account_num, "+
							"\n 	'0' AS check_digit, "+
							"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
							"\n 	ab.proceeds AS pymnt_amt, "+
							"\n 	ab.currency AS currency_cd, "+
							"\n 	'40' AS pf_tipo_cuenta, "+
							"\n 	'21' AS pfcodtipopago, "+
							"\n 	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt, "+
							"\n 	'' AS op_start_time, "+
							"\n 	ab.internal_contact AS oprid_entered_by, "+
							"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
							"\n 	'CAPIT' AS id_tipo_instru, "+
							"\n 	'CAPIT' AS id_subtipo_instru "+
							"\n FROM ab_tran ab "+
							"\n 	LEFT JOIN portfolio "+
							"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
							"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
							"\n 			FROM ab_tran_history "+
							"\n 			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
							"\n 			            FROM ab_tran_history "+
							"\n 			            WHERE TRAN_STATUS= 3 "+
							"\n 			            GROUP BY TRAN_NUM) AUX_TABLE  "+
							"\n 							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
							"\n 							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
							"\n 			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
							"\n 	LEFT JOIN ab_tran_info_view tran_info_tipo_compraventa "+
							"\n 		ON (ab.tran_num = tran_info_tipo_compraventa.tran_num   "+
							"\n 			AND tran_info_tipo_compraventa.type_name = '"+ sTranTipoCompraVenta +"') "+
							"\n  "+
							"\n 	LEFT JOIN party_portfolio pp "+
							"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
							"\n 	LEFT JOIN party i_portfolio_name "+
							"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
							"\n  "+
							"\n WHERE 1=1 "+
							"\n 	AND ab.external_bunit = "+ iPartySindeval +" --SDINDEVAL - BU "+
							"\n 	AND tran_info_tipo_compraventa.value = 'Llamada Capital' "+
							"\n 	AND ab.toolset = "+ iToolsetEquity +" --Equity "+
							"\n 	AND ab.ins_type = "+ iInsTypeEqtCkdKcall +" -- EQT-CKD-KCALL "+
							"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
							"\n		AND ab.trade_date = '"+sToday+"'"
			);
			
			
			// Llamadas de Capital Egreso Detcif
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Llamada de Capital Egreso Detcif");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_LCE_PF_SADFI_DETCIF.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	'SAD' AS origin, "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS pf_tran_id, "+
								"\n 	'1' AS seqno, "+
								"\n 	'CAPIT' AS pf_tipo_afectacion, "+
								"\n 	'CAPIT' AS id_subtipo_instru, "+
								"\n 	ab.proceeds AS amt_sel, "+
								"\n 	'100' AS percentage "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n 	LEFT JOIN ab_tran_info_view tran_info_tipo_compraventa "+
								"\n 		ON (ab.tran_num = tran_info_tipo_compraventa.tran_num   "+
								"\n 			AND tran_info_tipo_compraventa.type_name = '"+ sTranTipoCompraVenta +"') "+
								"\n  "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n  "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.external_bunit = "+ iPartySindeval +" --SDINDEVAL - BU "+
								"\n 	AND tran_info_tipo_compraventa.value = 'Llamada Capital' "+
								"\n 	AND ab.toolset = "+ iToolsetEquity +" --Equity "+
								"\n 	AND ab.ins_type = "+ iInsTypeEqtCkdKcall +" -- EQT-CKD-KCALL "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);
	
				// Compra de Acciones Egreso Hdrcif
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Compra de Acciones Egreso Hdrcif");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CAE_PF_SADFI_HDRCIF.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	'SAD' AS origin, "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS pf_tran_id, "+
								"\n 	ab.tran_status AS pf_status_cif, "+
								"\n 	party.long_name AS pf_nom_ben, "+
								"\n 	'40012' AS bank_cd, "+
								"\n 	CASE WHEN(LENGTH(pibunit_account_number.account_number)>=17) "+
								"\n 		THEN SUBSTR(pibunit_account_number.account_number,1,17) "+
								"\n 		ELSE pibunit_account_number.account_number "+
								"\n 	END AS bank_account_num, "+
								"\n 	SUBSTR(pibunit_account_number.account_number,18,18) AS check_digit, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
								"\n 	ab.position AS pymnt_amt, "+
								"\n 	ab.currency AS currency_cd, "+
								"\n 	'40' AS pf_tipo_cuenta, "+
								"\n 	'21' AS pfcodtipopago, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entered_dt, "+
								"\n 	'' AS op_start_time, "+
								"\n 	ab.internal_contact AS oprid_entered_by, "+
								"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
								"\n 	'ACCPT' AS id_tipo_instru, "+
								"\n 	'COMPAPT' AS id_subtipo_instru "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n 	LEFT JOIN party "+
								"\n 		ON (ab.internal_bunit = party.party_id) "+
								"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
								"\n 			FROM ab_tran_history "+
								"\n 			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
								"\n 			            FROM ab_tran_history "+
								"\n 			            WHERE TRAN_STATUS= 3 "+
								"\n 			            GROUP BY TRAN_NUM) AUX_TABLE  "+
								"\n 							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
								"\n 							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
								"\n 			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
								"\n 			 "+
								"\n 	--Internal account "+
								"\n 	LEFT JOIN ab_tran_settle_view absettlev "+
								"\n 		ON (ab.deal_tracking_num = absettlev.deal_tracking_num "+
								"\n 			AND absettlev.int_account_id!=0 "+
								"\n 			AND absettlev.TRAN_STATUS=3) "+
								"\n  "+
								"\n 	LEFT JOIN account pibunit_account_number "+
								"\n 		ON (pibunit_account_number.account_id = absettlev.int_account_id) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);

				// Compra de Acciones Egreso Detcif
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Compra de Acciones Egreso Detcif");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CAE_PF_SADFI_DETCIF.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	'SAD' AS origin, "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS pf_tran_id, "+
								"\n 	'ACCPT' AS pf_tipo_afectacion, "+
								"\n 	'COMPAPT' AS id_subtipo_instru, "+
								"\n 	ab.position AS amt_sel, "+
								"\n 	'100' AS percentage "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	 "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);			
				
				
				// Compra de Acciones Ingreso
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Compra de Acciones Ingreso");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_CAI_PF_SADFI_ING.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS item, "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	ab.position AS orig_item_amt, "+
								"\n 	'CLT0000007' AS cust_id, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entry_dt, "+
								"\n 	'CONT' AS pymnt_terms_cd, "+
								"\n 	ab.currency AS currency_cd, "+
								"\n 	'12' AS bank_cd, "+
								"\n 	account_num.bank_account_num  AS bank_account_num, "+
								"\n 	'T001' AS pf_id_proceso, "+
								"\n 	'0' AS secno, "+
								"\n 	'N' AS status, "+
								"\n 	'SADFI' AS origin_id, "+
								"\n 	'1' AS visual_rate "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n  "+
								"\n 	LEFT JOIN party internal_party "+
								"\n 		ON(ab.internal_bunit = internal_party.party_id) "+
								"\n  "+
								"\n 	LEFT JOIN party external_party "+
								"\n 		ON(ab.external_bunit = external_party.party_id) "+
								"\n  "+
								"\n 	LEFT JOIN user_mx_catalogo13 account_num "+
								"\n 		ON (account_num.internal_bunit_name =  internal_party.short_name "+
								"\n 			AND account_num.external_bunit_name = external_party.short_name) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	 "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n  "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_buy +" -- buy "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);		
				
				
				// Venta de acciones egreso hdrcif
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Venta de Acciones Egreso Hdrcif");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VAE_PF_SADFI_HDRCIF.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	'SAD' AS origin, "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS pf_tran_id, "+
								"\n 	ab.tran_status AS pf_status_cif, "+
								"\n 	party.long_name AS pf_nom_ben, "+
								"\n 	'40012' AS bank_cd, "+
								"\n 	'01218000102979764' bank_account_num, "+
								"\n 	'1' AS check_digit, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt, "+
								"\n 	ab.position AS pymnt_amt, "+
								"\n 	ab.currency AS currency_cd, "+
								"\n 	'40' AS pf_tipo_cuenta, "+
								"\n 	'21' AS pfcodtipopago, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entered_dt, "+
								"\n 	'' AS op_start_time, "+
								"\n 	ab.internal_contact AS oprid_entered_by, "+
								"\n 	ab_tran_approved_by.personnel_id AS op_approved_by, "+
								"\n 	'ACCPT' AS id_tipo_instru, "+
								"\n 	'VTAAPT' AS id_subtipo_instru "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n 	LEFT JOIN party "+
								"\n 		ON (ab.internal_bunit = party.party_id) "+
								"\n 	LEFT JOIN (	SELECT ab_tran_history.* "+
								"\n 			FROM ab_tran_history "+
								"\n 			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN "+
								"\n 			            FROM ab_tran_history "+
								"\n 			            WHERE TRAN_STATUS= 3 "+
								"\n 			            GROUP BY TRAN_NUM) AUX_TABLE  "+
								"\n 							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM  "+
								"\n 							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN) "+
								"\n 			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM "+
								"\n  "+
								"\n 	--Internal account "+
								"\n 	LEFT JOIN ab_tran_settle_view absettlev "+
								"\n 		ON (ab.deal_tracking_num = absettlev.deal_tracking_num "+
								"\n 			AND absettlev.int_account_id!=0 "+
								"\n 			AND absettlev.TRAN_STATUS=3) "+
								"\n  "+
								"\n 	LEFT JOIN account pibunit_account_number "+
								"\n 		ON (pibunit_account_number.account_id = absettlev.int_account_id) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	 "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);
				
				
				// Venta de acciones egreso detcif
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Venta de Acciones Egreso Detcif");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VAE_PF_SADFI_DETCIF.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	'SAD' AS origin, "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS pf_tran_id, "+
								"\n 	'1' AS seqno, "+
								"\n 	'ACCPT' AS pf_tipo_afectacion, "+
								"\n 	'VTAAPT' AS PF_STIPOAFECTACION, "+
								"\n 	ab.position AS amt_sel, "+
								"\n 	'100' AS percentage, "+
								"\n 	'1' AS visual_rate "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	 "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);		
				

				// Venta de acciones ingreso
				iRow = tMasterUserTables.addRow();
				tMasterUserTables.setString("Name", iRow, "Venta de Acciones Ingreso");
				tMasterUserTables.setString("UserTableName", iRow, EnumsUserTables.USER_MX_VAI_PF_SADFI_ING.toString());
				tMasterUserTables.setString("UserTableQuery", iRow, 
						"\n SELECT "+
								"\n 	CONCAT( "+
								"\n 		CONCAT( "+
								"\n 			TO_CHAR(ab.settle_date, 'YYYYMMDD'), "+
								"\n 			portfolio.name	 "+
								"\n 		),ab.tran_num "+
								"\n 	) AS item, "+
								"\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit, "+
								"\n 	ab.position AS orig_item_amt, "+
								"\n 	'CLT0000022' AS cust_id, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt, "+
								"\n 	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entry_dt, "+
								"\n 	'CONT' AS pymnt_terms_cd, "+
								"\n 	ab.currency AS currency_cd, "+
								"\n 	'12' AS bank_cd, "+
								"\n 	'764C'  AS bank_account_num, "+
								"\n 	'T001A' AS pf_id_proceso, "+
								"\n 	'0' AS secno, "+
								"\n 	'1' AS visual_rate, "+
								"\n 	'SADFI' AS origin_id "+
								"\n  "+
								"\n FROM ab_tran ab "+
								"\n 	LEFT JOIN portfolio "+
								"\n 		ON (ab.internal_portfolio = portfolio.id_number) "+
								"\n  "+
								"\n 	--portfolio name "+
								"\n 	 "+
								"\n 	LEFT JOIN party_portfolio pp "+
								"\n 		ON (ab.internal_portfolio = pp.portfolio_id) "+
								"\n 	LEFT JOIN party i_portfolio_name "+
								"\n 		ON (pp.party_id = i_portfolio_name.party_id) "+
								"\n  "+
								"\n WHERE 1=1 "+
								"\n 	AND ab.toolset = "+ iToolsetCash +""+
								"\n 	AND ab.cflow_type = "+ iCflowTypeAccionesAforeA2 +" -- Mov Acciones Afore A2 "+
								"\n 	AND ab.buy_sell = "+ iBuySell_sell +" -- sell "+
								"\n		AND ab.tran_status = "+ iTranStatusValidated +""+
								"\n		AND ab.trade_date = '"+sToday+"'"
				);		
				
		}

}
