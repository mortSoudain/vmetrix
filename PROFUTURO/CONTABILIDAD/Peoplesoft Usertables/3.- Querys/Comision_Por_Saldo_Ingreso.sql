SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	ab.internal_bunit AS business_unit,
	comision_saldo_dia.var AS orig_item_amt,
	ab.internal_bunit AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency AS currency_cd,
	'BBVA BANCOMER - BU' AS bank_cd,
	'764C' AS bank_account_num,
	id_proceso.pf_id_proceso AS pf_id_proceso,
	comision_saldo_dia.seqno AS seqno,
	'N' AS status,
	'SADFI' AS origin_id,
	'1' AS viual_rate
FROM ab_tran ab
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN (
			SELECT portfolio, var, descripcion, seqno
			FROM (
				SELECT portfolio, var_trabajadores var,'Trabajadores' Descripcion, '0' seqno
				FROM user_mx_comision_saldo_dia
				UNION
				SELECT portfolio, var_afore var, 'Afore' Descripcion, '1' seqno
				FROM user_mx_comision_saldo_dia
			)
		) comision_saldo_dia
		ON portfolio.name = comision_saldo_dia.portfolio
	LEFT JOIN user_mx_catalogo14_1 id_proceso
		ON (id_proceso.internal_bunit = internal_party.short_name
			AND id_proceso.descripcion = comision_saldo_dia.descripcion)
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND ab.cflow_type = 2017 -- comision por saldo
	AND ab.tran_status=3 --Validated