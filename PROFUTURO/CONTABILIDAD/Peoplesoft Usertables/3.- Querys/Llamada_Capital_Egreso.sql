UNIVERSO

TRAN_STATUS 			?
BUY_SELL 				?
TRADE_DATE				?
CFLOW_TYPE				?
EXTERNAL_BUNIT 			SDINDEVAL - BU
TRAN INFO COMPRA VENTA 	LLAMADA CAPITAL
TOOLSET					EQUITY
INS_TYPE				EQT-CKD-KCALL

--hdr
SELECT
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	'INDEVAL' AS pf_nom_ben,
	'40012' AS bank_cd,
	'01218000445541034' AS bank_account_num,
	'0' AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.proceeds AS pymnt_amt,
	ab.currency AS currency_cd,
	'40' AS pf_tipo_cuenta,
	'21' AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	'' AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'CAPIT' AS id_tipo_instru,
	'CAPIT' AS id_subtipo_instru
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN (	SELECT ab_tran_history.*
			FROM ab_tran_history
			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
			            FROM ab_tran_history
			            WHERE TRAN_STATUS= 3
			            GROUP BY TRAN_NUM) AUX_TABLE 
							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	LEFT JOIN ab_tran_info_view tran_info_tipo_compraventa
		ON (ab.tran_num = tran_info_tipo_compraventa.tran_num  
			AND tran_info_tipo_compraventa.type_name = 'Trad Tran Tipo Compra Venta')

	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)

WHERE 1=1
	AND ab.external_bunit = 20363 --SDINDEVAL - BU
	AND tran_info_tipo_compraventa.value = 'Llamada Capital'
	AND ab.toolset = 28 --Equity
	AND ab.ins_type = 1000043 -- EQT-CKD-KCALL
	AND ab.tran_status = 3 -- validated



--detcif
SELECT
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'CAPIT' AS pf_tipo_afectacion,
	'CAPIT' AS id_subtipo_instru,
	ab.proceeds AS amt_sel,
	'100' AS percentage
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN ab_tran_info_view tran_info_tipo_compraventa
		ON (ab.tran_num = tran_info_tipo_compraventa.tran_num  
			AND tran_info_tipo_compraventa.type_name = 'Trad Tran Tipo Compra Venta')

	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)

WHERE 1=1
	AND ab.external_bunit = 20363 --SDINDEVAL - BU
	AND tran_info_tipo_compraventa.value = 'Llamada Capital'
	AND ab.toolset = 28 --Equity
	AND ab.ins_type = 1000043 -- EQT-CKD-KCALL
	AND ab.tran_status = 3 -- validated
