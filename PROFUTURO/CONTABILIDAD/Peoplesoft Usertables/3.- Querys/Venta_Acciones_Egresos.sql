UNIVERSO

TRAN_STATUS 			?
INS_NUM					?
TRADE_DATE				?
CFLOW_TYPE 				MOV ACCIONES AFORE A2
TOOLSET					CASH
BUY_SELL 				SELL

--hdrcif
SELECT
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	party.long_name AS pf_nom_ben,
	'40012' AS bank_cd,
	'01218000102979764' bank_account_num,
	'1' AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	ab.currency AS currency_cd,
	'40' AS pf_tipo_cuenta,
	'21' AS pfcodtipopago,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entered_dt,
	'' AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'ACCPT' AS id_tipo_instru,
	'VTAAPT' AS id_subtipo_instru
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN party
		ON (ab.internal_bunit = party.party_id)
	LEFT JOIN (	SELECT ab_tran_history.*
			FROM ab_tran_history
			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
			            FROM ab_tran_history
			            WHERE TRAN_STATUS= 3
			            GROUP BY TRAN_NUM) AUX_TABLE 
							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM

	--Internal account
	LEFT JOIN ab_tran_settle_view absettlev
		ON (ab.deal_tracking_num = absettlev.deal_tracking_num
			AND absettlev.int_account_id!=0
			AND absettlev.TRAN_STATUS=3)

	LEFT JOIN account pibunit_account_number
		ON (pibunit_account_number.account_id = absettlev.int_account_id)

	--portfolio name
	
	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)
WHERE 1=1
	AND ab.toolset = 10 -- cash
	AND ab.cflow_type = 2030 -- Mov Acciones Afore A2
	AND ab.buy_sell = 1 --sell
	AND ab.tran_status = 3


--detcif
SELECT
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'ACCPT' AS pf_tipo_afectacion,
	'VTAAPT' AS PF_STIPOAFECTACION,
	ab.position AS amt_sel,
	'100' AS percentage,
	'1' AS visual_rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)

	--portfolio name
	
	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)
WHERE 1=1
	AND ab.toolset = 10 -- cash
	AND ab.cflow_type = 2030 -- Mov Acciones Afore A2
	AND ab.buy_sell = 1 --sell
	AND ab.tran_status = 3