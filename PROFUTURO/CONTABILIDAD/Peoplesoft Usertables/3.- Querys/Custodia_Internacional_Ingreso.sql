-- hdrcif
SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,

	'PS' || portfolio.name as business_unit,
	ab.position as orig_item_amt,
	'CLT0000007' as cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency as currency_cd,
	'911' AS bank_cd,
	account_num.bank_account_num  AS bank_account_num,
	'T006' AS pf_id_proceso,
	'0' AS secno,
	'N' AS status,
	'SADFI' AS origin_id,
	'' AS visual_rate ---------------------------------------incluir en el codigo java
FROM ab_tran ab
	-- Internal BU name
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	-- Protfolio name
	LEFT JOIN portfolio
		ON(ab.internal_portfolio=portfolio.id_number)
	-- Usuario que pasó a validada la transacción
	LEFT JOIN (	SELECT ab_tran_history.*
			FROM ab_tran_history
			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
			            FROM ab_tran_history
			            WHERE TRAN_STATUS= 3
			            GROUP BY TRAN_NUM) AUX_TABLE 
							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	-- Bank account number
	LEFT JOIN user_mx_catalogo13 account_num
		ON (account_num.internal_bunit_name =  internal_party.short_name
			AND account_num.external_bunit_name = 'STATESTREET - BU')
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_status = 3 -- validated
	AND ab.cflow_type IN (2041,2042) -- retiro custodio, deposito custodio
	AND ab.trade_date = HOY xd