-- pf_sadfi_hdrcif

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	ab.external_bunit AS pf_nom_ben,
	CASE WHEN(pebunit_country.country = 20040)
		THEN peiv.value
		ELSE 'N/A'
	END AS bank_cd,
	CASE WHEN(pebunit_country.country = 20040)
		THEN CASE WHEN(party_external.party_id = 20720) -- PARTY EXTERNAL BBVA-DER
				THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=17)
						THEN SUBSTR(pibunit_account_number.account_number,1,17)
						ELSE pibunit_account_number.account_number
					END
				ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18)
						THEN SUBSTR(pebunit_account_number.account_number,1,17)
						ELSE pebunit_account_number.account_number
					END
			END
		ELSE '00000000000000000'
	END AS bank_account_num,
	CASE WHEN(pebunit_country.country = 20040)
		THEN CASE WHEN(party_external.party_id = 20720) -- PARTY EXTERNAL BBVA-DER
			THEN SUBSTR(pibunit_account_number.account_number,18,18)
			ELSE SUBSTR(pebunit_account_number.account_number,18,18)
			END
		ELSE '0'
	END AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	ab.currency AS currency_cd,
	CASE WHEN(ab.currency = 70) 
		THEN '40'
		ELSE 'N/A'
	END AS pf_tipo_cuenta,
	CASE WHEN(ab.currency = 70)
		THEN
			CASE WHEN(party_external.party_id = 20720)
				THEN '15'
				ELSE '07'
			END
		ELSE '13'
	END AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	tran_info_hora.value AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'SL' AS id_tipo_instru,
	ab_tran_reviewed_by.personnel_id AS oprid_reviewed_by,
	CASE 
		WHEN(party_external.party_id IN (20057,20728)) THEN 'MTPRE'
		WHEN(party_external.party_id = 20061) THEN 'MT103'
		ELSE ''
	END AS pf_dummy_field1
FROM ab_tran ab
	INNER JOIN party party_external
		ON (ab.external_bunit = party_external.party_id)
	LEFT JOIN party_info_view peiv
		ON (ab.external_bunit = peiv.party_id
			AND peiv.type_name = 'Id_Consar') 
	LEFT JOIN party_address pebunit_country
		ON (ab.external_bunit = pebunit_country.party_id) 
	LEFT JOIN party_account pebunit_account
		ON (ab.external_bunit = pebunit_account.party_id)
	LEFT JOIN account pebunit_account_number
		ON (pebunit_account.account_id = pebunit_account_number.account_id
			AND pebunit_account_number.account_status = 1)

	LEFT JOIN ab_tran_settle_view absettlev
		ON (ab.deal_tracking_num = absettlev.deal_tracking_num
			AND absettlev.int_account_id!=0
			AND absettlev.TRAN_STATUS=3)

	LEFT JOIN account pibunit_account_number
		ON (pibunit_account_number.account_id = absettlev.int_account_id)

	LEFT JOIN ab_tran_info_view tran_info_hora
		ON (ab.tran_num = tran_info_hora.tran_num  
			AND tran_info_hora.type_name = 'Trad Tran Hora de Concertacion')
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 3
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 2
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_reviewed_by ON AB.TRAN_NUM = ab_tran_reviewed_by.TRAN_NUM
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND (ab.cflow_type = 2018
		OR ab.cflow_type = 271) -- AIMS o Margin Call AIMS
	AND ab.buy_sell = 0 -- buy
	AND ab.tran_status = 3 --Validated


--pf_sadfi_detcif
SELECT  
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	CASE WHEN(pebunit_country.country = 20040)
		THEN 'DSLMXP'
		ELSE 'DSLEXT'
	END AS pf_tipo_afectacion,
	tran_settle_view.settle_amount AS amt_sel,
	'100' AS percentage,
	ab.currency AS currency, -- para sacar visual rate
	ab.trade_date as trade_date -- para sacar visual rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN party_address pebunit_country
		ON (ab.external_bunit = pebunit_country.party_id) 
	LEFT JOIN ab_tran_settle_view tran_settle_view
		ON (ab.tran_num = tran_settle_view.tran_num) 
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND (ab.cflow_type = 2018
		OR ab.cflow_type = 271) -- AIMS o Margin Call AIMS
	AND ab.buy_sell = 0 -- buy
	AND ab.tran_status=3 --Validated