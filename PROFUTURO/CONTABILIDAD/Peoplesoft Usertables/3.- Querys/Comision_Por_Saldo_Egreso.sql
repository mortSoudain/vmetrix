-- pf_sadfi_hdrcif

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	'PROFUTURO AFORE SA DE CV BU' AS pf_nom_ben,
	'40012' AS bank_cd,
	'01218000102979764' AS bank_account_num,
	'1' AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	'CXSAFORE' AS remit_vendor,
	amount.monto_comision AS pymnt_amt,
	ab.currency AS currency_cd,
	CASE WHEN(ab.currency = 70) 
		THEN '40'
		ELSE 'N/A'
	END AS pf_tipo_cuenta,
	'21' AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	tran_info_hora.value AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN ab_tran_info_view tran_info_hora
		ON (ab.tran_num = tran_info_hora.tran_num  
			AND tran_info_hora.type_name = 'Trad Tran Hora de Concertacion')
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 3
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	LEFT JOIN user_mx_comision_saldo_dia amount
		ON (portfolio.name = amount.portfolio)
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND ab.cflow_type = 2017 -- comision por saldo
	AND ab.tran_status=3 --Validated


-- pf_sadfi_detcif			

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'COMIS' AS pf_tipo_afectacion,
	amount.monto_comision AS amt_sel,
	'100' AS percentage,
	'1' AS visual_rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN user_mx_comision_saldo_dia amount
		ON (portfolio.name = amount.portfolio)
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND ab.cflow_type = 2017 -- comision por saldo
	AND ab.tran_status=3 --Validated