-- hdrcif
-- + deposito
-- - retiro

-- hdrcif
SELECT
	'PAF01' AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	internal_party.long_name AS pf_nom_ben,
	'N/A' AS bank_cd,
	'00000000000000000' AS bank_account_num,
	'0' AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	ab.currency AS currency_cd,
	'N/A' AS pf_tipo_cuenta,
	'47' AS pfcodtipopago,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entered_dt,
	' ' AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'DVLEXT' AS id_tipo_instru,
	'DVLEXT' AS id_subtipo_instru,
	'0' AS oprid_reviewed_by,
	' ' AS pf_dummy_field1
FROM ab_tran ab
	-- Internal BU name
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	-- Protfolio name
	LEFT JOIN portfolio
		ON(ab.internal_portfolio=portfolio.id_number)
	-- Usuario que pasó a validada la transacción
	LEFT JOIN (	SELECT ab_tran_history.*
			FROM ab_tran_history
			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
			            FROM ab_tran_history
			            WHERE TRAN_STATUS= 3
			            GROUP BY TRAN_NUM) AUX_TABLE 
							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_status = 3 -- validated
	AND ab.cflow_type IN (2041,2042) -- retiro custodio, deposito custodio
	AND ab.trade_date = HOY xd

--detcif
SELECT
	'PAF01' AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'DVLEXT' AS pf_tipo_afectacion,
	ab.position AS amt_sel,
	'100' as percentage,
	'' AS visual_rate -------------------------------------- añadir este campo en el codigo de java
FROM ab_tran ab
	-- Protfolio name
	LEFT JOIN portfolio
		ON(ab.internal_portfolio=portfolio.id_number)
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_status = 3 -- validated
	AND ab.cflow_type IN (2041,2042) -- retiro custodio, deposito custodio
	AND ab.trade_date = HOY xd