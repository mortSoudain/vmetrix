-- pf_sadfi_hdrcif

SELECT	
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	ab.external_bunit AS pf_nom_ben,
	'N/A' AS bank_cd,
	'00000000000000000' AS bank_account_num,
	'0' AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	aux_data.ccy1 AS currency_cd, 
	'N/A' AS pf_tipo_cuenta,
	'13' AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	tran_info_hora.value AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'MT202' as pf_dummy_field1
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN fx_tran_aux_data aux_data
		ON (ab.tran_num = aux_data.tran_num)
	LEFT JOIN ab_tran_info_view tran_info_hora
		ON (ab.tran_num = tran_info_hora.tran_num  
		AND tran_info_hora.type_name = 'Trad Tran Hora de Concertacion')
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 3
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 1 -- sell
	AND ab.tran_status=3 --Validated


-- pf_sadfi_detcif
SELECT	
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'VNTDV' AS pf_tipo_afectacion,
	ab.position AS amt_sel,
	'100' AS percentage,
	fx_aux.rate AS visual_rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN fx_tran_aux_data fx_aux
		ON fx_aux.tran_num = ab.tran_num
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 1 -- sell
	AND ab.tran_status=3 --Validated


-- pf_sadfi_ing			
SELECT 
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	ab.internal_bunit AS business_unit,
	fx_aux.c_amt AS orig_item_amt,
	ab.external_bunit AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	currency.name AS currency_cd,
	'12' AS bank_cd,
	account_num.bank_account_num AS bank_account_num,
	'T210' AS pf_id_proceso,
	'0' AS seqno,
	'1' AS visual_rate,
	'SADFI' AS origin_id
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN fx_tran_aux_data fx_aux
		ON fx_aux.tran_num = ab.tran_num
	LEFT JOIN currency
		ON (fx_aux.ccy2 = currency.id_number)
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	LEFT JOIN party external_party
		ON (ab.external_bunit = external_party.party_id)
	LEFT JOIN user_mx_catalogo13 account_num
		ON (account_num.internal_bunit_name = internal_party.short_name
			AND account_num.external_bunit_name = 'BBVA BANCOMER - BU')
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 1 -- sell
	AND ab.tran_status=3 --Validated
