-- pf_sadfi_hdrcif			

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	ab.external_bunit AS pf_nom_ben,
	CASE WHEN(pebunit_country.country = 20040)
		THEN peiv.value
		ELSE 'N/A'
	END AS bank_cd,
	SUBSTR(account_num.bank_account_num,0,LENGTH(account_num.bank_account_num)-1)  AS bank_account_num,
	SUBSTR(account_num.bank_account_num,LENGTH(account_num.bank_account_num)) AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	'MXN' AS currency_cd,
	'40' AS pf_tipo_cuenta,
	CASE WHEN(ab.external_bunit = 20045)
		THEN '15'
		ELSE '07'
	END AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	tran_info_hora.value AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'CH' AS id_tipo_instru
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN party_address pebunit_country
		ON (ab.external_bunit = pebunit_country.party_id)
	LEFT JOIN party_info_view peiv
		ON (ab.external_bunit = peiv.party_id
			AND peiv.type_name = 'Id_Consar')
	LEFT JOIN ab_tran_info_view tran_info_hora
		ON (ab.tran_num = tran_info_hora.tran_num  
			AND tran_info_hora.type_name = 'Trad Tran Hora de Concertacion')
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 3
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	LEFT JOIN party external_party
		ON (ab.external_bunit = external_party.party_id)
	LEFT JOIN user_mx_cuentas_bancarias account_num
		ON (internal_party.short_name = account_num.internal_bunit_name
			AND external_party.short_name = account_num.external_bunit_name)
WHERE
	ab.toolset = 6 -- LoanDep
	AND ab.tran_type = 0 -- trading
	AND ab.ins_type=1000056 -- Depo-Chequeras
	AND ab.tran_status=3 --Validated



-- pf_sadfi_detcif

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'INIVER' AS pf_tipo_afectacion,
	ab.position as amt_sel,
	'100' AS percentage,
	ab.currency AS currency, -- para visual rate
	ab.trade_date AS trade_date -- para visual rate
	-- falta visual rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
WHERE
	ab.toolset = 6 -- LoanDep
	AND ab.tran_type = 0 -- trading
	AND ab.ins_type=1000056 -- Depo-Chequeras
	AND ab.tran_status=3 --Validated