UNIVERSO

TRAN_STATUS 			?
INS_NUM					?
TRADE_DATE				?
CFLOW_TYPE 				MOV ACCIONES AFORE A2
TOOLSET					CASH
BUY_SELL 				BUY

--ing
SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	ab.position AS orig_item_amt,
	'CLT0000007' AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency AS currency_cd,
	'12' AS bank_cd,
	account_num.bank_account_num  AS bank_account_num,
	'T001' AS pf_id_proceso,
	'0' AS secno,
	'N' AS status,
	'SADFI' AS origin_id,
	'1' AS visual_rate
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)

	LEFT JOIN party internal_party
		ON(ab.internal_bunit = internal_party.party_id)

	LEFT JOIN party external_party
		ON(ab.external_bunit = external_party.party_id)

	LEFT JOIN user_mx_catalogo13 account_num
		ON (account_num.internal_bunit_name =  internal_party.short_name
			AND account_num.external_bunit_name = external_party.short_name)

	--portfolio name
	
	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)

WHERE 1=1
	AND ab.toolset = 10 -- cash
	AND ab.cflow_type = 2030 -- Mov Acciones Afore A2
	AND ab.buy_sell = 0 --buy
	AND ab.tran_status = 3
