-- pf_sadfi_ing			

SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	ab.internal_bunit AS business_unit,
	ab.position AS orig_item_amt,
	ab.external_bunit AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency AS currency_cd,
	external_party.short_name AS bank_cd,
	CASE WHEN(ab.currency = 70) THEN account_num_bancomer.bank_account_num
		WHEN(ab.currency = 0) THEN account_num_statestreet.bank_account_num
		ELSE '00000000000000000'
	END AS bank_account_num,
	'T005' AS pf_id_proceso,
	'0' AS secno,
	'N' AS status,
	'SADFI' AS origin_id,
	ab.currency AS currency, -- para visual rate
	ab.trade_date AS trade_date --para visual rate
FROM ab_tran ab
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	LEFT JOIN party external_party
		ON (ab.external_bunit = external_party.party_id)
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN user_mx_catalogo13 account_num_bancomer
		ON (internal_party.short_name = account_num_bancomer.internal_bunit_name
			AND account_num_bancomer.external_bunit_name = 'BBVA BANCOMER - BU')
	LEFT JOIN user_mx_catalogo13 account_num_statestreet
		ON (internal_party.short_name = account_num_statestreet.internal_bunit_name
			AND account_num_statestreet.external_bunit_name = 'STATESTREET - BU')
WHERE
	ab.toolset = 10 -- cash
	AND ab.tran_type = 0 -- trading
	AND (ab.cflow_type = 2018
		OR ab.cflow_type = 271) -- AIMS o Margin Call AIMS
	AND ab.buy_sell = 1 -- sell
	AND ab.tran_status=3 --Validated