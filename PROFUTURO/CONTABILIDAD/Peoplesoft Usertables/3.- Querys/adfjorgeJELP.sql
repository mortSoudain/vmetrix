
			WITH 
			dif_liq AS (
					SELECT ab.trade_date, ab.deal_tracking_num, ab.tran_num, ab.position, p.name as portfolio
					FROM ab_tran ab
						INNER JOIN ab_tran_info_view estado_dif_liq
							ON (ab.tran_num = estado_dif_liq.tran_num  
								AND estado_dif_liq.type_name = 'Trad Tran Estado Liquidacion'
								AND estado_dif_liq.value = 'Diferencia_Liquidacion')
						-- Protfolio name
						LEFT JOIN portfolio p
							ON(ab.internal_portfolio=p.id_number)
					WHERE
						ab.toolset = 10 -- cash
						AND ab.tran_status = 3 -- validated
				),

			resumen AS (
				SELECT
						CASE WHEN(SIGN(SUM(dif_liq.position))=1) -- deposito
							THEN 'Depositos'
							ELSE 'Retiros'
						END AS saldo,
						dif_liq.portfolio,
						SUM(dif_liq.position) as position
					FROM dif_liq
					GROUP BY
						'Depositos',
						dif_liq.portfolio
				),
       
        max_trade_date as (
					SELECT
						MAX(dif_liq.trade_date) as trade_date,
						dif_liq.portfolio
					FROM dif_liq
					GROUP BY
						dif_liq.portfolio
				),
        max_tran_num as (
					SELECT
						MAX(dif_liq.tran_num) as tran_num,
						dif_liq.portfolio
					FROM dif_liq, max_trade_date
          WHERE dif_liq.trade_date = max_trade_date.trade_date AND dif_liq.portfolio = max_trade_date.portfolio
					GROUP BY
						dif_liq.portfolio
				)

			select * from max_tran_num JOIN resumen on (max_tran_num.portfolio = resumen.portf
