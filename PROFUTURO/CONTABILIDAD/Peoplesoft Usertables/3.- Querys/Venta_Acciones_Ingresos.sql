UNIVERSO

TRAN_STATUS 			?
INS_NUM					?
TRADE_DATE				?
CFLOW_TYPE 				MOV ACCIONES AFORE A2
TOOLSET					CASH
BUY_SELL 				SELL

--ing
SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) AS business_unit,
	ab.position AS orig_item_amt,
	'CLT0000022' AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency AS currency_cd,
	'12' AS bank_cd,
	'764C'  AS bank_account_num,
	'T001A' AS pf_id_proceso,
	'0' AS secno,
	'1' AS visual_rate,
	'SADFI' AS origin_id

FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)

	--portfolio name
	
	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)

WHERE 1=1
	AND ab.toolset = 10 -- cash
	AND ab.cflow_type = 2030 -- Mov Acciones Afore A2
	AND ab.buy_sell = 1 --sell
	AND ab.tran_status = 3
