-- hdrcif
-- + deposito
-- - retiro


-- deposito y retiro custodio 
SELECT

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN 'Deposito'
		ELSE 'Retiro'
	END AS saldo,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN 'PS' || portfolio.name
		ELSE 'PAF01'
	END AS business_unit,

	'SAD' as origin,

	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,

	ab.tran_status AS pf_status_cif,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN 'PROFUTURO AFORE SA DE CV'
		ELSE internal_party.long_name
	END AS pf_nom_ben,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN '40012'
		ELSE '90902'
	END AS bank_cd,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN '90218000190022000'
		ELSE SUBSTR(pibunit_account_number.account_number,1,17)
	END AS bank_account_num,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN '4'
		ELSE 	SUBSTR(pibunit_account_number.account_number,18,18)
	END AS check_digit,

	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	ab.position AS pymnt_amt,
	'MXN' AS currency_cd,
	'40' AS pf_tipo_cuenta,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN '07'
		ELSE '51'
	END AS pfcodtipopago,

	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS entered_dt,

	' ' AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'INDVL' AS id_tipo_instru,

	CASE WHEN(SIGN(ab.position)=1) -- deposito
		THEN 'DVLMXP'
		ELSE 'DVLAMX'
	END AS id_subtipo_instru

FROM ab_tran ab

	-- Internal BU name
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)

	-- Protfolio name

	LEFT JOIN portfolio
		ON(ab.internal_portfolio=portfolio.id_number)

	-- Internal account number

	LEFT JOIN ab_tran_settle_view absettlev
		ON (ab.deal_tracking_num = absettlev.deal_tracking_num
			AND absettlev.int_account_id!=0
			AND absettlev.TRAN_STATUS=3) -- validated

	LEFT JOIN account pibunit_account_number
		ON (pibunit_account_number.account_id = absettlev.int_account_id)

	-- Usuario que pasó a validada la transacción

	LEFT JOIN (	SELECT ab_tran_history.*
			FROM ab_tran_history
			INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
			            FROM ab_tran_history
			            WHERE TRAN_STATUS= 3
			            GROUP BY TRAN_NUM) AUX_TABLE 
							ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
							AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
			ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM

WHERE

	ab.toolset = 10 -- cash
	AND ab.tran_status = 3 -- validated
	AND ab.cflow_type IN (2041,2042) -- retiro custodio, deposito custodio

	AND ab.trade_date = HOY xd