SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	ab.internal_bunit AS business_unit,
	CAST (CASE WHEN (ab.external_bunit=20045) THEN profile.pymt
      WHEN (ab.external_bunit=20080) THEN profile.pymt+physcash.cflow
      WHEN (ab.external_bunit = 20037 OR ab.external_bunit = 20406) THEN ab2.var
      ELSE 0
	END AS FLOAT) AS orig_item_amt,
	ab.external_bunit as cust_id,
	TO_CHAR(ab.maturity_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	'MXN' AS currency_cd,
	'BBVA Bancomer - BU' AS bank_cd,
	account_num.bank_account_num AS bank_account_num,
	CASE WHEN (ab.external_bunit=20045) THEN 'T009I'
      WHEN (ab.external_bunit=20080) THEN 'T009'
      WHEN (ab.external_bunit = 20037 OR ab.external_bunit = 20406) THEN ab2.pf_id_proceso
      ELSE '0'
	END AS pf_id_proceso,
	CASE WHEN (ab.external_bunit=20045) THEN '0'
      WHEN (ab.external_bunit=20080) THEN '1'
      WHEN (ab.external_bunit = 20037 OR ab.external_bunit = 20406) THEN ab2.seqno
      ELSE '0'
	END AS seqno,
	'N' AS status,
	'1' AS visual_rate,
	'SADFI' AS origin_id
FROM ab_tran ab
	LEFT JOIN party internal_party
		ON (ab.internal_bunit = internal_party.party_id)
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN user_mx_catalogo13 account_num
		ON (account_num.internal_bunit_name = internal_party.short_name
			AND account_num.external_bunit_name = 'BBVA BANCOMER - BU')
	LEFT JOIN (
		SELECT ab.tran_num, ab.external_bunit, physcash.cflow var, 'T009' AS pf_id_proceso, '0' as seqno
			FROM ab_tran ab JOIN physcash ON (ab.ins_num = physcash.ins_num AND physcash.cflow_type=26)
			WHERE (ab.external_bunit IN(20037,20406))
    	UNION
    	SELECT ab.tran_num, ab.external_bunit, profile.pymt var, 'T009I' AS pf_id_proceso, '1' as seqno
			FROM ab_tran ab JOIN profile ON (ab.ins_num=profile.ins_num)
			WHERE (ab.external_bunit IN(20037,20406))
	) ab2 ON ab.tran_num=ab2.tran_num
	LEFT JOIN physcash 
		ON (ab.ins_num = physcash.ins_num AND physcash.cflow_type=26)
	LEFT JOIN profile
		ON (ab.ins_num=profile.ins_num)
WHERE
	ab.toolset = 6 -- LoanDep
	AND ab.tran_type = 0 -- trading
	AND ab.ins_type=1000056 -- Depo-Chequeras
	AND ab.tran_status=3 --Validated