-- pf_sadfi_hdrcif

SELECT
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	ab.tran_status AS pf_status_cif,
	ab.external_bunit AS pf_nom_ben,
	peiv.value AS bank_cd,
	CASE WHEN(party_external.party_id = 20045) -- PARTY EXTERNAL BANCOMER
			THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=18)
					THEN SUBSTR(pibunit_account_number.account_number,1,17)
					ELSE pibunit_account_number.account_number
				END
			ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18)
					THEN SUBSTR(pebunit_account_number.account_number,1,17)
					ELSE pebunit_account_number.account_number
				END
		END AS bank_account_num,

	CASE WHEN(party_external.party_id = 20045) -- PARTY EXTERNAL BANCOMER
		THEN CASE WHEN(LENGTH(pibunit_account_number.account_number)>=18)
				THEN SUBSTR(pibunit_account_number.account_number,18,18)
				ELSE '0'
			END
		ELSE CASE WHEN(LENGTH(pebunit_account_number.account_number)>=18)
				THEN SUBSTR(pebunit_account_number.account_number,18,18)
				ELSE '0'
			END
	END AS check_digit,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS pymnt_dt,
	fx_aux.c_amt AS pymnt_amt,
	'MXN' AS currency_cd,
	'40' AS pf_tipo_cuenta,
	CASE WHEN(party_external.party_id = 20045)
		THEN '15'
		ELSE '07'
	END AS pfcodtipopago,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entered_dt,
	tran_info_hora.value AS op_start_time,
	ab.internal_contact AS oprid_entered_by,
	ab_tran_approved_by.personnel_id AS op_approved_by,
	'CPADV' AS id_tipo_instru,
	'CPADV' AS id_subtipo_instru
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN party party_external
		ON (ab.external_bunit = party_external.party_id)
	LEFT JOIN party_account pebunit_account
		ON (ab.external_bunit = pebunit_account.party_id)
	LEFT JOIN account pebunit_account_number
		ON (pebunit_account.account_id = pebunit_account_number.account_id
			AND pebunit_account_number.account_status = 1)

	LEFT JOIN ab_tran_settle_view absettlev
		ON (ab.deal_tracking_num = absettlev.deal_tracking_num
			AND absettlev.int_account_id!=0
			AND absettlev.TRAN_STATUS=3)

	LEFT JOIN account pibunit_account_number
		ON (pibunit_account_number.account_id = absettlev.int_account_id)

	LEFT JOIN ab_tran_info_view tran_info_hora
		ON (ab.tran_num = tran_info_hora.tran_num  
			AND tran_info_hora.type_name = 'Trad Tran Hora de Concertacion')
	LEFT JOIN (	SELECT ab_tran_history.*
				FROM ab_tran_history
				INNER JOIN (SELECT TRAN_NUM, MAX(VERSION_NUMBER) AS MAX_VN
				            FROM ab_tran_history
				            WHERE TRAN_STATUS= 3
				            GROUP BY TRAN_NUM) AUX_TABLE 
								ON ab_tran_history.TRAN_NUM = AUX_TABLE.TRAN_NUM 
								AND ab_tran_history.VERSION_NUMBER = AUX_TABLE.MAX_VN)
				ab_tran_approved_by ON AB.TRAN_NUM = ab_tran_approved_by.TRAN_NUM
	LEFT JOIN party_info_view peiv
		ON (ab.external_bunit = peiv.party_id
			AND peiv.type_name = 'Id_Consar')
	LEFT JOIN currency curr_name
		ON(ab.currency = curr_name.id_number)
	LEFT JOIN fx_tran_aux_data fx_aux
		ON fx_aux.tran_num = ab.tran_num
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 0 -- buy
	AND ab.tran_status = 3 --Validated

-- pf_sadfi_detcif
SELECT	
	ab.internal_bunit AS business_unit,
	'SAD' AS origin,
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS pf_tran_id,
	'1' AS seqno,
	'CPADV' AS pf_tipo_afectacion,
	fx_aux.c_amt AS amt_sel, 
	'100' AS percentage,
	'1' AS visual_rate 
FROM ab_tran ab
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN fx_tran_aux_data fx_aux
		ON fx_aux.tran_num = ab.tran_num
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 0 -- buy
	AND ab.tran_status=3 --Validated

-- pf_sadfi_ing			

SELECT
	CONCAT(
		CONCAT(
			TO_CHAR(ab.settle_date, 'YYYYMMDD'),
			portfolio.name	
		),ab.tran_num
	) AS item,
	ab.internal_bunit AS business_unit,
	ab.position AS orig_item_amt,
	ab.external_bunit AS cust_id,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS asof_dt,
	TO_CHAR(ab.settle_date, 'DD-MM-YYYY') AS accounting_dt,
	TO_CHAR(ab.trade_date, 'DD-MM-YYYY') AS entry_dt,
	'CONT' AS pymnt_terms_cd,
	ab.currency AS currency_cd,
	'STATESTREET - BU' AS bank_cd,
	account_num.bank_account_num  AS bank_account_num,
	id_proceso.pf_id_proceso AS pf_id_proceso,
	'0' AS seqno,
	'N' AS status,
	ab.rate AS visual_rate,
	'SADFI' AS origin_id
FROM ab_tran ab
	LEFT JOIN party internal_party
		ON(ab.internal_bunit = internal_party.party_id)
	LEFT JOIN currency curr_name
		ON(ab.currency = curr_name.id_number)
	LEFT JOIN portfolio
		ON (ab.internal_portfolio = portfolio.id_number)
	LEFT JOIN user_mx_catalogo13 account_num
		ON (account_num.internal_bunit_name =  internal_party.short_name
			AND account_num.external_bunit_name = 'STATESTREET - BU')
	LEFT JOIN user_mx_catalogo14_2 id_proceso
		ON (curr_name.name = id_proceso.currency_name)
WHERE
	ab.toolset = 9 -- fx
	AND ab.tran_type = 0 -- trading
	AND ab.buy_sell = 0 -- buy
	AND ab.tran_status=3 --Validated


