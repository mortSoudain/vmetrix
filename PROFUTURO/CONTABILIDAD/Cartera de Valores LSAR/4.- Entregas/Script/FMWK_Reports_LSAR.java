/*$Header: v 1.1, 31/Jan/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Peoplesoft_User_Tables.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			15 Noviembre 2017
 * Version:					1.0
 * Description:				
 * 
 * Related Files/Configurations:
 * 							- Workflow EOD Manual	:	15.- EOD - Reportes Contables
 * 															WFLOW - Task Afore - Reportes LSAR
 * 							- RPT File				:	ReporteLSAR.rpt
 * 							- Reports Outdir		:	X:\OpenLink\outdir\LSAR
 *                       
 * REVISION HISTORY
 * Date:                    31 Enero 2018
 * Version/Autor:           1.1 Camilo Stuardo Mülchi - VMetrix SpA.
 * Description:            	- Se agrega a la query de la deuda gubernamental que solo tome
 * 							  transacciones con tran_type TRADING y asset_type TRADING.
 *                         
 ************************************************************************************/
package com.afore.custom_reports;

import java.io.IOException;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCountry;
import com.afore.enums.EnumsInsGroups;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsParty;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SystemUtil;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.SIMULATION_RUN_TYPE;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Reports_LSAR implements IScript {
	
	// Declare utils and general variables
		String		sScriptName		=	this.getClass().getSimpleName();
		UTIL_Log	LOG				=	new UTIL_Log(sScriptName);
		UTIL_Afore	UtilAfore		=	new UTIL_Afore();
	
	// Declare time variables
		int			iToday			=	0;
		String		sTodayDefault	=	"";
	
	// Declare global tables
		Table		tData			=	Util.NULL_TABLE;
            
	public void execute(IContainerContext context) throws OException {
		
		LOG.markStartScript();
		
			// Initialize Global Variables
			initializeScript();
				
			// Setting initial data
			setDataOnTable(tData);
			
			// Select distinct portfolios
			Table tDistinctPortfolios = Table.tableNew("Distinct Portfolios");
			tDistinctPortfolios.select(tData, "DISTINCT, portfolio", "titulos GT 0");
			
			// Generate report for each portfolio on query
			for(int i = 1; i<= tDistinctPortfolios.getNumRows(); i++){
				int iPortfolio = tDistinctPortfolios.getInt("portfolio", i);
				try {
					generarReporte(tData, iPortfolio);
				} catch (IOException e) {
					Ask.ok(e.getMessage());
				}
			}
		
		LOG.markEndScript();
	}
	
	private void initializeScript() {
		
		//Initialize time variables
		try {
			int iHoy		= OCalendar.today();
			int iJueves		= OCalendar.getLgbd(iHoy);
			iToday			= OCalendar.getLgbd(iJueves);
			sTodayDefault	= OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DEFAULT);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to initialize time variables");
		}
		
		//Initialize global tables
		try {
			tData = Table.tableNew("Data Table");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to initialize time variables");
		}
		
	}

	private Table formatData(Table tUnformattedTable) throws OException {
		
		// Declare query filter parameters
		int iFlag;
		String sInsType;
		String sTicker;
		
		String sClasificacionGubernamental;
		String sTipoInstrumento;
		String sContieneTicker;
		
		//Initialize aux table		
		Table tClasificacionGubernamental = Table.tableNew();
		String sQueryClasificacionGubernamental = 
				"\n SELECT "+
				"\n 	clasificacion_gubernamental, "+
				"\n 	tipo_instrumento, "+
				"\n 	ticker "+
				"\n FROM "+ EnumsUserTables.USER_MX_CLASIFIC_GUB_LSAR.toString();
		
		DBaseTable.execISql(tClasificacionGubernamental, sQueryClasificacionGubernamental);
		
		// Search for every deal tagged with flag = 1 (deuda gubernamental)
		for( int iRow = 1 ; iRow < tUnformattedTable.getNumRows(); iRow++){
			
			// Initialize query filter parameters
			iFlag = -1;
			sInsType = "";
			sTicker = "";
			
			sClasificacionGubernamental = "";
			sTipoInstrumento = "";
			sContieneTicker = "";
			
			iFlag = tUnformattedTable.getInt("flag", iRow);
			sInsType = tUnformattedTable.getString("tipo_de_instrumento", iRow);
			sTicker = tUnformattedTable.getString("ticker", iRow);
			
			//If concept is deuda gubernamental
			if (iFlag==1){
				
				//Search for alias for instrument on user table
				for( int jRow = 1 ; jRow < tClasificacionGubernamental.getNumRows(); jRow++){
					sClasificacionGubernamental = tClasificacionGubernamental.getString("clasificacion_gubernamental", jRow);
					sTipoInstrumento = tClasificacionGubernamental.getString("tipo_instrumento", jRow);
					sContieneTicker = tClasificacionGubernamental.getString("ticker", jRow);
					
					// If comparative containing ticker is empty or null, just compare instrument
					if(sContieneTicker!=null && !sContieneTicker.equals("")){
						if(sInsType.equals(sTipoInstrumento) && sTicker.contains(sContieneTicker)){
							tUnformattedTable.setString("tipo_de_instrumento", iRow, sClasificacionGubernamental);
						}
					} else {
						if(sInsType.equals(sTipoInstrumento)){
							tUnformattedTable.setString("tipo_de_instrumento", iRow, sClasificacionGubernamental);
						}
					}
				}	
				
			}

		}

		return tUnformattedTable;
		
	}

	private void generarReporte(Table tData, int iPortfolio) throws OException, IOException {
	
		String sPortfolio = Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, iPortfolio);
		
		Table tOutput = Table.tableNew();
		tOutput.select(tData, "*", "flag NE 6 AND portfolio EQ " + iPortfolio);		
	
		//Setting up parameter table
			Table tParameters = Table.tableNew("Parametros Crystal");
			int iNewRow = tParameters.addRow();
			
			tParameters.addCol("varPortfolio", COL_TYPE_ENUM.COL_STRING);
			tParameters.addCol("varFecha", COL_TYPE_ENUM.COL_DATE_TIME);		
			
			tParameters.setString("varPortfolio", iNewRow, sPortfolio);
			tParameters.setDateTime("varFecha", iNewRow, ODateTime.strToDateTime(sTodayDefault));
		
		// Configure filenames and paths
			String sTemplatePath		=	Crystal.getRptDir();
			String sTemplateFileName	=	"ReporteLSAR";
			String sOutputPath			=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_LSAR", "path");
			String sOutputFileName		=	"ReporteLSAR-"+sPortfolio+"-"+sTodayDefault;
			
			String sTemplateFullFileName	=	sTemplatePath+"\\"+sTemplateFileName+".rpt";
			String sOutputFullFileNameXLS	=	sOutputPath+sOutputFileName+".xls";
			String sOutputFullFileNamePDF	=	sOutputPath+sOutputFileName+".pdf";
			String sOutputFullFileNameDOC	=	sOutputPath+sOutputFileName+".doc";
		
		//Exporting excel
		Crystal.tableExportCrystalReport(tOutput, 
				sTemplateFullFileName, 
				CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
				sOutputFullFileNameXLS, 
				CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
				tParameters);
		
		//Exporting pdf
		Crystal.tableExportCrystalReport(tOutput, 
				sTemplateFullFileName, 
				CRYSTAL_EXPORT_TYPES.PDF, 
				sOutputFullFileNamePDF, 
				CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
				tParameters);
		
		//Exporting doc
		Crystal.tableExportCrystalReport(tOutput, 
				sTemplateFullFileName, 
				CRYSTAL_EXPORT_TYPES.MS_WORD, 
				sOutputFullFileNameDOC, 
				CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
				tParameters);				
	}

	private void setDataOnTable(Table tDisplay) throws OException {
		
		Table tTable = Table.tableNew();
		
		//Query Filters
			int iTranStatusValidated				= TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
			int iToolsetBonds						= TOOLSET_ENUM.BOND_TOOLSET.toInt();//5
			int iToolsetEquity						= TOOLSET_ENUM.EQUITY_TOOLSET.toInt();//28	
			int iTranTypeHolding					= TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt();//2
			int iTranTypeTrading					= TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();//[1.1]CS31012018
			int iAssetTypeTrading					= ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt();//[1.1]CS31012018
//			int iExternalLentityGobFederal			= EnumsParty.mx;
//			int iExternalLentityTesoreria			= 0;
			int iCountryMexico						= EnumsCountry.MX_COUNTRY_MEXICO.toInt();//20040
			int iInsGroupRentaVariableNacional		= EnumsInsGroups.MX_RENTA_VARIABLE_NACIONAL.toInt();//20016
			int iInsGroupRentaVariableExtranjero	= EnumsInsGroups.MX_RENTA_VARIABLE_EXTRANJERO.toInt();//20015
		
		String sQueryDeudaGubernamental = 
				"\n SELECT "+
						"\n   AB.DEAL_TRACKING_NUM, "+
						"\n   CAST(AB.mvalue AS FLOAT) AS TITULOS, "+
						"\n   I.NAME AS TIPO_DE_INSTRUMENTO, "+
						"\n   AB.INTERNAL_PORTFOLIO AS PORTFOLIO, "+
						"\n   'DEUDA GUBERNAMENTAL' AS CONCEPTO, "+
						"\n   h.ticker AS TICKER, "+
						"\n   1 AS FLAG "+
						"\n FROM AB_TRAN AB "+
						"\n   LEFT JOIN header h "+
						"\n   	ON(ab.ins_num = h.ins_num) "+
						"\n   INNER JOIN AB_TRAN AB2 "+
						"\n     ON (AB.INS_NUM = AB2.INS_NUM "+
						"\n         AND AB2.EXTERNAL_LENTITY IN (20392,20022) --GOB FEDERAL O TESORERIA "+
						"\n         AND AB2.TRAN_TYPE = "+ iTranTypeHolding +")-- HOLDING "+
						"\n   LEFT JOIN INSTRUMENTS I "+
						"\n     ON (AB.INS_TYPE = I.ID_NUMBER) "+
						"\n WHERE 1=1 "+
						"\n   AND AB.INTERNAL_PORTFOLIO !=-1 "+
						"\n   AND AB.TRAN_STATUS = "+ iTranStatusValidated +" -- VALIDATED "+
						"\n   AND AB.TRAN_TYPE = "+ iTranTypeTrading +" -- TRADING "+//[1.1]CS31012018
						"\n   AND AB.ASSET_TYPE = "+ iAssetTypeTrading +" -- TRADING "+//[1.1]CS31012018
						"\n   AND AB.TOOLSET = "+ iToolsetBonds +" --BONDS ";
		
		String sQueryDeudaPrivadaNacional =
				"\n -- DEUDA PRIVADA NACIONAL "+
						"\n SELECT "+
						"\n   AB.DEAL_TRACKING_NUM, "+
						"\n   CAST(AB.mvalue AS FLOAT) AS TITULOS, "+
						"\n   PARTYINDUSTRIA.VALUE AS TIPO_DE_INSTRUMENTO, "+
						"\n   AB.INTERNAL_PORTFOLIO AS PORTFOLIO, "+
						"\n   'DEUDA PRIVADA NACIONAL' AS CONCEPTO, "+
						"\n   2 AS FLAG "+
						"\n FROM AB_TRAN AB "+
						"\n   INNER JOIN AB_TRAN AB2 "+
						"\n     ON (AB.INS_NUM = AB2.INS_NUM "+
						"\n         AND AB2.TRAN_TYPE = "+ iTranTypeHolding +" -- HOLDING "+
						"\n         AND AB2.EXTERNAL_LENTITY NOT IN (20392,20022)) -- NOT GOB FEDERAL NI TESORERIA "+
						"\n   INNER JOIN PARTY HOLDINGPARTY "+
						"\n     ON (HOLDINGPARTY.PARTY_ID = AB2.EXTERNAL_LENTITY) "+
						"\n   INNER JOIN PARTY_ADDRESS PARTYHOLDINGCOUNTRY "+
						"\n     ON (HOLDINGPARTY.PARTY_ID = PARTYHOLDINGCOUNTRY.PARTY_ID AND PARTYHOLDINGCOUNTRY.COUNTRY = "+ iCountryMexico +") -- COUNTRY MEXICO "+
						"\n   INNER JOIN PARTY_INFO_VIEW PARTYINDUSTRIA "+
						"\n    ON (HOLDINGPARTY.PARTY_ID = PARTYINDUSTRIA.PARTY_ID AND PARTYINDUSTRIA.TYPE_NAME = 'MX Industria') "+
						"\n WHERE 1=1 "+
						"\n   AND AB.INTERNAL_PORTFOLIO !=-1 "+
						"\n   AND AB.TRAN_STATUS = "+ iTranStatusValidated +" "+
						"\n   AND AB.TOOLSET = "+ iToolsetBonds +" --BONDS ";
		
		String sQueryDeudaInternacional =
				"\n -- DEUDA INTERNACIONAL "+
						"\n SELECT "+
						"\n   AB.DEAL_TRACKING_NUM, "+
						"\n   CAST(AB.mvalue AS FLOAT) AS TITULOS, "+
						"\n   'Deuda Internacional' AS TIPO_DE_INSTRUMENTO, "+
						"\n   AB.INTERNAL_PORTFOLIO AS PORTFOLIO, "+
						"\n   'DEUDA INTERNACIONAL' AS CONCEPTO, "+
						"\n   3 AS FLAG "+
						"\n FROM AB_TRAN AB "+
						"\n   INNER JOIN AB_TRAN AB2 "+
						"\n     ON (AB.INS_NUM = AB2.INS_NUM "+
						"\n         AND AB2.TRAN_TYPE = "+ iTranTypeHolding +" -- HOLDING "+
						"\n         AND AB2.EXTERNAL_LENTITY NOT IN (20392,20022))  -- NOT GOB FED O TESORERIA "+
						"\n   INNER JOIN PARTY HOLDINGPARTY "+
						"\n     ON (HOLDINGPARTY.PARTY_ID = AB2.EXTERNAL_LENTITY) "+
						"\n   INNER JOIN PARTY_ADDRESS PARTYHOLDINGCOUNTRY "+
						"\n     ON (HOLDINGPARTY.PARTY_ID = PARTYHOLDINGCOUNTRY.PARTY_ID AND PARTYHOLDINGCOUNTRY.COUNTRY != "+ iCountryMexico +") -- COUNTRY DISTINTO A MEXICO "+
						"\n   INNER JOIN PARTY_INFO_VIEW PARTYINDUSTRIA "+
						"\n    ON (HOLDINGPARTY.PARTY_ID = PARTYINDUSTRIA.PARTY_ID AND PARTYINDUSTRIA.TYPE_NAME = 'MX Industria') "+
						"\n WHERE 1=1 "+
						"\n   AND AB.INTERNAL_PORTFOLIO !=-1 "+
						"\n   AND AB.TRAN_STATUS = "+ iTranStatusValidated +" -- VALIDATED "+
						"\n   AND AB.TOOLSET = "+ iToolsetBonds +" --BONDS ";
		
		String sQueryRentaVariableNacional =
				"\n -- RENTA VARIABLE NACIONAL "+
						"\n SELECT "+
						"\n   AB.DEAL_TRACKING_NUM, "+
						"\n   CAST(AB.mvalue AS FLOAT) AS TITULOS, "+
						"\n   IG.ins_group_name AS TIPO_DE_INSTRUMENTO, "+
						"\n   AB.INTERNAL_PORTFOLIO AS PORTFOLIO, "+
						"\n   'RENTA VARIABLE NACIONAL' AS CONCEPTO, "+
						"\n   4 AS FLAG "+
						"\n FROM AB_TRAN AB "+
						"\n   INNER JOIN HEADER H "+
						"\n     ON (AB.INS_NUM = H.INS_NUM) "+
						"\n   INNER JOIN INS_GROUPS IG "+
						"\n     ON (H.INS_GROUP_ID = IG.INS_GROUP_ID AND IG.INS_GROUP_ID = "+ iInsGroupRentaVariableNacional +") -- Renta Variable Nacional "+
						"\n WHERE 1=1 "+
						"\n   AND AB.INTERNAL_PORTFOLIO !=-1 "+
						"\n   AND AB.TRAN_STATUS = "+ iTranStatusValidated +" -- validated "+
						"\n   AND AB.TOOLSET = "+ iToolsetEquity +" -- equity ";
		
		String sQueryRentaVariableInternacional =
				"\n -- RENTA VARIABLE INTERNACIONAL "+
						"\n SELECT "+
						"\n   AB.DEAL_TRACKING_NUM, "+
						"\n   CAST(AB.mvalue AS FLOAT) AS TITULOS, "+
						"\n   IG.ins_group_name AS TIPO_DE_INSTRUMENTO, "+
						"\n   AB.INTERNAL_PORTFOLIO AS PORTFOLIO, "+
						"\n   'RENTA VARIABLE INTERNACIONAL' AS CONCEPTO, "+
						"\n   5 AS FLAG "+
						"\n FROM AB_TRAN AB "+
						"\n   INNER JOIN HEADER H "+
						"\n     ON (AB.INS_NUM = H.INS_NUM) "+
						"\n   INNER JOIN INS_GROUPS IG "+
						"\n     ON (H.INS_GROUP_ID = IG.INS_GROUP_ID AND IG.INS_GROUP_ID = "+ iInsGroupRentaVariableExtranjero +") --RENTA VARIABLE EXTRANJERO "+
						"\n WHERE 1=1 "+
						"\n   AND AB.INTERNAL_PORTFOLIO !=-1 "+
						"\n   AND AB.TRAN_STATUS = "+ iTranStatusValidated +" -- VALIDATED "+
						"\n   AND AB.TOOLSET = "+ iToolsetEquity +" -- EQUITY ";
		
		Table tDeudaGubernamental = Table.tableNew();
		Table tDeudaPrivadaNacional = Table.tableNew();
		Table tDeudaInternacional = Table.tableNew();
		Table tRentaVariableNacional = Table.tableNew();
		Table tRentaVariableInternacional = Table.tableNew();
		
		DBaseTable.execISql(tDeudaGubernamental, sQueryDeudaGubernamental);
		DBaseTable.execISql(tDeudaPrivadaNacional, sQueryDeudaPrivadaNacional);
		DBaseTable.execISql(tDeudaInternacional, sQueryDeudaInternacional);
		DBaseTable.execISql(tRentaVariableNacional, sQueryRentaVariableNacional);
		DBaseTable.execISql(tRentaVariableInternacional, sQueryRentaVariableInternacional);
		
		tTable.select(tDeudaGubernamental, "*", "deal_tracking_num GT 0");
		tTable.select(tDeudaPrivadaNacional, "*", "deal_tracking_num GT 0");
		tTable.select(tDeudaInternacional, "*", "deal_tracking_num GT 0");
		tTable.select(tRentaVariableNacional, "*", "deal_tracking_num GT 0");
		tTable.select(tRentaVariableInternacional, "*", "deal_tracking_num GT 0");
		
		// Format data
		Table tFormattedTable = Table.tableNew();
		tFormattedTable = formatData(tTable);
		
		getIntraDaySIM(tFormattedTable, EnumsUserSimResultType.MX_SIM_ACCT_RESULT_CT_MERC_PRECIO_SUCIO, iToday, "costo_mercado");
		
		Table tResumeTable = Table.tableNew();
		tResumeTable.select(tFormattedTable, "DISTINCT, tipo_de_instrumento, portfolio, concepto, flag", "deal_tracking_num GT 0");		
		tResumeTable.select(tFormattedTable, "SUM, titulos, costo_mercado", "tipo_de_instrumento EQ $tipo_de_instrumento "
				+ "AND portfolio EQ $portfolio "
				+ "AND concepto EQ $concepto "
				+ "AND flag EQ $flag");
		
		
		// Setting resume lines for each portfolio
		Table tDistinctPortfolios = Table.tableNew();
		tDistinctPortfolios.select(tFormattedTable, "DISTINCT, portfolio", "deal_tracking_num GT 0");
		
		for (int i = 1 ; i<=tDistinctPortfolios.getNumRows() ; i++){
			
			int iPortfolio = tDistinctPortfolios.getInt("portfolio", i);
			
			Table tAuxTitulos = Table.tableNew();
			tAuxTitulos.select(tResumeTable, "SUM, titulos", "portfolio EQ "+iPortfolio);
			
			Table tAuxCostoTotalMercado = Table.tableNew();
			tAuxCostoTotalMercado.select(tResumeTable, "SUM, costo_mercado", "portfolio EQ "+iPortfolio);
			
			int iRow = tResumeTable.addRow();
			tResumeTable.setDouble("titulos", iRow, tAuxTitulos.getDouble("titulos", 1));
			tResumeTable.setInt("portfolio", iRow, iPortfolio);
			tResumeTable.setString("concepto", iRow, "TOTAL");
			tResumeTable.setInt("flag", iRow, 6);
			tResumeTable.setDouble("costo_mercado", iRow, tAuxCostoTotalMercado.getDouble("costo_mercado", 1));
			
		}
		
		// Setting percent values
			Table tAuxTotales = Table.tableNew();
			tAuxTotales.select(tResumeTable, "portfolio, costo_mercado", "concepto EQ TOTAL");
			tAuxTotales.setColName("costo_mercado", "costo_total_mercado");
			tResumeTable.select(tAuxTotales, "costo_total_mercado", "portfolio EQ $portfolio");
			tResumeTable.addFormulaColumn("iif(COL('costo_total_mercado') == 0, 0, (COL('costo_mercado')*100)/COL('costo_total_mercado'))", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "porcentaje");
		

			tDisplay.select(tResumeTable, "titulos, tipo_de_instrumento, portfolio, concepto, flag, costo_mercado, porcentaje", "titulos GT 0");
		
		
	}
	
	public void getIntraDaySIM(Table tTable, EnumsUserSimResultType SimResType, 
			int iJulianDate, String sColDestino) throws OException{
	      
			// Setting portfolio table
			String sPortafolioQuery = "SELECT id_number, name as Portfolio FROM portfolio";
			Table tPortfolio = Table.tableNew();
			DBaseTable.execISql(tPortfolio, sPortafolioQuery);
			
			
			for (int i =1; i<tPortfolio.getNumRows();i++){
				int iPortfolio = tPortfolio.getInt("id_number", i);
			      Table tAuxSimResults = Util.NULL_TABLE;
			      
			      // Run simulation and get values
			      tAuxSimResults = SimResult.tableLoadSrun(iPortfolio, SIMULATION_RUN_TYPE.INTRA_DAY_SIM_TYPE, iJulianDate, 0);
			      Table tAuxTranResults = SimResult.getTranResults(tAuxSimResults);
			      
			      // Get col num of searched column
			      int iColSim = tAuxTranResults.getColNum("" + SimResType.toInt());
			      
			      // if the col is found
		            if (iColSim > 1){
		            	
		            	tAuxTranResults.setColName(iColSim, sColDestino);
		            	tTable.select(tAuxTranResults, sColDestino, "deal_num EQ $deal_tracking_num");
		            }
			}
	}

}
