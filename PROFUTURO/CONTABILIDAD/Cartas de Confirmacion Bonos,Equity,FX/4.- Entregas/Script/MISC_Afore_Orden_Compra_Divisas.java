/*$Header: v 1.3, 08/02/2018 $*/
/**
 * File Name:              MISC_Afore_Orden_Compra_Divisas.java
 * 
 * Author:                 Jorge Vergara - VMetrix International
 * Creation Date:          Agosto 2017
 * Version:                1.0
 * Description:            Genera PDF de Comprobante Compra Divisas (FX). 
 *
 * Date:                   18/08/2017
 * Version/Autor:          1.1 / Jorge Vergara
 * Description:            Se incorpora envio de correos. Utiliza FMWK de Cartas de Confirmacion para SendMail.
 * 
 * Date:                   30/01/2018
 * Version/Autor:          1.2 / Basthian Matthews
 * Description:            Se incorpora columna con el campo Buy_Sell para identificar compra o venta.
 *
 *
 * Date:                   08/02/2018
 * Version/Autor:          1.3 / Basthian Matthews
 * Description:            Se cambia el formato a la fecha y hora de operacion y liquidacion.
 * 
 ************************************************************************************/

package com.profuturo.misc_afore;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.fmwk_letters_mail.FMWK_Mail_Core;
import com.afore.fmwk_letters_model.FMWK_Model_Letters;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBase;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Transaction;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TRANF_FIELD;

public class MISC_Afore_Orden_Compra_Divisas implements IScript {

	String sScriptName = this.getClass().getSimpleName();	   
	UTIL_Log LOG = new UTIL_Log(sScriptName);
	String crystalTemplate = "";
	String outputDir = "";
	String outputFileName = "";
	int iToday= 0;
	String sToday= "";
	int iExternalBU=0, iDealNum=0;
	Transaction tran = null;
	
	@Override
	public void execute(IContainerContext context) throws OException {
		// TODO Auto-generated method stub

		LOG.markStartScript();
		Table tArgs = Util.NULL_TABLE;
		Table tData = Util.NULL_TABLE;
		Table tSourceData = Util.NULL_TABLE;
		Table tParam = Util.NULL_TABLE;
		String sWhere = "", sMonedaLiq="", sMonto="", sTranInfoType="", sDiasHabiles="", sBuySell="", sRowCreationNew="";
		int idCierreCotizacion=EnumsTranInfoFields.MX_TRAN_CIERRE_COTIZACION.toInt();;
		
		iToday= OCalendar.today();
		sToday= OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		
		tArgs = context.getArgumentsTable();
		//tData = context.getDataurnTable();

		if(Table.isTableValid(tArgs) != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt() || tArgs.getNumRows() <= 0){
			LOG.printMsg(EnumTypeMessage.ERROR, "Null Arguments - No documents selected for process...");
			Util.exitFail();
		}

		int iRow =0;

		iRow= tArgs.unsortedFindString("col_name", "*SourceEventData", SEARCH_CASE_ENUM.CASE_INSENSITIVE);

		if (iRow > 0){
			tData = Table.tableNew();
			tSourceData = tArgs.getTable("doc_table", iRow);

			if(Table.isTableValid(tSourceData) != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt() || tSourceData.getNumRows() <= 0){
				LOG.printMsg(EnumTypeMessage.ERROR, "Null Source Event Data - No documents selected for process...");
				Util.exitFail();
			}
			
			sTranInfoType = "tran_info_type_" + idCierreCotizacion;
			
			sWhere = "deal_tracking_num, tran_num, portfolio_id, internal_contact, tran_settle_date, external_bunit, "+ 
			"tran_currency, tran_position, tran_price, " + sTranInfoType;
			
			tData.select(tSourceData, sWhere, "deal_tracking_num GT 0");
			
			iDealNum = tData.getInt("deal_tracking_num", 1);
					
			tData.addCol("varFondo", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varOperador", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varMercado", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varIntermediario", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varPaisOperacion", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varTipoDivisa", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varMonedaLiq", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varMonto", COL_TYPE_ENUM.COL_DOUBLE);
			tData.addCol("varExtension", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varFechaOperacion", COL_TYPE_ENUM.COL_STRING);
			
			tData.addCol("varCompraVenta", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("varDiasHabiles", COL_TYPE_ENUM.COL_STRING);

			tData.setString("varFondo", 1, Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, tData.getInt("portfolio_id", 1)));
			tData.setString("varOperador", 1, Ref.getName(SHM_USR_TABLES_ENUM.PERSONNEL_TABLE, tData.getInt("internal_contact", 1)));
			tData.setString("varMercado", 1,"DIV. CAMBIOS(MEX)");
			
			iExternalBU = tData.getInt("external_bunit", 1);
			tData.setString("varIntermediario", 1, Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iExternalBU));
			
			tData.setString("varPaisOperacion", 1,"MEX");
			tData.setString("varTipoDivisa", 1, Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, tData.getInt("tran_currency", 1)));
			
			tData.setString("varExtension", 1, tData.getString(sTranInfoType, 1));
			
			//TODO Obtiene Moneda Liquidacion y Monto desde TranPointer (tambien obtiene dif de dias y campo buy_sell)
			tran = new Transaction(tData.getInt("tran_num", 1));
			
			sMonedaLiq = tran.getField(TRANF_FIELD.TRANF_BOUGHT_CURRENCY.toInt());
			sMonto = tran.getField(TRANF_FIELD.TRANF_FX_C_AMT.toInt());
			sBuySell = getCompraVenta();
			sDiasHabiles = getDiasHabiles();
			sRowCreationNew = getRowCreationNew();
			
			tData.setString("varMonedaLiq", 1, sMonedaLiq);
			tData.setDouble("varMonto", 1, Str.strToDouble(sMonto, 1));
			tData.setString("varCompraVenta", 1, sBuySell);
			tData.setString("varDiasHabiles", 1, sDiasHabiles);
			tData.setString("varFechaOperacion", 1, sRowCreationNew);

					
			//TODO Format Data
			this.formatData(tData);
			
			//TODO Setea parametros
			tParam = Table.tableNew();
			if (!this.setParameter(tParam))Util.exitFail();
			
			//TODO GENERA SALIDA CRYSTAL, PDF
			if (!this.genCrystal(tData, tParam)) Util.exitFail();
			LOG.printMsg(EnumTypeMessage.INFO, "PDF generated successfully - " + outputFileName);
			//SystemUtil.createProcess(outputFileName);
			
			//TODO ENVIA EMAIL
			if (!this.sendMail()) Util.exitFail();
			
		} else
			LOG.printMsg(EnumTypeMessage.ERROR, "Query Result doesn't contain Table *SourceEventData");

		if(Table.isTableValid(tData) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tData.destroy(); 
		if(Table.isTableValid(tParam) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tParam.destroy(); 
		LOG.markEndScript();
	}
	
	private String getRowCreationNew() throws OException {
		
		int iTranNum = this.tran.getFieldInt(TRANF_FIELD.TRANF_TRAN_NUM.toInt());
		
		Table tAux = Table.tableNew();
		StringBuilder sQueryAux = new StringBuilder();
		
		sQueryAux
			.append("\n     SELECT tran_num, TO_CHAR(row_creation, 'DD/MM/YYYY HH24:MI:SS') as row_creation ")
			.append("\n       FROM( ")
			.append("\n           SELECT tran_num, row_creation,   ")
			.append("\n           ROW_NUMBER() OVER (PARTITION BY tran_num ORDER BY version_number ASC) AS num_row  ")
			.append("\n           FROM ab_tran_history   ")
			.append("\n           WHERE tran_status IN ( 2 )   ")
			.append("\n           AND update_type =  9 ")
			.append("\n       ) tran_history  ")
			.append("\n     WHERE num_row = 1 ")
			.append("\n     AND tran_num = "+iTranNum);
		
		DBaseTable.execISql(tAux, sQueryAux.toString());
		
		String sRowCreation = tAux.getString("row_creation", 1);

		return sRowCreation;
	}

	@SuppressWarnings("finally")
	public boolean genCrystal(Table data, Table tParam) throws OException{
		boolean bRet = true;
		int iRet =0;
		
		UTIL_Afore util = new UTIL_Afore();
		
		LOG.printMsg(EnumTypeMessage.INFO, "Generating report, PDF format...");
		
		//Ejemplo outputFileName: C:\OpenLink\outdir\Confirmacion_FX_$DEAL_NUM.pdf
		crystalTemplate = util.getVariableGlobal("FINDUR", sScriptName, "crystal_template");
		outputFileName = util.getVariableGlobal("FINDUR", sScriptName, "output_filename");
		
		outputFileName = outputFileName.replace("$DEAL_NUM", ""+iDealNum);
		
		try {

			iRet = Crystal.tableExportCrystalReport(data, crystalTemplate, CRYSTAL_EXPORT_TYPES.PDF, outputFileName, CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR, tParam);
			
			if(iRet != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()){
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to generate Crystal Report: " + outputFileName);
				bRet = false;
			}
		}catch(NullPointerException np){
			LOG.printMsg(EnumTypeMessage.ERROR, "NPE::Unable to save output file, please check crystal_template/output_filename in 'user_configurable_variables'");
			bRet = false;
		}catch(OException oe){
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to generate Crystal Report: " + outputFileName + " ::: " + oe.getMessage());
			bRet = false;
		}finally{
			return bRet;
		}
	}

	public boolean setParameter(Table tParam) throws OException{
		boolean bReturn = true;
		tParam.addCol("parFecha", COL_TYPE_ENUM.COL_DATE_TIME);
		tParam.addCol("parUser", COL_TYPE_ENUM.COL_STRING);
		tParam.addRow();
		
		tParam.setDateTime("parFecha", 1, ODateTime.strToDateTime(sToday));
		tParam.setString("parUser", 1, Util.getEnv("AB_LOGON_USER"));
		
		return bReturn;
	}
	
	public void formatData(Table tData)throws OException{
		tData.convertColToString(tData.getColNum("deal_tracking_num"));
		
		tData.setColName("deal_tracking_num", "varFolio");
		tData.setColName("tran_settle_date", "varFechaLiquidacion");
		tData.setColName("tran_position", "varNroDivisas");
		tData.setColName("tran_price", "varTipoCambio");
		
	}
	
	public boolean sendMail() throws OException{
		boolean result=true;
		FMWK_Mail_Core mail = new FMWK_Mail_Core();
		FMWK_Model_Letters<?> model = new FMWK_Model_Letters<>();
		
		String correos = model.getCorreosDestinatario(iExternalBU);
		
		if(null != correos && !correos.isEmpty()){
		
			if(correos.lastIndexOf(";") == (correos.length()-1))
				correos = correos.substring(0, correos.length()-1);
			
			String sDate = ODateTime.getServerCurrentDateTime().toString();
			
					result = mail.sendEmailWithAttachment(
					correos, 
					sDate + "; " + iDealNum, 
					"", 
					outputFileName
			);
			
			if(result){
				LOG.printMsg(EnumTypeMessage.INFO, "Email enviado exitosamente a destinatario(s): " + correos);
			}else{
				LOG.printMsg(EnumTypeMessage.ERROR, "No es posible enviar Email a destinatario(s): " + correos);
				result = false;
			}
		}else{
			LOG.printMsg(EnumTypeMessage.ERROR, "La contraparte con tiene correo configurado para el envio del documento");
			result = false;
		}
		
		return result;
	}
	
	public String getDiasHabiles() throws OException{
		int horas = 24;	
		
		int tradeDate = this.tran.getFieldInt(TRANF_FIELD.TRANF_SETTLE_DATE.toInt());
		int settlementDate = this.tran.getFieldInt(TRANF_FIELD.TRANF_SETTLE_DATE.toInt());
		
		int iIndexId = Ref.getValue(SHM_USR_TABLES_ENUM.INDEX_TABLE, "SPOT_UDI.MXN");
		
		int days = OCalendar.numGBDBetweenForIndex(tradeDate, settlementDate, iIndexId);
		
		long diferencia = days;
		
		diferencia = horas*diferencia;
		
		if(diferencia == 0)
			return "MD";
		else
			return String.valueOf(diferencia)+"H";
	}
	
	public String getCompraVenta() throws OException{
		
		int iBuySell = this.tran.getFieldInt(TRANF_FIELD.TRANF_BUY_SELL.toInt());

		return iBuySell==0? "COMPRA":"VENTA";
	}
	
}
