package com.afore.fmwk_letters_model;

import static com.afore.fmwk_letters_helper.FMWK_Helper_Core.getDiferenciaDiasEntreFechas;
import static com.afore.fmwk_letters_helper.FMWK_Helper_Core.oprintln;

import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsParty;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.fmwk_letters_helper.FMWK_Helper_Core;
import com.afore.fmwk_letters_helper.FMWK_Helper_Letter;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TRANF_FIELD;

public class FMWK_Model_Letters<T> extends FMWK_Model_Core<T> {
	
	public String getGenero(){
		
		return FMWK_Helper_Letter.getUserName();
		
	}

	public String getOperador(){
		
		int contact_id = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_INTERNAL_CONTACT);
		
		return FMWK_Helper_Letter.getPersonnelLongName(contact_id);
	}
	
	public String getintermediario(){
		
		int bunit_id = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_EXTERNAL_BUNIT);
		
		return FMWK_Helper_Letter.getBuLongName(bunit_id);
	}
	
	public String getfondo(){
		String internal_pfolio = this.getFieldToInt(TRANF_FIELD.TRANF_INTERNAL_PORTFOLIO);
		String internal_bunit = this.getFieldToInt(TRANF_FIELD.TRANF_INTERNAL_BUNIT);
		
		return internal_pfolio + " - " + internal_bunit;
	}
	
	
	public String getEmisor(){
		String ticker = this.getFieldToInt(TRANF_FIELD.TRANF_TICKER);
		
		if(ticker != null && !ticker.equals("") && ticker.contains("_"))
			return ticker.split("_")[1];
		else
			return "";
	}
	
	public String getFechaLiquidacion(){
		int value = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_SETTLE_DATE);
		
		try {
			return FMWK_Helper_Letter.getLongDate(value);
		} catch (OException e) {
			//TODO mensaje de error
			return "";
		}
	}

	public String getFechaOperacion(){

		int value = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_TRADE_DATE);
		
		try {
			
			return FMWK_Helper_Letter.getLongDate(value);
			
		} catch (OException e) {
			//TODO mensaje de error
			return "";
		}
	}
	
	public String getFechaEmision(){
		int value = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_START_DATE);
		
		try {
			return FMWK_Helper_Letter.getLongDate(value);
		} catch (OException e) {
			//TODO mensaje de error
			return "";
		}
	}
	
	public String getTipoMercado(){
		return this.getFieldToInt(TRANF_FIELD.TRANF_INS_TYPE);
	}
	
	public String getTipoValor(){
		String ticker = this.getFieldToInt(TRANF_FIELD.TRANF_TICKER);
		
		if(ticker != null && !ticker.equals("") && ticker.contains("_"))
			return ticker.split("_")[0];
		else
			return "";
	}
	
	public String getSerie(){
		String ticker = this.getFieldToInt(TRANF_FIELD.TRANF_TICKER);
		
		if(ticker != null && !ticker.equals("") && ticker.contains("_"))
			return ticker.split("_")[2];
		else
			return "";
	}
	
	public String getTipoCambio(){

		double settle_fx = FMWK_Helper_Letter.getSettleFx(this.tranPointer);
		
		try {
			return Str.doubleToStr(settle_fx, 6);
		} catch (OException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "1.0";
	}
	
	public String getClase(){
		String ticker = this.getFieldToInt(TRANF_FIELD.TRANF_TICKER);
		
		if(ticker != null && !ticker.equals("") && ticker.contains("_"))
			return ticker.split("_")[1];
		else
			return "";
	}
	
	public String getDiasVencimiento() throws OException{
		long dias = getDiferenciaDiasEntreFechas(
						this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_SETTLE_DATE),
						this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_MAT_DATE)
					);
		
		return String.valueOf(dias);
	}
	
	public String getVuMfInteres(){
		
		String strMiIntereses = this.getVuMiInteres();
		String strSettlefx = this.getTipoCambio();

		if(strMiIntereses != null && strMiIntereses != null){
			if(strSettlefx != null && strSettlefx != null){

			
				double dMiIntereses = Double.valueOf(strMiIntereses.replaceAll(",", ""));
				double dSettleFx = Double.valueOf(strSettlefx.replaceAll(",", ""));
				double result = dMiIntereses*dSettleFx;
				
				try {
					return Str.doubleToStr((result < 0 ? result*-1 : result), 8);
				} catch (OException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		}

		oprintln("Error al obtener el interés en carta directo");
		return "";
	}
	
	public String getVuMiInteres(){
		String strPrecioSucio = this.getVuMiPrecioSucio();
		String strPrecioLimpio = this.getVuMiPrecioLimpio();
		
		if(strPrecioSucio != null && strPrecioLimpio != null){
			double precioLimpio = Double.valueOf(strPrecioSucio.replaceAll(",", ""));
			double precioSucio = Double.valueOf(strPrecioLimpio.replaceAll(",", ""));
			double result = precioSucio - precioLimpio;
			
			try {
				return Str.doubleToStr((result < 0 ? result*-1 : result), 8);
			} catch (OException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		oprintln("Error al obtener el interés en carta directo");
		return "";
	}
	
	public String getVtMfInteres(){

//		double sttleFx = Double.valueOf(strSttleFx.replaceAll(",", ""));
		
		String strSettlefx = this.getTipoCambio();
		
		String str_accrued_interest = getVtMiInteres();
		
		if(strSettlefx != null && strSettlefx != ""){
			
			double settle_fx = Double.valueOf(strSettlefx.replaceAll(",", ""));
			double accrued_interest = Double.valueOf(str_accrued_interest.replaceAll(",", ""));
			double result = accrued_interest * settle_fx;
			
			try {
				return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
			} catch (OException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			return String.valueOf((result < 0 ? result*-1 : result));
		}

		oprintln("Error al obtener el interés en carta directo");
		return "";
	}
	
	public String getVtMiInteres(){
		
		double principal = 0;
		double procceds = 0;
		double accrued_interest = 0;
		
		try {
			principal = Str.strToDouble(this.getFieldToInt(TRANF_FIELD.TRANF_PRINCIPAL).replace(",", ""));
			procceds = Str.strToDouble(this.getFieldToInt(TRANF_FIELD.TRANF_PROCEEDS).replace(",", ""));
			accrued_interest = Math.abs(procceds) - Math.abs(principal); 

			double result = accrued_interest;
			return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
		} catch (OException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
//			return String.valueOf((result < 0 ? result*-1 : result));

		oprintln("Error al obtener el interés en carta directo");
		return "";
	}
	
	public String getVtMfPrecioLimpio(){
		
		String strVtMfPrecioSucio = this.getVtMfPrecioSucio();
		String strVtMfInteres = this.getVtMfInteres();
		
		if(strVtMfPrecioSucio != null && !strVtMfPrecioSucio.equals("")){
			if(strVtMfInteres != null && !strVtMfInteres.equals("")){
				double vtMfPrecioSucio = Double.valueOf(strVtMfPrecioSucio.replaceAll(",", ""));
				double vtMfInteres = Double.valueOf(strVtMfInteres.replaceAll(",", ""));
				double result = vtMfPrecioSucio - vtMfInteres;
				
				try {
					return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
				} catch (OException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				return String.valueOf((result < 0 ? result*-1 : result));
			}
		}
		
		oprintln("Error al obtener el precio limpio Valores Totales, Moneda del Fondo en carta directo");
		return "";
	}
		
	public String getVtMiPrecioLimpio(){
		
		String strVtMiPrecioSucio = this.getVtMiPrecioSucio();
		String strVtMiInteres = this.getVtMiInteres();
		
		if(strVtMiPrecioSucio != null && !strVtMiPrecioSucio.equals("")){
			if(strVtMiInteres != null && !strVtMiInteres.equals("")){
				double vtMiPrecioSucio = Double.valueOf(strVtMiPrecioSucio.replaceAll(",", ""));
				double vuMiInteres = Double.valueOf(strVtMiInteres.replaceAll(",", ""));
				double result = vtMiPrecioSucio - vuMiInteres;
				
				try {
					return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
				} catch (OException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				return String.valueOf((result < 0 ? result*-1 : result));
			}
		}
		
		oprintln("Error al obtener el precio limpio Valores Totales, Moneda del instrumento en carta directo");
		return "";
	}
	
//	TODO:modificar logica precio limpio
	public String getVuMiPrecioLimpio(){
		
		String strClearPrice = this.getTranInfoField(EnumsTranInfoFields.MX_TRAN_CLEAN_PRICE.toString());
		
		if(strClearPrice != null && !strClearPrice.equals("")){
			
				double dClearPrice = Double.valueOf(strClearPrice.replaceAll(",", ""));
				
				double result = dClearPrice;
				
				try {
					return Str.doubleToStr((result < 0 ? result*-1 : result), 8);
				} catch (OException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
		}
		
		oprintln("Error al obtener el precio limpio Valores Totales, Moneda del instrumento en carta directo");
		return "";
	
	}
	

	public String getVuMfPrecioLimpio(){

		String strSttleFx = this.getTipoCambio();
		String strVuMiPrecioLimpio = this.getVuMiPrecioLimpio();
				
		if(strSttleFx != null && !strSttleFx.equals("")){
			if(strVuMiPrecioLimpio != null && !strVuMiPrecioLimpio.equals("")){
				double settleFx = Double.valueOf(strSttleFx.replaceAll(",", ""));
				double vuMiPrecioLimpio = Double.valueOf(strVuMiPrecioLimpio.replaceAll(",", ""));
				double result = settleFx * vuMiPrecioLimpio;
				
				return String.valueOf((result < 0 ? result*-1 : result));
			}
		}
		
		oprintln("Error al obtener el precio limpio Valores Unitarios, Moneda del Fondo en carta directo");
		return "";
	}
	
	public String getVuMiPrecioSucio(){
		String strPrice = this.getFieldToInt(TRANF_FIELD.TRANF_PRICE);

		if(strPrice != null && !strPrice.equals("")){
			double price = Double.valueOf(strPrice.replaceAll(",", ""));
			double result = price;
				
			return String.valueOf((result < 0 ? result*-1 : result));
		}
		
		oprintln("Error al obtener el precio sucio Valores Unitarios, Moneda del Fondo en carta directo");
		return "";
	}
	
	public String getVuMfPrecioSucio(){
		String strPrice = this.getFieldToInt(TRANF_FIELD.TRANF_PRICE);
		String strSettlePx = this.getTipoCambio(); 
		
		int ccy = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_NOTNL_CURRENCY);

		if(strSettlePx != null && !strSettlePx.equals("")){
			if(strPrice != null && !strPrice.equals("")){
				double price = Double.valueOf(strPrice.replaceAll(",", ""));
				double settlePx = Double.valueOf(strSettlePx.replaceAll(",", ""));
				double result = ccy == EnumsCurrency.MX_CURRENCY_MXN.toInt() ? price : settlePx * price;
				
				return String.valueOf((result < 0 ? result*-1 : result));
			}
		}
		
		oprintln("Error al obtener el precio sucio Valores Unitarios, Moneda del Fondo en carta directo");
		return "";
	}
	
	public String getVtMiPrecioSucio(){

		String strVtMiPrecioSucio = this.getFieldToInt(TRANF_FIELD.TRANF_PROCEEDS);
		
		if(strVtMiPrecioSucio != null && !strVtMiPrecioSucio.equals("")){
			
			double result = Double.valueOf(strVtMiPrecioSucio.replaceAll(",", ""));
			
			try {
				return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
			} catch (OException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			return String.valueOf((result < 0 ? result*-1 : result));
		}
		
		oprintln("Error al obtener el precio sucio Valores Totales, Moneda del Fondo en carta directo");
		return "";
	}
	
	public String getVtMfPrecioSucio(){

		String strVtMfPrecioSucio = this.getFieldToInt(TRANF_FIELD.TRANF_SETTLE_PROCEEDS);
		
		if(strVtMfPrecioSucio != null && !strVtMfPrecioSucio.equals("")){
			
			double result = Double.valueOf(strVtMfPrecioSucio.replaceAll(",", ""));
			
			try {
				return Str.doubleToStr((result < 0 ? result*-1 : result), 2);
			} catch (OException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			return String.valueOf((result < 0 ? result*-1 : result));
		}
		
		oprintln("Error al obtener el precio sucio Valores Totales, Moneda del Fondo en carta directo");
		return "";
	}
	
	public String getTitulos(){
		try {
			return Str.formatAsNotnlAcct(Math.abs(this.tranPointer.getPosition()), Util.NOTNL_WIDTH, 0);
		} catch (OException e) {
			oprintln(e.getMessage());
		}
		
		return null;
	}
	
	public String getCorreosDestinatario(int external_bunit) throws OException{
		String query = "select xml_value from party_info_xml where party_id = "+external_bunit+" and type_id = 20018";
		Table tCorreos =  FMWK_Helper_Core.getTable(query, "Table_Email");
		
		return tCorreos.getXml(1, 1);
	}
	
	public String getDiasHabiles() throws OException{
		int horas = 24;
//		long restarDias = 0;
		int tradeDate = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_TRADE_DATE);
		int settlementDate = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_SETTLE_DATE);
		
		int iIndexId = Ref.getValue(SHM_USR_TABLES_ENUM.INDEX_TABLE, "SPOT_UDI.MXN");
		
		int days = OCalendar.numGBDBetweenForIndex(tradeDate, settlementDate, iIndexId);
		
		long diferencia = days;
		
		diferencia = horas*diferencia;
		
		if(diferencia == 0)
			return "MD";
		else
			return String.valueOf(diferencia)+"H";
	}
	
	public double getAccrueldInterest(){
		
		return 0;
		
	}
	
	public String getMonedaLiquidacionFut(){
		
		String sMercadoFut = this.getMercadoFut();
		String sMercadoFutMexder = EnumsParty.MX_MEXDER_BU.toString();
		String sSettleCurrencyMxn = EnumsCurrency.MX_CURRENCY_MXN.toString();
		String sSettleCurrencyUsd = EnumsCurrency.MX_CURRENCY_USD.toString();
		String sCcy = "";
		
		if(sMercadoFut.equals(sMercadoFutMexder)){
			sCcy = sSettleCurrencyMxn;
		} else {
			sCcy = sSettleCurrencyUsd;
		}	
		
		return sCcy;
		
	}
	
	public String getMonedaLiquidacionEqt(){
		
		try {
			Table tCurrency = Table.tableNew();
			String sQuery = "Select settle_ccy from security_tran_aux_data where tran_num = " +this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_TRAN_NUM);
			DBaseTable.execISql(tCurrency, sQuery);
			int iCurrency = tCurrency.getInt(1, 1);
			String sCcy = Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, iCurrency);
						
			return sCcy;
		} catch (OException e) {
			oprintln(e.getMessage());
		}
		
		return null;
	}
	
	public String getMonedaLiquidacionBond(){
		
		try {
			Table tCurrency = Table.tableNew();
			String sQuery = "Select settle_ccy from security_tran_aux_data where tran_num = " +this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_TRAN_NUM);
			DBaseTable.execISql(tCurrency, sQuery);
			int iCurrency = tCurrency.getInt(1, 1);
			String sCcy = Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, iCurrency);
						
			return sCcy;
		} catch (OException e) {
			oprintln(e.getMessage());
		}
		
		return null;
	}
	
	public String getMercadoFut(){
		try {
			int iExchangePit = this.tranInsPointer.getFieldInt(TRANF_FIELD.TRANF_EXTERNAL_BUNIT.toInt(), 0);
			String sExchangePit = Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iExchangePit);
			
			return sExchangePit;
		} catch (OException e) {
			oprintln(e.getMessage());
		}
		
		return null;
	}

	public String getAimEfectivo(){
		
		String sCoberturaAim = this.getTranInfoField("Tipo de AIM");
		
		return "Efectivo".equals(sCoberturaAim) ? "100" : "0";
	}
	
	public String getAimEspecie(){
		
		String sCoberturaAim = this.getTranInfoField("Tipo de AIM");
		
		return "Efectivo".equals(sCoberturaAim) ? "0" : "100";
	}

	public String getPaisContraparte() throws OException{
		
		int iContraparte = this.getFieldIntToJvsValue(TRANF_FIELD.TRANF_EXTERNAL_BUNIT);
		
		String query = "select country from party_address where party_id = " + iContraparte;
		
		Table tPaisContraparte =  FMWK_Helper_Core.getTable(query, "Table_Contraparte");
		
		int iPais = tPaisContraparte.getInt("country", 1);
		
		String sPais = Ref.getName(SHM_USR_TABLES_ENUM.COUNTRY_TABLE, iPais);
		
		Ask.ok(sPais);
		
		return sPais;
	}
	
	public String getMercadoPrimario(){
		
		String sMercadoPrimario = this.getTranInfoField("Mercado Primario");
		
		if(sMercadoPrimario!=null){
			if(sMercadoPrimario.equals("Yes")) sMercadoPrimario = "Y";
			else if(sMercadoPrimario.equals("No")) sMercadoPrimario = "N";
			else sMercadoPrimario = "";
		}
		
		return sMercadoPrimario;
	}
	
}
