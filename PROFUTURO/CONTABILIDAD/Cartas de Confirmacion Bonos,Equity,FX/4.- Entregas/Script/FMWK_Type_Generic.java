package com.afore.fmwk_letters_types;

import com.afore.fmwk_letters_annotation.FMWK_ClassMethods_Core;
import com.afore.fmwk_letters_annotation.FMWK_Report_Core;
import com.afore.fmwk_letters_model.FMWK_Model_Letters;
import com.olf.openjvs.enums.TRANF_FIELD;

/*
 * Esta clase no es del framework*/

@FMWK_ClassMethods_Core({FMWK_Model_Letters.class})
public abstract class FMWK_Type_Generic {
	
	@FMWK_Report_Core(fieldName = "varDiasHabiles", methodValue="getDiasHabiles")
	private String diasHabiles;
	
	@FMWK_Report_Core(fieldName = "varCompraVenta", tranFieldValue=TRANF_FIELD.TRANF_BUY_SELL)
	private String tipoCarta;
	
//	@FMWK_Report_Core(fieldName = "varGenero", methodValue="getUserName")
	@FMWK_Report_Core(fieldName = "varGenero")
	private String genero;
	
	@FMWK_Report_Core(fieldName = "varFolio", tranFieldValue=TRANF_FIELD.TRANF_DEAL_TRACKING_NUM)
	private String folio;
	
	@FMWK_Report_Core(fieldName = "varPais", tranFieldValue=TRANF_FIELD.TRANF_HOL_LIST)
	private String pais;
	
	@FMWK_Report_Core(fieldName = "varFechaOperacion")
	private String fechaOperacion;
	
	@FMWK_Report_Core(fieldName = "varFechaLiquidacion")
	private String fechaLiquidacion;
	
	@FMWK_Report_Core(fieldName = "varExtension", tranInfoFieldValue="Cierre-Cotizacion")
	private String extension;
	
	@FMWK_Report_Core(fieldName = "varEmisor")
	private String emisor;
	
	@FMWK_Report_Core(fieldName = "varFondo")
	private String fondo;
	
	@FMWK_Report_Core(fieldName = "varOperador")
	private String operador;
	
	@FMWK_Report_Core(fieldName = "varMercadoPrimario", methodValue="getMercadoPrimario")
	private String mercadoPrimario;
	
	@FMWK_Report_Core(fieldName = "varMercado")
	private String mercado;
	
	@FMWK_Report_Core(fieldName = "varTipoMercado")
	private String tipoMercado;
	
	@FMWK_Report_Core(fieldName = "varTipoValor")
	private String tipoValor;
	
	@FMWK_Report_Core(fieldName = "varSerie")
	private String serie;
	
	@FMWK_Report_Core(fieldName = "varTipoCambio") //TODO debe ser el settleFX que se saca de una curva
	private String tipoCambio;
	
	@FMWK_Report_Core(fieldName = "varIntermediario")
	private String intermediario;
	
	@FMWK_Report_Core(fieldName = "varMonedaLiquidacion")
	private String monedaLiquidacion;

	/*aquí generamos getters y getters solamente para los campos que requieren 
	 * ser modificados y obtenidos por algún motivo*/
	public String getTipoCarta() {
		return tipoCarta;
	}
	
	public String getPais() {
		return pais;
	}

	public void setTipoCarta(String tipoCarta) {
		this.tipoCarta = tipoCarta;
	}

	public String getTipoMercado() {
		return tipoMercado;
	}

	public void setTipoMercado(String tipoMercado) {
		this.tipoMercado = tipoMercado;
	}

	public String getTipoValor() {
		return tipoValor;
	}

	public void setTipoValor(String tipoValor) {
		this.tipoValor = tipoValor;
	}

	public void setMercadoPrimario(String mercadoPrimario) {
		this.mercadoPrimario = mercadoPrimario;
	}

	public void setMercado(String mercado) {
		this.mercado = mercado;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	
	public String getEmisor() {
		return this.emisor;
	}
}
