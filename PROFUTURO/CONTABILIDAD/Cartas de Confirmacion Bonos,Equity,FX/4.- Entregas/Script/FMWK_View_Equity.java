package com.afore.fmwk_letters_view;

import com.afore.fmwk_letters_helper.FMWK_Helper_Equity;
import com.afore.fmwk_letters_service.FMWK_Service_Letters;
import com.afore.fmwk_letters_types.FMWK_Type_Equity;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;

/*
 * Esta clase no es del framework*/
public class FMWK_View_Equity implements IScript {

	@Override
	public void execute(IContainerContext context) throws OException {
		FMWK_Service_Letters<FMWK_Type_Equity> equity = 
				new FMWK_Service_Letters<>(FMWK_Type_Equity.class);
		
		equity.setCommonFields();
				
		String importeComision 		= FMWK_Helper_Equity.getImporteComision(equity);
		String ivaComision 			= FMWK_Helper_Equity.getIvaComision(equity);
		String tipoMercado			= FMWK_Helper_Equity.getTipoMercado(equity);
		String emisor				= equity.getData().getEmisor() + " " + equity.getIssuerLongName();
				
		equity.getData().setImporteComision(importeComision);
		equity.getData().setIvaComision(ivaComision);
		equity.getData().setTipoMercado(tipoMercado);
		equity.getData().setEmisor(emisor);
		
		equity.generateReport();
	}

}
