/*$Header: v 1.0, 31/Ene/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Cruce_Precios_Valmex_GAF.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Enero 2018
 * Version:					1.0
 * Description:				Script diseñado para generar el reporte de Cruce de precios a Valmex y GAF.
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/

package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCuentasNav;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.BUY_SELL_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;

public class FMWK_Reports_Cruce_Precios_Valmex_GAF implements IScript {
	
	String sScriptName		=	this.getClass().getSimpleName();	   
	UTIL_Afore UtilAfore	=	new UTIL_Afore();
	UTIL_Log LOG 			=	new UTIL_Log(sScriptName);

	int iToday				=	0;
	String sTodayFull		=	"";
	String sTodayFullHour	=	"";
	String sTodayShort		=	"";
	String sTodayNoSlash	=	"";
	String sHour			=	"";
	int iLgbd				=	0;
	String sLgbdShort		=	"";
	
	String sFullFileNameXLS	=	"";
	String sFullFileNameTXT	=	"";

	Table tData = Util.NULL_TABLE;

	@Override
	public void execute(IContainerContext context) throws OException {
		
		LOG.markStartScript();
		
			// Initialize Global Variables
				initializeScript();
			
			// Set data on tData Global Table
				setTableData();
			
			// Generate reports
				generateReports();
				
			// Send mails
				sendMails();
		
		LOG.markEndScript();	
		
	}

	private void sendMails() throws OException {
		
		// Declare mail variables
			String sMailsValmex =	"";
			String sMailsGAF	=	"";
			String sBottomImage	=	"";
			String sMessage		=	"";
			boolean mailResult	= 	false;
		
		try {
			
			// Setting mail content
				sMailsValmex	=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Cruce_Precios_Valmex_GAF", "mails_valmex");
				sMailsGAF		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Cruce_Precios_Valmex_GAF", "mails_gaf");
				sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
				sMessage 		=	"Estimados, \n\n"
					+ "Enviamos en el archivo adjunto el Cruce de Precios de PROFUTURO, correspondiente al dia de hoy "+sTodayShort;

		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load variables for send mail, "+e.getMessage());
		}

		// Sending Valmex Mails
			mailResult = UtilAfore.sendMail(
					sMailsValmex,
					"Cruce de Precios PROFUTURO",
					sMessage,
					sBottomImage,
					sFullFileNameTXT
					);
			
			if (!mailResult){
				LOG.printMsg(EnumTypeMessage.ERROR, "No fue posible el envío de mail a Valmex");
			}
		
		// Sending GAF Mails
			mailResult = UtilAfore.sendMail(
					sMailsGAF,
					"Cruce de Precios Profuturo del "+sTodayFull+" "+sHour,
					sMessage,
					sBottomImage,
					sFullFileNameXLS
					);
			
			if (!mailResult){
				LOG.printMsg(EnumTypeMessage.ERROR, "No fue posible el envío de mail a GAF");
			}
			
	}

	private void generateReports() throws OException {
		
		//Getting data from tData global table
			Table tOutput = Table.tableNew();
			tOutput.select(tData, "emisora, "
					+ "serie, "
					+ "vol_ventas, "
					+ "num_ventas, "
					+ "vol_compras, "
					+ "num_compras, "
					+ "precio_accion, "
					+ "isin", "emisora GT 0");
			
		// Order tOutput
			tOutput.addGroupBy("emisora");
			tOutput.addGroupBy("serie");
			tOutput.groupBy();	
			
		// Configure paths
			String sTemplatePath = Crystal.getRptDir();
			String sTemplateName = "ReporteCrucePreciosValmexGAF.rpt";
			String sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
		
			String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Cruce_Precios_Valmex_GAF", "path");
			String sFileName = "Profuturo-"+sTodayNoSlash;
			sFullFileNameXLS = sFilePath+sFileName+".xls";
			sFullFileNameTXT = sFilePath+sFileName+".txt";
		
		// Generar TXT
			generarReporteTXT(tOutput,sFullFileNameTXT);
		
		// Generar Excel
			generarReporteXML(tOutput,sFullTemplateName,sFullFileNameXLS);
		
	}

	private void generarReporteXML(Table tOutput, String sFullTemplateName, String sFullFileNameXLS) throws OException {
		
		//Getting signer users
			
			String sUserMxFirmasReportes = EnumsUserTables.USER_MX_FIRMAS_REPORTES.toString();

			Table tSignerUsers = Table.tableNew();
			String sQuerySignerUsers =
					"\n SELECT "+
					"\n   ufr.usuario, "+
					"\n   ufr.cargo "+
					"\n FROM "+sUserMxFirmasReportes+" ufr"+
					"\n WHERE "+
					"\n   rownum <= 2 "+
					"\n   AND ufr.REPORTE = 'FMWK_Reports_Cruce_Precios_Valmex_GAF' "+
					"\n ORDER BY orden asc ";
			DBaseTable.execISql(tSignerUsers, sQuerySignerUsers);
			String sUsuario1 = tSignerUsers.getString("usuario", 1);
			String sCargo1 = tSignerUsers.getString("cargo", 1);
			String sUsuario2 = tSignerUsers.getString("usuario", 2);
			String sCargo2 = tSignerUsers.getString("cargo", 2);

		//Setting parameters table
			Table tParameters = Table.tableNew("Parametros Crystal");
			int iNewRow = tParameters.addRow();
			tParameters.addCol("varFechaValuacion", COL_TYPE_ENUM.COL_DATE_TIME);
			tParameters.addCol("varFechaDia", COL_TYPE_ENUM.COL_DATE_TIME);
			tParameters.addCol("varUsuario1", COL_TYPE_ENUM.COL_STRING);
			tParameters.addCol("varCargo1", COL_TYPE_ENUM.COL_STRING);
			tParameters.addCol("varUsuario2", COL_TYPE_ENUM.COL_STRING);
			tParameters.addCol("varCargo2", COL_TYPE_ENUM.COL_STRING);
			
			tParameters.setDateTime("varFechaValuacion", iNewRow, ODateTime.strToDateTime(sTodayShort));
			tParameters.setDateTime("varFechaDia", iNewRow, ODateTime.strToDateTime(sLgbdShort));
			tParameters.setString("varUsuario1", iNewRow, sUsuario1);
			tParameters.setString("varCargo1", iNewRow, sCargo1);
			tParameters.setString("varUsuario2", iNewRow, sUsuario2);
			tParameters.setString("varCargo2", iNewRow, sCargo2);
		
		//Exporting excel
			try {
				Crystal.tableExportCrystalReport(tOutput, 
						sFullTemplateName, 
						CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
						sFullFileNameXLS, 
						CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
						tParameters);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create the XML report : "+e.getMessage());
			}
		
	}
	

	private void setTableData() throws OException {
		
		Table tSourceData = Util.NULL_TABLE;
		Table tSourceDataClone = Util.NULL_TABLE;
		
		Table tAuxSourceData = Util.NULL_TABLE;
		Table tAuxSourceDataSell = Util.NULL_TABLE;
		
		String sAuxTicker="", sB2 = "B2";
		String sAuxArrayTicker[]= null;
		Double dAuxPosition = 0.0;
		int iAuxBuySell=0;
		
		tSourceData = Table.tableNew();
		
		//Setting query variables
		int iToolsetEquity				=	TOOLSET_ENUM.EQUITY_TOOLSET.toInt(); //28
		int iInsTypeEquitySiefore		=	EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt();//1000006
		int iNavIdConceptoPrecioAccion	=	EnumsCuentasNav.MX_PRECIO_ACCION.toInt();//20
		int iTranInfoViewTranRegistros	=	EnumsTranInfoFields.MX_TRAN_REGISTROS.toInt();//?
		
		String sUserMxNavHist = EnumsUserTables.USER_MX_NAV_HIST.toString();
			
		StringBuilder sb = new StringBuilder();
		sb
		.append("\n SELECT ")
		.append("\n 	ab_holding.ticker, ")
		.append("\n 	ab_TRADING.TRADE_DATE, ")
		.append("\n 	ab_TRADING.BUY_SELL, ")
		.append("\n  	CAST(SUM(ab_TRADING.TRAN_POSITION) AS FLOAT) AS TRAN_POSITION,")
		.append("\n  	SUM(ab_TRADING.REGISTROS) AS REGISTROS,  ")
		.append("\n 	navd.NAV_VALUE as precio_accion, ")
		.append("\n 	ab_holding.isin ")
		.append("\n  ")
		.append("\n FROM ( ")
		.append("\n 	SELECT ")
		.append("\n 		H.TICKER, ")
		.append("\n 		H.isin, ")
		.append("\n 		AB_1.INS_NUM, ")
		.append("\n 		AB_1.EXTERNAL_BUNIT ")
		.append("\n 	FROM AB_TRAN AB_1 ")
		.append("\n 		LEFT JOIN header h ")
		.append("\n 			ON (AB_1.ins_num = h.ins_num) ")
		.append("\n 	where 1=1 ")
		.append("\n 		AND AB_1.ins_type = "+ iInsTypeEquitySiefore +"  ")
		.append("\n 		and AB_1.toolset = "+ iToolsetEquity +"  ")
		.append("\n 		and AB_1.tran_type = 2 --holding ")
		.append("\n 		and AB_1.tran_status = 3 -- validated ")
		.append("\n ) ab_holding ")
		.append("\n  ")
		.append("\n LEFT JOIN ( ")
		.append("\n 	SELECT ")
		.append("\n 		ab_2.TRADE_DATE, ")
		.append("\n 		ab_2.ins_num, ")
		.append("\n 		ab_2.buy_sell AS BUY_SELL, ")
		.append("\n 		CAST(ab_2.position AS FLOAT) as tran_position, ")
		.append("\n 		CAST(abtiv.value AS INT) as registros ")
		.append("\n 	FROM AB_TRAN ab_2 ")
		.append("\n 	 	LEFT JOIN ab_tran_info_view abtiv  ")
		.append("\n 	 		ON (ab_2.tran_num = abtiv.tran_num  ")
		.append("\n 	 		AND abtiv.type_id  = "+ iTranInfoViewTranRegistros +") -- Tran Info Registros  ")
		.append("\n 	WHERE 1=1 ")
		.append("\n 		AND ab_2.ins_type = "+ iInsTypeEquitySiefore +"  ")
		.append("\n 		and ab_2.toolset = "+ iToolsetEquity +"  ")
		.append("\n 		and ab_2.tran_type = 0 --TRADING ")
		.append("\n 		and ab_2.tran_status IN (3,22) -- validated, closeout ")
		.append("\n 		AND AB_2.TRADE_DATE = '"+sLgbdShort+"' ")
		.append("\n ) AB_TRADING ON ab_holding.INS_NUM = AB_TRADING.INS_NUM ")
		.append("\n  ")
		.append("\n LEFT JOIN PARTY_PORTFOLIO PP ")
		.append("\n 	ON (ab_holding.EXTERNAL_BUNIT=PP.PARTY_ID) ")
		.append("\n  ")
		.append("\n LEFT JOIN user_mx_nav_hist navd  ")
		.append("\n 	ON (PP.PORTFOLIO_ID = navd.NAV_PORTFOLIO_ID  ")
		.append("\n 	AND navd.NAV_DATE = '"+sLgbdShort+"' ")
		.append("\n 	AND navd.NAV_ID_CONCEPTO = "+ iNavIdConceptoPrecioAccion +")  ")
		.append("\n GROUP BY ")
		.append("\n ab_holding.ticker, ")
		.append("\n ab_TRADING.TRADE_DATE, ")
		.append("\n ab_TRADING.BUY_SELL, ")
		.append("\n navd.NAV_VALUE, ")
		.append("\n ab_holding.isin ");
				DBaseTable.execISql(tSourceData, sb.toString());
				
				if(Table.isTableValid(tSourceData) != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt() || tSourceData.getNumRows() <= 0){
					LOG.printMsg(EnumTypeMessage.ERROR, "Null Source Event Data - No documents selected for process...");
					Util.exitFail();
				}

				
				//Agrupa Compras y Ventas Antes de Aplicar redondeos.
				tSourceDataClone = tSourceData.copyTable();
				tSourceData.group("ticker, buy_sell");
				tSourceData.distinctRows();
				tSourceData.setColValDouble("tran_position", 0.0);
				
				tSourceData.fillSetSourceTable(tSourceDataClone);
				tSourceData.fillAddMatch("ticker", "ticker");
				tSourceData.fillAddMatch("buy_sell", "buy_sell");
				tSourceData.fillAddData("tran_position", "tran_position");
				tSourceData.fillAddData("registros", "registros");
				tSourceData.fillSum();
								
				//Obtiene Serie del Ticker
				tSourceData.addCol("serie", COL_TYPE_ENUM.COL_STRING);
				tSourceData.addCol("emisora", COL_TYPE_ENUM.COL_STRING);
				
				for (int i=1; i<= tSourceData.getNumRows(); i++){
					sAuxTicker = tSourceData.getString("ticker", i);
					sAuxArrayTicker = sAuxTicker.split("_");
					
					if(Str.isNull(sAuxArrayTicker[2])==0 && Str.isNotEmpty(sAuxArrayTicker[2])==1) //Serie valida
						tSourceData.setString("serie", i, sAuxArrayTicker[2]);
					if(Str.isNull(sAuxArrayTicker[1])==0 && Str.isNotEmpty(sAuxArrayTicker[1])==1) //Serie valida
						tSourceData.setString("emisora", i, sAuxArrayTicker[1]);
				}
				
				//Genera tAuxSourceData para controlar las series B2, fracciones de acciones
				tAuxSourceData = Table.tableNew("Buy");
				tAuxSourceDataSell = Table.tableNew("Sell");
				
				tAuxSourceData.select(tSourceData, "ticker, tran_position, buy_sell", "serie EQ " + sB2 + " AND buy_sell EQ " + BUY_SELL_ENUM.BUY.toInt());
				tAuxSourceDataSell.select(tSourceData, "ticker, tran_position, buy_sell", "serie EQ " + sB2 + " AND buy_sell EQ " + BUY_SELL_ENUM.SELL.toInt());
				tAuxSourceDataSell.setColName("tran_position", "sell_position");
				tAuxSourceData.select(tAuxSourceDataSell, "sell_position", "ticker EQ $ticker");
				
				
				tAuxSourceData.addFormulaColumn("truncate(COL('tran_position') , 0)",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "buy_trunc");
				tAuxSourceData.addFormulaColumn("truncate(abs(COL('sell_position')) , 0)",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "sell_trunc");
				tAuxSourceData.addFormulaColumn("COL('tran_position')-COL('buy_trunc')",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "buy_decimal");
				tAuxSourceData.addFormulaColumn("abs(COL('sell_position'))-COL('sell_trunc')",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "sell_decimal");
				
				tAuxSourceData.addFormulaColumn("iif(COL('buy_decimal')>0.0,1.0-COL('buy_decimal'), 0.0)",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "buy_dec_rest");
				tAuxSourceData.addFormulaColumn("iif(COL('sell_decimal')>0.0,1.0-COL('sell_decimal'), 0.0)",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "sell_dec_rest");
				
				tAuxSourceData.addFormulaColumn("COL('tran_position')+COL('buy_dec_rest')",  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "buy_rounded");
				
				String sFrmSellRound="iif((COL('sell_decimal')+COL('buy_dec_rest'))>=1.0,COL('sell_position')-COL('sell_dec_rest'),COL('sell_position')+COL('sell_decimal'))";
				tAuxSourceData.addFormulaColumn(sFrmSellRound,  COL_TYPE_ENUM.COL_DOUBLE.toInt(), "sell_rounded");
				
				//TODO aplica ROUND o TRUNC dependiendo de la serie
				for (int i=1; i<= tSourceData.getNumRows(); i++){
					sAuxTicker = tSourceData.getString("ticker", i);
					sAuxArrayTicker = sAuxTicker.split("_");
					dAuxPosition = 0.0;
					iAuxBuySell = tSourceData.getInt("buy_sell", i);
					

					if(sAuxArrayTicker[2].equalsIgnoreCase("A2") && iAuxBuySell == BUY_SELL_ENUM.BUY.toInt()){
						dAuxPosition = tSourceData.getDouble("tran_position", i);
						tSourceData.setDouble("tran_position", i, Math.floor(dAuxPosition));
					}else if(sAuxArrayTicker[2].equalsIgnoreCase("B2")){
						int iB2Row = tAuxSourceData.unsortedFindString("ticker", sAuxTicker, SEARCH_CASE_ENUM.CASE_INSENSITIVE);
						if(iB2Row > 0){
							//Obtiene los datos calculados anteriormente para Compra/Venta B2
							if(iAuxBuySell == BUY_SELL_ENUM.BUY.toInt()) dAuxPosition = tAuxSourceData.getDouble("buy_rounded", iB2Row);
							else dAuxPosition = tAuxSourceData.getDouble("sell_rounded", iB2Row);
							
						}else{
							LOG.printMsg(EnumTypeMessage.WARNING, "Se intenta generar venta sin una compra asociada serie B2");
							
							//Si se imprime la venta sin su respectiva compra, se redondea de forma que sea "Menos Negativo"
							if(iAuxBuySell == BUY_SELL_ENUM.SELL.toInt() && dAuxPosition == 0.0)
								dAuxPosition = Math.floor(Math.abs(tSourceData.getDouble("tran_position", i)));
							
						}

						tSourceData.setDouble("tran_position", i, dAuxPosition);
					}
				}//END FOR tSourceData


				//Format data table
				tSourceData.mathABSCol("tran_position");
				
				
				// Getting buy deals (cambio en la logica al revés, buy es venta y sell es compra)
				Table tBuyDeals = Table.tableNew("BuyDeals");
				tBuyDeals.select(tSourceData, "*", "buy_sell EQ 1");
				tBuyDeals.setColName("tran_position", "vol_compras");
				tBuyDeals.setColName("registros", "num_compras");
				
				// Getting sell deals
				Table tSellDeals = Table.tableNew("SellDeals");
				tSellDeals.select(tSourceData, "*", "buy_sell EQ 0");
				tSellDeals.setColName("tran_position", "vol_ventas");
				tSellDeals.setColName("registros", "num_ventas");
				
				// Mixing them again on a resume table
				Table tDistinctTickers = Table.tableNew();
				tDistinctTickers.select(tSourceData, "DISTINCT, ticker", "ticker GT 0");
				tDistinctTickers.select(tBuyDeals, "*", "ticker EQ $ticker");
				tDistinctTickers.select(tSellDeals, "*", "ticker EQ $ticker");

				tData.select(
						tDistinctTickers, 
						"emisora,"
						+ "serie,"
						+ "vol_ventas,"
						+ "num_ventas,"
						+ "vol_compras,"
						+ "num_compras,"
						+ "precio_accion,"
						+ "isin", "ticker GT 0"
					);
		
	}

	private void initializeScript() throws OException {
		
		iToday			=	OCalendar.today();
		sHour			=	Util.timeGetServerTimeHMS();
		sTodayFull		=	OCalendar.formatDateInt(iToday,DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		sTodayShort		=	OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
		sTodayFullHour	=	OCalendar.formatDateInt(iToday,DATE_FORMAT.DATE_FORMAT_ISO8601)+"-09:00:00.000";
		sTodayNoSlash	=	sTodayFull.replaceAll("/", "");
		
		iLgbd = OCalendar.getLgbd(iToday);
		
		sLgbdShort = OCalendar.formatDateInt(iLgbd, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);

		tData = Table.tableNew();
		
	}

	private void generarReporteTXT(Table tOutput, String sFullFileNameTXT) throws OException {
		
			// Creating TXT format
			StringBuilder sb = new StringBuilder();
			for (int i=1 ; i <= tOutput.getNumRows(); i++){
				
				String sPrecioAccion = String.format("%2.6f", tOutput.getDouble("precio_accion",i));
				sPrecioAccion = UtilAfore.padLeft(sPrecioAccion, 9, "0");
				
				String sIsin			=	tOutput.getString("isin",i);
				String sNumVentas		=	UtilAfore.formatAsIntForReport(Integer.toString(tOutput.getInt("num_ventas",i)), 9, 0);
				String sVolVentas		=	UtilAfore.formatAsDblForReport(Double.toString(tOutput.getDouble("vol_ventas",i)), 13, 0);
				String sNumCompras		=	UtilAfore.formatAsIntForReport(Integer.toString(tOutput.getInt("num_compras",i)), 9, 0);
				String sVolCompras		=	UtilAfore.formatAsDblForReport(Double.toString(tOutput.getDouble("vol_compras",i)), 13, 0);
				
				String sSOHChar = Character.toString ((char) 1);
				
				sb
				.append("35=S").append(sSOHChar)
				.append("22=4").append(sSOHChar)
				.append("48=").append(sIsin).append(sSOHChar)
				.append("631=").append(sPrecioAccion).append(sSOHChar)
				.append("20011=").append(sPrecioAccion).append(sSOHChar)
				.append("20012=").append(sNumVentas).append(sSOHChar)
				.append("20013=").append(sVolVentas).append(sSOHChar)
				.append("20014=").append(sPrecioAccion).append(sSOHChar)
				.append("20015=").append(sNumCompras).append(sSOHChar)
				.append("20016=").append(sVolCompras).append(sSOHChar)
				.append("20017=").append(sPrecioAccion).append(sSOHChar)
				.append("60=").append(sTodayFullHour).append(sSOHChar)
				.append("117=").append(i+99).append(sSOHChar)
				.append("693=").append(0).append(sSOHChar)
				.append("\n");
			}
		
		//Exporting TXT
			Str.printToFile(sFullFileNameTXT, sb.toString());	
	}
}
