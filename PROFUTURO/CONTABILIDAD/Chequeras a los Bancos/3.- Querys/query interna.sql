SELECT
	ab.deal_tracking_num,
	ab.external_bunit,
	SUBSTR (e_bunit_name.short_name, 0, LENGTH(e_bunit_name.short_name)-5) as contraparte,
	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) as fondo,
	ab.position as monto,
	CAST(ab.price*100 AS FLOAT) as tasa,
	ab.maturity_date - ab.start_date as plazo,
	profile.pymt as intereses,
	cast(ab.position + profile.pymt as Float) as total_importe,
	account.account_number as cuenta
FROM ab_tran ab

	-- External Bunit and Portfolio Names
	LEFT JOIN party e_bunit_name
		ON (ab.external_bunit = e_bunit_name.party_id)

	LEFT JOIN party_portfolio pp
		ON (ab.internal_portfolio = pp.portfolio_id)
	LEFT JOIN party i_portfolio_name
		ON (pp.party_id = i_portfolio_name.party_id)

	-- Internal Account
	LEFT JOIN (
		SELECT a.*
		FROM ab_tran_settle_view a
		INNER JOIN (
		    SELECT deal_tracking_num, MAX(EVENT_NUM) EVENT_NUM
		    FROM ab_tran_settle_view
		    WHERE tran_status = 3 
		    AND int_account_id != 0
		    GROUP BY deal_tracking_num
		) b ON a.deal_tracking_num = b.deal_tracking_num AND a.event_num = b.event_num
		WHERE tran_status = 3 
		AND int_account_id != 0
	) absettlev on ab.deal_tracking_num = absettlev.deal_tracking_num

	LEFT JOIN account
		ON (account.account_id = absettlev.int_account_id)

	-- Intereses
	LEFT JOIN (
		SELECT a.*
		FROM profile a
		INNER JOIN (
		    SELECT ins_num, MAX(profile_seq_num) profile_seq_num
		    FROM profile
		    GROUP BY ins_num
		) b ON a.ins_num = b.ins_num AND a.profile_seq_num = b.profile_seq_num
	) profile on ab.ins_num = profile.ins_num

WHERE 1=1
	AND ab.toolset = 6 -- Loandep
	AND ab.tran_status = 3  -- Validated
	AND ab.ins_type = 1000056 -- Depo-Chequera








	LEFT JOIN ab_tran_settle_view absettlev
		ON (ab.deal_tracking_num = absettlev.deal_tracking_num
			AND absettlev.int_account_id!=0
			AND absettlev.TRAN_STATUS=3)

	LEFT JOIN account pibunit_account_number
		ON (pibunit_account_number.account_id = absettlev.int_account_id)



	LEFT JOIN profile
		ON (ab.ins_num = profile.ins_num)