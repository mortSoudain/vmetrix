/*$Header: v 1.0, 02/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Chequeras_Bancos_Interno.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Febrero 2018
 * Version:					1.0
 * Description:				Script que genera reportes de chequeras a los bancos y env�a los archivos por email
 * 
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/

package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;

public class FMWK_Reports_Chequeras_Bancos_Interno implements IScript {
	
	// Declare utils and general variables
	String sScriptName		=	this.getClass().getSimpleName();	   
	UTIL_Afore UtilAfore	=	new UTIL_Afore();
	UTIL_Log LOG			=	new UTIL_Log(sScriptName);
	
	// Declare time variables
	int iToday					=	0;
	String sHour				=	"";
	String sTodayDefault		=	"";
	String sTodayAAAAMMDD		=	"";
	String sTodaySlashDDMMAAAA	=	"";

	// Declare global tables
	Table tData		=	Util.NULL_TABLE;
	Table tOutput	=	Util.NULL_TABLE;
	
	// Declare output FileNames
	String sFullFileNameXLS	=	"";

	@Override
	public void execute(IContainerContext context) throws OException {
		
		LOG.markStartScript();
		
		// Initialize Global Variables
			initializeScript();
		
		// Set data on tData Global Table
			setTableData();
			
		// Generate report
			generateReport();
			
		// Send mails
			sendMails();
	
		LOG.markEndScript();
	}

	private void initializeScript() {
		
		// Load time variables
		try {
			iToday	=	OCalendar.today();
			sHour	=	Util.timeGetServerTimeHMS();
			sTodayDefault = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			sTodayAAAAMMDD = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601);
			sTodaySlashDDMMAAAA = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load system time variables : " + e.getMessage());
		}

		// Create tables
		try {
			tData	=	Table.tableNew("Data Table");
			tOutput	=	Table.tableNew("Output Table");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to initialize tData or tOutput Table : " + e.getMessage());
		}

	}

	private void setTableData() {
		
		StringBuilder sQueryPrincipal = new StringBuilder();
		
		// Query filters
			int iToolsetLoandep = TOOLSET_ENUM.LOANDEP_TOOLSET.toInt(); //6
			int iTranStatusValidated = TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
			int iInsTypeDepoChequera = EnumsInstrumentsMX.MX_DEPO_CHEQUERA.toInt();//1000056
		
		//Setting Principal Query
			sQueryPrincipal
			.append("\n SELECT ")
			.append("\n 	ab.deal_tracking_num, ")
			.append("\n 	ab.external_bunit, ")
			.append("\n 	SUBSTR (e_bunit_name.short_name, 0, LENGTH(e_bunit_name.short_name)-5) as contraparte, ")
			.append("\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) as fondo, ")
			.append("\n 	ab.position as monto, ")
			.append("\n 	CAST(ab.price*100 AS FLOAT) as tasa, ")
			.append("\n 	ab.maturity_date - ab.start_date as plazo, ")
			.append("\n 	profile.pymt as intereses, ")
			.append("\n 	cast(ab.position + profile.pymt as Float) as total_importe, ")
			.append("\n 	account.account_number as cuenta ")
			.append("\n FROM ab_tran ab ")
			.append("\n  ")
			.append("\n 	-- External Bunit and Portfolio Names ")
			.append("\n 	LEFT JOIN party e_bunit_name ")
			.append("\n 		ON (ab.external_bunit = e_bunit_name.party_id) ")
			.append("\n  ")
			.append("\n 	LEFT JOIN party_portfolio pp ")
			.append("\n 		ON (ab.internal_portfolio = pp.portfolio_id) ")
			.append("\n 	LEFT JOIN party i_portfolio_name ")
			.append("\n 		ON (pp.party_id = i_portfolio_name.party_id) ")
			.append("\n  ")
			.append("\n 	-- Internal Account ")
			.append("\n 	LEFT JOIN ( ")
			.append("\n 		SELECT a.* ")
			.append("\n 		FROM ab_tran_settle_view a ")
			.append("\n 		INNER JOIN ( ")
			.append("\n 		    SELECT deal_tracking_num, MAX(EVENT_NUM) EVENT_NUM ")
			.append("\n 		    FROM ab_tran_settle_view ")
			.append("\n 		    WHERE tran_status = "+ iTranStatusValidated +"  ")
			.append("\n 		    AND int_account_id != 0 ")
			.append("\n 		    GROUP BY deal_tracking_num ")
			.append("\n 		) b ON a.deal_tracking_num = b.deal_tracking_num AND a.event_num = b.event_num ")
			.append("\n 		WHERE tran_status = "+ iTranStatusValidated +"  ")
			.append("\n 		AND int_account_id != 0 ")
			.append("\n 	) absettlev on ab.deal_tracking_num = absettlev.deal_tracking_num ")
			.append("\n  ")
			.append("\n 	LEFT JOIN account ")
			.append("\n 		ON (account.account_id = absettlev.int_account_id) ")
			.append("\n  ")
			.append("\n 	-- Intereses ")
			.append("\n 	LEFT JOIN ( ")
			.append("\n 		SELECT a.* ")
			.append("\n 		FROM profile a ")
			.append("\n 		INNER JOIN ( ")
			.append("\n 		    SELECT ins_num, MAX(profile_seq_num) profile_seq_num ")
			.append("\n 		    FROM profile ")
			.append("\n 		    GROUP BY ins_num ")
			.append("\n 		) b ON a.ins_num = b.ins_num AND a.profile_seq_num = b.profile_seq_num ")
			.append("\n 	) profile on ab.ins_num = profile.ins_num ")
			.append("\n  ")
			.append("\n WHERE 1=1 ")
			.append("\n 	AND ab.toolset = "+ iToolsetLoandep +" -- Loandep ")
			.append("\n 	AND ab.tran_status = "+ iTranStatusValidated +"  -- Validated ")
			.append("\n 	AND ab.ins_type = "+ iInsTypeDepoChequera +" -- Depo-Chequera ")
			.append("\n 	AND ab.trade_date = '"+sTodayDefault+"'");
			
			// Load query into table
			try {
				DBaseTable.execISql(tData, sQueryPrincipal.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load principal query into tData Table : " + e.getMessage());
			}
		
	}

	private void generateReport() throws OException {
		
		// Select data to show on report
			try {
				tOutput.select(tData,
						"contraparte, "
						+ "fondo, "
						+ "monto, "
						+ "tasa, "
						+ "plazo, "
						+ "intereses, "
						+ "total_importe, "
						+ "cuenta ",
						"deal_tracking_num GT 0"
					);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to set selected columns into tOutput Table : "
					+ e.getMessage());
			}
					
		// Configure paths and filenames
			String sTemplatePath = Crystal.getRptDir();
			String sTemplateName = "ReporteChequerasBancos.rpt";
			String sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
		
			String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "path");
			String sFileName = "Reportes_Chequeras_Bancos_Interno_"+sTodayAAAAMMDD;
			sFullFileNameXLS = sFilePath+sFileName+".xls";
			
		//Exporting excel
			try {
				Crystal.tableExportCrystalReport(tOutput, 
						sFullTemplateName, 
						CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
						sFullFileNameXLS, 
						CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
						);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to export Excel document. "+e.getMessage());
			}
		
	}
	
	private void sendMails() throws OException {
		
		int iResponse = Ask.okCancel("Desea enviar los emails?");
		
			if(iResponse==1){
			// Declare mail variables
				String sMails		=	"";
				String sSubject		=	"";
				String sMessage		=	"";
				String sBottomImage	=	"";
				boolean mailResult	=	false;
					
			// Setting mail content from user_configurable_variables
				try {
					
					sMails			=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "mails");
					sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "subject");
					sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Interno", "body");
					sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
				
				} catch (OException e) {
					LOG.printMsg(EnumTypeMessage.ERROR,
							"Unable to load variables for send mail, "+e.getMessage());
				}
				
			// Format
				sSubject		=	sSubject.replace("$TODAY", sTodaySlashDDMMAAAA);
				sMessage		=	sMessage.replace("$TODAY", sTodaySlashDDMMAAAA);
	
			// Sending Mails
				mailResult = UtilAfore.sendMail(
						sMails,
						sSubject,
						sMessage,
						sBottomImage,
						sFullFileNameXLS
						);
				
				if (!mailResult){
					Ask.ok("No fue posible enviar el email");
				} else {
					Ask.ok("Email enviado exitosamente");
				}
			}
	}

}
