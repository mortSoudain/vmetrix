/*$Header: v 1.0, 02/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Chequeras_Bancos_Externo.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Febrero 2018
 * Version:					1.0
 * Description:				Script que genera reportes de chequeras a los bancos y env�a los archivos por email
 * 
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/

package com.afore.custom_reports;

import java.util.ArrayList;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;

public class FMWK_Reports_Chequeras_Bancos_Externo implements IScript {
	
	// Declare utils and general variables
	String sScriptName		=	this.getClass().getSimpleName();	   
	UTIL_Afore UtilAfore	=	new UTIL_Afore();
	UTIL_Log LOG			=	new UTIL_Log(sScriptName);
	
	// Declare time variables
	int iToday					=	0;
	String sHour				=	"";
	String sTodayDefault		=	"";
	String sTodayAAAAMMDD		=	"";
	String sTodaySlashDDMMAAAA	=	"";

	// Declare global tables
	Table tData		=	Util.NULL_TABLE;
	
	ArrayList<Boolean> bMailResults = new ArrayList<Boolean>();

	@Override
	public void execute(IContainerContext context) throws OException {
		
		LOG.markStartScript();
		
		// Initialize Global Variables
			initializeScript();
		
		// Set data on tData Global Table
			setTableData();
			
		//Generate reports and send mails
			generateReportAndSendMails();
	
		LOG.markEndScript();
	}


	private void initializeScript() {
		
		// Load time variables
		try {
			iToday	=	OCalendar.today();
			sHour	=	Util.timeGetServerTimeHMS();
			sTodayDefault = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			sTodayAAAAMMDD = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601);
			sTodaySlashDDMMAAAA = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load system time variables : " + e.getMessage());
		}

		// Create tables
		try {
			tData	=	Table.tableNew("Data Table");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to initialize tData Table : " + e.getMessage());
		}

	}

	private void setTableData() {
		
		StringBuilder sQueryPrincipal = new StringBuilder();
		
		// Query filters
		int iToolsetLoandep = TOOLSET_ENUM.LOANDEP_TOOLSET.toInt(); //6
		int iTranStatusValidated = TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
		int iInsTypeDepoChequera = EnumsInstrumentsMX.MX_DEPO_CHEQUERA.toInt();//1000056
	
		//Setting Principal Query
			sQueryPrincipal
			.append("\n SELECT ")
			.append("\n 	ab.deal_tracking_num, ")
			.append("\n 	ab.external_bunit, ")
			.append("\n 	SUBSTR (e_bunit_name.short_name, 0, LENGTH(e_bunit_name.short_name)-5) as contraparte, ")
			.append("\n 	SUBSTR (i_portfolio_name.short_name, 0, LENGTH(i_portfolio_name.short_name)-5) as fondo, ")
			.append("\n 	ab.position as monto, ")
			.append("\n 	CAST(ab.price*100 AS FLOAT) as tasa, ")
			.append("\n 	ab.maturity_date - ab.start_date as plazo, ")
			.append("\n 	profile.pymt as intereses, ")
			.append("\n 	cast(ab.position + profile.pymt as Float) as total_importe, ")
			.append("\n 	account.account_number as cuenta ")
			.append("\n FROM ab_tran ab ")
			.append("\n  ")
			.append("\n 	-- External Bunit and Portfolio Names ")
			.append("\n 	LEFT JOIN party e_bunit_name ")
			.append("\n 		ON (ab.external_bunit = e_bunit_name.party_id) ")
			.append("\n  ")
			.append("\n 	LEFT JOIN party_portfolio pp ")
			.append("\n 		ON (ab.internal_portfolio = pp.portfolio_id) ")
			.append("\n 	LEFT JOIN party i_portfolio_name ")
			.append("\n 		ON (pp.party_id = i_portfolio_name.party_id) ")
			.append("\n  ")
			.append("\n 	-- Internal Account ")
			.append("\n 	LEFT JOIN ( ")
			.append("\n 		SELECT a.* ")
			.append("\n 		FROM ab_tran_settle_view a ")
			.append("\n 		INNER JOIN ( ")
			.append("\n 		    SELECT deal_tracking_num, MAX(EVENT_NUM) EVENT_NUM ")
			.append("\n 		    FROM ab_tran_settle_view ")
			.append("\n 		    WHERE tran_status = "+ iTranStatusValidated +"  ")
			.append("\n 		    AND int_account_id != 0 ")
			.append("\n 		    GROUP BY deal_tracking_num ")
			.append("\n 		) b ON a.deal_tracking_num = b.deal_tracking_num AND a.event_num = b.event_num ")
			.append("\n 		WHERE tran_status = "+ iTranStatusValidated +"  ")
			.append("\n 		AND int_account_id != 0 ")
			.append("\n 	) absettlev on ab.deal_tracking_num = absettlev.deal_tracking_num ")
			.append("\n  ")
			.append("\n 	LEFT JOIN account ")
			.append("\n 		ON (account.account_id = absettlev.int_account_id) ")
			.append("\n  ")
			.append("\n 	-- Intereses ")
			.append("\n 	LEFT JOIN ( ")
			.append("\n 		SELECT a.* ")
			.append("\n 		FROM profile a ")
			.append("\n 		INNER JOIN ( ")
			.append("\n 		    SELECT ins_num, MAX(profile_seq_num) profile_seq_num ")
			.append("\n 		    FROM profile ")
			.append("\n 		    GROUP BY ins_num ")
			.append("\n 		) b ON a.ins_num = b.ins_num AND a.profile_seq_num = b.profile_seq_num ")
			.append("\n 	) profile on ab.ins_num = profile.ins_num ")
			.append("\n  ")
			.append("\n WHERE 1=1 ")
			.append("\n 	AND ab.toolset = "+ iToolsetLoandep +" -- Loandep ")
			.append("\n 	AND ab.tran_status = "+ iTranStatusValidated +"  -- Validated ")
			.append("\n 	AND ab.ins_type = "+ iInsTypeDepoChequera +" -- Depo-Chequera ")
			.append("\n 	AND ab.trade_date = '"+sTodayDefault+"'");
			
			// Load query into table
			try {
				DBaseTable.execISql(tData, sQueryPrincipal.toString());
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load principal query into tData Table : " + e.getMessage());
			}
	}
	

	private void generateReportAndSendMails() throws OException {
		
		int iResponse = Ask.okCancel("Desea enviar los emails?");
		boolean bAllMailSended = true;
		
		if(iResponse==1){
		
			//Select distinct counterparts
			Table tDistinctPartys = Table.tableNew();
			tDistinctPartys.select(tData, "DISTINCT, external_bunit", "deal_tracking_num GT 0");
			
			for (int iRow = 1; iRow <= tDistinctPartys.getNumRows(); iRow++){
				int iParty = tDistinctPartys.getInt("external_bunit", iRow);
				generateReportAndSendMail(iParty);
			}
			
			for (Boolean bResult : bMailResults){
				if(!bResult) bAllMailSended=false;
			}
			
			if(!bMailResults.isEmpty()){
				if(!bAllMailSended){
					Ask.ok("Error al enviar alguno de los emails. Revisar el LOG para mayor informacion.");
				} else {
					Ask.ok("Emails enviados exitosamente.");
				}
			} else {
				Ask.ok("No hubo datos para generar un reporte. No fue enviado email.");
			}
		}
		
	}

	private void generateReportAndSendMail(int iParty) throws OException {
		
		Table tOutput	=	Table.tableNew();
		
		String sParty = Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, iParty);
		
		// Select data to show on report
			try {
				tOutput.select(tData,
						"contraparte, "
						+ "fondo, "
						+ "monto, "
						+ "tasa, "
						+ "plazo, "
						+ "intereses, "
						+ "total_importe, "
						+ "cuenta ",
						"external_bunit EQ "+iParty
					);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
					"Unable to set selected columns into tOutput Table : "
					+ e.getMessage());
			}
										
			// Configure paths and filenames
				String sTemplatePath = Crystal.getRptDir();
				String sTemplateName = "ReporteChequerasBancos.rpt";
				String sFullTemplateName = sTemplatePath+"\\"+sTemplateName;
			
				String sFilePath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Externo", "path");
				String sFileName = "Reportes_Chequeras_Bancos_("+sParty+")_"+sTodayAAAAMMDD;
				String sFullFileNameXLS = sFilePath+sFileName+".xls";
					
			//Exporting excel
				try {
					Crystal.tableExportCrystalReport(tOutput, 
							sFullTemplateName, 
							CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
							sFullFileNameXLS, 
							CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR
							);
				} catch (OException e) {
					LOG.printMsg(EnumTypeMessage.ERROR, "Unable to export Excel document. "+e.getMessage());
				}
				
		//-------------------- Sending mail
				
				// Declare mail variables
					String sMailsTotal		= ""; 
					String sMailsDefault	=	"";
					String sMailsParty		=	"";
					String sSubject		=	"";
					String sMessage		=	"";
					String sBottomImage	=	"";
					boolean mailResult	=	false;
						
				// Setting mail content from user_configurable_variables
					try {
						
						sMailsDefault	=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Externo", "mails");
						sSubject		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Externo", "subject");
						sMessage		=	UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Externo", "body");
						sBottomImage	=	UtilAfore.getVariableGlobal("FINDUR", "Send_Mail", "image");
					
					} catch (OException e) {
						LOG.printMsg(EnumTypeMessage.ERROR,
								"Unable to load variables for send mail, "+e.getMessage());
					}
					
				//Setting party mails
					try {
						sMailsParty = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Chequeras_Bancos_Externo", "mails_"+sParty);
					} catch (OException e) {
						LOG.printMsg(EnumTypeMessage.ERROR,
								"Unable to load party mails, "+e.getMessage());
					}
				
				// Concatenate default mails plus party mails
				if (sMailsParty==null) sMailsParty="";
				sMailsTotal = sMailsDefault + ";" + sMailsParty;
				
					
				// Format
					sSubject		=	sSubject.replace("$TODAY", sTodaySlashDDMMAAAA);
					sSubject		=	sSubject.replace("$CONTRAPARTE", sParty);
					sMessage		=	sMessage.replace("$TODAY", sTodaySlashDDMMAAAA);

				// Sending Mails
					mailResult = UtilAfore.sendMail(
							sMailsTotal,
							sSubject,
							sMessage,
							sBottomImage,
							sFullFileNameXLS
							);
					bMailResults.add(mailResult);
					
					if (!mailResult){
						LOG.printMsg(EnumTypeMessage.ERROR,
								"Unable to send mail.");
					}
					
			tOutput.destroy();				
	}
}