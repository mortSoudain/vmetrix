with 
maxdates as ( 
	select 
		max(ucm.fecha) fecha 
	from user_mx_creditmetrics_hist ucm 
),
operations as (
	select 
		ucm.fecha 
		,ucm.internal_portfolio 
		,ucm.ticker 
		,ucm.issuer 
		,ucm.sector 
		,ucm.currency 
		,ucm.position 
		,ucm.pe 
		,ucm.credit_var 
		,ucm.ul 
		,ucm.credit_rating 
		,ucm.credit_rating_adjusted 
		,ucm.severidad_perdida 
		,ucm.pe_baja 
		,ucm.credit_var_baja 
		,ucm.ul_baja 
		,ucm.pe_alta 
		,ucm.credit_var_alta 
		,ucm.ul_alta 
	from user_mx_creditmetrics_hist ucm 
		inner join maxdates max 
			on max.fecha = ucm.fecha
),
last_nav_date as ( 
  SELECT 
    max(nav_date) as nav_date 
  FROM user_mx_historical_nav 
), 
total_nav as (
	SELECT
		p.id_number as internal_portfolio,
		n.nav_portfolio as internal_portfolio_name,
		CAST(SUM(n.nav_value) AS float) as current_nav
	FROM user_mx_historical_nav n
	    INNER JOIN portfolio p on n.nav_portfolio = p.name
	    INNER JOIN last_nav_date lnd on lnd.nav_date = n.nav_date
	WHERE 
    	n.baja_logica = 0 
	GROUP BY p.id_number,n.nav_portfolio
)
select
	o.internal_portfolio
	,n.current_nav
	,ABS(SUM(o.pe)) as ohd_pe
	,ABS(SUM(o.pe)) / n.current_nav as ohd_porcentaje_nav
	,ulp.limite_prudencial
	,ulp.limite_perdida_esp
	,(ABS(SUM(o.pe)) / n.current_nav) / ulp.limite_prudencial as ohd_uso_prudencial
	,(ABS(SUM(o.pe)) / n.current_nav) / ulp.limite_perdida_esp as ohd_uso
	,CASE
		WHEN ((ABS(SUM(o.pe)) / n.current_nav) <= ulp.limite_prudencial)
			THEN 'OK'
      	WHEN ((ABS(SUM(o.pe)) / n.current_nav) > ulp.limite_prudencial
			AND (ABS(SUM(o.pe)) / n.current_nav) <= ulp.limite_perdida_esp)
      		THEN 'WARNING'
      	WHEN ((ABS(SUM(o.pe)) / n.current_nav) > ulp.limite_perdida_esp)
      		THEN 'ALERTA'
      ELSE ' '
	END AS status
from operations o
	inner join total_nav n on n.internal_portfolio = o.internal_portfolio
	inner join user_mx_limite_perdida_esp ulp on ulp.internal_portfolio = o.internal_portfolio
group by
	o.internal_portfolio
	,n.current_nav
	,ulp.limite_perdida_esp
	,ulp.limite_prudencial