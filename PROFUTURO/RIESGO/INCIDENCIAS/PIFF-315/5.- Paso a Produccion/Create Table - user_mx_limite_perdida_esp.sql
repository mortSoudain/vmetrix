-- Creacion de User Tables

-- user_mx_creditmetrics_hist

create table TST_GRUPO_163961_01AUG16.user_mx_limite_perdida_esp
(
 internal_portfolio int ,
 limite_perdida_esp double precision ,
 limite_prudencial double precision 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_limite_perdida_esp');