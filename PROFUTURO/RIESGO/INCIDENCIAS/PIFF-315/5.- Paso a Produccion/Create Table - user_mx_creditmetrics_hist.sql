-- Creacion de User Tables

-- user_mx_creditmetrics_hist

create table TST_GRUPO_163961_01AUG16.user_mx_creditmetrics_hist
(
 fecha date ,
 internal_portfolio int ,
 ticker varchar2(255) ,
 issuer int ,
 sector varchar2(255) ,
 currency int ,
 position double precision ,
 pe double precision ,
 credit_var double precision ,
 ul double precision ,
 credit_rating int ,
 credit_rating_adjusted int ,
 severidad_perdida double precision ,
 pe_baja double precision ,
 credit_var_baja double precision ,
 ul_baja double precision ,
 pe_alta double precision ,
 credit_var_alta double precision ,
 ul_alta double precision 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_creditmetrics_hist');