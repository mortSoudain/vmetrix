/*$Header: v1.4 13/Jul/2018 $*/
/**
 * File Name:                  MISC_Afore_CreditMetrics.java
 * Input File Name:            None
 * Author:                     VMetrix - Sebastian Valenzuela
 * Creation Date:              22 Noviembre 2017
 * Purpose:					   Proceso que realiza el cï¿½lculo de Credit Metrics - Perdida Esperada /Credit VaR
 * 
 * REVISION HISTORY			   
 * Date:                       30 Enero 2018
 * Version:					   v1.1
 * Autor:              		   Sebastian Valenzuela - VMetrix International SPA
 * Description:            	   Se agregan al computo los Instrumentos BOND-MBS , BOND-MBSUDI
 * 
 * Date:                       13 Marzo 2018
 * Version:					   v1.2
 * Autor:                      Gustavo Rojas - VMetrix International SPA
 * Description:            	   - Se sustituye calculo de simulacion de sensibilidad por las sensibilidades
 * 							   del vector Valmer (Duration - Convexity)
 * 							   - Se agrega escenario estresado al calculo del Loss Given Default entregando
 * 							   resultados a la alta y a la baja para perdidas esperadas, Credit Var y UL.
 * 							   - Se agrega Try Cacth Global y se formatea tabla de salida desde codigo
 *  
 * Date:                       15 Marzo 2018
 * Version:					   v1.3
 * Autor:                      Gustavo Rojas - VMetrix International SPA
 * Description:            	   Se agrega escenario sin Stress (al 100%) con combobox que permite ver todo o solo al 100%
 *
 * Date:                       13 Julio 2018
 * Version:					   v1.4
 * Autor:                      Basthian Matthews - VMetrix International SPA
 * Description:            	   Se agrega columna severidad de pÃ©rdida, y se agrega cÃ¡culo para el lÃ­mite de pÃ©rdida esperada,
 *							   estos cÃ¡lculos se almacenan en una user table para la visualizacion de lÃ­mites.
 *
 * 
 */

package com.profuturo.misc_afore;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.Index;
import com.olf.openjvs.Math;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SEARCH_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class MISC_Afore_CreditMetrics implements IScript
{	
	// Basic util variables
		private String		sScriptName			=	this.getClass().getSimpleName();
		private UTIL_Log	_Log				=	new UTIL_Log(sScriptName);
		private UTIL_Afore	UtilAfore			=	new UTIL_Afore();

	// UDW Variables
		ENUM_UDW_ACTION		callback_type;
		ENUM_UDW_EVENT		callback_event;
		private Table 		tblProperties		=	Util.NULL_TABLE;
		private Table 		tblGlobalProperties	=	Util.NULL_TABLE;
		private Table 		tDisplay			=	Util.NULL_TABLE;
		private Table 		tReturnt			=	Util.NULL_TABLE;
		private String 		sCellName			=	null;
  
	// User
		private String		sUserName		=	"";
		private int			iUserId			=	0;
		
	// Time variables
		private int 		iToday			=	0;
		private String		sTodayDefault	=	"";
		private ODateTime	oToday			=	null;
		
		private static final String SPOT_CURVE     = "SPOT_FX.MXN";
	  	private static final double SCENARIOS_NUM  = 100000.0;

  public void execute(IContainerContext context) throws OException {
	  
	  initializeScript();
	  
	  _Log.markStartScript();
	  
	  callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());

			switch(callback_type) {
			
	    		case UDW_INIT_ACTION:
	    			
	    			// Adding visual elements
						setVisualElements();
	
					// Adding columns layout
						addLayoutColumns();
	    			
					// FINALLY, Setting table properties
						setTableProperties();
	    			    	    		    	
	    			break;
			    
	    		case UDW_EDIT_ACTION:
	    			
					// Getting callback info
						callback_event	=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
						tReturnt		=	UserDataWorksheet.getData();
						sCellName		=	tReturnt.scriptDataGetCallbackName();

	    			switch(callback_event){
	    				
			    		case UDW_CUSTOM_BUTTON_EVENT:
			    		
			    			if (sCellName.compareTo("vm_calc_credit") == 0){
			    				calculateCreditMetrics(context);
								//setDataOnTable(sTodayDefault);
			    			}
			    			
			    			else if (sCellName.compareTo("vm_save_credit") == 0){		
			    				
								Table tSecurityGroupsTable						= Ref.retrievePersonnelSecurityGroups(iUserId);
								int iSecurityGroupRiskManagerWflow				= tSecurityGroupsTable.unsortedFindString("name", "Wfllow", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
								int iSecurityGroupRiskManagerConsultorRiesgos	= tSecurityGroupsTable.unsortedFindString("name", "Consultor Riesgos", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
								
								if(iSecurityGroupRiskManagerWflow!= -1 || iSecurityGroupRiskManagerConsultorRiesgos!= -1){
									
									Table tCalculatedValues = Table.tableNew();
									tCalculatedValues.select(tReturnt, "fecha,internal_portfolio,ticker,issuer,sector,currency,position,pe,credit_var,ul,credit_rating,credit_rating_adjusted,severidad_perdida,pe_baja,credit_var_baja,ul_baja,pe_alta,credit_var_alta,ul_alta", "internal_portfolio GT 0");
									
									// Actualizacion data on usertable historic creditmetrics
									Table tUserMxCreditMetricsHist = Table.tableNew("user_mx_creditmetrics_hist");
									String sSql = "select * from user_mx_creditmetrics_hist";
									DBaseTable.execISql(tUserMxCreditMetricsHist, sSql);			
									
									// Add new and updated values
									tUserMxCreditMetricsHist.addCol("update", COL_TYPE_ENUM.COL_INT);
									tCalculatedValues.addCol("update", COL_TYPE_ENUM.COL_INT);
									// Select updated values
									tUserMxCreditMetricsHist.select(tCalculatedValues, "fecha,internal_portfolio,ticker,issuer,sector,currency,position,pe,credit_var,ul,credit_rating,credit_rating_adjusted,severidad_perdida,pe_baja,credit_var_baja,ul_baja,pe_alta,credit_var_alta,ul_alta", "fecha EQ $fecha AND internal_portfolio EQ $internal_portfolio AND ticker EQ $ticker AND currency EQ $currency");
									// Set updated
									tUserMxCreditMetricsHist.setColValInt("update", 1);
									// Identify updated values (matched) on new values table
									tCalculatedValues.select(tUserMxCreditMetricsHist, "update", "fecha EQ $fecha AND internal_portfolio EQ $internal_portfolio AND ticker EQ $ticker AND currency EQ $currency");
									// Select new values
									tUserMxCreditMetricsHist.select(tCalculatedValues, "fecha,internal_portfolio,ticker,issuer,sector,currency,position,pe,credit_var,ul,credit_rating,credit_rating_adjusted,severidad_perdida,pe_baja,credit_var_baja,ul_baja,pe_alta,credit_var_alta,ul_alta", "update EQ 0");
									tUserMxCreditMetricsHist.delCol("update");
									
									DBUserTable.clear(tUserMxCreditMetricsHist);
									DBUserTable.bcpIn(tUserMxCreditMetricsHist);
									
									Ask.ok("Datos almacenados exitosamente");
																		
								} else {
			    					Ask.ok("El usuario "+sUserName+" no tiene permisos para realizar modificaciones.\nLos cambios no fueron almacenados.");
								}
			    			}
			    			
			    			else if (sCellName.equals("btnConsultaHistorica")){
			    				String sSelectedDate = tReturnt.scriptDataGetWidgetString("fecha");
								setDataOnTable(sSelectedDate);
			    			}
			    			
			    			break;
			    			
						default:
							break;
		    		}
	    		default:
	    			break;		
	    	}
			
		_Log.markEndScript();
	    Util.exitSucceed();
    }



	private void setDataOnTable(String sSelectedDate) throws OException {
		
		int iSelectedDate				=	OCalendar.parseString(sSelectedDate);
		String sSelectedFormattedDate	=	OCalendar.formatDateInt(iSelectedDate, DATE_FORMAT.DATE_FORMAT_ISO8601_EXTENDED);
		sSelectedFormattedDate			=	sSelectedFormattedDate.replace("-", "/");
		
		Table tCreditMetrics = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n with ");
		sb.append("\n maxdates as ( ");
		sb.append("\n 	select ");
		sb.append("\n 		max(ucm.fecha) fecha ");
		sb.append("\n 	from user_mx_creditmetrics_hist ucm ");
		sb.append("\n 	where ucm.fecha = TO_DATE('"+sSelectedFormattedDate+"', 'YYYY/MM/DD') ");
		sb.append("\n ) ");
		sb.append("\n select ");
		sb.append("\n 	ucm.fecha ");
		sb.append("\n 	,ucm.internal_portfolio ");
		sb.append("\n 	,ucm.ticker ");
		sb.append("\n 	,ucm.issuer ");
		sb.append("\n 	,ucm.sector ");
		sb.append("\n 	,ucm.currency ");
		sb.append("\n 	,ucm.position ");
		sb.append("\n 	,ucm.pe ");
		sb.append("\n 	,ucm.credit_var ");
		sb.append("\n 	,ucm.ul ");
		sb.append("\n 	,ucm.credit_rating ");
		sb.append("\n 	,ucm.credit_rating_adjusted ");
		sb.append("\n 	,ucm.severidad_perdida ");
		sb.append("\n 	,ucm.pe_baja ");
		sb.append("\n 	,ucm.credit_var_baja ");
		sb.append("\n 	,ucm.ul_baja ");
		sb.append("\n 	,ucm.pe_alta ");
		sb.append("\n 	,ucm.credit_var_alta ");
		sb.append("\n 	,ucm.ul_alta ");
		sb.append("\n from user_mx_creditmetrics_hist ucm ");
		sb.append("\n 	inner join maxdates max ");
		sb.append("\n 		on max.fecha = ucm.fecha ");
		
		DBaseTable.execISql(tCreditMetrics, sb.toString());
				
		UserDataWorksheet.setRefreshDataTable(tCreditMetrics);
}



	private void calculateCreditMetrics(IContainerContext context) throws OException {
		
		Table tCalculatedValues = Table.tableNew();
		
		int iCreditRatingStress  = 0;
		int iCreditRatingDgrade  = 0;
		double dConfidenceLevel  = 0.0;		
		
		//Clear Temporary User Table
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Start th calculation Credit Metics");
			    				
		String sCreditConfidenceLevel = tReturnt.scriptDataGetWidgetString("vm_opt_ted_cl");
		String sCreditRatingStress    = tReturnt.scriptDataGetWidgetString("vm_opt_crs");
		String sCreditRatingDgrade    = tReturnt.scriptDataGetWidgetString("vm_opt_dg");
		
		sCreditConfidenceLevel = Str.stripBlanks(sCreditConfidenceLevel);
		
		
		if(sCreditConfidenceLevel.isEmpty() || sCreditConfidenceLevel.equals("") || sCreditConfidenceLevel == null ){
			
			tReturnt.scriptDataSetWidgetString("vm_opt_ted_cl","0.95");
			dConfidenceLevel = 0.95;
			
			
		}else{
			dConfidenceLevel = Str.strToDouble(sCreditConfidenceLevel);
		}
		
		
		
		if(sCreditRatingStress.isEmpty() || sCreditRatingStress.equals("") || sCreditRatingStress == null){
			iCreditRatingStress = 0;
		}else{
			iCreditRatingStress = getAllCreditRatingsSPByCreditName(sCreditRatingStress);
		}
		
		if(sCreditRatingDgrade.isEmpty() || sCreditRatingDgrade.equals("") || sCreditRatingDgrade == null){
			iCreditRatingDgrade = 0;
		}else{
			iCreditRatingDgrade = Str.strToInt(sCreditRatingDgrade);
		}

		Table tCreditMetricsResult = processCalculation(context.getArgumentsTable(),tReturnt,iCreditRatingStress,iCreditRatingDgrade,dConfidenceLevel);
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Calculate Final Table");
				
		//SET DATA ON TABLE
		tCalculatedValues.select(tCreditMetricsResult, "internal_portfolio,ticker,currency,issuer,sector,position,total_pe(pe),total_pe_baja(pe_baja),total_pe_alta(pe_alta),creditvar_total(credit_var),creditvar_total_baja(credit_var_baja),creditvar_total_alta(credit_var_alta),ul,ul_baja,ul_alta,rating_id_base(credit_rating),rating_id_adj(credit_rating_adjusted),severidad_perdida", "ins_num GT 0");
		//Set Date
		tCalculatedValues.addCol("fecha", COL_TYPE_ENUM.COL_DATE_TIME);
		tCalculatedValues.setColValDateTime("fecha",oToday);

		Ask.ok("Process Complete");
		
		// Set calculated values on tDisplay
			UserDataWorksheet.setRefreshDataTable(tCalculatedValues);	
	
}



	private void setTableProperties() {
		
		try {
			
			// Initialize properties tables
				tblProperties			=	UserDataWorksheet.initProperties();
				tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
			// Setting col properties
				//UserDataWorksheet.setColReadOnly("titulos", tDisplay, tblProperties);
		
			// Setting UDW properties
				UserDataWorksheet.setDisableSave(tblGlobalProperties);
				UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
				UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
				UserDataWorksheet.setDisableReload(tblGlobalProperties);
				UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set some of the properties tables on setTableProperties function."
					+ "Error: " + e.getMessage());
		}
	
	}



	private void addLayoutColumns() {
		
		try {
			tDisplay.addCol("fecha", COL_TYPE_ENUM.COL_DATE_TIME);
			tDisplay.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT);
			tDisplay.addCol("ticker", COL_TYPE_ENUM.COL_STRING);
			tDisplay.addCol("issuer", COL_TYPE_ENUM.COL_INT);
			tDisplay.addCol("sector", COL_TYPE_ENUM.COL_STRING);
			tDisplay.addCol("currency", COL_TYPE_ENUM.COL_INT);
			tDisplay.addCol("position", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("pe", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("credit_var", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("ul", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("pe_baja", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("credit_var_baja", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("ul_baja", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("pe_alta", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("credit_var_alta", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("ul_alta", COL_TYPE_ENUM.COL_DOUBLE);
			tDisplay.addCol("credit_rating", COL_TYPE_ENUM.COL_INT);
			tDisplay.addCol("credit_rating_adjusted", COL_TYPE_ENUM.COL_INT);
			tDisplay.addCol("severidad_perdida", COL_TYPE_ENUM.COL_DOUBLE);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to add some cols for layout on addLayoutColumns function."
					+ "Error: " + e.getMessage());
		}

	}



	private void setVisualElements() {
		
		try {
			// Creating display table
				tDisplay			=	Table.tableNew();
				Table tCreditRating =	getAllCreditRatingsSP();
    			Table tDowGrade     =	getAllDownGradeList();

    			tDisplay.scriptDataAddWidget("vm_opt_label", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
    					"x=10,y=52,h=20,w=100","label=Options ::");
    			tDisplay.scriptDataAddWidget("vm_calc_credit", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
    					"x=120,y=52,h=20,w=250","label= Calculate Credit Metrics");
    			tDisplay.scriptDataAddWidget("vm_save_credit", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
    					"x=120,y=72,h=20,w=250","label= Save");
    			tDisplay.scriptDataAddWidget("vm_opt_label_cl", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
    					"x=400,y=52,h=20,w=120","label= Confidence Level :");
    			tDisplay.scriptDataAddWidget("vm_opt_ted_cl", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_TED_WIDGET.toInt(), 
    					"x=530,y=52,h=20,w=100","label= ");
    			tDisplay.scriptDataAddWidget("vm_opt_label_cr", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
    					"x=400,y=72,h=20,w=120","label= Credit Rating Stress :");
    			tDisplay.scriptDataAddWidget("vm_opt_crs", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
    					"x=530,y=72,h=20,w=100","",tCreditRating);
    			tDisplay.scriptDataAddWidget("vm_opt_label_dg", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
    					"x=650,y=72,h=20,w=100","label= Downgrade CR :");
    			tDisplay.scriptDataAddWidget("vm_opt_dg", SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
    					"x=750,y=72,h=20,w=100","",tDowGrade);
    			
				tDisplay.scriptDataAddWidget("fecha", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_CALENDAR_WIDGET.toInt(), 
						"x=530, y=92, h=20, w=100", 
						"label="+sTodayDefault);
				
				tDisplay.scriptDataAddWidget("lblFecha", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=400, y=92, h=20, w=120", 
						"label= Fecha :");
				
				tDisplay.scriptDataAddWidget("btnConsultaHistorica", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
						"x=120, y=92, h=20, w=250", 
						"label=Consulta Historica");
			
			// Add data worksheet
				tDisplay.scriptDataMoveListBox("top=130,left=5,right=5,bottom=5");
			
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set some of the visual elements on setVisualElements function."
					+ "Error: " + e.getMessage());
		}
	
	}



	private void initializeScript() {
		
		try {
			
			iToday			=	OCalendar.today();
			sTodayDefault	=	OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			oToday			=	ODateTime.strToDateTime(sTodayDefault);
		
			sUserName		=	Ref.getUserName();
			iUserId		=	Ref.getUserId();
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
		
	}



	public Table processCalculation(Table tArgt , Table returnt , int iCreditRatingStress , int iStressNumber , double dConfidenceLevel)throws OException{
		
		int iToday                    = OCalendar.today();
		Table tCreditMetricsByIns     = Table.tableNew("Credit Metrics By Ins");
		Table tUserTableCreditMetrics = Table.tableNew("user_mx_creditmetrics_temp");
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve All Transaction Data");
		
		Table tAllData                = getAllTranData(iToday);
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve All Credit Ratings");
		Table tAllCreditRating        = getAllCreditRatingData(iCreditRatingStress,iStressNumber);
				
//		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve All Sens");
//		Table tAllSens 		          = getAllSens(tArgt);
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve All Credit Spread");
		Table tAllSpread 	          = getAllSpreadTable(); 
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix");
		Table tAllTranMatrix          = getTranMatrix(tAllSpread);
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix 1.1");

		Table tCreditMetrics = Table.tableNew("Credit Metrics Calc");
		
		
		tCreditMetrics.addCol("ins_num", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("ticker", COL_TYPE_ENUM.COL_STRING);
		tCreditMetrics.addCol("precio_sucio_24h", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("mat_year", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("currency", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("notnl", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("fx", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("sector", COL_TYPE_ENUM.COL_STRING);
		tCreditMetrics.addCol("rating_id", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("rating_id_stress", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("display_order", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("display_order_stress", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("duration_mac", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("duration_mod", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("recovery_rate", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("convexity", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("matrix_name", COL_TYPE_ENUM.COL_STRING);
		tCreditMetrics.addCol("tran_matrix_name", COL_TYPE_ENUM.COL_STRING);
		tCreditMetrics.addCol("rating_id_from", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("rating_id_to", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("display_order_to", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("spread", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("tran_rating_id_from", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("tran_rating_id_to", COL_TYPE_ENUM.COL_INT);
		tCreditMetrics.addCol("tran_spread", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetrics.addCol("tran_spread_sum", COL_TYPE_ENUM.COL_DOUBLE);


		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix 1.3");
				
		tCreditMetrics.select(tAllData, "*", "ins_num GT 0");
		tCreditMetrics.select(tAllCreditRating, "rating_id , display_order,rating_id_stress,display_order_stress", "ins_num EQ $ins_num");
		

		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix 1.4");
		
		tCreditMetrics.addFormulaColumn(" iif(COL('rating_id_stress') > 0 , COL('rating_id_stress') ,COL('rating_id'))", COL_TYPE_ENUM.COL_INT.toInt(), "rating_id_base");
		tCreditMetrics.addFormulaColumn(" iif(COL('rating_id_stress') > 0 , COL('display_order_stress') ,COL('display_order'))", COL_TYPE_ENUM.COL_INT.toInt(), "display_order_base");

		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix 1.5");
		
/**		Calculo de Sensibilidad por Simulacion **/
//		Table tAllSensByIns = tAllSens.cloneTable();
//		tAllSensByIns.select(tAllSens, "DISTINCT , *", "ins_num GT 0");
//		
//		tCreditMetrics.select(tAllSensByIns, "duration_mac,duration_mod,convexity", "ins_num EQ $ins_num");
		

		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Retrieve Tran Matrix 1.6");
		tCreditMetrics.select(tAllSpread, "rating_id_from,rating_id_to,spread,display_order_to", "matrix_name EQ $matrix_name AND rating_id_from EQ $rating_id_base");
		tCreditMetrics.select(tAllTranMatrix, "rating_id_from(tran_rating_id_from),rating_id_to(tran_rating_id_to),spread(tran_spread)", "rating_id_from EQ $rating_id_from AND rating_id_to EQ $rating_id_to");
		tCreditMetrics.select(tAllTranMatrix, "spread_acu(tran_spread_sum)", "rating_id_from EQ $rating_id_from AND rating_id_to EQ $rating_id_to");
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Calculate PE");
		//Credit Metrics Calculation rating_id_to
		tCreditMetrics.addFormulaColumn("COL('precio_sucio_24h') - (COL('precio_sucio_24h') * (COL('spread')/10000) * COL('duration_mod')) + (COL('precio_sucio_24h') * (COL('spread')/10000) * (COL('spread')/10000) * COL('convexity')) / 2", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "perdida_ind");
		
		//Se agrega calculo Loss Given Default al 100%
		tCreditMetrics.addFormulaColumn(" iif(COL('rating_id_to') == 21004 , (COL('recovery_rate') * 1.0 - 1) *  COL('notnl') *  COL('fx') ,COL('perdida_ind') - COL('precio_sucio_24h'))", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "dif_perdida_ind");
		tCreditMetrics.addFormulaColumn("COL('dif_perdida_ind') * COL('tran_spread') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "esperanza_perdida");
				
		//Se agrega calculo Loss Given Default a la baja 0.5
		tCreditMetrics.addFormulaColumn(" iif(COL('rating_id_to') == 21004 , (COL('recovery_rate') * 0.5 - 1) *  COL('notnl') *  COL('fx') ,COL('perdida_ind') - COL('precio_sucio_24h'))", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "dif_perdida_ind_baja");
		tCreditMetrics.addFormulaColumn("COL('dif_perdida_ind_baja') * COL('tran_spread') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "esperanza_perdida_baja");
		
		//Calculo Loss Given Defaults ALTA 1.5
		tCreditMetrics.addFormulaColumn(" iif(COL('rating_id_to') == 21004 , (COL('recovery_rate') * 1.5 - 1) *  COL('notnl') *  COL('fx') ,COL('perdida_ind') - COL('precio_sucio_24h'))", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "dif_perdida_ind_alta");
		tCreditMetrics.addFormulaColumn("COL('dif_perdida_ind_alta') * COL('tran_spread') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "esperanza_perdida_alta");
		
		// Severidad de PÃ©rdida
		tCreditMetrics.addFormulaColumn("( 1 - COL('recovery_rate') ) *  COL('notnl') *  COL('fx') *  COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "severidad_perdida");

		//Credit Metrics Temporary User Table
		tUserTableCreditMetrics.select(tCreditMetrics,"ins_num,tran_rating_id_from,tran_rating_id_to,tran_spread_sum,dif_perdida_ind,dif_perdida_ind_baja,dif_perdida_ind_alta,internal_portfolio" , " ins_num GT 0 ");
		
		int iRetVal  = DBUserTable.clear(tUserTableCreditMetrics);
		if (iRetVal != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()){
			_Log.printMsg(EnumTypeMessage.ERROR, (DBUserTable.dbRetrieveErrorInfo(iRetVal, "DBUserTable.clear() failed")));
		}else{
			_Log.printMsg(EnumTypeMessage.INFO, "\n --> Tuncate Table OK!");
		}

	
		//Import User Table
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Saving User Table...");
		DBUserTable.bcpIn(tUserTableCreditMetrics);
		
		//Credit Metrics by Instrument
		tCreditMetricsByIns.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT);
		tCreditMetricsByIns.addCol("ins_num", COL_TYPE_ENUM.COL_INT);
		tCreditMetricsByIns.addCol("ticker", COL_TYPE_ENUM.COL_STRING);
		tCreditMetricsByIns.addCol("rating_id_base", COL_TYPE_ENUM.COL_INT);
		tCreditMetricsByIns.addCol("display_order_base", COL_TYPE_ENUM.COL_INT);
		tCreditMetricsByIns.addCol("creditvar", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("creditvar_baja", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("creditvar_alta", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("rating_id_adj", COL_TYPE_ENUM.COL_INT);
		tCreditMetricsByIns.addCol("position", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("pe", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("pe_baja", COL_TYPE_ENUM.COL_DOUBLE);
		tCreditMetricsByIns.addCol("pe_alta", COL_TYPE_ENUM.COL_DOUBLE);

		
		//Get Credit Metrics Calculation
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Calculating the Credit VaR...");
		
		int iScenarioNumber = (int) Math.round(SCENARIOS_NUM * (1-dConfidenceLevel),0);
		
		Table tCreditVaRCalculation = getCreditVaRCalc(iScenarioNumber);
		
		
		_Log.printMsg(EnumTypeMessage.INFO, "\n --> Creating the Summary Table...");

		tCreditMetricsByIns.select(tCreditMetrics, "DISTINCT ,internal_portfolio,currency,sector,issuer,ins_num,ticker,rating_id_base,position,display_order_base,severidad_perdida ", "ins_num GT 0");
		
	
		tCreditMetricsByIns.select(tCreditMetrics, "SUM ,esperanza_perdida(pe),esperanza_perdida_baja(pe_baja),esperanza_perdida_alta(pe_alta)", "internal_portfolio EQ $internal_portfolio AND ins_num EQ $ins_num AND ticker EQ $ticker AND rating_id_base EQ $rating_id_base AND position EQ $position AND display_order_to GE $display_order_base");
		
		
		tCreditMetricsByIns.select(tCreditVaRCalculation, "creditvar,creditvar_baja, creditvar_alta,tran_rating_id_to(rating_id_adj)", "ins_num EQ $ins_num AND internal_portfolio EQ $internal_portfolio");
		
		tCreditMetricsByIns.addFormulaColumn("COL('pe') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "total_pe");
		tCreditMetricsByIns.addFormulaColumn("COL('pe_baja') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "total_pe_baja");
		tCreditMetricsByIns.addFormulaColumn("COL('pe_alta') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "total_pe_alta");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "creditvar_total");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar_baja') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "creditvar_total_baja");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar_alta') * COL('position') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "creditvar_total_alta");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar_total') - COL('total_pe') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "ul");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar_total_baja') - COL('total_pe_baja') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "ul_baja");
		tCreditMetricsByIns.addFormulaColumn("COL('creditvar_total_alta') - COL('total_pe_alta') ", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "ul_alta");

		//TODO DEBUG
		//tCreditVar.viewTable();
		//tCreditMetrics.viewTable();
		//tCreditVaRCalculation.viewTable();
		//tAllData.viewTable();
		//tAllCreditRating.viewTable();
		//tAllSens.viewTable();
		//tAllSpread.viewTable();
		
		//TODO DEBUG
		//tCreditMetricsByIns.viewTable();
		
		return tCreditMetricsByIns;
	}
	
	
	
	
	private Table getAllTranData(int iToday) throws OException{
		
	 	Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("All Tran Data");
			
		StringBuffer sb = new StringBuffer();
			
		
		sb.append("\n SELECT ab.internal_portfolio ");
		sb.append("\n, ab.ins_num ");
		sb.append("\n, CAST(SUM(ab.mvalue) as float) as position");
		sb.append("\n, h.ticker ");
		sb.append("\n, ab.currency ");
		sb.append("\n, p.notnl ");
		sb.append("\n, CAST(1 as float) as fx ");
		sb.append("\n, piv.value as sector ");
		sb.append("\n, v.precio_sucio_24h");
		sb.append("\n, s.recovery_rate ");
		sb.append("\n, abi.external_bunit as issuer ");
		sb.append("\n, v.dias_por_vencer/360 mat_year");
		sb.append("\n, v.convexidad convexity");
		sb.append("\n, v.duracion duration_mod");
		sb.append("\n, v.duracion duration_mac");
		sb.append("\n FROM ab_tran ab , header h , profile p , ab_tran abi ,party_info_view piv , user_mx_vectorprecios_diario v , user_mx_sector s ");
		sb.append("\n WHERE ab.ins_num = p.ins_num");
		sb.append("\n AND ab.ins_num      = h.ins_num");
		sb.append("\n AND ab.ins_num      = abi.ins_num");
		sb.append("\n AND (TO_DATE('" + ODateTime.strToDateTime(OCalendar.formatDateInt(iToday)) + "','DD-MM-YYYY HH:MI:SS AM') >= p.start_date AND TO_DATE('" + ODateTime.strToDateTime(OCalendar.formatDateInt(iToday)) + "','DD-MM-YYYY HH:MI:SS AM') < p.end_date )"); 
		sb.append("\n AND ab.tran_status  = "+ TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n AND ab.tran_type    = "+ TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n AND ab.toolset      = "+ TOOLSET_ENUM.BOND_TOOLSET.toInt()); 
		sb.append("\n AND abi.tran_status = "+ TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n AND abi.tran_type   = "+ TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt());
		sb.append("\n AND piv.party_id = abi.external_lentity ");
		sb.append("\n AND piv.type_name = 'MX Sector' ");
		sb.append("\n AND piv.value not in ('Gubernamental') ");
		sb.append("\n AND s.sector_name = piv.value ");
		sb.append("\n AND v.ticker = h.ticker ");
		sb.append("\n GROUP BY ab.internal_portfolio,abi.external_bunit, ab.ins_num,h.ticker,ab.currency,p.notnl,piv.value,v.precio_sucio_24h,s.recovery_rate,v.dias_por_vencer/360, v.convexidad, v.duracion");

			try {
				
				@SuppressWarnings("unused")
				int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
				if (tReturn.getNumRows() <= 0){

					_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacion Necesaria para el Calculo de Credit Metrics \n ");
					tReturn.destroy();
					Util.exitFail();			
				}
			
			} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
			}

			//Add Matrix Name Logic
			
			tReturn.addCol("matrix_name", COL_TYPE_ENUM.COL_STRING);
			
			Table tSpotFX = getSpotCurve();
			
			for(int i =1 ; i<= tReturn.getNumRows() ; i++){
				
				
				if(tReturn.getInt("currency", i) == Ref.getValue(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, "MXN")){
					
					if(tReturn.getInt("mat_year", i) < 2){
						tReturn.setString("matrix_name", i , "V2");
					}else if(tReturn.getInt("mat_year", i) >= 2 && tReturn.getInt("mat_year", i) < 5){
						tReturn.setString("matrix_name", i , "V2");
					}else if(tReturn.getInt("mat_year", i) >= 5 && tReturn.getInt("mat_year", i) < 10){
						tReturn.setString("matrix_name", i , "V5");
					}else if(tReturn.getInt("mat_year", i) >= 10 && tReturn.getInt("mat_year", i) < 15){
						tReturn.setString("matrix_name", i , "V10");
					}else if(tReturn.getInt("mat_year", i) >= 15 && tReturn.getInt("mat_year", i) < 20){
						tReturn.setString("matrix_name", i , "V15");
					}else if(tReturn.getInt("mat_year", i) >= 20 && tReturn.getInt("mat_year", i) < 25){
						tReturn.setString("matrix_name", i , "V20");
					}else if(tReturn.getInt("mat_year", i) >= 25 && tReturn.getInt("mat_year", i) < 30){
						tReturn.setString("matrix_name", i , "V25");
					}else if(tReturn.getInt("mat_year", i) >= 30){
						tReturn.setString("matrix_name", i , "V30");
					}
					
				}else{
					
					if(tReturn.getInt("mat_year", i) < 2){
						tReturn.setString("matrix_name", i , "V2U");
					}else if(tReturn.getInt("mat_year", i) >= 2 && tReturn.getInt("mat_year", i) < 5){
						tReturn.setString("matrix_name", i , "V2U");
					}else if(tReturn.getInt("mat_year", i) >= 5 && tReturn.getInt("mat_year", i) < 10){
						tReturn.setString("matrix_name", i , "V5U");
					}else if(tReturn.getInt("mat_year", i) >= 10 && tReturn.getInt("mat_year", i) < 15){
						tReturn.setString("matrix_name", i , "V10U");
					}else if(tReturn.getInt("mat_year", i) >= 15 && tReturn.getInt("mat_year", i) < 20){
						tReturn.setString("matrix_name", i , "V15U");
					}else if(tReturn.getInt("mat_year", i) >= 20 && tReturn.getInt("mat_year", i) < 25){
						tReturn.setString("matrix_name", i , "V20U");
					}else if(tReturn.getInt("mat_year", i) >= 25 && tReturn.getInt("mat_year", i) < 30){
						tReturn.setString("matrix_name", i , "V25U");
					}else if(tReturn.getInt("mat_year", i) >= 30){
						tReturn.setString("matrix_name", i , "V30U");
					}
					
					tSpotFX.sortCol("Spot");
					int iFind = tSpotFX.findString("Spot", Ref.getName(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, tReturn.getInt("currency", i)),SEARCH_ENUM.FIRST_IN_GROUP);
					tReturn.setDouble("fx", i , 1/tSpotFX.getDouble("Price (Mid)", iFind) );
					
				}
				
			}
			
			return tReturn;
		
		
	}
	
	public Table getAllCreditRatingData(int iRatingId , int iStressValue) throws OException{
		
	 	Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("All Credit Rating Data");
			
		StringBuffer sb = new StringBuffer();
			
		sb.append("\n SELECT 	x.tran_num");
		sb.append("\n ,ab.ins_num");
		sb.append("\n ,sp.rating_id as rating_id");
		sb.append("\n ,sp.display_order");
		if( iRatingId > 0 && iStressValue > 0){
			sb.append("\n ,CASE WHEN sp.rating_id = "+iRatingId+" THEN (SELECT cr.rating_id FROM credit_rating cr WHERE  cr.display_order = sp.display_order+"+iStressValue+" AND cr.credit_source_id = 20012)");
			sb.append("\n ELSE 0 END as rating_id_stress");
			sb.append("\n ,CASE WHEN sp.rating_id = "+iRatingId+" THEN (SELECT cr.display_order FROM credit_rating cr WHERE  cr.display_order = sp.display_order+"+iStressValue+" AND cr.credit_source_id = 20012)");
			sb.append("\n ELSE 0 END as display_order_stress");
		}else{
			sb.append("\n ,(SELECT cr.rating_id FROM credit_rating cr WHERE  cr.display_order = sp.display_order+"+iStressValue+" AND cr.credit_source_id = 20012)");
			sb.append("\n  as rating_id_stress");
			sb.append("\n ,(SELECT cr.display_order FROM credit_rating cr WHERE  cr.display_order = sp.display_order+"+iStressValue+" AND cr.credit_source_id = 20012)");
			sb.append("\n  as display_order_stress");
		}
		sb.append("\n FROM (");
		sb.append("\n SELECT 	t.tran_num,");
		sb.append("\n MAX(display_order) display_order");
		sb.append("\n FROM ab_tran_info_view t");
		sb.append("\n LEFT JOIN credit_rating cr");
		sb.append("\n ON cr.rating_name = t.value");
		sb.append("\n AND (t.type_name like '%_LP' ");
		sb.append("\n OR  t.type_name like '%_CP') ");
		sb.append("\n AND t.value <> 'NR'");
		sb.append("\n GROUP BY t.tran_num) x LEFT JOIN credit_rating sp ");
		sb.append("\n ON sp.display_order = (CASE WHEN x.display_order = -1 ");
		sb.append("\n THEN 23 ");
		sb.append("\n ELSE x.display_order ");
		sb.append("\n END) ");
		sb.append("\n AND sp.credit_source_id = 20012");
		sb.append("\n INNER JOIN ab_tran ab ");
		sb.append("\n ON ab.tran_num = x.tran_num ");
		sb.append("\n AND ab.tran_status = "+ TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n AND ab.tran_type   = "+ TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt());
		sb.append("\n AND ab.toolset     = "+ TOOLSET_ENUM.BOND_TOOLSET.toInt());
		sb.append("\n AND ab.ins_type in ("+ EnumsInstrumentsMX.MX_BONDMBSUDI.toInt()+","+ EnumsInstrumentsMX.MX_BONDMBS.toInt()+","+ EnumsInstrumentsMX.MX_CBND.toInt()+","+EnumsInstrumentsMX.MX_CBNDF.toInt()+","+EnumsInstrumentsMX.MX_CBNDEUROBOND.toInt()+","+EnumsInstrumentsMX.MX_CBNDUDI.toInt()+")");
		
		
			try {
				
				@SuppressWarnings("unused")
				int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
				if (tReturn.getNumRows() <= 0){

					_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacin Necesaria para el Calculo de Credit Metrics \n ");
					tReturn.destroy();
					Util.exitFail();			
				}
			
			} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
			}
			
			return tReturn;
		
		
	}
	
	
	public Table getAllSens(Table tArg) throws OException{
		
		Table tResultList;
		Table tResultSim;
	    Table tArgt               = tArg.copyTable();  
  			
		tResultList = Sim.createResultListForSim ();

	   	SimResult.addResultForSim(tResultList, SimResultType.create(EnumsUserSimResultType.MX_SIM_RISK_SENSITIVITY.toString()));
		        
	    //Add Column
	    tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);
	    
    
	    Table tAllTrades = getAllValidatedBonds();

		    
	    if(tAllTrades.getNumRows() <= 0){
	    	return Util.NULL_TABLE;
	    }
	    
	    
		// Creating a query id based on the list of transactions
		int iQuery = Query.tableQueryInsert(getAllValidatedBonds(), "tran_num");
		
		// The QueryId is now set into argt to prepare for the execution of a simulation.
		tArgt.setInt("QueryId", 1, iQuery);
	
		// Get all deals for specific Portfolio from tDeals and into a query table
		tResultSim = Sim.runRevalByQidFixed (tArgt, tResultList, Ref.getLocalCurrency ());
		
		Table tGenResult = SimResult.getGenResults(tResultSim);
		
		//Get Custom Sim Result
		tGenResult.sortCol(1);
		int iFindRow   = tGenResult.findInt(1, EnumsUserSimResultType.MX_SIM_RISK_SENSITIVITY.toInt(), SEARCH_ENUM.FIRST_IN_GROUP);
		Table tAllSens = tGenResult.getTable("result", iFindRow);
		
		return tAllSens;

	}
	
	
	public Table getAllValidatedBonds() throws OException{
		
	 	Table tReturn = Util.NULL_TABLE;
		tReturn       = Table.tableNew("All Validated Bonds");
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nSELECT tran_num");
		sb.append("\n FROM ab_tran ab");
		sb.append("\n WHERE ab.toolset in ("+TOOLSET_ENUM.BOND_TOOLSET.toInt()+")");
		sb.append("\n AND ab.ins_type in ("+ EnumsInstrumentsMX.MX_CBND.toInt()+","+EnumsInstrumentsMX.MX_CBNDF.toInt()+","+EnumsInstrumentsMX.MX_CBNDEUROBOND.toInt()+","+EnumsInstrumentsMX.MX_CBNDUDI.toInt()+")");
		sb.append("\n AND ab.tran_type in ("+TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt()+")");
		sb.append("\n AND ab.tran_status in (").append(TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt())
								 .append(")");

		
		try {
			
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
			
		} catch (OException exception){
			
			_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
			tReturn.destroy();
			Util.exitFail();
		}
		
		return tReturn;
		
	}

	
	public Table getAllSpreadTable() throws OException {
		
		Table tReturn    = Util.NULL_TABLE;
		Table tReturnAux = Util.NULL_TABLE;
		tReturn          = Table.tableNew("All Global Spread Table");
		tReturnAux       = Table.tableNew("Return Table Aux");
			
		StringBuffer sb = new StringBuffer();
			
		tReturnAux.addCol("matrix_name", COL_TYPE_ENUM.COL_STRING);
		tReturnAux.addCol("rating_id_from", COL_TYPE_ENUM.COL_INT);
		tReturnAux.addCol("rating_id_to", COL_TYPE_ENUM.COL_INT);
		tReturnAux.addCol("display_order_to", COL_TYPE_ENUM.COL_INT);
				
		
		sb.append("\n SELECT u.id , u.matrix_name, u.spread , cr_from.rating_id as rating_id_from , cr_to.rating_id as rating_id_to , cr_to.display_order as display_order_to");
		sb.append("\n FROM user_mx_creditrisk_sp_matrix u , credit_rating cr_from , credit_rating  cr_to");
		sb.append("\n WHERE u.credit_rating_from =  cr_from.rating_name");
		sb.append("\n AND u.credit_rating_to =  cr_to.rating_name");
		sb.append("\n AND cr_from.credit_source_id = 20012");
		sb.append("\n AND cr_to.credit_source_id = 20012");
		
		try {
				
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
			if (tReturn.getNumRows() <= 0){
	
						_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacin Necesaria para el Calculo de Credit Metrics \n ");
						tReturn.destroy();
						Util.exitFail();			
			}
			
		} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
		}
		
	
		tReturnAux.select(tReturn, "DISTINCT , matrix_name,rating_id_from", "rating_id_from GT 0");
		tReturnAux.setColValInt("rating_id_to", 21004);
		tReturnAux.setColValInt("display_order_to", 22);
		
		
		tReturn.select(tReturnAux, "matrix_name,rating_id_from,rating_id_to,display_order_to", "rating_id_to GT 0");

		
		return tReturn;
		
		
		
	}
	
	
	public Table getTranMatrix(Table tAllSpread) throws OException{
	
		
		Table tTranMatrix     = Table.tableNew("Tran Matrix");
		Table tTranMatrixFrom = Table.tableNew("Tran Matrix CR From");
		
		tTranMatrixFrom.addCol("rating_id_from", COL_TYPE_ENUM.COL_INT);
		
		tTranMatrix       = tAllSpread.cloneTable();
		tTranMatrix.addRow();
		tTranMatrix.setString("matrix_name", 1, "Matriz de Transicion");
		tTranMatrix.select(tAllSpread, "*", "matrix_name EQ $matrix_name AND id GT 0");
		
		tTranMatrix.addCol("spread_acu", COL_TYPE_ENUM.COL_DOUBLE);
		
		tTranMatrix.copyColDistinct("rating_id_from", tTranMatrixFrom, "rating_id_from");
		
		for(int i = 1 ; i <= tTranMatrixFrom.getNumRows();i++){
			
			Table tTranMatrixAux = Table.tableNew();
			tTranMatrixAux.select(tTranMatrix, "*", "rating_id_from EQ "+tTranMatrixFrom.getInt("rating_id_from", i));
			tTranMatrixAux.addCol("spread_acu", COL_TYPE_ENUM.COL_DOUBLE);
			tTranMatrixAux.sortCol("display_order_to");

			
			for(int j=1 ; j <= tTranMatrixAux.getNumRows();j++){
				
				if(tTranMatrixAux.getInt("display_order_to", j) == 1){
					
					tTranMatrixAux.setDouble("spread_acu", j, tTranMatrixAux.getDouble("spread", j));
					
				
				}else{
					
					
					tTranMatrixAux.setDouble("spread_acu", j, tTranMatrixAux.getDouble("spread", j)+tTranMatrixAux.getDouble("spread_acu", j-1));

				}
				
			}
			
			tTranMatrix.select(tTranMatrixAux, "spread_acu", "rating_id_from EQ $rating_id_from AND rating_id_to EQ $rating_id_to AND display_order_to EQ $display_order_to ");
			
		}
		
		return tTranMatrix;
		
	}
	
	public Table getCreditVaRCalc(int iScenarioNumber) throws OException {
		
		Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("Credit Var Calc");
			
		StringBuffer sb = new StringBuffer();
		
		sb.append("\n with base_table as ( ");
		sb.append("\n      select CAST(round(dbms_random.value(0, 1), 12) as float) as randomNum");
		sb.append("\n	      from dual");
		sb.append("\n	      connect by level <= 100000),");
		sb.append("\n	    base_table_ordered as (");
		sb.append("\n	     select randomNum as random1 from base_table");
		sb.append("\n	      order by random1 desc");
		sb.append("\n	    ),");
		sb.append("\n	    base_table_tuncrated as (");
		sb.append("\n	        select base_table_ordered.*, rownum as numLinea from base_table_ordered");
		sb.append("\n	        where rownum <="+iScenarioNumber );
		sb.append("\n	    ),     ");
		sb.append("\n	    base_table_filtered as (");
		sb.append("\n	        select * from base_table_tuncrated");
		sb.append("\n	        where numLinea = "+iScenarioNumber);
		sb.append("\n	    ),   ");
		sb.append("\n	    tickers as (");
		sb.append("\n	    select distinct ins_num, internal_portfolio");
		sb.append("\n	    from user_mx_creditmetrics_temp");
		sb.append("\n	    ),");
		sb.append("\n	    base_tickers_table as (");
		sb.append("\n	      select tickers.ins_num, tickers.internal_portfolio, b.random1 from base_table_filtered b ");
		sb.append("\n	      cross join tickers");
		sb.append("\n	    ), value_set as (");
		sb.append("\n	      select distinct  cm.ins_num, ");
		sb.append("\n	      btt.random1 as randomNumber,");
		sb.append("\n	      cm.internal_portfolio,");
		sb.append("\n	      cm.tran_rating_id_to,");
		sb.append("\n	      cm.dif_perdida_ind as perdida_individual,");
		sb.append("\n	      cm.dif_perdida_ind_baja as perdida_individual_baja,");
		sb.append("\n	      cm.dif_perdida_ind_alta as perdida_individual_alta");
		sb.append("\n	      from base_tickers_table btt");
		sb.append("\n	      inner join user_mx_creditmetrics_temp cm on ( btt.ins_num = cm.ins_num");
		sb.append("\n	              and btt.internal_portfolio = cm.internal_portfolio");
		sb.append("\n	     and cm.tran_rating_id_to = ( ");
		sb.append("\n	        select min(c.tran_rating_id_to) ");
		sb.append("\n	        from user_mx_creditmetrics_temp c");
		sb.append("\n	        where c.ins_num = cm.ins_num");
		sb.append("\n	        and c.internal_portfolio = cm.internal_portfolio");
		sb.append("\n	        and c.tran_spread_sum > btt.random1 )");
		sb.append("\n	      ) )  ");
		sb.append("\n	      SELECT a.ins_num , a.internal_portfolio ,a.perdida_individual as creditvar, a.perdida_individual_baja as creditvar_baja,a.perdida_individual_alta as creditvar_alta, a.randomNumber ,a.tran_rating_id_to");
		sb.append("\n	      FROM value_set a");
		sb.append("\n	      where a.randomNumber = ( ");
		sb.append("\n	              select max(b.randomNumber) ");
		sb.append("\n	              from value_set b");
		sb.append("\n	              where a.ins_num = b.ins_num");
		sb.append("\n	              and a.internal_portfolio = b.internal_portfolio )");
		

		try {
				
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
			if (tReturn.getNumRows() <= 0){
	
						_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacin Necesaria para el Calculo de Credit Metrics \n ");
						tReturn.destroy();
						Util.exitFail();			
			}
			
		} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
		}
		
		return tReturn;

	}
	
	
	public Table getAllCreditRatingsSP() throws OException {
		
		Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("Credit Ratings S & P");
			
		StringBuffer sb = new StringBuffer();
		
		sb.append("\n SELECT rating_id,rating_name ");
		sb.append("\n FROM credit_rating ");
		sb.append("\n WHERE credit_source_id = 20012 ");
		
		try {
				
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
			if (tReturn.getNumRows() <= 0){
	
						_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacin de Credit Ratings \n ");
						tReturn.destroy();
						Util.exitFail();			
			}
			
		} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
		}
		
		
		return tReturn;

	}
	
	public int getAllCreditRatingsSPByCreditName(String sCreditName) throws OException {
		
		Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("Credit Ratings S & P By Name");
			
		StringBuffer sb = new StringBuffer();
		
		sb.append("\n SELECT rating_id ");
		sb.append("\n FROM credit_rating ");
		sb.append("\n WHERE credit_source_id = 20012 ");
		sb.append("\n AND rating_name = '"+sCreditName+"'");
		
		try {
				
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
				
			if (tReturn.getNumRows() <= 0){
	
						_Log.printMsg(EnumTypeMessage.ERROR," No Existe Informacin de Credit Ratings \n ");
						tReturn.destroy();
						Util.exitFail();			
			}
			
		} catch (OException exception){
				
				_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
				Util.exitFail();
		}
		
		
		return tReturn.getInt(1, 1);

	}
	
	
	public Table getAllDownGradeList() throws OException {
		
		Table tReturn   = Util.NULL_TABLE;
		tReturn         = Table.tableNew("DownGrade List CR");
		
		tReturn.addCol("id", COL_TYPE_ENUM.COL_INT);
		tReturn.addCol("down_value", COL_TYPE_ENUM.COL_STRING);
		tReturn.addNumRows(5);
		
		tReturn.setInt("id",1,1);
		tReturn.setInt("id",2,2);
		tReturn.setInt("id",3,3);
		tReturn.setInt("id",4,4);
		tReturn.setInt("id",5,5);
		
		tReturn.setString("down_value",1,"1");
		tReturn.setString("down_value",2,"2");
		tReturn.setString("down_value",3,"3");
		tReturn.setString("down_value",4,"4");
		tReturn.setString("down_value",5,"5");
		
		return tReturn;
		
	}
	
	public Table getSpotCurve() throws OException{
		
		
		Table tSpotTable = Table.tableNew("Spot Curve");
		
		try {
			tSpotTable = Index.getOutput(SPOT_CURVE);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,"Cant retieve the Spot Curve \n "+ e.getMessage());
			Util.exitFail();
		}
		
		return tSpotTable;
		
	}

}
