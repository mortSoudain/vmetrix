-- Creacion de User Tables

-- user_mx_traspasos_ani_catalogo

create table TST_GRUPO_163961_01AUG16.user_mx_traspasos_ani_catalogo
(
 ticker varchar2(255) ,
 categoria_name varchar2(255) ,
 afecto_limite int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_traspasos_ani_catalogo');