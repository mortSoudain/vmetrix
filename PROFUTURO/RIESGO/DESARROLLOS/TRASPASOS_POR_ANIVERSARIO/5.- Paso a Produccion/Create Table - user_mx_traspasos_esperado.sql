-- Creacion de User Tables

-- user_mx_traspasos_esperado

create table TST_GRUPO_163961_01AUG16.user_mx_traspasos_esperado
(
 internal_portfolio int ,
 monto_esperado double precision ,
 monto_esperado_sbp double precision 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_traspasos_esperado');
