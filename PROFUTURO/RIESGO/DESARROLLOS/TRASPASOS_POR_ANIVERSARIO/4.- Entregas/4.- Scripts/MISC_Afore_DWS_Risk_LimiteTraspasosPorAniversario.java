/*$Header: v 1.0, 13/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Risk_LimiteTraspasosPorAniversario.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de limites de traspasos por aniversario
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;
import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsPortfolios;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.PFOLIO_RESULT_TYPE;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;

public class MISC_Afore_DWS_Risk_LimiteTraspasosPorAniversario implements IScript {
	
	// Declare utils
		protected String	sScriptName		=	this.getClass().getSimpleName();
		protected UTIL_Log	_Log			=	new UTIL_Log(sScriptName);
		UTIL_Afore			UtilAfore		=	new UTIL_Afore();
	
	// UDW internal functions variables
		ENUM_UDW_ACTION	callback_type;
		ENUM_UDW_EVENT	callback_event;
	    private Table	tblProperties		=	Util.NULL_TABLE;
	    private Table	tblGlobalProperties	=	Util.NULL_TABLE;
		private Table	tDisplay			=	Util.NULL_TABLE;
		private Table	tReturnt			=	Util.NULL_TABLE;
		private String	sCellName			=	null;
		
	// UDW header values
		private String sSelectedPortfolio	=	"";
		private int iSelectedPortfolio		=	0;
		private Table tPortfolios			=	Util.NULL_TABLE;
		
	// Time variables
		private int 		iToday			=	0;
		
	// Used ENUMS
		private static final EnumTypeMessage MSG_ERROR	=	EnumTypeMessage.ERROR;
		private static final EnumTypeMessage MSG_DEBUG	=	EnumTypeMessage.DEBUG;
		private static final EnumTypeMessage MSG_INFO	=	EnumTypeMessage.INFO;
		private static final COL_TYPE_ENUM COL_INT		=	COL_TYPE_ENUM.COL_INT;
		private static final COL_TYPE_ENUM COL_STRING	=	COL_TYPE_ENUM.COL_STRING;
		private static final COL_TYPE_ENUM COL_DOUBLE	=	COL_TYPE_ENUM.COL_DOUBLE;
		
		// UDW
		private static final int LABEL				=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt();
		private static final int COMBOBOX			=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt();
		private static final int TEXTAREA			=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_TED_WIDGET.toInt();
		private static final int BUTTON				=	SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt();
		
		// TRAN_STATUS
		private static final int STATUS_NEW			=	TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt();			//2
		private static final int STATUS_VALIDATED	=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();		//3
		private static final int STATUS_PROPOSED	=	TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt();		//7
		private static final int STATUS_PENDING		=	TRAN_STATUS_ENUM.TRAN_STATUS_PENDING.toInt();		//1
		
 		// PORTFOLIOS
 		private static final int SB1	=	EnumsPortfolios.MX_PORTFOLIO_SB1.toInt();
 		private static final int SB2	=	EnumsPortfolios.MX_PORTFOLIO_SB2.toInt();
 		private static final int SB3	=	EnumsPortfolios.MX_PORTFOLIO_SB3.toInt();
 		private static final int SB4	=	EnumsPortfolios.MX_PORTFOLIO_SB4.toInt();
		
		// USER TABLES
		private static final String USER_MX_TRASPASOS_ANI_CATALOGO	=	EnumsUserTables.USER_MX_TRASPASOS_ANI_CATALOGO.toString();
		private static final String USER_MX_TRASPASOS_ESPERADO		=	EnumsUserTables.USER_MX_TRASPASOS_ESPERADO.toString();

            
	public void execute(IContainerContext context) {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		initializeScript();
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
					
				// Adding columns layout
					addLayoutColumns();
									
				// FINALLY, Setting table properties
					setTableProperties();
										
				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					getCallBackInfo();

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						
		    			if (sCellName.equals("btnTraspasoEsperado")){
		    				updateTraspasoEsperado();		    				
		    			}
		    									
						break;
					case UDW_COMBO_BOX_EVENT:
						
		    			if (sCellName.equals("filterFondo")){
							setDataOnTable(tReturnt,iSelectedPortfolio);
							updateHeaderValues(tReturnt);
							filterDealsByPortfolio(tReturnt,iSelectedPortfolio);
		    			}
		    			
						break;
					case UDW_TED_EVENT:
						
		    			if (sCellName.equals("valueTraspasoEsperado") || sCellName.equals("valueTraspasoEsperadoSBP")){
							verifyFormatTraspasoEsperado();
		    			}
		    			
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
		
		_Log.markEndScript();

	}

	private void initializeScript() {
		_Log.markStartScript();

		try {
			iToday			=	OCalendar.today();
			tPortfolios		=	getPortfolios();
			callback_type	=	ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
		} catch (OException e) {
			_Log.printMsg(
					MSG_ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
	}

	private void addVisualElements() {
		try {
			// Creating display table
				tDisplay = Table.tableNew();
				
			// Set some header values
				sSelectedPortfolio	=	tPortfolios.getString("Portfolios", 1);
				iSelectedPortfolio 	=	Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sSelectedPortfolio);
				
			// Columna 1
				tDisplay.scriptDataAddWidget("labelFondo",					LABEL,		"x=100, y=20, h=20, w=220","label=Fondo:");
				tDisplay.scriptDataAddWidget("filterFondo",					COMBOBOX,	"x=240, y=20, h=20, w=220","label="+sSelectedPortfolio, tPortfolios);
				tDisplay.scriptDataAddWidget("labelNAV",					LABEL,		"x=100, y=40, h=20, w=220","label=NAV:");
				tDisplay.scriptDataAddWidget("valueNAV",					LABEL,		"x=240, y=40, h=20, w=143","just=right,label=");
				tDisplay.scriptDataAddWidget("labelTraspasoEsperado",		LABEL,		"x=100, y=60, h=20, w=220","label=Traspaso Esperado:");
				tDisplay.scriptDataAddWidget("valueTraspasoEsperado",		TEXTAREA,	"x=240, y=60, h=20, w=145","just=right,label=");
				tDisplay.scriptDataAddWidget("btnTraspasoEsperado",			BUTTON,		"x=390, y=60, h=36, w=70","label=Guardar");
				tDisplay.scriptDataAddWidget("labelTraspasoEsperadoSBP",	LABEL,		"x=100, y=80, h=20, w=220","label=Traspaso Esperado SBP:");
				tDisplay.scriptDataAddWidget("valueTraspasoEsperadoSBP",	TEXTAREA,	"x=240, y=80, h=20, w=145","just=right,label=");
				tDisplay.scriptDataAddWidget("labelTraspasoEfectivo",		LABEL,		"x=100, y=100, h=20, w=220","label=Traspaso Efectivo:");
				tDisplay.scriptDataAddWidget("valueTraspasoEfectivo",		LABEL,		"x=240, y=100, h=20, w=143","just=right,label=");

			// Columna 2
				tDisplay.scriptDataAddWidget("labelPorcentajeEsperado",		LABEL,		"x=600, y=20, h=20, w=130","label=% Esperado:");
				tDisplay.scriptDataAddWidget("valuePorcentajeEsperado",		LABEL,		"x=800, y=20, h=20, w=40","just=right,label=");
				tDisplay.scriptDataAddWidget("labelTraspasoSujetoALimite",	LABEL,		"x=600, y=40, h=20, w=130","label=% Efectivo Sujeto a Lim.:");
				tDisplay.scriptDataAddWidget("valuePorcentajeSujetoALimite",LABEL,		"x=800, y=40, h=20, w=40","just=right,label=");
				tDisplay.scriptDataAddWidget("labelCotaMin",				LABEL,		"x=600, y=60, h=20, w=130","label=% Cota Min (-5%):");
				tDisplay.scriptDataAddWidget("valueCotaMin",				LABEL,		"x=800, y=60, h=20, w=40","just=right,label=");
				tDisplay.scriptDataAddWidget("labelCotaMax",				LABEL,		"x=600, y=80, h=20, w=130","label=% Cota Max (+5%):");
				tDisplay.scriptDataAddWidget("valueCotaMax",				LABEL,		"x=800, y=80, h=20, w=40","just=right,label=");
				
				tDisplay.scriptDataMoveListBox("top=130,left=4,right=5,bottom=5");
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to set some of the visual elements on setVisualElements function."
					+ "Error: " + e.getMessage());
		}
	}

	private void addLayoutColumns() {
		try {
			tDisplay.addCol("internal_portfolio",	COL_INT);
			tDisplay.addCol("categoria_name",		COL_STRING);
			tDisplay.addCol("initial_pos",		COL_DOUBLE);
			tDisplay.addCol("initial_pos_not",		COL_DOUBLE);
			tDisplay.addCol("traspaso",	COL_DOUBLE);
			tDisplay.addCol("traspaso_not",	COL_DOUBLE);
			tDisplay.addCol("traspaso_sbp",	COL_DOUBLE);
			tDisplay.addCol("cota_min",				COL_DOUBLE);
			tDisplay.addCol("cota_max",				COL_DOUBLE);
			tDisplay.addCol("status_cota_min",		COL_STRING);
			tDisplay.addCol("status_cota_max",		COL_STRING);	
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to add some cols for layout on addLayoutColumns function."
					+ "Error: " + e.getMessage());
		}		
	}
	
	private void setDataOnTable(Table tDisplay, int iSelectedPortfolio) {
		
		Table tTraspasos	=	Util.NULL_TABLE;
		tPortfolios			=	getPortfolios();
				
		// Get trades
			_Log.printMsg(MSG_INFO,"\n -> Obteniendo trades desde query");
			try{
				tTraspasos = Table.tableNew();
				StringBuilder sb = new StringBuilder();
				sb.append("\n /* ");
				sb.append("\n LIMITE TRASPASOS - Query Filtros (New) ");
				sb.append("\n  ");
				sb.append("\n Pending 1 ");
				sb.append("\n New 2 ");
				sb.append("\n Simulated 7 ");
				sb.append("\n  ");
				sb.append("\n Validated 3 ");
				sb.append("\n Canceled 5 ");
				sb.append("\n Deleted 14 ");
				sb.append("\n Closeout 22 ");
				sb.append("\n  ");
				sb.append("\n */ ");
				sb.append("\n with price_base as ");
				sb.append("\n ( ");
				sb.append("\n 	select 	id_number ");
				sb.append("\n 			,name ");
				sb.append("\n 			,case 	when id_number in (0, 3) then 100 ");
				sb.append("\n 					when id_number = 2 then 10000 ");
				sb.append("\n 					when id_number = 8 then 1000 ");
				sb.append("\n 					when id_number = 9 then 10 ");
				sb.append("\n 					else 1 ");
				sb.append("\n 			end base ");
				sb.append("\n 	from price_input_format ");
				sb.append("\n ), ");
				sb.append("\n trades as  ");
				sb.append("\n ( ");
				sb.append("\n 	select 	ab.deal_tracking_num ");
				sb.append("\n 			,ab.tran_num ");
				sb.append("\n 			,ab.ins_num ");
				sb.append("\n 			,ab.toolset ");
				sb.append("\n 			,ab.ins_type ");
				sb.append("\n 			,h.ticker ");
				sb.append("\n 			,ab.reference ");
				sb.append("\n 			,ab.currency ");
				sb.append("\n 			,ab.cflow_type ");
				sb.append("\n 			,ab.trade_date ");
				sb.append("\n 			,ab.internal_portfolio ");
				sb.append("\n 			,ab.external_portfolio ");
				sb.append("\n 			,ab.tran_status ");
				sb.append("\n 			,0 antigua_nueva ");
				sb.append("\n 	 		,case   ");
				sb.append("\n 	 			when cat.afecto_limite is null then 'NO MAPEADO'  ");
				sb.append("\n 	 			when cat.afecto_limite = 0 then 'NO AFECTO A LIMITE'  ");
				sb.append("\n 	 			when cat.afecto_limite = 1 then 'AFECTO A LIMITE'  ");
				sb.append("\n 	 		end afecto_limite   ");
				sb.append("\n 			,cat.categoria_name ");
				sb.append("\n 			,case 	when ab.toolset in (9, 10) then ab.position  ");
				sb.append("\n 					else ab.mvalue  ");
				sb.append("\n 			end ohd_position ");
				sb.append("\n 			,p.notnl ");
				sb.append("\n 			,ab.price ");
				sb.append("\n 			,pb.base ");
				sb.append("\n 			,NVL(v.precio_sucio_24h, 1.0) ohd_precio_24h ");
				sb.append("\n 			,NVL(fx.precio_sucio_24h, 1.0) ohd_fx_mxn ");
				sb.append("\n 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 					when ab.toolset = 41 and ab.ins_type = 61004 then ab.mvalue*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*NVL(fx.precio_sucio_24h, 1.0)) ");
				sb.append("\n 					when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.mvalue*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*pb.base*NVL(fx.precio_sucio_24h, 1.0)) ");
				sb.append("\n 					when ab.toolset = 50 then ab.mvalue*NVL(v.precio_sucio_24h, 1.0)/pb.base ");
				sb.append("\n 					else ab.mvalue*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 			end ohd_mtm ");
				sb.append("\n 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 					when ab.toolset in (34, 41) then ab.mvalue*p.notnl*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 					when ab.toolset = 50 then ab.mvalue*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 					else ab.mvalue*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n 			end ohd_mtm_not ");
				sb.append("\n 	from ab_tran ab ");
				sb.append("\n 		inner join system_dates d on ab.trade_date <= d.business_date ");
				sb.append("\n 		left join parameter p on p.ins_num = ab.ins_num and p.param_seq_num = 0 ");
				sb.append("\n 		left join misc_ins m on m.ins_num = ab.ins_num and p.param_seq_num = 0 ");
				sb.append("\n 		left join price_base pb on pb.id_number = m.price_inp_format ");
				sb.append("\n 		left join header h on h.ins_num = ab.ins_num ");
				sb.append("\n 		left join user_mx_traspasos_ani_catalogo cat on cat.ticker = h.ticker ");
				sb.append("\n 		left join user_mx_vectorprecios_diario v on v.ticker = replace(h.ticker, 'USD/MXN', '*C_MXPUSD_FIX') ");
				sb.append("\n 		left join currency c on c.id_number = ab.currency ");
				sb.append("\n 		left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		 ");
				sb.append("\n 																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	 ");
				sb.append("\n 	where ab.tran_type = 0 ");
				sb.append("\n 			and ab.asset_type = 2 ");
				sb.append("\n 			and ab.tran_status = 3 ");
				sb.append("\n 			and ab.ins_type <> 1000006 ");
				sb.append("\n 			and ab.toolset <> 6 ");
				sb.append("\n 			and (ab.toolset not in (9, 10) or ab.trade_date >= d.business_date) ");
				sb.append("\n 			and ab.cflow_type not in (2051, 2052) ");
				sb.append("\n 			 ");
				sb.append("\n 	union all ");
				sb.append("\n  ");
				sb.append("\n 	select 	ab.deal_tracking_num ");
				sb.append("\n 			,ab.tran_num ");
				sb.append("\n 			,ab.ins_num ");
				sb.append("\n 			,ab.toolset ");
				sb.append("\n 			,ab.ins_type ");
				sb.append("\n 			,h.ticker ");
				sb.append("\n 			,ab.reference ");
				sb.append("\n 			,ab.currency ");
				sb.append("\n 			,ab.cflow_type ");
				sb.append("\n 			,ab.trade_date ");
				sb.append("\n 			,ab.internal_portfolio ");
				sb.append("\n 			,case when ab.external_portfolio = 0  ");
				sb.append("\n 					then (case 	when ab.internal_portfolio = 20010  ");
				sb.append("\n 								then 20014  ");
				sb.append("\n 								else ab.internal_portfolio-1  ");
				sb.append("\n 						 end) ");
				sb.append("\n 					else ab.external_portfolio ");
				sb.append("\n 			end external_portfolio ");
				sb.append("\n 			,ab.tran_status ");
				sb.append("\n 			,1 antigua_nueva ");
				sb.append("\n 	 		,case   ");
				sb.append("\n 	 			when cat.afecto_limite is null then 'NO MAPEADO'  ");
				sb.append("\n 	 			when cat.afecto_limite = 0 then 'NO AFECTO A LIMITE'  ");
				sb.append("\n 	 			when cat.afecto_limite = 1 then 'AFECTO A LIMITE'  ");
				sb.append("\n 	 		end afecto_limite   ");
				sb.append("\n 			,cat.categoria_name ");
				sb.append("\n 			,ab.position ohd_position ");
				sb.append("\n 			,p.notnl ");
				sb.append("\n 			,ab.price ");
				sb.append("\n 			,pb.base ");
				sb.append("\n 			,NVL(v.precio_sucio_24h, 1.0) ohd_precio_24h ");
				sb.append("\n 			,NVL(fx.precio_sucio_24h, 1.0) ohd_fx_mxn ");
				sb.append("\n			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n					when ab.toolset = 41 and ab.ins_type = 61004 then ab.position*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*NVL(fx.precio_sucio_24h, 1.0)) ");
				sb.append("\n					when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.position*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*pb.base*NVL(fx.precio_sucio_24h, 1.0)) ");
				sb.append("\n					when ab.toolset = 50 then ab.position*NVL(v.precio_sucio_24h, 1.0)/pb.base ");
				sb.append("\n					else ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n			end ohd_mtm ");
				sb.append("\n			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n					when ab.toolset in (34, 41) then ab.position*p.notnl*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n					when ab.toolset = 50 then ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n					else ab.position*NVL(v.precio_sucio_24h, 1.0) ");
				sb.append("\n			end ohd_mtm_not ");
				sb.append("\n 	from ab_tran ab ");
				sb.append("\n 			inner join system_dates d on ab.trade_date = d.business_date ");
				sb.append("\n 			left join parameter p on p.ins_num = ab.ins_num and p.param_seq_num = 0 ");
				sb.append("\n 			left join misc_ins m on m.ins_num = ab.ins_num and p.param_seq_num = 0 ");
				sb.append("\n 			left join price_base pb on pb.id_number = m.price_inp_format ");
				sb.append("\n 			left join header h on h.ins_num = ab.ins_num ");
				sb.append("\n 			left join user_mx_traspasos_ani_catalogo cat on cat.ticker = h.ticker ");
				sb.append("\n 			left join user_mx_vectorprecios_diario v on v.ticker = replace(h.ticker, 'USD/MXN', '*C_MXPUSD_FIX') ");
				sb.append("\n 			left join currency c on c.id_number = ab.currency ");
				sb.append("\n 			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		 ");
				sb.append("\n 																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	 ");
				sb.append("\n 	where ab.tran_type = 0 ");
				sb.append("\n 			and ab.asset_type = 2 ");
				sb.append("\n 			and ab.tran_status in (1, 2, 7) ");
				sb.append("\n 			and (ab.toolset in (34, 41, 50) or ab.buy_sell = 1) ");
				sb.append("\n 			and ab.ins_type <> 1000006 ");
				sb.append("\n 			and ab.toolset <> 6 ");
				sb.append("\n 			and (ab.external_portfolio <> 0 or ab.toolset in (34, 41, 50)) ");
				sb.append("\n 			and (ab.toolset not in (9, 10) or ab.cflow_type in (2051, 2052)) ");
				sb.append("\n ) ");
				sb.append("\n select 	t.* ");
				sb.append("\n 		,case when t.antigua_nueva = 0 then t.ohd_mtm else 0 end ohd_initial_pos ");
				sb.append("\n 		,case when t.antigua_nueva = 0 then t.ohd_mtm_not else 0 end ohd_initial_pos_not ");
				sb.append("\n 		,case when t.antigua_nueva = 1  ");
				sb.append("\n 				then ( ");
				sb.append("\n 						case 	when t.internal_portfolio = 20010 and t.external_portfolio = 20014 then t.ohd_mtm ");
				sb.append("\n 								when t.internal_portfolio <> 20010 and t.external_portfolio <> 20014 then t.ohd_mtm ");
				sb.append("\n 						end ");
				sb.append("\n 					) ");
				sb.append("\n 		end ohd_traspaso ");
				sb.append("\n 		,case when t.antigua_nueva = 1  ");
				sb.append("\n 		then ( ");
				sb.append("\n 				case 	when t.internal_portfolio = 20010 and t.external_portfolio = 20014 then t.ohd_mtm_not ");
				sb.append("\n 						when t.internal_portfolio <> 20010 and t.external_portfolio <> 20014 then t.ohd_mtm_not ");
				sb.append("\n 				end ");
				sb.append("\n 			) ");
				sb.append("\n 		end ohd_traspaso_not ");
				sb.append("\n 		,case when t.antigua_nueva = 1 and t.external_portfolio = 20014 then t.ohd_mtm else 0 end ohd_traspaso_sbp ");
				sb.append("\n from trades t ");
				sb.append("\n where t.internal_portfolio = "+iSelectedPortfolio+" ");
				sb.append("\n --where t.internal_portfolio = $$siefore$$ ");

				DBaseTable.execISql(tTraspasos, sb.toString());
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get traspasos data on tTraspasos Table, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
		
		// Paste values into display, as a resume by portfolio and category
			_Log.printMsg(MSG_INFO,"\n -> Generando resumen por portfolio y seteando tabla tDisplay");
			try{
				tDisplay.select(tTraspasos, "DISTINCT, internal_portfolio, categoria_name, afecto_limite", "internal_portfolio GT 0");				
				tDisplay.select(tTraspasos, "SUM, initial_pos, initial_pos_not, traspaso, traspaso_not, traspaso_sbp", "internal_portfolio EQ $internal_portfolio AND categoria_name EQ $categoria_name");						
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to paste resume values by portfolio and category into tDisplay, on setDataOnTable function."
						+ "Error: " + e.getMessage());		
			}
			
		// Get absolute values
			try {
				//tDisplay.mathABSCol("initial_pos");
				//tDisplay.mathABSCol("initial_pos_not");
				tDisplay.mathABSCol("traspaso");
				tDisplay.mathABSCol("traspaso_not");
				tDisplay.mathABSCol("traspaso_sbp");
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get absolute values of columns, on setDataOnTable function."
						+ "Error: " + e.getMessage());		
			}
	}
	
	private void updateHeaderValues(Table tReturnt) {
		
		double dSumaPosicionInicial		=	0.0;
		double dSumaTransferir			=	0.0;
		double dPorcentajeSujLim		=	0.0;
		double dPorcentajeCotaMin		=	0.0;
		double dPorcentajeCotaMax		=	0.0;
		double dTraspasoEfectivo		=	0.0;
		double dCurrentNAV				=	0.0;
		double dTraspasoEsperado		=	0.0;
		double dTraspasoEsperadoSBP		=	0.0;
		double dPorcentajeEsperado		=	0.0;
		
		// Calculate sum Posicion inicial ,sum posicion a transferir y traspaso efectivo
			try {
				for(int i=1; i<= tReturnt.getNumRows(); i++){
					
					int iPortfolio				=	tReturnt.getInt("internal_portfolio", i);
					String sAfectoLimite		=	tReturnt.getString("afecto_limite", i);
					String sCategoriaName		=	tReturnt.getString("categoria_name", i);
					double dPosicionInicial		=	tReturnt.getDouble("initial_pos", i);
					double dPosicionTransferir	=	tReturnt.getDouble("traspaso", i) + tReturnt.getDouble("traspaso_sbp", i);
					
					if(iPortfolio == iSelectedPortfolio && !sCategoriaName.contains("Derivados")){
						dTraspasoEfectivo = dTraspasoEfectivo + dPosicionTransferir;
						
						if(sAfectoLimite.equals("AFECTO A LIMITE")){
							dSumaPosicionInicial = dSumaPosicionInicial + dPosicionInicial;
							dSumaTransferir	= dSumaTransferir + dPosicionTransferir;
						}
					}
				}
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to calculate sum posicion inicial, sum posicion a transferir or traspaso efectivo values on updateHeaderValues function."
						+ "Error: " + e.getMessage());
			}
		
		// Calculate percents
			if(dSumaTransferir == 0.0 || dSumaPosicionInicial== 0.0)
				dPorcentajeSujLim = 0.0;
			else
				dPorcentajeSujLim = dSumaTransferir / dSumaPosicionInicial;
			
			dPorcentajeCotaMin = dPorcentajeSujLim-0.05;
			dPorcentajeCotaMax = dPorcentajeSujLim+0.05;
			
//		
//		// Add cols cota min and cota max
//			try {
//				if(tReturnt.getColNum("cota_min")==-1) tReturnt.addCol("cota_min", COL_DOUBLE);
//				if(tReturnt.getColNum("cota_max")==-1) tReturnt.addCol("cota_max", COL_DOUBLE);
//				if(tReturnt.getColNum("status_cota_min")==-1) tReturnt.addCol("status_cota_min", COL_STRING);
//				if(tReturnt.getColNum("status_cota_max")==-1) tReturnt.addCol("status_cota_max", COL_STRING);
//			} catch (OException e) {
//				_Log.printMsg(MSG_ERROR,
//						"Unable to add cols cota_min or cota_max on updateHeaderValues function."
//						+ "Error: " + e.getMessage());
//			}
			
		// Calculate cota_min and cota_max
			// TODO:Calculate only when matched portfolio
			try {
				for(int i=1; i<= tReturnt.getNumRows(); i++){
					double dPosicionInicial = tReturnt.getDouble("initial_pos", i);
					double dPosicionTransferir = tReturnt.getDouble("traspaso", i) + tReturnt.getDouble("traspaso_sbp", i);
					
					double dCotaMin =  dPosicionInicial * dPorcentajeCotaMin;
					double dCotaMax =  dPosicionInicial * dPorcentajeCotaMax;
					
					String sAfectoLimite = tReturnt.getString("afecto_limite", i);
					
					if(sAfectoLimite.equals("AFECTO A LIMITE")){
						
						if(dCotaMin<0) tReturnt.setDouble("cota_min", i, 0);
						else tReturnt.setDouble("cota_min", i, dCotaMin);
						
						tReturnt.setDouble("cota_max", i, dCotaMax);
						
						if(dPosicionTransferir<dCotaMin) tReturnt.setString("status_cota_min", i, "ERROR");
						else tReturnt.setString("status_cota_min", i, "OK");
						
						if(dPosicionTransferir>dCotaMax) tReturnt.setString("status_cota_max", i, "ERROR");
						else tReturnt.setString("status_cota_max", i, "OK");
					} else {
						tReturnt.setString("status_cota_min", i, "-");
						tReturnt.setString("status_cota_max", i, "-");					
					}
				}
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to calculate cota_min or cota_max values on updateHeaderValues function."
						+ "Error: " + e.getMessage());
			}
			
		// Get NAV, monto esperado and calculate porcentaje esperado
			try {
				int iFindedPortfolioRow	=	tPortfolios.unsortedFindInt("id_number", iSelectedPortfolio);
				dCurrentNAV				=	tPortfolios.getDouble("current_nav", iFindedPortfolioRow);
				dTraspasoEsperado		=	tPortfolios.getDouble("monto_esperado", iFindedPortfolioRow);
				dTraspasoEsperadoSBP	=	tPortfolios.getDouble("monto_esperado_sbp", iFindedPortfolioRow);
				dPorcentajeEsperado		=	((dTraspasoEsperado+dTraspasoEsperadoSBP)/dCurrentNAV)*100;
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get nav or monto esperado on updateHeaderValues function."
						+ "Error: " + e.getMessage());
			}
			
		// Print debug values
			_Log.printMsg(MSG_DEBUG, "NAV:" + dCurrentNAV);
			_Log.printMsg(MSG_DEBUG, "Traspaso Esperado:" + dTraspasoEsperado);
			_Log.printMsg(MSG_DEBUG, "Traspaso Esperado SBP:" + dTraspasoEsperadoSBP);
			_Log.printMsg(MSG_DEBUG, "Traspaso Efectivo:" + dTraspasoEfectivo);
			
			_Log.printMsg(MSG_DEBUG, "Porcentaje Esperado:" + dPorcentajeEsperado);
			_Log.printMsg(MSG_DEBUG, "Suma Posicion Inicial:" + dSumaPosicionInicial);
			_Log.printMsg(MSG_DEBUG, "Suma Posicion a Transferir:" + dSumaTransferir);
			_Log.printMsg(MSG_DEBUG, "Porcentaje Sujeto a Limite:" + dPorcentajeSujLim);
			_Log.printMsg(MSG_DEBUG, "Cota Min:" + dPorcentajeCotaMin);
			_Log.printMsg(MSG_DEBUG, "Cota Max:" + dPorcentajeCotaMax);
			
		// Set header values
			try {
				tReturnt.scriptDataSetWidgetString("valueNAV", String.format("%1$,.2f", dCurrentNAV));
				tReturnt.scriptDataSetWidgetString("valueTraspasoEsperado", String.format("%1$,.2f", dTraspasoEsperado));
				tReturnt.scriptDataSetWidgetString("valueTraspasoEsperadoSBP", String.format("%1$,.2f", dTraspasoEsperadoSBP));
				tReturnt.scriptDataSetWidgetString("valueTraspasoEfectivo", String.format("%1$,.2f", dTraspasoEfectivo));
				
				tReturnt.scriptDataSetWidgetString("valuePorcentajeEsperado", String.format("%1$,.2f", dPorcentajeEsperado));
				tReturnt.scriptDataSetWidgetString("valuePorcentajeSujetoALimite", String.format("%1$,.2f", dPorcentajeSujLim*100));
				tReturnt.scriptDataSetWidgetString("valueCotaMin", String.format("%1$,.2f", dPorcentajeCotaMin*100));
				tReturnt.scriptDataSetWidgetString("valueCotaMax", String.format("%1$,.2f", dPorcentajeCotaMax*100));
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to set some header values on updateHeaderValues function."
						+ "Error: " + e.getMessage());
			}
	}

	private void filterDealsByPortfolio(Table tReturnt, int iSelectedPortfolio) {
		
		//Show all data before filter
			try{
				tReturnt.rowAllShow();
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to show all rows, on filterDealsByPortfolio function."
						+ "Error: " + e.getMessage());
			}
		
		// Apply filter
			try{
				for(int i=1; i<= tReturnt.getNumRows(); i++){
					int iPortfolio = tReturnt.getInt("internal_portfolio", i);
					if(iPortfolio != iSelectedPortfolio){
						tReturnt.rowHide(i);
					}
				}
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to hide some rows, on filterDealsByPortfolio function."
						+ "Error: " + e.getMessage());
			}
		
		// Refresh display in case of update
			try{
				UserDataWorksheet.setRefreshDataTable(tReturnt);
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to refresh tDisplay, on filterDealsByPortfolio function."
						+ "Error: " + e.getMessage());
			}
	}

	private void setTableProperties() {
		try {
			// Initialize properties tables
				tblProperties			=	UserDataWorksheet.initProperties();
				tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
				
			// Setting col properties
				UserDataWorksheet.setColReadOnly("internal_portfolio",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("categoria_name",		tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("initial_pos",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("initial_pos_not",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("traspaso",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("traspaso_not",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("traspaso_sbp",	tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("cota_min",			tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("cota_max",			tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("status_cota_min",		tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("status_cota_max",		tDisplay, tblProperties);
	
			// Setting UDW properties
				UserDataWorksheet.setDisableSave(tblGlobalProperties);
				UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
				UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
				UserDataWorksheet.setDisableReload(tblGlobalProperties);
				UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to set some of the properties tables on setTableProperties function."
					+ "Error: " + e.getMessage());
		}
	}

	private void getCallBackInfo() {
		try {
			callback_event		=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
			tReturnt			=	UserDataWorksheet.getData();
			sCellName			=	tReturnt.scriptDataGetCallbackName();
			sSelectedPortfolio 	=	tReturnt.scriptDataGetWidgetString("filterFondo");
			iSelectedPortfolio 	=	Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sSelectedPortfolio);
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to get callback info,on getCallBackInfo function."
					+ "Error: " + e.getMessage());
		}
	}

	private void updateTraspasoEsperado() {
		
		String sTraspasoEsperado		= 	"";
		String sCurrentNav				=	"";
		String sTraspasoEsperadoSBP		= 	"";

		double dTraspasoEsperado		=	0.0;
		double dCurrentNAV				=	0.0;
		double dTraspasoEsperadoSBP		=	0.0;

		Table tUserMxTraspasosEsperado	=	Util.NULL_TABLE;
			
		// Get traspaso esperado and nav from UDW 
			try {
				sTraspasoEsperado		=	tReturnt.scriptDataGetWidgetString("valueTraspasoEsperado");
				sTraspasoEsperadoSBP	=	tReturnt.scriptDataGetWidgetString("valueTraspasoEsperadoSBP");
				sCurrentNav		 		=	tReturnt.scriptDataGetWidgetString("valueNAV");
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get Traspaso Esperado or Current Nav from UDW on updateTraspasoEsperado function."
						+ "Error: " + e.getMessage());
			}

		dCurrentNAV				=	Double.valueOf(sCurrentNav.replace(",",""));
		dTraspasoEsperado		=	Double.valueOf(sTraspasoEsperado.replace(",",""));
		dTraspasoEsperadoSBP	=	Double.valueOf(sTraspasoEsperadoSBP.replace(",",""));

		// If traspaso esperado < NAV set it, else don't
			if(dTraspasoEsperado < dCurrentNAV) {
				int iSearchedRow	=	0;
				
				try {
					// Get usertable that stores this traspaso esperado
						tUserMxTraspasosEsperado	=	Table.tableNew(USER_MX_TRASPASOS_ESPERADO);
						String sQuery				=	"select internal_portfolio, monto_esperado, monto_esperado_sbp from "+USER_MX_TRASPASOS_ESPERADO;
						DBaseTable.execISql(tUserMxTraspasosEsperado, sQuery);
					
					// Search portfolio on usertable
						iSearchedRow 				=	tUserMxTraspasosEsperado.unsortedFindInt("internal_portfolio", iSelectedPortfolio);
						
				} catch (OException e) {
					_Log.printMsg(MSG_ERROR,
							"Unable to get info into tUserMxTraspasosEsperado,on updateTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
							
			// If it wasn't founded, add new row, else update value
				try {
					if (iSearchedRow==-1){
						int iNewRow = tUserMxTraspasosEsperado.addRow();
						tUserMxTraspasosEsperado.setInt("internal_portfolio", iNewRow, iSelectedPortfolio);
						tUserMxTraspasosEsperado.setDouble("monto_esperado", iNewRow, dTraspasoEsperado);
						tUserMxTraspasosEsperado.setDouble("monto_esperado_sbp", iNewRow, dTraspasoEsperadoSBP);
					} else {
						tUserMxTraspasosEsperado.setInt("internal_portfolio", iSearchedRow, iSelectedPortfolio);
						tUserMxTraspasosEsperado.setDouble("monto_esperado", iSearchedRow, dTraspasoEsperado);
						tUserMxTraspasosEsperado.setDouble("monto_esperado_sbp", iSearchedRow, dTraspasoEsperadoSBP);
					}
				} catch (OException e) {
					_Log.printMsg(MSG_ERROR,
							"Unable to set internal_portfolio or monto_esperado into tUserMxTraspasosEsperado,on updateTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
			
			// Clear previous info and push new values
				try {
					DBUserTable.clear(tUserMxTraspasosEsperado);
					if(DBUserTable.bcpIn(tUserMxTraspasosEsperado)!=-1){
						Ask.ok("El traspaso esperado fue guardado exitosamente.");
					} else {
						Ask.ok("Hubo un error al intentar guardar el valor");
					}
				} catch (OException e) {
					_Log.printMsg(MSG_ERROR,
							"Unable to clear or update info into tUserMxTraspasosEsperado,on updateTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
			
			// Recalculate and set porcentaje de nav
				try {
					double dPorcentajeEsperado		=	((dTraspasoEsperado+dTraspasoEsperadoSBP)/dCurrentNAV)*100;
					tReturnt.scriptDataSetWidgetString("valuePorcentajeEsperado", String.format("%1$,.2f", dPorcentajeEsperado));
				} catch (OException e) {
					_Log.printMsg(MSG_ERROR,
							"Unable to set porcentaje esperado into header of UDW,on updateTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
			} else {
				try {
					Ask.ok("El traspaso esperado excede al NAV.\nPor favor configure un valor que no exceda al NAV de la Siefore.");
				} catch (OException e) {
					_Log.printMsg(MSG_ERROR,
							"Unable to show Ask dialog,on updateTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
		}
	}

	private void verifyFormatTraspasoEsperado() {
		
		String sSettedTraspasoEsperado	=	"";
		double dSettedTraspasoEsperado	=	0.0;
		double dOldTraspasoEsperado		=	0.0;
		
		String sSettedTraspasoEsperadoSBP	=	"";
		double dSettedTraspasoEsperadoSBP	=	0.0;
		double dOldTraspasoEsperadoSBP		=	0.0;
		
		// Get setted traspaso esperado
			try {
				sSettedTraspasoEsperado				=	tReturnt.scriptDataGetWidgetString("valueTraspasoEsperado");
				sSettedTraspasoEsperadoSBP			=	tReturnt.scriptDataGetWidgetString("valueTraspasoEsperadoSBP");
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get traspaso esperado from UDW header,on verifyFormatTraspasoEsperado function."
						+ "Error: " + e.getMessage());
			}
		
		// Get old traspaso esperado (from usertable)
			try {
				// Get usertable that stores this traspaso esperado
					Table tUserMxTraspasosEsperado	=	Table.tableNew(USER_MX_TRASPASOS_ESPERADO);
					String sQuery					=	"select internal_portfolio, monto_esperado, monto_esperado_sbp from "+USER_MX_TRASPASOS_ESPERADO;
					DBaseTable.execISql(tUserMxTraspasosEsperado, sQuery);
				
				// Search portfolio on usertable
					int iSearchedRow 				=	tUserMxTraspasosEsperado.unsortedFindInt("internal_portfolio", iSelectedPortfolio);
					
				// Get old traspaso esperado
					dOldTraspasoEsperado			=	tUserMxTraspasosEsperado.getDouble("monto_esperado", iSearchedRow);
					dOldTraspasoEsperadoSBP			=	tUserMxTraspasosEsperado.getDouble("monto_esperado_sbp", iSearchedRow);
				
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get old traspaso esperado from tUserMxTraspasosEsperado,on verifyFormatTraspasoEsperado function."
						+ "Error: " + e.getMessage());
			}

		// Try to convert setted traspaso esperado to double,
		// if it's not possible; throw message and set the old one
			try {
				dSettedTraspasoEsperado = Double.valueOf(sSettedTraspasoEsperado.replace(",",""));
				tReturnt.scriptDataSetWidgetString("valueTraspasoEsperado", String.format("%1$,.2f", dSettedTraspasoEsperado));
				
				dSettedTraspasoEsperadoSBP = Double.valueOf(sSettedTraspasoEsperadoSBP.replace(",",""));
				// Como los traspasos de SB1 van siempre a SBP, en caso de ser esta siefore, monto a SBP queda siempre en 0
				if(iSelectedPortfolio == SB1) dSettedTraspasoEsperadoSBP = 0.0;
				tReturnt.scriptDataSetWidgetString("valueTraspasoEsperadoSBP", String.format("%1$,.2f", dSettedTraspasoEsperadoSBP));
				
			} catch (NumberFormatException | OException e) {
				
				_Log.printMsg(MSG_ERROR,
						"Unable to set traspaso_esperado on UDW header,on verifyFormatTraspasoEsperado function."
						+ "Error: " + e.getMessage());
				
				try {
					Ask.ok("El valor ingresado no corresponde a un numero.");
					tReturnt.scriptDataSetWidgetString("valueTraspasoEsperado", String.format("%1$,.2f", dOldTraspasoEsperado));
					tReturnt.scriptDataSetWidgetString("valueTraspasoEsperadoSBP", String.format("%1$,.2f", dOldTraspasoEsperadoSBP));
				} catch (OException e1) {
					_Log.printMsg(MSG_ERROR,
							"Unable to set old traspaso_esperado on UDW header,on verifyFormatTraspasoEsperado function."
							+ "Error: " + e.getMessage());
				}
			}
		

	}

	private Table getPortfolios (){
		
		Table tNAV = Util.NULL_TABLE;
		
		// Get Portfolios
			try {
				tPortfolios = Table.tableNew();
				StringBuilder sb = new StringBuilder();
				sb.append("\n select ");
				sb.append("\n 	p.id_number ");
				sb.append("\n 	,p.name ");
				sb.append("\n 	,ute.monto_esperado ");
				sb.append("\n 	,ute.monto_esperado_sbp ");
				sb.append("\n from portfolio p ");
				sb.append("\n 	left join "+USER_MX_TRASPASOS_ESPERADO+" ute on ute.internal_portfolio = p.id_number ");
				sb.append("\n where ");
				sb.append("\n 	p.portfolio_type = 0 ");
				sb.append("\n 	and p.restricted = 1 ");
				sb.append("\n 	and p.id_number IN("+SB1+","+SB2+","+SB3+","+SB4+") ");
				DBaseTable.execISql(tPortfolios, sb.toString());
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get portfolios on getPortfolios function."
						+ "Error: " + e.getMessage());
			}
			
		// Add portfolio none
			try {
				int iNewRow = tPortfolios.addRow();
				tPortfolios.setInt("id_number", iNewRow, 0);
				tPortfolios.setString("name", iNewRow, "None");
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to add Portfolio Ninguno on getPortfolios function."
						+ "Error: " + e.getMessage());
			}
		
		// Get NAV
			try {
				tNAV = UtilAfore.getAllTotalNAV();
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to get NAV on getPortfolios function."
						+ "Error: " + e.getMessage());
			}
		
		// Paste NAV and format tPortfolios Table
			try {
				tPortfolios.select(tNAV, "current_nav", "internal_portfolio EQ $id_number");
				tPortfolios.colHide("id_number");
				tPortfolios.colHide("monto_esperado");
				tPortfolios.colHide("monto_esperado_sbp");
				tPortfolios.colHide("current_nav");
				tPortfolios.setColName("name", "Portfolios");
				tPortfolios.sortCol("Portfolios");
			} catch (OException e) {
				_Log.printMsg(MSG_ERROR,
						"Unable to paste NAV on tPortfolios Table on getPortfolios function."
						+ "Error: " + e.getMessage());
			}
		
		return tPortfolios;
	}
}
