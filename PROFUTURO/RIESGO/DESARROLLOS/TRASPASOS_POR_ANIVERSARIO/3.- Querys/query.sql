 /* 
 LIMITE TRASPASOS - Query Filtros (New) 
  
 Pending 1 
 New 2 
 Simulated 7 
  
 Validated 3 
 Canceled 5 
 Deleted 14 
 Closeout 22 
  
 */ 
 with price_base as 
 ( 
 	select 	id_number 
 			,name 
 			,case 	when id_number in (0, 3) then 100 
 					when id_number = 2 then 10000 
 					when id_number = 8 then 1000 
 					when id_number = 9 then 10 
 					else 1 
 			end base 
 	from price_input_format 
 ), 
 trades as  
 ( 
 	select 	ab.deal_tracking_num 
 			,ab.tran_num 
 			,ab.ins_num 
 			,ab.toolset 
 			,ab.ins_type 
 			,h.ticker 
 			,ab.reference 
 			,ab.currency 
 			,ab.cflow_type 
 			,ab.trade_date 
 			,ab.internal_portfolio 
 			,ab.external_portfolio 
 			,ab.tran_status 
 			,0 antigua_nueva 
 	 		,case   
 	 			when cat.afecto_limite is null then 'NO MAPEADO'  
 	 			when cat.afecto_limite = 0 then 'NO AFECTO A LIMITE'  
 	 			when cat.afecto_limite = 1 then 'AFECTO A LIMITE'  
 	 		end afecto_limite   
 			,cat.categoria_name 
 			,case 	when ab.toolset in (9, 10) then ab.position  
 					else ab.mvalue  
 			end ohd_position 
 			,p.notnl 
 			,ab.price 
 			,pb.base 
 			,NVL(v.precio_sucio_24h, 1.0) ohd_precio_24h 
 			,NVL(fx.precio_sucio_24h, 1.0) ohd_fx_mxn 
 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0)  
 				when ab.toolset = 41 and ab.ins_type = 61004 then ab.mvalue*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*NVL(fx.precio_sucio_24h, 1.0)) 
 				when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.mvalue*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*pb.base*NVL(fx.precio_sucio_24h, 1.0)) 
 				when ab.toolset = 50 then ab.mvalue*NVL(v.precio_sucio_24h, 1.0)/pb.base 
 				else ab.mvalue*NVL(v.precio_sucio_24h, 1.0)  
 			end ohd_mtm 
 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0)  
 				when ab.toolset = 41 and ab.ins_type = 61004 then ab.mvalue*p.notnl*NVL(v.precio_sucio_24h, 1.0) 
 				when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.mvalue*p.notnl*NVL(v.precio_sucio_24h, 1.0)/pb.base 
 				when ab.toolset = 50 then ab.mvalue*NVL(v.precio_sucio_24h, 1.0) 
 				else ab.mvalue*NVL(v.precio_sucio_24h, 1.0)  
 			end ohd_mtm_not 
 	from ab_tran ab 
 		inner join system_dates d on ab.trade_date <= d.business_date 
 		left join parameter p on p.ins_num = ab.ins_num and p.param_seq_num = 0 
 		left join misc_ins m on m.ins_num = ab.ins_num and p.param_seq_num = 0 
 		left join price_base pb on pb.id_number = m.price_inp_format 
 		left join header h on h.ins_num = ab.ins_num 
 		left join user_mx_traspasos_ani_catalogo cat on cat.ticker = h.ticker 
 		left join user_mx_vectorprecios_diario v on v.ticker = replace(h.ticker, 'USD/MXN', '*C_MXPUSD_FIX') 
 		left join currency c on c.id_number = ab.currency 
 		left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		 
 																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	 
 	where ab.tran_type = 0 
 			and ab.asset_type = 2 
 			and ab.tran_status = 3 
 			and ab.ins_type <> 1000006 
 			and ab.toolset <> 6 
 			and (ab.toolset not in (9, 10) or ab.settle_date >= d.business_date) 
 			and ab.cflow_type not in (2051, 2052) 
 			 
 	union all 
  
 	select 	ab.deal_tracking_num 
 			,ab.tran_num 
 			,ab.ins_num 
 			,ab.toolset 
 			,ab.ins_type 
 			,h.ticker 
 			,ab.reference 
 			,ab.currency 
 			,ab.cflow_type 
 			,ab.trade_date 
 			,ab.internal_portfolio 
 			,case when ab.external_portfolio = 0  
 					then (case 	when ab.internal_portfolio = 20010  
 								then 20014  
 								else ab.internal_portfolio-1  
 						 end) 
 					else ab.external_portfolio 
 			end external_portfolio 
 			,ab.tran_status 
 			,1 antigua_nueva 
 	 		,case   
 	 			when cat.afecto_limite is null then 'NO MAPEADO'  
 	 			when cat.afecto_limite = 0 then 'NO AFECTO A LIMITE'  
 	 			when cat.afecto_limite = 1 then 'AFECTO A LIMITE'  
 	 		end afecto_limite   
 			,cat.categoria_name 
 			,ab.position ohd_position 
 			,p.notnl 
 			,ab.price 
 			,pb.base 
 			,NVL(v.precio_sucio_24h, 1.0) ohd_precio_24h 
 			,NVL(fx.precio_sucio_24h, 1.0) ohd_fx_mxn 
 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0)  
 				when ab.toolset = 41 and ab.ins_type = 61004 then ab.position*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*NVL(fx.precio_sucio_24h, 1.0)) 
 				when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.position*p.notnl*(NVL(v.precio_sucio_24h, 1.0) - ab.price*pb.base*NVL(fx.precio_sucio_24h, 1.0)) 
 				when ab.toolset = 50 then ab.position*NVL(v.precio_sucio_24h, 1.0)/pb.base 
 				else ab.position*NVL(v.precio_sucio_24h, 1.0)  
 			end ohd_mtm 
 			,case 	when ab.toolset in (9, 10) then ab.position*NVL(v.precio_sucio_24h, 1.0)  
 				when ab.toolset = 41 and ab.ins_type = 61004 then ab.position*p.notnl*NVL(v.precio_sucio_24h, 1.0) 
 				when ab.toolset in (34, 41) and ab.ins_type <> 61004 then ab.position*p.notnl*NVL(v.precio_sucio_24h, 1.0)/pb.base 
 				when ab.toolset = 50 then ab.position*NVL(v.precio_sucio_24h, 1.0) 
 				else ab.position*NVL(v.precio_sucio_24h, 1.0)  
 			end ohd_mtm_not 
 	from ab_tran ab 
 			inner join system_dates d on ab.trade_date = d.business_date 
 			left join parameter p on p.ins_num = ab.ins_num and p.param_seq_num = 0 
 			left join misc_ins m on m.ins_num = ab.ins_num and p.param_seq_num = 0 
 			left join price_base pb on pb.id_number = m.price_inp_format 
 			left join header h on h.ins_num = ab.ins_num 
 			left join user_mx_traspasos_ani_catalogo cat on cat.ticker = h.ticker 
 			left join user_mx_vectorprecios_diario v on v.ticker = replace(h.ticker, 'USD/MXN', '*C_MXPUSD_FIX') 
 			left join currency c on c.id_number = ab.currency 
 			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		 
 																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	 
 	where ab.tran_type = 0 
 			and ab.asset_type = 2 
 			and ab.tran_status in (1, 2, 7) 
 			and (ab.toolset in (34, 41, 50) or ab.buy_sell = 1) 
 			and ab.ins_type <> 1000006 
 			and ab.toolset <> 6 
 			and (ab.external_portfolio <> 0 or ab.toolset in (34, 41, 50)) 
 			and (ab.toolset not in (9, 10) or ab.cflow_type in (2051, 2052)) 
 ) 
select 	t.* 
	,case when t.antigua_nueva = 0 then t.ohd_mtm else 0 end ohd_initial_pos 
	,case when t.antigua_nueva = 0 then t.ohd_mtm_not else 0 end ohd_initial_pos_not 
	,case when t.antigua_nueva = 1  
		then ( 
 			case 	when t.internal_portfolio = 20010 and t.external_portfolio = 20014 then t.ohd_mtm 
 					when t.internal_portfolio <> 20010 and t.external_portfolio <> 20014 then t.ohd_mtm 
 			end 
 		) 
	end ohd_traspaso 
	,case when t.antigua_nueva = 1  
	then ( 
			case 	when t.internal_portfolio = 20010 and t.external_portfolio = 20014 then t.ohd_mtm_not 
					when t.internal_portfolio <> 20010 and t.external_portfolio <> 20014 then t.ohd_mtm_not 
			end 
		) 
	end ohd_traspaso_not 
	,case when t.antigua_nueva = 1 and t.external_portfolio = 20014 then t.ohd_mtm else 0 end ohd_traspaso_sbp 
from trades t 
where t.internal_portfolio = 20013
 --where t.internal_portfolio = $$siefore$$ 