/*$Header: v 1.0, 26/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Risk_LimiteLiquidezLlamadasCapitalEnCKD.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Julio 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de limites de liquidez por llamadas a capital en CKD
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.afore.util.risk.query.UTIL_VaRConsar;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class MISC_Afore_DWS_Risk_LimiteLiquidezLlamadasCapitalEnCKD implements IScript {
	
	// Basic util variables
		private String			sScriptName	=	this.getClass().getSimpleName();
		private UTIL_Log		_Log		=	new UTIL_Log(sScriptName);
		private UTIL_Afore		UtilAfore	=	new UTIL_Afore();
		private UTIL_VaRConsar	UtilVar 	=	new UTIL_VaRConsar(0);

	// UDW Variables
		ENUM_UDW_ACTION	callback_type;
		ENUM_UDW_EVENT	callback_event;
		private Table tblProperties			=	Util.NULL_TABLE;
		private Table tblGlobalProperties	=	Util.NULL_TABLE;
		private Table tDisplay				=	Util.NULL_TABLE;
	
	// Used ENUMS
		private static final EnumTypeMessage MSG_INFO	=	EnumTypeMessage.INFO;
		private static final EnumTypeMessage MSG_ERROR	=	EnumTypeMessage.ERROR;
		private static final COL_TYPE_ENUM COL_INT		=	COL_TYPE_ENUM.COL_INT;
		private static final COL_TYPE_ENUM COL_STRING	=	COL_TYPE_ENUM.COL_STRING;
		private static final COL_TYPE_ENUM COL_DOUBLE	=	COL_TYPE_ENUM.COL_DOUBLE;

		// TOOLSET
		private static final int TOOLSET_EQUITY		=	TOOLSET_ENUM.EQUITY_TOOLSET.toInt();			//28
		
		// TRAN_TYPE
		private static final int TRAN_TYPE_TRADING	=	TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();		//0
		
		// TRAN_STATUS
		private static final int STATUS_NEW			=	TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt();			//2
		private static final int STATUS_VALIDATED	=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();		//3
		private static final int STATUS_PROPOSED	=	TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt();		//7
		
		// INS_TYPE
		private static final int INS_TYPE_EQT_CKD_PREFUND	=	EnumsInstrumentsMX.MX_EQT_CKDPREFUND.toInt();	//1000056
		private static final int INS_TYPE_EQT_CKD_KCALL		=	EnumsInstrumentsMX.MX_EQT_CKDKCALL.toInt();		//1000043
		private static final String INS_TYPE_REVERSE_T_BS		=	"REVERSE-T-B/S";
		private static final String INS_TYPE_PRESTAMO_VAL_PR	=	"PRESTAMO-VAL-PR";
				
		// USER TABLES
		private static final String USER_MX_ACTIVOS_ALTA_CALIDAD	=	EnumsUserTables.USER_MX_ACTIVOS_ALTA_CALIDAD.toString();

	public void execute(IContainerContext context) throws OException {
		
		initializeScript();
		
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					setVisualElements();

				// Adding columns layout
					addLayoutColumns();
				
				// Setting initial data
					setDataOnTable(tDisplay);
				
				// FINALLY, Setting table properties
					setTableProperties();

				break;
				
			case UDW_EDIT_ACTION:
				break;
			default:
				break;
		}
		
		_Log.markEndScript();
	}
	
	private void initializeScript() {
		try {
			_Log.markStartScript();
			callback_type	=	ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
		} catch (OException e) {
			_Log.printMsg(
					MSG_ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
	}

	private void setVisualElements() {
		try {
			// Creating display table
				tDisplay = Table.tableNew();
			
			// Add data worksheet
				tDisplay.scriptDataMoveListBox("top=50,left=5,right=5,bottom=5");
			
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to set some of the visual elements on setVisualElements function."
					+ "Error: " + e.getMessage());
		}
	}
	
	private void addLayoutColumns() {
		try {
			tDisplay.addCol("internal_portfolio", COL_INT);
			tDisplay.addCol("llamadas_capital", COL_DOUBLE);
			tDisplay.addCol("liquidez_llamadas_capital", COL_DOUBLE);
			tDisplay.addCol("porcentaje_llamadas_capital", COL_DOUBLE);
			tDisplay.addCol("limite", COL_DOUBLE);
			tDisplay.addCol("uso", COL_DOUBLE);
			tDisplay.addCol("status", COL_STRING);
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to add some cols for layout on addLayoutColumns function."
					+ "Error: " + e.getMessage());
		}
	}
	
	private void setDataOnTable(Table tDisplay) {
		
		Table tAllTotalNAV					=	Util.NULL_TABLE;
		Table tVaR							=	Util.NULL_TABLE;
		Table tSumLlamadasCapital			=	Util.NULL_TABLE;
		Table tSumLiquidezLlamadasCapital	=	Util.NULL_TABLE;
		Table tResumeTable					=	Util.NULL_TABLE;
		
		// Get info into used tables
			_Log.printMsg(MSG_INFO, "\n --> Obteniendo Portfolios");
			Table tPortfolios				=	getPortfolios();
			_Log.printMsg(MSG_INFO, "\n --> Obteniendo Llamadas de Capital");
			Table tLlamadasCapital			=	getLlamadasCapital();
			_Log.printMsg(MSG_INFO, "\n --> Obteniendo Liquidez para Llamadas de Capital");
			Table tLiquidezLlamadasCapital	=	getLiquidezLlamadasCapital();
			
		// Get NAV
			try {
				_Log.printMsg(MSG_INFO, "\n --> Obteniendo NAV");
				tAllTotalNAV = UtilAfore.getAllTotalNAV();
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to get NAV, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
		//Get VaR
			try {
				_Log.printMsg(MSG_INFO, "\n --> Obteniendo VaR");
				tVaR						=	Table.tableNew();
				String sVar						=	UtilVar.getOutput_VaRMarginal();
				DBaseTable.execISql(tVaR, sVar);
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to get VaR, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
		// Paste NAV and VaR
			try {
				_Log.printMsg(MSG_INFO, "\n --> Pegando VaR y NAV a tablas de Llamada de Capital");
				tLlamadasCapital.select(tAllTotalNAV, "current_nav", "internal_portfolio EQ $internal_portfolio");
				tLiquidezLlamadasCapital.select(tAllTotalNAV, "current_nav", "internal_portfolio EQ $internal_portfolio");
				tLiquidezLlamadasCapital.select(tVaR, "var_ins", "internal_portfolio EQ $internal_portfolio AND ins_num EQ $ins_num");
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to paste VaR or NAV into tLlamadasCapital or tLiquidezLlamadasCapital, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
		// Add calculated values
			try {
				_Log.printMsg(MSG_INFO, "\n --> Calculando Campos...");
				tLlamadasCapital.addFormulaColumn("COL('position') / COL('current_nav')", COL_DOUBLE.toInt(), "porcentaje");
				tLiquidezLlamadasCapital.addFormulaColumn("COL('mtm') / COL('current_nav')", COL_DOUBLE.toInt(), "ILt");
				tLiquidezLlamadasCapital.addFormulaColumn("COL('var_ins') / COL('mtm')", COL_DOUBLE.toInt(), "porcenjaje_var");
				tLiquidezLlamadasCapital.addFormulaColumn("1 - COL('porcenjaje_var')", COL_DOUBLE.toInt(), "one_minus_porcentaje_var");
				tLiquidezLlamadasCapital.addFormulaColumn("COL('mtm') * COL('one_minus_porcentaje_var')", COL_DOUBLE.toInt(), "mtm_mult_one_minus_porcentaje_var");
				tLiquidezLlamadasCapital.addFormulaColumn("COL('mtm_mult_one_minus_porcentaje_var') / COL('current_nav')", COL_DOUBLE.toInt(), "liquidez");
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to set calculated values, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
		// Generate resume by portfolios tables
			try {
				_Log.printMsg(MSG_INFO, "\n --> Generando Tablas de Resumen");
				
				tSumLlamadasCapital = Table.tableNew();
				tSumLiquidezLlamadasCapital = Table.tableNew();
				tResumeTable = Table.tableNew();
	
				tSumLlamadasCapital.select(tLlamadasCapital, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
				tSumLlamadasCapital.select(tLlamadasCapital, "SUM, position, porcentaje", "internal_portfolio EQ $internal_portfolio");
				tSumLiquidezLlamadasCapital.select(tLiquidezLlamadasCapital, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
				tSumLiquidezLlamadasCapital.select(tLiquidezLlamadasCapital, "SUM, mtm_mult_one_minus_porcentaje_var, liquidez", "internal_portfolio EQ $internal_portfolio");
	
				tResumeTable.select(tPortfolios, "*", "internal_portfolio GT 0");
				tResumeTable.select(tSumLlamadasCapital, "position, porcentaje", "internal_portfolio EQ $internal_portfolio");
				tResumeTable.select(tSumLiquidezLlamadasCapital, "mtm_mult_one_minus_porcentaje_var, liquidez", "internal_portfolio EQ $internal_portfolio");
				tResumeTable.addFormulaColumn("COL('porcentaje') / COL('liquidez')", COL_DOUBLE.toInt(), "uso");
				tResumeTable.addCol("status", COL_STRING);
				
				for (int i =1; i<= tResumeTable.getNumRows(); i++){
					double dLimite = tResumeTable.getDouble("liquidez", i); 
					double dUso = tResumeTable.getDouble("uso", i);
					
					if(dUso<=dLimite){
						tResumeTable.setString("status", i, "OK");
					} else {
						tResumeTable.setString("status", i, "ALERTA");
					}
				}
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to generate resume tables, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
		
		// Set info of resume table into tDisplay
			try {
				_Log.printMsg(MSG_INFO, "\n --> Seteando Informacion en Display");
				tDisplay.select(tResumeTable,
						"internal_portfolio"
						+ ",position(llamadas_capital)"
						+ ",porcentaje(porcentaje_llamadas_capital)"
						+ ",mtm_mult_one_minus_porcentaje_var(liquidez_llamadas_capital)"
						+ ",liquidez(limite)"
						+ ",uso"
						+ ",status"
						, "internal_portfolio GT 0");
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to paste values of resume table on tDisplay, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
		// Dispose memory
			try {
				_Log.printMsg(MSG_INFO, "\n --> Eliminando tablas temporales...");
				tPortfolios.destroy();
				tLlamadasCapital.destroy();
				tLiquidezLlamadasCapital.destroy();
				tAllTotalNAV.destroy();
				tVaR.destroy();
				tSumLlamadasCapital.destroy();
				tSumLiquidezLlamadasCapital.destroy();
				tResumeTable.destroy();
			} catch (OException e) {
				_Log.printMsg(
						MSG_ERROR,
						"Unable to destroy unused Tables, on setDataOnTable function."
						+ "Error: " + e.getMessage());
			}
			
			_Log.printMsg(MSG_INFO, "\n --> Proceso finalizado");

	}
	
	private void setTableProperties() {
		try {
			// Initialize properties tables
				tblProperties			=	UserDataWorksheet.initProperties();
				tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
			// Setting col properties
				UserDataWorksheet.setColReadOnly("internal_portfolio", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("llamadas_capital", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("liquidez_llamadas_capital", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("porcentaje_llamadas_capital", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("limite", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("uso", tDisplay, tblProperties);
				UserDataWorksheet.setColReadOnly("status", tDisplay, tblProperties);
		
			// Setting UDW properties
				UserDataWorksheet.setDisableSave(tblGlobalProperties);
				UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
				UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
				UserDataWorksheet.setDisableReload(tblGlobalProperties);
				UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to set some of the properties tables on setTableProperties function."
					+ "Error: " + e.getMessage());
		}
	}

	private Table getPortfolios(){
		
		Table tPortfolios = Util.NULL_TABLE;
		
		try {
			tPortfolios = Table.tableNew();
			StringBuilder sb = new StringBuilder();
			sb.append("\n select ");
			sb.append("\n 	p.id_number as internal_portfolio");
			sb.append("\n 	,p.name ");
			sb.append("\n from portfolio p ");
			sb.append("\n where ");
			sb.append("\n 	p.portfolio_type = 0 ");
			sb.append("\n 	and p.restricted = 1 ");
			DBaseTable.execISql(tPortfolios, sb.toString());
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to get the data from query on getPortfolios function."
					+ "Error: " + e.getMessage());
		}

		return tPortfolios;
	}

	private Table getLiquidezLlamadasCapital() {
		
		Table tLiquidezLlamadasCapital =  Util.NULL_TABLE;
		
		try {
			tLiquidezLlamadasCapital = Table.tableNew();
			StringBuilder sb = new StringBuilder();
			sb.append("\n with trades as ( ");
			sb.append("\n select  ");
			sb.append("\n 	ab.ins_num  ");
			sb.append("\n 	,ab.internal_portfolio  ");
			sb.append("\n 	,ab.position  ");
			sb.append("\n 	,ab.price  ");
			sb.append("\n 	,ab.deal_tracking_num  ");
			sb.append("\n 	,ab.mvalue  ");
			sb.append("\n 	,uvd.precio_sucio_24h  ");
			sb.append("\n 	,CAST((ab.mvalue * uvd.precio_sucio_24h) AS FLOAT) ohd_mtm  ");
			sb.append("\n from ab_tran ab  ");
			sb.append("\n 	inner join instruments i on i.id_number = ab.ins_type  ");
			sb.append("\n 	inner join "+USER_MX_ACTIVOS_ALTA_CALIDAD+" uac on ");
			sb.append("\n 		( uac.instrument_type = i.name and ");
			sb.append("\n 			uac.instrument_type NOT IN ('"+INS_TYPE_REVERSE_T_BS+"','"+INS_TYPE_PRESTAMO_VAL_PR+"') ) ");
			sb.append("\n 	inner join header h ");
			sb.append("\n 		on h.ins_num = ab.ins_num ");
			sb.append("\n 	inner join user_mx_vectorprecios_diario uvd ");
			sb.append("\n 		on uvd.ticker = h.ticker ");
			sb.append("\n where  ");
			sb.append("\n  	ab.tran_type = "+TRAN_TYPE_TRADING+" --Trading  ");
			sb.append("\n 	and ab.tran_status IN ("+STATUS_NEW+","+STATUS_VALIDATED+","+STATUS_PROPOSED+") --New,Validated,Proposed  ");
			sb.append("\n 	and ab.internal_portfolio > -1 ");
			sb.append("\n 	and ab.asset_type = 2 --Trading ");
			sb.append("\n ) ");
			sb.append("\n select ");
			sb.append("\n 	t.internal_portfolio ");
			sb.append("\n 	,t.deal_tracking_num  ");
			sb.append("\n 	,t.ins_num ");
			sb.append("\n 	,t.mvalue  ");
			sb.append("\n 	,t.precio_sucio_24h ");
			sb.append("\n 	,SUM(ohd_mtm) as ohd_mtm ");
			sb.append("\n from trades t ");
			sb.append("\n group by ");
			sb.append("\n 	t.internal_portfolio ");
			sb.append("\n 	,t.deal_tracking_num  ");
			sb.append("\n 	,t.ins_num ");
			sb.append("\n 	,t.mvalue  ");
			sb.append("\n 	,t.precio_sucio_24h ");
			DBaseTable.execISql(tLiquidezLlamadasCapital, sb.toString());
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to get the data from query on getLiquidezLlamadasCapital function."
					+ "Error: " + e.getMessage());
		}

		return tLiquidezLlamadasCapital;
	}

	private Table getLlamadasCapital() {
		
		Table tLlamadasCapital = Util.NULL_TABLE;
		
		try {
			tLlamadasCapital = Table.tableNew();
			StringBuilder sb = new StringBuilder();
			sb.append("\n select ");
			sb.append("\n 	ab.internal_portfolio ");
			sb.append("\n 	,ab.ins_num ");
			sb.append("\n 	,h.ticker ");
			sb.append("\n 	,utc.monto_total as ohd_position ");
			sb.append("\n from ab_tran ab ");
			sb.append("\n 	inner join portfolio p ");
			sb.append("\n 		on ab.internal_portfolio = p.id_number ");
			sb.append("\n 	inner join header h ");
			sb.append("\n 		on h.ins_num = ab.ins_num ");
			sb.append("\n 	inner join user_mx_total_ckds utc ");
			sb.append("\n 		on utc.ticker = h.ticker ");
			sb.append("\n 		and utc.portfolio = p.name ");
			sb.append("\n where ");
			sb.append("\n 	ab.toolset = "+TOOLSET_EQUITY+" -- Equity ");
			sb.append("\n  	and ab.tran_type = "+TRAN_TYPE_TRADING+" --Trading ");
			sb.append("\n 	and ab.tran_status IN ("+STATUS_NEW+","+STATUS_VALIDATED+","+STATUS_PROPOSED+") --New,Validated,Proposed  ");
			sb.append("\n 	and ab.internal_portfolio > -1 ");
			sb.append("\n 	and ab.ins_type IN ("+INS_TYPE_EQT_CKD_KCALL+") -- EQT-CKD-KCALL ");			
			sb.append("\n group by ");
			sb.append("\n 	ab.internal_portfolio ");
			sb.append("\n   ,ab.ins_num ");
			sb.append("\n 	,h.ticker ");
			sb.append("\n 	,utc.monto_total ");
			DBaseTable.execISql(tLlamadasCapital, sb.toString());
		} catch (OException e) {
			_Log.printMsg(MSG_ERROR,
					"Unable to get the data from query on getLlamadasCapital function."
					+ "Error: " + e.getMessage());
		}
		
		return tLlamadasCapital;
	}
}
