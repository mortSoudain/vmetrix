select
	ab.internal_portfolio
	,ab.ins_num
	,h.ticker
	,utc.monto_total as ohd_position
from ab_tran ab
	inner join portfolio p
		on ab.internal_portfolio = p.id_number
	inner join header h
		on h.ins_num = ab.ins_num
	inner join user_mx_total_ckds utc
		on utc.ticker = h.ticker
		and utc.portfolio = p.name
where
	ab.toolset = 28 -- Equity
 	and ab.tran_type = 0 --Trading
	and ab.tran_status IN (2,3,7) --New,Validated,Proposed 
	and ab.internal_portfolio > -1
	and ab.ins_type IN (1000043) -- EQT-CKD-KCALL
group by
	ab.internal_portfolio
  	,ab.ins_num
	,h.ticker
	,utc.monto_total

-- Incluir NAV (Util.getAllNAV)