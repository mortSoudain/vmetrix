with trades as (
select 
	ab.ins_num 
	,ab.internal_portfolio 
	,ab.position 
	,ab.price 
	,CAST((ab.mvalue * uvd.precio_sucio_24h) AS FLOAT) ohd_mtm 
from ab_tran ab 
	inner join instruments i on i.id_number = ab.ins_type 
	inner join user_mx_activos_alta_calidad uac on
		( uac.instrument_type = i.name and
			uac.instrument_type NOT IN ('REVERSE-T-B/S','PRESTAMO-VAL-PR') )
	inner join header h
		on h.ins_num = ab.ins_num
	inner join user_mx_vectorprecios_diario uvd
		on uvd.ticker = h.ticker
where 
 	ab.tran_type = 0 --Trading 
	and ab.tran_status IN (2,3,7) --New,Validated,Proposed 
	and ab.internal_portfolio > -1
	and ab.asset_type = 2 --Trading
)
select
	t.internal_portfolio
	,t.ins_num
	,SUM(ohd_mtm) as ohd_mtm
from trades t
group by
	t.internal_portfolio
	,t.ins_num

-- MtM (price * position)
-- Incluir NAV (Util.getAllNAV)
-- Incluir VaR (Esc. Móviles)