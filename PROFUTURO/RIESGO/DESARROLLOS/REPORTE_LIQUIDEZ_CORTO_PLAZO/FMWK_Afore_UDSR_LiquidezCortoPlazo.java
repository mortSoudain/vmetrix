/**
File Name:              FMWK_Afore_UDSR_LiquidezCortoPlazo.java

Author:                 Gustavo Rojas - VMetrix International SpA 
Creation Date:          Octubre 2017

REVISION HISTORY  		

Main Script:            This is the Main Script
Parameter Script:       None
Display Script:         None

Description:            Considera la suma todos los flujos futuros por recibir y pagar antes de un año plazo, 
						se establece un cociente entre este valor y el total de activo del Portafolio.


Assumptions:            None

Instructions:

Script Category:

Uses EOD Results:       Yes

Which EOD Results are used: None

When can the script be run: SIM

************************************************************************************/
package com.afore.udsr.risk;

import standard.include.JVS_INC_STD_UserSimRes;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OConsole;
import com.olf.openjvs.OException;
import com.olf.openjvs.PluginCategory;
import com.olf.openjvs.PluginType;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.ScriptAttributes;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.COL_FORMAT_BASE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.PFOLIO_RESULT_TYPE;
import com.olf.openjvs.enums.SCRIPT_CATEGORY_ENUM;
import com.olf.openjvs.enums.SCRIPT_TYPE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;
import com.olf.openjvs.enums.USER_RESULT_OPERATIONS;
@ScriptAttributes(allowNativeExceptions=false)
@PluginCategory(SCRIPT_CATEGORY_ENUM.SCRIPT_CAT_SIM_RESULT)
@PluginType(SCRIPT_TYPE_ENUM.MAIN_SCRIPT)

public class FMWK_Afore_UDSR_LiquidezCortoPlazo implements IScript
{
	private final String sPlugInName = this.getClass().getSimpleName();
	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _Lib = new UTIL_Afore();
	
	//Tabla de Queries
	private final static String QUERY_RESULT_TABLE = "query_result";
	
	private Table tTrans = Util.NULL_TABLE;
	private JVS_INC_STD_UserSimRes m_INCSTDUserSimRes;
	private int iToday;
	public FMWK_Afore_UDSR_LiquidezCortoPlazo(){
		m_INCSTDUserSimRes = new JVS_INC_STD_UserSimRes();
	 }

    public void execute(IContainerContext context) throws OException
    {
    	_Log.markStartScript();
    	try{
    		Table tReturnt = context.getReturnTable();
    		Table tArgt    = context.getArgumentsTable();
    		
	    	int tmp = m_INCSTDUserSimRes.USR_RunMode(tArgt);
	
	    	if( tmp == USER_RESULT_OPERATIONS.USER_RES_OP_CALCULATE.toInt())
	    	{
	    		compute_result(tArgt, tReturnt);
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_FORMAT.toInt())
	    	{
	    		format_result(tReturnt);
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_DWEXTRACT.toInt())
	    	{

	    		dw_extract_result(tArgt, tReturnt);
	    	}
	    	else
	    	{
	    		m_INCSTDUserSimRes.USR_Fail("Incorrect operation code", tArgt);
		    }
	    		
    	}catch (OException e){
    			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{
    		_Log.markEndScript();
    	}
    	return;// main/0
    }

    
	private void compute_result(Table tArgt, Table tReturnt) throws OException 
	{
		iToday = OCalendar.today();
		tTrans = m_INCSTDUserSimRes.USR_Transactions(tArgt);
		
		Table tTotalNavByPortfolio = Table.tableNew("NAV by Portfolio Table");
		Table tFxResults = Table.tableNew("FX Sim Result");
		
		Table tCashAndFX = Table.tableNew("All Cash and FX payments");
		Table tGenResults = tArgt.getTable("sim_results", 1).getTable("result_class", 4);
		
		//Add Query Id for UserSimRes executions
		tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);

		//Getting current NAV by portfolio
		tTotalNavByPortfolio = getNav();
		
		//Add portfolios and mvalues
		doAddPortfolio();
		
		//Getting Cash and FX payments
		tCashAndFX = getCashFxByDay(tArgt);

		//Getting FX Spot Rate
		tFxResults = tGenResults.getTable("result", tGenResults.unsortedFindInt("result_type", PFOLIO_RESULT_TYPE.FX_RESULT.toInt()));
		//Getting all cashflows by deal OLD METHOD
		//tCashflowByDay = tGenResults.getTable("result", tGenResults.unsortedFindInt("result_type", PFOLIO_RESULT_TYPE.CFLOW_BY_DAY_RESULT.toInt()));
		
		//Get CashflowByDay Sim Result
		Table tCashflowByDay = getCashflowByDaySimResult();

		//Getting all cashflows to deals scope
		tTrans.select(tCashflowByDay, "*", "tran_num EQ $tran_num");

		//Adds Date and  Portfolios to Final Table 
		tReturnt.addCol("fecha", COL_TYPE_ENUM.COL_INT);
		tReturnt.select(tTrans, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
		
		//Getting all Cash and FX to deals scope
		if (tCashAndFX.getNumRows()>0)
		{
			tCashAndFX.select(tFxResults, "result(tc)", "id EQ $currency");
			tCashAndFX.addFormulaColumn("COL('cflow')*(1/COL('tc'))*COL('mvalue')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "oneyear");
			tTrans.select(tCashAndFX, "deal_num, tran_num, ins_num, internal_portfolio, ins_type, oneyear, currency", "deal_num GT 0");
		}
		
		int iLimite = Str.strToInt(_Lib.getVariableGlobal("FINDUR", sPlugInName, "limite"));
		
		tReturnt.select(tTotalNavByPortfolio, "current_nav(total_activo)", "internal_portfolio EQ $internal_portfolio");
		tReturnt.select(tTrans, "SUM,oneyear(total_flujos)", "internal_portfolio EQ $internal_portfolio");


		tReturnt.addCol("limite", COL_TYPE_ENUM.COL_STRING);
		
		tReturnt.addFormulaColumn("COL('total_flujos')/COL('total_activo')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "liqui_corto_plazo");
		tReturnt.addFormulaColumn("iif(COL('liqui_corto_plazo')>"+(iLimite/100)+",1,0)", COL_TYPE_ENUM.COL_INT.toInt(), "res");
		tReturnt.addCol("liqui_percent", COL_TYPE_ENUM.COL_STRING);
		tReturnt.addCol("cumplimiento", COL_TYPE_ENUM.COL_STRING);
		for(int i=1;i<=tReturnt.getNumRows();i++)
		{
			tReturnt.setString("liqui_percent", i, Str.formatAsPercent(tReturnt.getDouble("liqui_corto_plazo", i),2));
			if (tReturnt.getInt("res", i)==0)
				tReturnt.setString("cumplimiento", i, "NO");
			else
				tReturnt.setString("cumplimiento", i, "OK");			
		}

		
		tReturnt.setColValInt("fecha", iToday);
		tReturnt.setColValString("limite", iLimite + "%");
		tReturnt.colHide("liqui_corto_plazo");
		tReturnt.colHide("res");
	}
	
	// *****************************************************************************
	void format_result(Table tReturnt) throws OException
	{
	   // Set column titles
		
		tReturnt.setColTitle( "fecha",  			"Fecha");
		tReturnt.setColTitle( "internal_portfolio",	"Portfolio");
		tReturnt.setColTitle( "total_activo",		"NAV");
		tReturnt.setColTitle( "total_flujos",		"Flujos < 1y");
		tReturnt.setColTitle( "liqui_percent",		"Liquidez\nCorto Plazo(%)");
		tReturnt.setColTitle( "limite",				"Limite(%)");
		tReturnt.setColTitle( "cumplimiento",		"Cumplimiento");
		
		tReturnt.setColFormatAsDate("fecha", DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		tReturnt.setColFormatAsRef("internal_portfolio", SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE);
		
		tReturnt.setColFormatAsNotnl("total_activo", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tReturnt.setColFormatAsNotnl("total_flujos", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());

	}
	
	void dw_extract_result(Table tArgt, Table tReturnt) throws OException
	{
	}	
	
	private Table getNav() throws OException
	{
		Table tTotalNav = Table.tableNew();
		int iYesterday = OCalendar.getLgbd(iToday);
		tTotalNav = _Lib.getAllTotalNAV(iToday);
		
		if (tTotalNav.getNumRows()==0)
			tTotalNav = _Lib.getAllTotalNAV(iYesterday);
	
		return tTotalNav;
	}
	
	private Table getCashflowByDaySimResult() throws OException
	{
		Table tCashByDaySim = Table.tableNew("All Cashflows known and projected by Ins");
		Table tBonds = Table.tableNew("Bonos");
		Table tSwaps = Table.tableNew("Swaps");
		StringBuffer sbSwaps = new StringBuffer();
		StringBuffer sbBonds = new StringBuffer();
		String sFecha = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_ISO8601_EXTENDED);
		
		//Query con suma de flujos de Bonos mayor a hoy y menor a 1 año
//		sb.append("\nSelect sum(CASE WHEN pr.accounting_date <= add_months(DATE '" + sFecha + "', 12) ");
//		sb.append("\n			then (case when c.name = 'MXN' then (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue else (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue * v.precio_sucio end) ");
//		sb.append("\n			else 0  ");
//		sb.append("\n		end) ohd_sum_flujos_bonos ");
//		sb.append("\nfrom ab_tran ab ");
//		sb.append("\n		left join profile pr on pr.ins_num = ab.ins_num ");
//		sb.append("\n		left join physcash n on n.ins_num = pr.ins_num and n.accounting_date = pr.accounting_date ");
//		sb.append("\n		left join parameter p on p.ins_num = ab.ins_num ");
//		sb.append("\n		inner join idx_def i on i.index_id = p.proj_index and i.db_status = 1 ");
//		sb.append("\n		inner join currency c on c.id_number = p.currency ");
//		sb.append("\n		left join user_mx_vectorprecios_diario v on v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name) ");
//		sb.append("\nwhere ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()); 
//		sb.append("\n			and ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
//		sb.append("\n			and ab.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt());
//		sb.append("\n			and ((DATE '" + sFecha + "' > pr.start_date and DATE '" + sFecha + "' < pr.end_date) or DATE '" + sFecha + "' < pr.start_date) ");

		

		//Query detalle - Flujos Bonos mayor dia hoy
		sbBonds.append("\nSelect ab.tran_num ");
		sbBonds.append("\n 		,ab.toolset ");
		sbBonds.append("\n		,ab.mvalue ");
		sbBonds.append("\n		,ab.ins_num ");
		sbBonds.append("\n		,p.fx_flt ");
		sbBonds.append("\n		,ab.currency ");
		sbBonds.append("\n		,i.index_name proj_index ");
		sbBonds.append("\n		,pr.profile_seq_num + 1 cupon ");
		sbBonds.append("\n		,case when DATE '" + sFecha + "' > pr.start_date and DATE '" + sFecha + "' < pr.end_date then 'A' else 'F' end tipo_cupon ");
		sbBonds.append("\n		,pr.daycount_factor ");
		sbBonds.append("\n		,pr.notnl ");
		sbBonds.append("\n		,NVL(n.cflow, 0) ohd_amortizacion ");
		sbBonds.append("\n		,pr.rate ");
		sbBonds.append("\n		,pr.float_spread ");
		sbBonds.append("\n		,pr.pymt + NVL(n.cflow, 0) ohd_pymt ");
		sbBonds.append("\n		,(pr.pymt + NVL(n.cflow, 0)) * ab.mvalue ohd_pago_mo ");
		sbBonds.append("\n		,case when c.name = '" + EnumsCurrency.MX_CURRENCY_MXN.toString() + "' then 1.0 else v.precio_sucio end ohd_fx_mxn ");
		sbBonds.append("\n		,case when c.name = '" + EnumsCurrency.MX_CURRENCY_MXN.toString() + "' then (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue else (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue * v.precio_sucio end ohd_pago_mxn ");
		sbBonds.append("\n		,pr.accounting_date ");
		sbBonds.append("\n		,case when pr.accounting_date <= add_months(DATE '" + sFecha + "', 12) "); 
		sbBonds.append("\n			then (case when c.name = '" + EnumsCurrency.MX_CURRENCY_MXN.toString() + "' then (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue else (pr.pymt + NVL(n.cflow, 0)) * ab.mvalue * v.precio_sucio end) ");
		sbBonds.append("\n			else 0 "); 
		sbBonds.append("\n		end ohd_oneyear ");
		sbBonds.append("\nfrom ab_tran ab ");
		sbBonds.append("\n		left join profile pr on pr.ins_num = ab.ins_num ");
		sbBonds.append("\n		left join physcash n on n.ins_num = pr.ins_num and n.accounting_date = pr.accounting_date ");
		sbBonds.append("\n		left join parameter p on p.ins_num = ab.ins_num ");
		sbBonds.append("\n		inner join idx_def i on i.index_id = p.proj_index and i.db_status = 1 ");
		sbBonds.append("\n		inner join currency c on c.id_number = p.currency ");
		sbBonds.append("\n		left join user_mx_vectorprecios_diario v on v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name) ");
		sbBonds.append("\nwhere ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()); 
		sbBonds.append("\n			and ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sbBonds.append("\n			and ab.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt());
		sbBonds.append("\n			and ((DATE '" + sFecha + "' > pr.start_date and DATE '" + sFecha + "' < pr.end_date) or DATE '" + sFecha + "' < pr.start_date) ");
		sbBonds.append("\norder by ab.tran_num, pr.profile_seq_num ");

		sbSwaps.append("\n --Swaps ");
		sbSwaps.append("\n Select ab.tran_num ");
		sbSwaps.append("\n 		,ab.toolset ");
		sbSwaps.append("\n 		,ab.mvalue ");
		sbSwaps.append("\n 		,ab.ins_num ");
		sbSwaps.append("\n 		,0 fx_flt ");
		sbSwaps.append("\n 		,ab.currency ");
		sbSwaps.append("\n 		,'' proj_index ");
		sbSwaps.append("\n 		,pr.profile_seq_num + 1 cupon ");
		sbSwaps.append("\n		,case when DATE '" + sFecha + "' > pr.start_date and DATE '" + sFecha + "' < pr.end_date then 'A' else 'F' end tipo_cupon ");
		sbSwaps.append("\n 		,pr.daycount_factor ");
		sbSwaps.append("\n 		,pr.notnl ");
		sbSwaps.append("\n 		,NVL(n.cflow, 0) ohd_amortizacion ");
		sbSwaps.append("\n 		,pr.rate ");
		sbSwaps.append("\n 		,pr.float_spread ");
		sbSwaps.append("\n 		,pr.pymt + NVL(n.cflow, 0) ohd_pymt ");
		sbSwaps.append("\n 		,(pr.pymt + NVL(n.cflow, 0)) * ab.mvalue ohd_pago_mo ");
		sbSwaps.append("\n 		,0 ohd_fx_mxn ");
		sbSwaps.append("\n 		,0 ohd_pago_mxn ");
		sbSwaps.append("\n 		,pr.accounting_date ");
		sbSwaps.append("\n		,case when pr.accounting_date <= add_months(DATE '" + sFecha + "', 12) "); 
		sbSwaps.append("\n			then NVL(n.cflow, pr.pymt) ");
		sbSwaps.append("\n			else 0.0 "); 
		sbSwaps.append("\n		end ohd_oneyear ");
		sbSwaps.append("\n from ab_tran ab ");
		sbSwaps.append("\n 		left join profile pr on pr.ins_num = ab.ins_num ");
		sbSwaps.append("\n 		left join physcash n on n.ins_num = pr.ins_num and n.accounting_date = pr.accounting_date ");
		sbSwaps.append("\n 		inner join currency c on c.id_number = ab.currency ");
		sbSwaps.append("\n 		left join user_mx_vectorprecios_diario v on v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name) ");
		sbSwaps.append("\n where ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()); 
		sbSwaps.append("\n			and ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sbSwaps.append("\n			and ab.toolset = " + TOOLSET_ENUM.SWAP_TOOLSET.toInt());
		sbSwaps.append("\n			and ((DATE '" + sFecha + "' > pr.start_date and DATE '" + sFecha + "' < pr.end_date) or DATE '" + sFecha + "' < pr.start_date) ");
		sbSwaps.append("\n order by ab.tran_num, pr.profile_seq_num ");
		
		//OConsole.oprint("\nSql: " + sbDetail.toString());
		
        try {
        	
        	@SuppressWarnings("unused")
			int iRetB = DBaseTable.execISql(tBonds, sbBonds.toString());
			int iRetS = DBaseTable.execISql(tSwaps, sbSwaps.toString());
        	
        } catch (OException e){
                OConsole.oprint("Cashflows Query failed.");
                Util.exitFail();
        }
        
        try {
        	
            tCashByDaySim.select(tBonds, "*", "tran_num GT 0");
            tCashByDaySim.select(tSwaps, "*", "tran_num GT 0");
        	
        } catch (OException e){
                OConsole.oprint("Unable to make a select from Swaps or Bonds Tables");
                Util.exitFail();
        }
                
		return tCashByDaySim;
		
	}
	
	private Table getInstrumentTranNum() throws OException
	{
		Table tResult = Table.tableNew("Instruments Scope");
		
		int iQuery = 0;
        try {
        	iQuery = Query.tableQueryInsert(tTrans, "tran_num");
        } catch (OException exception){
                OConsole.oprint("Table query insert failed.");
                Util.exitFail();
        }
        
		String sSql = "\nSelect ab_ins.tran_num From ab_tran ab, ab_tran ab_ins, " + QUERY_RESULT_TABLE + " trans " +
					  "\nWhere ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +
					  "\nAnd ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() +
					  "\nAnd ab.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt() +
					  "\nAnd ab_ins.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +
					  "\nAnd ab_ins.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() +
					  "\nAnd ab_ins.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt() +
					  "\nAnd ab.ins_num = ab_ins.ins_num" + 
					  "\nAnd ab.tran_num = trans.query_result" +
					  "\nAnd trans.unique_id = " +  iQuery;

		OConsole.oprint("\nSQL: " + sSql );
		try{
			int iret = DBaseTable.execISql(tResult, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		//Query.clear(iQuery);
		return tResult;
	}
	
	private void doAddPortfolio() throws OException
	{
		Table tTmp = Table.tableNew();
		String sSql = "\nSelect ab.deal_tracking_num deal_num, ab.ins_num, ab.ins_type, ab.internal_portfolio, ab.mvalue" +
					  "\nfrom ab_tran ab where ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + 
					  "\nand ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tTmp, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		tTrans.select(tTmp, "ins_num, ins_type, internal_portfolio, mvalue", "deal_num EQ $deal_num");
	}
	
	private Table getCashFxByDay(Table tArgt) throws OException
	{
		Table tCashFxByDay = Table.tableNew();
		
		// Run the simulation for trades queried
		Table tResList = Sim.createResultListForSim ();
		SimResultType simResultType = SimResultType.create(EnumsUserSimResultType.MX_SIM_RISK_TODAY_CASHFX_POSITIONS.toString());
		SimResult.addResultForSim(tResList, simResultType);

		int iQuery = 0;
        try {
        	iQuery = Query.tableQueryInsert(tTrans, "tran_num");
    		
			// The QueryId is now set into argt to prepare for the execution of a simulation.
    		tArgt.setInt("QueryId", 1, iQuery);

    		// Get all deals for specific Portfolio from tDeals and into a query table
    		Table tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());

    		Query.clear(iQuery);
    		
    		// Extract the results from the simulation table for further processing
    		Table tResultsGen = SimResult.getGenResults(tSimRes);

    		int iFind = tResultsGen.unsortedFindInt("result_type", simResultType.getId());
    		
    		tCashFxByDay = tResultsGen.getTable("result", iFind);
        	
        	
        } catch (OException exception){
                OConsole.oprint("Cashflow by day sim result for instruments failed.");
                Util.exitFail();
        }
 
		return tCashFxByDay;
	}

}