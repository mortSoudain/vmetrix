/**$Header: v1.0  06/02/2018 $*/
/*
 * File Name:               FMWK_Reports_Metricas_LiquidezCortoPlazo.java
 * Input File Name:         None
 * Author:                  Gustavo Rojas Arteaga - VMetrix
 * Creation Date:           06/02/2018
 * 
 * REVISION HISTORY
 *  Release:				     
 *  Date:				        
 *  Author:					
 *  Description:			     
 *  			      
 *  
 */
package com.afore.reportes.metricas;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;


public class FMWK_Reports_Metricas_LiquidezCortoPlazo implements IScript {
	
	private final String sPlugInName = this.getClass().getSimpleName();

	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _utilafore = new UTIL_Afore();

	@Override
	public void execute(IContainerContext context) throws OException 
	{
		_Log.markStartScript();
		_Log.setDEBUG_STATUS(EnumStatus.OFF);
		
		Table tArgt			= Util.NULL_TABLE;
		Table tReturnt		= Util.NULL_TABLE;
		Table tResList		= Util.NULL_TABLE;
		Table tSimRes		= Util.NULL_TABLE;
		Table tTrans		= Util.NULL_TABLE;
		Table tGenResults	= Util.NULL_TABLE;
		Table tLiqCortoPla	= Util.NULL_TABLE;

		int iQuery, iFecha;
		
		try{
			
			tArgt    = context.getArgumentsTable();
    		tReturnt = context.getReturnTable();
    		
    		iFecha = OCalendar.today();
    		
    		if(tArgt.getColNum("QueryId") <= 0)
    			tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);

			tResList = Sim.createResultListForSim ();
    		SimResultType usrLiquidez   = SimResultType.create(EnumsUserSimResultType.MX_SIM_RISK_LIQUIDEZ_CORTO_PLAZO.toString());
    		SimResult.addResultForSim(tResList, usrLiquidez);
    		
    		tTrans = getAllTrades();
    		
			iQuery = Query.tableQueryInsert(tTrans, "tran_num");
			
			if(tArgt.getNumRows() < 1)
				tArgt.addRow();
			
			tArgt.setInt("QueryId", 1, iQuery);
			
			tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
    		
    		Query.clear(iQuery);
    		
    		tGenResults	= SimResult.getGenResults(tSimRes);
    		tLiqCortoPla	= SimResult.getGenResultTables(tGenResults, EnumsUserSimResultType.MX_SIM_RISK_LIQUIDEZ_CORTO_PLAZO.toInt()).getTable("results", 1);

    		tReturnt.addCol("fecha", COL_TYPE_ENUM.COL_INT);
    		
    		tReturnt.select(tLiqCortoPla, "*", "internal_portfolio GT 0");
    		
    		tReturnt.setColValInt("fecha", iFecha);
    		
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally
		{
			_Log.markEndScript();
		}

	}

	private Table getAllTrades() throws OException
	{
		Table tTmp = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		
		sb.append("\n select ab.deal_tracking_num deal_num, ab.tran_num, ab.ins_num, ab.ins_type, ab.internal_portfolio, ins.external_bunit ");
		sb.append("\n 				from ab_tran ab, ab_tran ins ");
		sb.append("\n 				where ab.ins_num = ins.ins_num ");
		sb.append("\n 				and ab.tran_status =  " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +" ");
		sb.append("\n 				and ins.tran_status =  " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +" ");
		sb.append("\n 				and ab.tran_type =  " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() +" ");
		sb.append("\n 				and ins.tran_type =  " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() +" ");
		sb.append("\n 				and ab.toolset in (" + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.BOND_TOOLSET.toInt() + "," + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ") ");
		sb.append("\n UNION ");
		sb.append("\n select ab.deal_tracking_num deal_num, ab.tran_num, ab.ins_num, ab.ins_type, ab.internal_portfolio, ab.external_bunit ");
		sb.append("\n from ab_tran ab ");
		sb.append("\n where ab.tran_status =  " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +" ");
		sb.append("\n and ab.tran_type =  " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() +" ");
		sb.append("\n and ab.toolset = " + TOOLSET_ENUM.SWAP_TOOLSET.toInt() + " ");
		sb.append("\n and ab.ins_type = "+EnumsInstrumentsMX.MX_SWAP_TIIE_28D.toInt()+" ");
		
		_Log.printMsg(EnumTypeMessage.DEBUG, "Sql:" + sb.toString());	
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tTmp, sb.toString());
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
				
		return tTmp;
	}
}
