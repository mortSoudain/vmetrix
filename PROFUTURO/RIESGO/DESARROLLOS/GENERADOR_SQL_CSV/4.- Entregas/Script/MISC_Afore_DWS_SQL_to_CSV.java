/*$Header: v 1.0, 04/Jun/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DRSK_BBG_Response.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Junio 2018
 * Version:					1.0
 * Description:				Script creado para generar archivos .CSV, a partir de consultas almacenadas en archivos .SQL.
 * 
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;

import com.afore.enums.EnumTypeMessage;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASK_SELECT_TYPES;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;

public class MISC_Afore_DWS_SQL_to_CSV implements IScript {

	// Basic util variables
	private String sScriptName = this.getClass().getSimpleName();
	private UTIL_Log _Log = new UTIL_Log(sScriptName);
	private UTIL_Afore UtilAfore = new UTIL_Afore();

	private Table tAsk = Util.NULL_TABLE;

	// Time variables
	private int iToday = 0;
	private String sTodayDefault = "";

	// Specific script variables
	private String SQL_FILEPATH = "";
	private String CSV_FILEPATH = "";

	@Override
	public void execute(IContainerContext context) {

		_Log.markStartScript();

			initializeGlobalVariables();
	
			setVisualElements();
	
			generateCsvFile();

		_Log.markEndScript();

	}
	
	private void initializeGlobalVariables() {

		try {
			tAsk = Table.tableNew("Ask");
			iToday = OCalendar.today();
			sTodayDefault = OCalendar.formatDateInt(iToday,
					DATE_FORMAT.DATE_FORMAT_ISO8601);
			SQL_FILEPATH = UtilAfore.getVariableGlobal("FINDUR",
					"MISC_Afore_DWS_SQL_to_CSV", "sql_path");
			CSV_FILEPATH = UtilAfore.getVariableGlobal("FINDUR",
					"MISC_Afore_DWS_SQL_to_CSV", "csv_path");
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeGlobalVariables function. Error: "
							+ e.getMessage());
		}
	}
	
	private void setVisualElements() {

		Table tFileNames = readSqlFileNamesFromSqlDirectory();

		try {
			Ask.setAvsTable(tAsk, tFileNames, "SQL", 1,
					ASK_SELECT_TYPES.ASK_SINGLE_SELECT.jvsValue(), 1);
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to combo box tFileNames Table on Ask object, on setVisualElements function. Error: "
							+ e.getMessage());
		}

	}

	private void generateCsvFile() {

		int iReturnValue = 0;
		
		// Get return value
			try {
				iReturnValue = Ask
						.viewTable(
								tAsk,
								"SQL to CSV",
								"Seleccione una consulta SQL de la lista, presione OK y se generara el archivo CSV.");
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to show Ask window, on generateCsvFiles function. Error: "
								+ e.getMessage());
			}
			
		// If return value is not 0 or negative, generate file
			if (iReturnValue > 0) {
				
				String sFileName = "";

				// Get selected Filename
					try {
						Table tSelectedFile = Table.tableNew("Selected File");
						tSelectedFile = tAsk.getTable("return_value", 1);
						sFileName = tSelectedFile.getString("return_val", 1);
					} catch (OException e) {
						_Log.printMsg(EnumTypeMessage.ERROR,
								"Unable to show Ask message, on generateCsvFiles function. Error: "
										+ e.getMessage());
					}

				// Read File info (query)
					String sFileQuery = readFile(SQL_FILEPATH + sFileName,
						Charset.defaultCharset());

				// Execute query and set data info Table
					Table tQueryData = getTableFromQuery(sFileQuery);

				// View generated query data
					try {
						tQueryData.viewTable();
					} catch (OException e) {
						_Log.printMsg(EnumTypeMessage.ERROR,
								"Unable to show Ask message, on generateCsvFiles function. Error: "
										+ e.getMessage());
					}

				// Generate csv output file
					generateCSV(tQueryData, sFileName);
				
			} 
		// Else, user aborted script
			else {
				
				try {
					Ask.ok("User pressed cancel. Aborting...");
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to show Ask message, on generateCsvFiles function. Error: "
									+ e.getMessage());
				}
				
				//Util.exitFail();
			}

	}

	private Table readSqlFileNamesFromSqlDirectory() {

		int iNewRow = 0;
		Table tFileNames = Util.NULL_TABLE;

		try {
			tFileNames = Table.tableNew();
			tFileNames.addCol("SQL", COL_TYPE_ENUM.COL_STRING);
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to create tFileNames Table, on readSqlFileNamesFromSqlDirectory function. Error: "
							+ e.getMessage());
		}

		File folder = new File(SQL_FILEPATH);
		File[] listOfFiles = folder.listFiles();

		try {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					String sFileName = listOfFiles[i].getName();
					String sFileExtension = FilenameUtils
							.getExtension(sFileName);

					if (sFileExtension.equals("sql")
							|| sFileExtension.equals("SQL")) {
						iNewRow = tFileNames.addRow();
						tFileNames.setString("SQL", iNewRow, sFileName);
					}
				}
			}
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable set some String sFileName on tFileNames Table, on readSqlFileNamesFromSqlDirectory function. Error: "
							+ e.getMessage());
		}

		return tFileNames;
	}

	private String readFile(String path, Charset encoding) {
		byte[] encoded = null;

		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to real file, on readFile function. Error: "
							+ e.getMessage());
		}

		return new String(encoded, encoding);
	}

	private Table getTableFromQuery(String sFileQuery) {

		Table tQueryData = Util.NULL_TABLE;

		try {
			tQueryData = Table.tableNew("Query Data");
			DBaseTable.execISql(tQueryData, sFileQuery);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to load data from query, on getTableFromQuery function. Error: "
							+ e.getMessage());
		}

		return tQueryData;
	}

	private void generateCSV(Table tQueryData, String sFileName) {
		
		int iSaveResult = 0;
		String sOutputFilename = CSV_FILEPATH + sFileName + " - "
				+ sTodayDefault + ".csv";

		// Generate CSV
			try {
				iSaveResult = tQueryData.printTableDumpToFile(sOutputFilename);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to generate csv, on generateCSV function. Error: "
								+ e.getMessage());
			}
		
		// Send result message
			try {
				if (iSaveResult != -1) {
					Ask.ok("Archivo generado exitosamente");
				} else {
					Ask.ok("No fue posible generar el archivo CSV");
				}
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable show generate file result, on generateCSV function. Error: ");
			}
	}

}
