/**$Header: v1.0  11/12/2017 $*/
/*
 * File Name:               FMWK_Reports_Normativo_Resumen_ClaseActivo.java
 * Input File Name:         None
 * Author:                  Gustavo Rojas Arteaga - VMetrix
 * Creation Date:           11/12/2017
 * 
 * REVISION HISTORY
 *  Release:				     
 *  Date:				        
 *  Author:					
 *  Description:			      
 *  			      

 */
package com.afore.normativos;

import standard.include.JVS_INC_STD_Simulation;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsPortfolios;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.enums.EnumsUserCflowType;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.FIXED_OR_FLOAT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Reports_Normativo_Resumen_ClaseActivo implements IScript {

	private final String sPlugInName = this.getClass().getSimpleName();
	
	int iToday;
	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _utilafore = new UTIL_Afore();
	
	JVS_INC_STD_Simulation  m_INCSTDSimulation = new JVS_INC_STD_Simulation();
	
	@Override
	public void execute(IContainerContext context) throws OException {

		_Log.markStartScript();
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		iToday = OCalendar.today();
    	
		int iPortfolio = 0;
		int iQuery = 0;
		
		Table tArgt               = Util.NULL_TABLE;
		Table tReturnt            = Util.NULL_TABLE;
		Table tResList            = Util.NULL_TABLE;
		Table tAllTrades          = Util.NULL_TABLE;
		Table tPortfolio          = Util.NULL_TABLE;
		Table tTradesPortfolio    = Util.NULL_TABLE;
		Table tTradesClaseActivo  = Util.NULL_TABLE;
		Table tSimRes             = Util.NULL_TABLE;
		Table tTranResults        = Util.NULL_TABLE;
		Table tNav        		  = Util.NULL_TABLE;
		
		try{
			
			tArgt    = context.getArgumentsTable();
    		tReturnt = context.getReturnTable();
    		
    		if(tArgt.getColNum("QueryId") <= 0)
    			tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);

    		//Using MTM BASE PROFUTURO
			tResList = Sim.createResultListForSim ();
    		SimResultType srtBaseMTM   = SimResultType.create(EnumsUserSimResultType.MX_SIM_MTM_BASE_PROFUTURO.toString());
    		SimResult.addResultForSim(tResList, srtBaseMTM);
    		
    		//Getting NAV
    		tNav = getNav();

    		//Getting all trades
    		tAllTrades = getTranVigentes();
    		
    		tPortfolio = Table.tableNew("Portfolios");
    		tPortfolio.select(tAllTrades, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
    		tPortfolio.sortCol("internal_portfolio");
    			
			for (int iRowPortfolio = 1; iRowPortfolio <= tPortfolio.getNumRows(); iRowPortfolio++) {
				
				iPortfolio = tPortfolio.getInt("internal_portfolio", iRowPortfolio);
				
				Table tTradesSimulacion = Table.tableNew("Trade Simulacion");
				
				tTradesSimulacion.select(tAllTrades, "DISTINCT, deal_num, tran_num", "internal_portfolio EQ " + iPortfolio);
				
				iQuery = Query.tableQueryInsert(tTradesSimulacion, "tran_num");
				
				if(tArgt.getNumRows() < 1)
					tArgt.addRow();
				
				tArgt.setInt("QueryId", 1, iQuery);
				
				_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Running SIM Result for Portfolio " + iPortfolio);
				tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
	    		
	    		Query.clear(iQuery);
	    		
	    		tTranResults = SimResult.getTranResults(tSimRes);
	    		
	    		tTradesSimulacion.select(tTranResults, srtBaseMTM.getId()+"(base_mtm)", "deal_num EQ $deal_num AND deal_leg EQ 0");

	    		_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Setting MTM from Sim for Portfolio " + iPortfolio);
	    		tAllTrades.select(tTradesSimulacion, "base_mtm", "deal_num EQ $deal_num");

	    		if(Table.isTableValid(tTradesPortfolio )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTradesPortfolio.destroy();
	    		if(Table.isTableValid(tSimRes          )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tSimRes.destroy();
				if(Table.isTableValid(tTranResults     )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTranResults.destroy();
				if(Table.isTableValid(tTradesClaseActivo) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTradesClaseActivo.destroy();
			}    		

			_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Setting Nav");

			//Sum
			//Set final table
			tReturnt.addCol("clase_activo", COL_TYPE_ENUM.COL_STRING);
			tReturnt.addCol("clase_activo_sub1", COL_TYPE_ENUM.COL_STRING);
			tReturnt.addCol("clase_activo_sub2", COL_TYPE_ENUM.COL_STRING);
			tReturnt.addCol("ca_type", COL_TYPE_ENUM.COL_INT);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SB1.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SB2.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SB3.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SB4.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SBP.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SCP.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			tReturnt.addCol(EnumsPortfolios.MX_PORTFOLIO_SLP.toString(), COL_TYPE_ENUM.COL_DOUBLE);
			
			//Create Files
			int iRow;
			//RENTA FIJA GUBERNAMENTAL
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "GUBERNAMENTAL");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Nominal");
			tReturnt.setInt("ca_type", iRow, 1);
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "GUBERNAMENTAL");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Flotante");
			tReturnt.setInt("ca_type", iRow, 2);
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "GUBERNAMENTAL");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Real");
			tReturnt.setInt("ca_type", iRow, 3);

			//RENTA FIJA PRIVADA
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "PRIVADA");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Nominal");
			tReturnt.setInt("ca_type", iRow, 4);
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "PRIVADA");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Flotante");
			tReturnt.setInt("ca_type", iRow, 5);
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA FIJA");
			tReturnt.setString("clase_activo_sub1", iRow, "PRIVADA");
			tReturnt.setString("clase_activo_sub2", iRow, "Tasa Real");
			tReturnt.setInt("ca_type", iRow, 6);
			
			//RENTA VARIABLE
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA VARIABLE");
			tReturnt.setString("clase_activo_sub1", iRow, "CAPITALES NACIONALES");
			tReturnt.setInt("ca_type", iRow, 7);

			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "RENTA VARIABLE");
			tReturnt.setString("clase_activo_sub1", iRow, "CAPITALES EXTRANJEROS");
			tReturnt.setInt("ca_type", iRow, 8);
			
			//BURSATILIZADOS
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "BURSATILIZADOS");
			tReturnt.setInt("ca_type", iRow, 9);
			
			//FIBRAS
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "FIBRAS");
			tReturnt.setInt("ca_type", iRow, 10);
			
			//INSTRUMENTOS ESTRUCTURADOS
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "INSTRUMENTOS ESTRUCTURADOS");
			tReturnt.setInt("ca_type", iRow, 11);
			
			//OPERACIONES LIQUIDEZ
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "OPERACIONES LIQUIDEZ");
			tReturnt.setInt("ca_type", iRow, 12);
			
			//EXPOSICION INDIRECTA DIVISAS
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "EXPOSICION INDIRECTA DIVISAS");
			tReturnt.setInt("ca_type", iRow, 13);
			
			//EXPOSICION DIRECTA DIVISAS
			iRow = tReturnt.addRow();
			tReturnt.setString("clase_activo", iRow, "EXPOSICION DIRECTA DIVISAS");
			tReturnt.setInt("ca_type", iRow, 14);

			//SUMAS
			
			for(int i=1;i<=tPortfolio.getNumRows();i++)
			{
				String sPortafolio = Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, tPortfolio.getInt("internal_portfolio", i));

				tReturnt.select(tAllTrades, "SUM,base_mtm(" + sPortafolio + ")", "internal_portfolio EQ " + tPortfolio.getInt("internal_portfolio", i) + " AND ca_type EQ $ca_type");
			}
			
		}catch (Throwable e){
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{
			if(Table.isTableValid(tResList         ) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tResList.destroy();
			_Log.markEndScript();
		}

	}
	
	
	private Table getTranVigentes() throws OException {
		
		Table tReturn = Util.NULL_TABLE;
		tReturn       = Table.tableNew("Deals por Clases de Activo");

		StringBuffer sb = new StringBuffer();
		
//		sb.append("SELECT u.exp_defn_id, u.nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
//		sb.append("   CASE \n");
//		sb.append("      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_MXN.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 1 \n"); 	//Renta Fija Gubernamentales Tasa Nominal
//		sb.append("      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND pa.fx_flt = " + FIXED_OR_FLOAT.FLOAT_RATE.toInt() + ") THEN 2 \n");																		//Renta Fija Gubernamentales Tasa Flotante
//		sb.append("      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_UDI.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 3 \n");	//Renta Fija Gubernamentales Tasa Real
//		sb.append("      WHEN (u.nemotecnico <> 'BURSATILIZADOS' AND ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_MXN.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 4 \n");	//Renta Fija Privados Tasa Nominal
//		sb.append("      WHEN (u.nemotecnico <> 'BURSATILIZADOS' AND ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND pa.fx_flt = " + FIXED_OR_FLOAT.FLOAT_RATE.toInt() + ") THEN 5 \n");																		//Renta Fija Privados Tasa Flotante
//		sb.append("      WHEN (u.nemotecnico <> 'BURSATILIZADOS' AND ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_UDI.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 6 \n");	//Renta Fija Privados Tasa Real
//		sb.append("		 WHEN (u.nemotecnico = 'BURSATILIZADOS') THEN 9  \n");
//		sb.append("		 WHEN (u.nemotecnico = 'FIBRAS TOTAL') THEN 10   \n");
//		sb.append("		 WHEN (u.nemotecnico = 'ESTRUCTURADOS (CKDS)') THEN 11 \n");
//		sb.append("		 WHEN (u.nemotecnico = 'EXPOSICION DIRECTA A DIVISAS') THEN 14 \n");
//		sb.append("		 WHEN (u.nemotecnico LIKE '%CAPITALES NACIONALES MAXIMO%') THEN 7 \n");
//		sb.append("		 WHEN (u.nemotecnico LIKE '%CAPITALES EXTRANJEROS MAXIMO%') AND (\n");
//		sb.append("							h.ticker = '1B_CHNTRAC_11' OR \n");
//		sb.append("							h.ticker like '56_%' OR \n");
//		sb.append("							h.ticker like '1A_%' OR \n");
//		sb.append("							h.ticker like '1ASP_%' OR \n");
//		sb.append("							h.ticker like '1E_%' OR \n");
//		sb.append("							h.ticker like '1ESP_%' OR \n");
//		sb.append("							h.ticker like '1I_%' OR \n");
//		sb.append("							h.ticker like '1ISP_%' OR \n");
//		sb.append("							h.ticker like '56SP_%' OR \n");
//		sb.append("							h.ticker like 'YY_%' OR \n");
//		sb.append("							h.ticker like 'YYSP_%' OR \n");
//		sb.append("							h.ticker like '1M_%' OR \n");
//		sb.append("							h.ticker like '1MSP_%' ) THEN 8 \n");
//		sb.append("		 WHEN (u.nemotecnico LIKE '%FUTUROS DE RENTA VARIABLE%') THEN 8 \n");
//		sb.append("     ELSE 0 END as ca_type \n");
//		sb.append(" FROM ab_tran ab , ab_tran ab2 , header h, user_mx_limites u , ab_tran_info_view abi, parameter pa, \n");
//		sb.append("		instruments ins \n");
//		sb.append(" 	LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
//		sb.append(" WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "\n");
//		sb.append(" AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + "\n");
//		sb.append(" AND ab.tran_num = abi.tran_num \n");
//		sb.append(" AND abi.type_name = '" + EnumsTranInfoFields.MX_ALL_INS_CLASE_ACTIVO.toString() + "'\n");
//		sb.append(" AND ab.ins_num = h.ins_num \n");
//		sb.append(" AND ab2.ins_num = h.ins_num \n");
//		sb.append(" AND abi.value = u.nemotecnico \n");
//		sb.append(" AND ab.ins_num = ab2.ins_num \n");
//		sb.append(" AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "\n");
//		sb.append(" AND ab2.tran_type = " + + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + "\n");
//		sb.append(" AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
//													 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
//		sb.append(" AND ins.id_number = ab2.ins_type \n");
//		sb.append(" AND ins.id_number = ab.ins_type \n");
//		sb.append(" AND pa.ins_num = ab2.ins_num \n");
//		sb.append(" AND pa.ins_num = ab.ins_num \n");
//		sb.append(" AND pa.param_seq_num = 0 \n");
//		sb.append(" ORDER BY u.exp_defn_id,ab2.deal_tracking_num\n");
		
		
		
		sb.append("SELECT * FROM ( \n");
		sb.append("		-- Operaciones de Deuda Renta Fija Gubernamental y Privada \n");
		sb.append("		SELECT u.exp_defn_id, u.nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
		sb.append("		   CASE \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_MXN.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + " ) THEN 1 \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND pa.fx_flt = " + FIXED_OR_FLOAT.FLOAT_RATE.toInt() + ") THEN 2 \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)>0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_UDI.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 3 \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_MXN.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 4 \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND pa.fx_flt = " + FIXED_OR_FLOAT.FLOAT_RATE.toInt() + ") THEN 5 \n");
		sb.append("		      WHEN (ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " AND NVL(gob.id,0)=0 AND ab2.currency = " + EnumsCurrency.MX_CURRENCY_UDI.toInt() + " AND pa.fx_flt = " + FIXED_OR_FLOAT.FIXED_RATE.toInt() + ") THEN 6 \n");
		sb.append("		     ELSE 0 END as ca_type \n");
		sb.append("		 FROM ab_tran ab , ab_tran ab2 , header h, user_mx_limites u , ab_tran_info_view abi, parameter pa, \n");
		sb.append("		                instruments ins \n");
		sb.append("		        LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
		sb.append("		 WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " \n");
		sb.append("		 AND ab.tran_num = abi.tran_num \n");
		sb.append("		 AND abi.type_name = '" + EnumsTranInfoFields.MX_ALL_INS_CLASE_ACTIVO.toString() + "' \n");
		sb.append("		 AND ab.ins_num = h.ins_num \n");
		sb.append("		 AND ab2.ins_num = h.ins_num \n");
		sb.append("		 AND abi.value = u.nemotecnico \n");
		sb.append("		 AND ab.ins_num = ab2.ins_num \n");
		sb.append("		 AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab2.tran_type = " + + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " \n");
		sb.append(" 	 AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
														 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
		sb.append("		 AND ins.id_number = ab2.ins_type \n");
		sb.append("		 AND ins.id_number = ab.ins_type \n");
		sb.append("		 AND pa.ins_num = ab2.ins_num \n");
		sb.append("		 AND pa.ins_num = ab.ins_num \n");
		sb.append("		 AND pa.param_seq_num = 0 \n");
		sb.append("		 UNION \n");
		sb.append("		 -- Operaciones de Renta Variable \n");
		sb.append("		 SELECT u.exp_defn_id, u.nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
		sb.append("		   CASE \n");
		sb.append("		      WHEN (u.nemotecnico = 'BURSATILIZADOS') THEN 9 \n");
		sb.append("		      WHEN (u.nemotecnico = 'FIBRAS TOTAL') THEN 10 \n");
		sb.append("		      WHEN (u.nemotecnico = 'ESTRUCTURADOS (CKDS)') THEN 11 \n");
		sb.append("		      WHEN (u.nemotecnico = 'EXPOSICION DIRECTA A DIVISAS') THEN 14 \n");
		sb.append("		      WHEN (u.nemotecnico LIKE '%CAPITALES NACIONALES MAXIMO%') THEN 7 \n");
		sb.append("		      WHEN (u.nemotecnico LIKE '%CAPITALES EXTRANJEROS MAXIMO%') AND ( \n");
		sb.append("		                                            h.ticker = '1B_CHNTRAC_11' OR \n");
		sb.append("		                                            h.ticker like '56_%' OR \n");
		sb.append("		                                            h.ticker like '1A_%' OR \n");
		sb.append("		                                            h.ticker like '1ASP_%' OR \n");
		sb.append("		                                            h.ticker like '1E_%' OR \n");
		sb.append("		                                            h.ticker like '1ESP_%' OR \n");
		sb.append("		                                            h.ticker like '1I_%' OR \n");
		sb.append("		                                            h.ticker like '1ISP_%' OR \n");
		sb.append("		                                            h.ticker like '56SP_%' OR \n");
		sb.append("		                                            h.ticker like 'YY_%' OR \n");
		sb.append("		                                            h.ticker like 'YYSP_%' OR \n");
		sb.append("		                                            h.ticker like '1M_%' OR \n");
		sb.append("		                                            h.ticker like '1MSP_%' ) THEN 8 \n");
		sb.append("		      WHEN (u.nemotecnico LIKE '%FUTUROS DE RENTA VARIABLE%') THEN 8 \n");
		sb.append("		      ELSE 0 END as ca_type \n");
		sb.append("		 FROM ab_tran ab , ab_tran ab2 , header h, user_mx_limites u , ab_tran_info_view abi, parameter pa, \n");
		sb.append("		                instruments ins \n");
		sb.append("		        LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
		sb.append("		 WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " \n");
		sb.append("		 AND ab.tran_num = abi.tran_num \n");
		sb.append("		 AND abi.type_name = '" + EnumsTranInfoFields.MX_ALL_INS_CLASE_ACTIVO.toString() + "' \n");
		sb.append("		 AND ab.ins_num = h.ins_num \n");
		sb.append("		 AND ab2.ins_num = h.ins_num \n");
		sb.append("		 AND abi.value = u.nemotecnico \n");
		sb.append("		 AND ab.ins_num = ab2.ins_num \n");
		sb.append("		 AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab2.tran_type = " + + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " \n");
		sb.append(" 	 AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
												 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
		sb.append("		 AND ins.id_number = ab2.ins_type \n");
		sb.append("		 AND ins.id_number = ab.ins_type \n");
		sb.append("		 AND pa.ins_num = ab2.ins_num \n");
		sb.append("		 AND pa.ins_num = ab.ins_num \n");
		sb.append("		 AND pa.param_seq_num = 0 \n");
		sb.append("		 UNION \n");
		sb.append("		 -- Operaciones de Liquidez: Instrumentos de Deuda Renta Fija Gubernamental y Privada con Fecha Vencimiento menor igual a 3 dias mas saldo inicial y excedentes de SL \n");
		sb.append("		SELECT 0 exp_defn_id, 'SIN CLASIFICACION' nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
		sb.append("		   12 ca_type \n");
		sb.append("		 FROM ab_tran ab , ab_tran ab2 , parameter pa, \n");
		sb.append("		                instruments ins \n");
		sb.append("		        LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
		sb.append("		 WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " \n");
		sb.append("		 AND ab.ins_num = ab2.ins_num \n");
		sb.append("		 AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab2.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " \n");
		sb.append(" 	 AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
											 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
		sb.append("		 AND ins.id_number = ab2.ins_type \n");
		sb.append("		 AND ins.id_number = ab.ins_type \n");
		sb.append("		 AND pa.ins_num = ab2.ins_num \n");
		sb.append("		 AND pa.ins_num = ab.ins_num \n");
		sb.append("		 AND pa.param_seq_num = 0 \n");
		sb.append("		 AND ((ab2.toolset = " + TOOLSET_ENUM.BOND_TOOLSET.toInt() + " and ab2.maturity_date <= TO_DATE('" + OCalendar.formatDateInt(OCalendar.today()+3, DATE_FORMAT.DATE_FORMAT_MDSY_SLASH) + "','DD/MM/YYYY')) or (ab2.toolset = " + TOOLSET_ENUM.CASH_TOOLSET.toInt() + " and ab2.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt() + "," + EnumsUserCflowType.MX_CFLOW_TYPE_SALDO_INICIAL.toInt() + "))) \n");
		sb.append("		 UNION \n");
		sb.append("		 -- Operaciones Divisas en Directo HOLDING \n");
		sb.append("		SELECT 0 exp_defn_id, 'SIN CLASIFICACION' nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
		sb.append("		   14 ca_type \n");
		sb.append("		 FROM ab_tran ab , ab_tran ab2 , parameter pa, \n");
		sb.append("		                instruments ins \n");
		sb.append("		        LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
		sb.append("		 WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " \n");
		sb.append("		 AND ab.ins_num = ab2.ins_num \n");
		sb.append("		 AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab2.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " \n");
		sb.append(" 	 AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
		sb.append("		 AND ins.id_number = ab2.ins_type \n");
		sb.append("		 AND ins.id_number = ab.ins_type \n");
		sb.append("		 AND pa.ins_num = ab2.ins_num \n");
		sb.append("		 AND pa.ins_num = ab.ins_num \n");
		sb.append("		 AND pa.param_seq_num = 0 \n");
		sb.append("		 AND ((ab2.toolset = " + TOOLSET_ENUM.LOANDEP_TOOLSET.toInt() + " and ab2.currency not in (" + EnumsCurrency.MX_CURRENCY_MXN.toInt() + "," + EnumsCurrency.MX_CURRENCY_UDI.toInt() + ")) or (ab2.toolset = " + TOOLSET_ENUM.CASH_TOOLSET.toInt() + " and ab2.cflow_type = " + EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt() + ") or (ab2.toolset in (" + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ") and ab2.ins_type <> " + EnumsInstrumentsMX.MX_FX_FUT.toInt() + ")) \n");
		sb.append("		 UNION \n");
		sb.append("\n  -- Operaciones Divisas en Directo NON-HOLDING");
		sb.append("\n SELECT ");
		sb.append("\n 	0 exp_defn_id ");
		sb.append("\n 	,'SIN CLASIFICACION' nemotecnico ");
		sb.append("\n 	,ab.internal_portfolio ");
		sb.append("\n 	,ab.ins_num  ");
		sb.append("\n 	,ab.ins_type ");
		sb.append("\n 	,ab.toolset ");
		sb.append("\n 	,ab.currency ");
		sb.append("\n 	,ab.deal_tracking_num as deal_num ");
		sb.append("\n 	,ab.tran_num ");
		sb.append("\n 	,NVL(gob.id,0) guber_type ");
		sb.append("\n 	,0 fx_flt ");
		sb.append("\n 	,14 ca_type ");
		sb.append("\n FROM ab_tran ab ");
		sb.append("\n 	LEFT JOIN instruments ins on ins.id_number = ab.ins_num ");
		sb.append("\n 	LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type  ");
		sb.append("\n WHERE ");
		sb.append("\n 	ab.tran_status = 3 -- Validated ");
		sb.append("\n 	AND ab.tran_type = 0 -- Trading ");
		sb.append("\n 	AND ab.internal_portfolio IN (20010,20011,20012,20013,20014,20015,20016) ");
		sb.append("\n 	AND ab.toolset = 1 -- Swaps ");
		sb.append("\n 	AND ab.ins_type = 1000060 --Swap-Tiie28d");
		sb.append("\n 	UNION ");
		sb.append("		  -- Operaciones Divisas en Indirecto \n");
		sb.append("		SELECT 0 exp_defn_id, 'SIN CLASIFICACION' nemotecnico, ab2.internal_portfolio, ab.ins_num , ab2.ins_type, ab2.toolset, ab2.currency, ab2.deal_tracking_num as deal_num, ab2.tran_num, NVL(gob.id,0) guber_type, pa.fx_flt, \n");
		sb.append("		   13 ca_type \n");
		sb.append("		 FROM ab_tran ab , ab_tran ab2 , parameter pa, \n");
		sb.append("		                instruments ins \n");
		sb.append("		        LEFT JOIN user_mx_activos_alta_calidad gob ON ins.name = gob.instrument_type \n");
		sb.append("		 WHERE ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " \n");
		sb.append("		 AND ab.ins_num = ab2.ins_num \n");
		sb.append("		 AND ab2.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " \n");
		sb.append("		 AND ab2.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " \n");
		sb.append(" 	 AND ab2.internal_portfolio in (" + EnumsPortfolios.MX_PORTFOLIO_SB1.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB2.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB3.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SB4.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SBP.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SCP.toInt() + ","
													 + EnumsPortfolios.MX_PORTFOLIO_SLP.toInt() + ")\n");
		sb.append("		 AND ins.id_number = ab2.ins_type \n");
		sb.append("		 AND ins.id_number = ab.ins_type \n");
		sb.append("		 AND pa.ins_num = ab2.ins_num \n");
		sb.append("		 AND pa.ins_num = ab.ins_num \n");
		sb.append("		 AND pa.param_seq_num = 0 \n");
		sb.append("		 AND ((ab2.currency not in (" + EnumsCurrency.MX_CURRENCY_MXN.toInt() + "," + EnumsCurrency.MX_CURRENCY_UDI.toInt() + ")) or (ab2.ins_type = " + EnumsInstrumentsMX.MX_FX_FUT.toInt() + ")) \n");
		sb.append("		) WHERE CA_TYPE > 0 ORDER BY exp_defn_id,deal_num");


		_Log.printMsg(EnumTypeMessage.DEBUG,"\n SQL : "+sb.toString());
		
		try {
			
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
			
			if(iRet != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt() || tReturn.getNumRows() < 1){
				_Log.printMsg(EnumTypeMessage.ERROR,"No se pudo obtener las operaciones");
			}
		
		} catch (OException exception){
			
			_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
			_Log.printMsg(EnumTypeMessage.ERROR,"Exception: \n "+ exception.getMessage());
			tReturn.destroy();
			Util.exitFail();
		}
		
		return tReturn;
	}
	
	private Table getNav() throws OException
	{
		int iToday = OCalendar.today();
		Table tTotalNav = Table.tableNew();
		int iYesterday = OCalendar.getLgbd(iToday);
		tTotalNav = _utilafore.getAllTotalNAV(iToday);
		
		if (tTotalNav.getNumRows()==0)
			tTotalNav = _utilafore.getAllTotalNAV(iYesterday);
	
		return tTotalNav;
	}

}
