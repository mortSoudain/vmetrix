package com.afore.fmwk_webservice_factory;


import com.afore.enums.EnumsAforeWebService;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Borhis;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Fix_Rates;
/*import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Bursatilidad;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_CargaFixing;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_CreditRatings;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Curvas;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Gentec;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_Nav;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_PrecioLimpio;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_ProbIncumplimiento;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_SeveridadPerdida;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_VoBo_Validacion;*/
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_PrecioSucio;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_InstrumentScenario;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_InstrumentScenarioStress;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_TasaRendimiento;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_VectorValmer_TE;
import com.afore.fmwk_webservice_imp.FMWK_Implement_WebService_VectorValmer_TE_Hist;
import com.afore.fmwk_webservice_interfaces.FMWK_Interfaces_WebService;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.OException;
import com.olf.openjvs.Util;

public class FMWK_Factory_WebService {

	private static FMWK_Factory_WebService factoryInstace = null;

	
	public static FMWK_Factory_WebService getFactory(){
		if(factoryInstace== null)
			factoryInstace = new FMWK_Factory_WebService();
		return factoryInstace;
	}
	
	public FMWK_Interfaces_WebService getFactory_WebService (int Request) throws OException {	

		if (Request == EnumsAforeWebService.MX_WS_PRECIOS.value())
			return new FMWK_Implement_WebService_PrecioSucio(Request);
		else if (Request == EnumsAforeWebService.MX_WS_ESCENARIOS.value())
			return new FMWK_Implement_WebService_InstrumentScenario(Request);
		else if (Request == EnumsAforeWebService.MX_WS_ESCENARIOS_FIJOS.value())
			return new FMWK_Implement_WebService_InstrumentScenarioStress(Request);
		else if (Request == EnumsAforeWebService.MX_WS_TASA_RENDIMIENTO.value())
			return new FMWK_Implement_WebService_TasaRendimiento(Request);
		else if (Request == EnumsAforeWebService.MX_WS_LOAD_VECTOR_VALMER_TE.value())
			return new FMWK_Implement_WebService_VectorValmer_TE(Request);
		else if (Request == EnumsAforeWebService.MX_WS_LOAD_VECTOR_VALMER_TE_HIST.value())
			return new FMWK_Implement_WebService_VectorValmer_TE_Hist(Request);
		else if (Request == EnumsAforeWebService.MX_WS_PRECIOS_SIGUIENTE_DIA.value())
			return new FMWK_Implement_WebService_PrecioSucio(Request);
		else if (Request == EnumsAforeWebService.MX_WS_FIX_RATES.value())
			return new FMWK_Implement_WebService_Fix_Rates(Request);
		else if (Request == EnumsAforeWebService.MX_WS_BORHIS.value())
			return new FMWK_Implement_WebService_Borhis(Request);
		else{
			String sErrorMessage = 	"No se puede crear el Factory";		
			Util.exitFail(sErrorMessage);
			return null;
		}

	}
	
	public FMWK_Interfaces_WebService getFactory_WebService (int Request, String Ticker) throws OException {	
		if (Request == EnumsAforeWebService.MX_WS_ESCENARIOS.value())
			return new FMWK_Implement_WebService_InstrumentScenario(Request,Ticker);
		else if (Request == EnumsAforeWebService.MX_WS_ESCENARIOS_FIJOS.value())
			return new FMWK_Implement_WebService_InstrumentScenarioStress(Request,Ticker);
		/*else if (Request == EnumAforeWebService.MX_WS_PROB_INCUMPLIMIENTO.value())
			return new FMWK_Implement_WebService_ProbIncumplimiento(Request,Ticker);
		else if (Request == EnumAforeWebService.MX_WS_SEVERIDAD_PERDIDA.value())
			return new FMWK_Implement_WebService_SeveridadPerdida(Request,Ticker);*/
		else {
			String sErrorMessage = 	"No se puede crear el Factory (" + Ticker + ")";		
			Util.exitFail(sErrorMessage);
			return null;	
		}

	}
	
	public FMWK_Interfaces_WebService getFactory_WebService (int Request, UTIL_Log log) throws OException {	
		/*if (Request == EnumAforeWebService.MX_WS_NAV.value())
			return new FMWK_Implement_WebService_Nav(Request,log);
		else if (Request == EnumAforeWebService.MX_WS_FIXING.value()){
			Request = EnumAforeWebService.MX_WS_PRECIOS.value();
			return new FMWK_Implement_WebService_CargaFixing(EnumAforeWebService.MX_WS_PRECIOS.value(),log);
		}else {
			String sErrorMessage = 	"No se puede crear el Factory (" + log.getClass().getSimpleName() + ")";		
			Util.exitFail(sErrorMessage);
		}*/
		return null;	
	}
	
	public FMWK_Interfaces_WebService getFactory_WebService (int request,  UTIL_Log log, int operacion) throws OException {
		/*if( request == EnumAforeWebService.MX_WS_EOFDAY.value() )
			return new FMWK_Implement_WebService_VoBo_Validacion(request, log, operacion);
		else {
			String sErrorMessage = 	"No se puede crear el Factory (" + log.getClass().getSimpleName() + ")";		
			Util.exitFail(sErrorMessage);
		}*/
		return null;
	} 
	
}
