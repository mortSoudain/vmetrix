package com.afore.fmwk_webservice_interfaces;

import com.olf.openjvs.OException;

public interface FMWK_Interfaces_WebService {
	
	public void importDataWS() throws OException;
	public void getRequest()throws OException;

}
