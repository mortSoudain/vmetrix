package com.afore.fmwk_webservice_imp;

import cl.vmetrix.profuturo.viewlayer.WSBorhisClient;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserTables;
import com.afore.fmwk_webservice_abstract.FMWK_Abstract_WebService;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Implement_WebService_Borhis extends FMWK_Abstract_WebService{
	
	// Basic util variables	
		private String		sScriptName		=	this.getClass().getSimpleName();
	
	// Script Global Variables
		private static final String CABECERAXML			=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		private static final String ETIQUETA_CABECERA	=	"<response>";
		private static final String ETIQUETA_PIE_PAG	=	"</response>";
        private WSBorhisClient client 					=	null;         
		private String sUrl						=	"";
		
	// User
		private int			iUserId			=	0;
		
	// Time variables
		private int 		iToday			=	0;
		private String		sTodayDefault	=	"";
		private ODateTime	oToday			=	null;

	public FMWK_Implement_WebService_Borhis(int Request)
			throws OException {
		super(Request);
	}
	
	@Override
	public void importDataWS(){
		
    	LOG.markStartScript();
    	LOG.setDEBUG_STATUS(EnumStatus.ON);
    	
    		initializeScript();
    		
    		// Initialize Web Service
				try{
					client = new WSBorhisClient( sUrl );
					LOG.printMsg(EnumTypeMessage.INFO, "Web Service Instance Created Successfully");
				} catch (Throwable e) {
					LOG.printMsg(EnumTypeMessage.ERROR, "Unable Create WS Instance, please check jar file  ::: " + e.getMessage());
					Util.exitFail();
				}
			// Update Internal Rating For Borhis
				updateInternalRating();
			
			// Update Borhis Matriz Transicion
				updateBorhisMatriz();
			
    	LOG.markEndScript();
	}
	
	private void updateBorhisMatriz() {
				
		//Execute WebService Function
			String sXMLResponse = "";
			
			try {
				sXMLResponse = client.getMatrizTransicion();
			} catch (Exception e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to get response from function getMatrizTransicion, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// DEBUG:
			LOG.printMsg(EnumTypeMessage.DEBUG, "XML Webservice Reponse Matriz:" +sXMLResponse);
			
		// Format XML Reponse
			sXMLResponse = CABECERAXML + ETIQUETA_CABECERA + sXMLResponse + ETIQUETA_PIE_PAG;
		
		// Get values of XML into table
			Table tResponse = Util.NULL_TABLE;
			
			try {
				tResponse = Table.xmlStringToTable(sXMLResponse);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to parse XML reponse to Table tResponse, on updateBorhisMatriz function. Error: " + e.getMessage());
			}
			
		// Get real Table with values and apply format
			Table tResults = Util.NULL_TABLE;

			try {
				Table tAux = tResponse.getTable("results", 1);
				Table tAux2 = tAux.getTable("ticker", 1);
				tResults = Table.tableNew();
				tResults.select(tAux2, "tickerName,estadoIni,estado0,estado1,estado2,estado3,estado4", "tickerName GT 0");
				tResults.setColName("tickerName", "ticker");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set new format to Table tResponse, on updateBorhisMatriz function. Error: " + e.getMessage());
			}
			
		// Add new cols
			try {
				tResults.addCol("estado_ini", COL_TYPE_ENUM.COL_INT);
				tResults.addCol("estado_0", COL_TYPE_ENUM.COL_DOUBLE);
				tResults.addCol("estado_1", COL_TYPE_ENUM.COL_DOUBLE);
				tResults.addCol("estado_2", COL_TYPE_ENUM.COL_DOUBLE);
				tResults.addCol("estado_3", COL_TYPE_ENUM.COL_DOUBLE);
				tResults.addCol("estado_4", COL_TYPE_ENUM.COL_DOUBLE);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to add new cols on Table tResults, on updateBorhisMatriz function. Error: " + e.getMessage());
			}
			
		// Set converted values
			try {
				for(int i=1; i<=tResults.getNumRows();i++){
					
					String sEstadoIni = tResults.getString("estadoIni", i);
					String sEstado0 = tResults.getString("estado0", i);
					String sEstado1 = tResults.getString("estado1", i);
					String sEstado2 = tResults.getString("estado2", i);
					String sEstado3 = tResults.getString("estado3", i);
					String sEstado4 = tResults.getString("estado4", i);
					
					int iEstadoIni = 0;
					double dEstado0 = 0.0;
					double dEstado1 = 0.0;
					double dEstado2 = 0.0;
					double dEstado3 = 0.0;
					double dEstado4 = 0.0;
					
					if (sEstadoIni != null && sEstadoIni.length() > 0) {
						iEstadoIni	=	Integer.valueOf(sEstadoIni);
					}
					
					if (sEstado0 != null && sEstado0.length() > 0) {
						dEstado0	=	Double.valueOf(sEstado0);
					}
					
					if (sEstado1 != null && sEstado1.length() > 0) {
						dEstado1	=	Double.valueOf(sEstado1);
					}
					
					if (sEstado2 != null && sEstado2.length() > 0) {
						dEstado2	=	Double.valueOf(sEstado2);
					}
					
					if (sEstado3 != null && sEstado3.length() > 0) {
						dEstado3	=	Double.valueOf(sEstado3);
					}
					
					if (sEstado4 != null && sEstado4.length() > 0) {
						dEstado4	=	Double.valueOf(sEstado4);
					}
					
					tResults.setInt("estado_ini", i, iEstadoIni);
					tResults.setDouble("estado_0", i, dEstado0);
					tResults.setDouble("estado_0", i, dEstado0);
					tResults.setDouble("estado_1", i, dEstado1);
					tResults.setDouble("estado_2", i, dEstado2);
					tResults.setDouble("estado_3", i, dEstado3);
					tResults.setDouble("estado_4", i, dEstado4);					
					
					}
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set new converted values on Table tResults, on updateBorhisMatriz function. Error: " + e.getMessage());
			}
			
			// Get Party Id and Short Name
				tResults = getPartyIdFromTicker(tResults);
				
			// Get values on table with name of historical
				Table tBorhisMatriz = Util.NULL_TABLE;
				
				try {
					tBorhisMatriz = Table.tableNew("user_mx_borhis_matriz");
					tBorhisMatriz.select(tResults, "party_id, ticker, estado_ini, estado_0, estado_1, estado_2, estado_3, estado_4", "party_id GT 0");
				} catch (OException e) {
					LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load historical values on Table tBorhisMatriz, on updateBorhisMatriz function. Error: " + e.getMessage());
				}
				
			// Clear and push new values
				updateUsertable(tBorhisMatriz);	
		
	}

	private void updateInternalRating() {
		
		//Execute WebService Function
			String sXMLResponse = "";
			try {
				sXMLResponse = client.getBorhis();
			} catch (Exception e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to get response from function getBorhis, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// DEBUG:
			LOG.printMsg(EnumTypeMessage.DEBUG, "XML Webservice Reponse Borhis:" +sXMLResponse);
		
		// Format XML Reponse
			sXMLResponse = CABECERAXML + ETIQUETA_CABECERA + sXMLResponse + ETIQUETA_PIE_PAG;
		
		// Get values of XML into table
			Table tResponse = Util.NULL_TABLE;
			
			try {
				tResponse = Table.xmlStringToTable(sXMLResponse);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to parse XML reponse to Table tResponse, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// Get real Table with values and apply format
			Table tResults = Util.NULL_TABLE;

			try {
				Table tAux = tResponse.getTable("results", 1);
				Table tAux2 = tAux.getTable("ticker", 1);
				tResults = Table.tableNew();
				tResults.select(tAux2, "pd,tickerName", "tickerName GT 0");
				tResults.setColName("tickerName", "ticker");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set new format to Table tResponse, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// Convert col default_prob string to double
			try {
				tResults.addCol("default_prob", COL_TYPE_ENUM.COL_DOUBLE);
				for (int i=1; i<=tResults.getNumRows();i++){
					String sDefaultProb = tResults.getString("pd", i);
					if (sDefaultProb != null && sDefaultProb.length() > 0) {
						Double dDefaultProb = Double.valueOf(sDefaultProb);
						tResults.setDouble("default_prob", i, dDefaultProb *100);
					} else {
						tResults.setDouble("default_prob", i, 0.0);
					}
				}
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to parse some of the string values to double (pd to default_prob) on Table tResults, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// Add fecha and personnel id cols
			try {
				tResults.addCol("fecha", COL_TYPE_ENUM.COL_DATE_TIME);
				tResults.addCol("personnel_id", COL_TYPE_ENUM.COL_INT);
				tResults.setColValDateTime("fecha", oToday);
				tResults.setColValInt("personnel_id", iUserId);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to add cols fecha and personnel_id on Table tResults, on updateInternalRating function. Error: " + e.getMessage());
			}
			
		// Get party_id and short_name from ticker
			tResults = getPartyIdFromTicker(tResults);
			
		// Get internal rating from default_prob
			tResults = getInternalRatingFromPD(tResults);
			
		// Load usertable user_mx_internal_rating
			Table tHistoricalValues = getHistoricalValues();
		
		// Paste borhis values into internal rating
			tHistoricalValues = pasteNewAndUpdateValues(tResults,tHistoricalValues);
		
		// Push the user table into db
			updateUsertable(tHistoricalValues);
		
	}

	private Table getHistoricalValues() {
		
		String	sUserMxInternalRating	=	EnumsUserTables.USER_MX_INTERNAL_RATING.toString();		
		Table	tHistoricalValues		=	Util.NULL_TABLE;
		
		try {
			tHistoricalValues = Table.tableNew(sUserMxInternalRating);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create Table tHistoricalValues, on getHistoricalValues function. Error: " + e.getMessage());
		}
		String	sQuery = "select party_id, short_name, internal_rating, default_prob, score, fecha, market_cap, total_debt, personnel_id from "+sUserMxInternalRating;
		
		try {
			DBaseTable.execISql(tHistoricalValues, sQuery);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load query into Table tHistoricalValues, on getHistoricalValues function. Error: " + e.getMessage());
		}
		
		return tHistoricalValues;
	}

	private Table getInternalRatingFromPD(Table tResults){
		
		Table	tInternalRating				=	Util.NULL_TABLE;
		String	sUserMxScoreCalificacion	=	EnumsUserTables.USER_MX_SCORE_CALIFICACION.toString();// user_mx_score_calificacion
		
		try {
			tInternalRating = Table.tableNew();
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create Table tInternalRating, on getInternalRatingFromPD function. Error: " + e.getMessage());
		}
		
		// Query
			StringBuilder sb = new StringBuilder();
			sb.append("\n select ");
			sb.append("\n 	c.calif_global as internal_rating ");
			sb.append("\n 	,c.cota_min ");
			sb.append("\n 	,c.cota_max ");
			sb.append("\n from "+ sUserMxScoreCalificacion +" c ");
			sb.append("\n where ");
			sb.append("\n 	c.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
		
		try {
			DBaseTable.execISql(tInternalRating, sb.toString());
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load query into Table tInternalRating, on getInternalRatingFromPD function. Error: " + e.getMessage());
		}
		
		try {
			tResults.select(tInternalRating, "internal_rating", "cota_min LE $default_prob AND cota_max GT $default_prob");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to paste info from Table tInternalRating into Table tResults, on getInternalRatingFromPD function. Error: " + e.getMessage());
		}
		
		return tResults;
	}

	private Table getPartyIdFromTicker(Table tResults){
		
		Table	tParty					=	Util.NULL_TABLE;
		Table	tResults2				=	Util.NULL_TABLE;
		int		iTranStatusValidated	=	TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt();//3
		int		iTranTypeHolding		=	TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt();//2
		String	sUserMxScoreParty		=	EnumsUserTables.USER_MX_SCORE_PARTY.toString();// user_mx_score_party
		
		try {
			tParty = Table.tableNew();
			tResults2 = Table.tableNew();
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create Table tParty, on getPartyIdFromTicker function. Error: " + e.getMessage());
		}
		
		//Query
			StringBuilder sb = new StringBuilder();
			sb.append("\n select ");
			sb.append("\n 	sp.party_id ");
			sb.append("\n 	,p.short_name ");
			sb.append("\n 	,h.ticker ");
			sb.append("\n from "+ sUserMxScoreParty +" sp ");
			sb.append("\n 	inner join party p ");
			sb.append("\n 		on p.party_id = sp.party_id ");
			sb.append("\n 	inner join ab_tran ab ");
			sb.append("\n 		on ab.external_lentity = p.party_id ");
			sb.append("\n 	inner join header h ");
			sb.append("\n 		on h.ins_num = ab.ins_num ");
			sb.append("\n where ");
			sb.append("\n 	ab.tran_status = "+ iTranStatusValidated +" -- Validated ");
			sb.append("\n 	and ab.tran_type = "+ iTranTypeHolding +" -- Holding ");
			sb.append("\n 	and sp.category_id = 3 -- Borhis ");
			sb.append("\n 	and sp.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
			
		try {
			DBaseTable.execISql(tParty, sb.toString());
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to load query into Table tParty, on getPartyIdFromTicker function. Error: " + e.getMessage());
		}
		
		try {
			tResults.select(tParty, "party_id, short_name", "ticker EQ $ticker");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to paste info from Table tParty into Table tResults, on getPartyIdFromTicker function. Error: " + e.getMessage());
		}		

		try {
			tResults2.select(tResults, "*", "party_id GT 0");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to paste info from Table tParty into Table tResults, on getPartyIdFromTicker function. Error: " + e.getMessage());
		}
		
		return tResults2;
	}
	
	private void updateUsertable(Table tUserTable){
		
		String	sUserTableName	=	"";
		
		// Get UserTable Name
			try {
				sUserTableName = tUserTable.getTableName();
			} catch (OException e) {
				LOG.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to get UserTable name, on updateUsertable function. Error: "
								+ e.getMessage());
			}
		
		// Clear previous version of UserTable on DB
			try {
				DBUserTable.clear(tUserTable);
			} catch (OException e) {
				LOG.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to clear UserTable "+sUserTableName+", on updateUsertable function. Error: "
								+ e.getMessage());
			}
		
		// Push memory table to DB 
			try {
				DBUserTable.bcpIn(tUserTable);
			} catch (OException e) {
				LOG.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to push new values of UserTable "+sUserTableName+", on updateUsertable function. Error: "
								+ e.getMessage());
			}
	}

	private Table pasteNewAndUpdateValues(Table tResults, Table tHistoricalValues){
		
		// Add cols 'update' to control if there is a new or a updated values
			try {
				tResults.addCol("update", COL_TYPE_ENUM.COL_INT);
				tHistoricalValues.addCol("update", COL_TYPE_ENUM.COL_INT);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to create cols 'update' on Table tResults or tHistoricalValues, on pasteNewAndUpdateValues function. Error: "
								+ e.getMessage());
			}
		
		// Select updated values
			try {
				tHistoricalValues.select(tResults, "party_id, short_name, internal_rating, default_prob, fecha, personnel_id", "party_id EQ $party_id AND fecha EQ $fecha");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to paste updated values from tResults to tHistoricalValues, on pasteNewAndUpdateValues function. Error: "
								+ e.getMessage());
			}
		
		// Set updated (Identify)
			try {
				tHistoricalValues.setColValInt("update", 1);
				tResults.select(tHistoricalValues, "update", "party_id EQ $party_id AND fecha EQ $fecha");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to set (Identify) updated values on Tables tResults or tHistoricalValues, on pasteNewAndUpdateValues function. Error: "
								+ e.getMessage());
			}
		
		// Select new values
			try {
				tHistoricalValues.select(tResults, "party_id, short_name, internal_rating, default_prob, fecha, personnel_id", "update EQ 0");
				tHistoricalValues.delCol("update");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR,
						"Unable to select new values from Table tResults to tHistoricalValues, on pasteNewAndUpdateValues function. Error: "
								+ e.getMessage());
			}
		
		return tHistoricalValues;
	}

	private void initializeScript() {
		try {
			sUrl			=	UTILGLOBAL.getVariableGlobal(SISTEMA_CONF_VAR,sScriptName, "url");
			
			iToday			=	OCalendar.today();
			sTodayDefault	=	OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DMLY_NOSLASH);
			oToday			=	ODateTime.strToDateTime(sTodayDefault);
			
			Table tInfo		=	Ref.getInfo();
			iUserId			=	tInfo.getInt("submitter_user_id", 1);
			if(iUserId==0){
				iUserId		=	Ref.getUserId();
			}
				
		} catch (OException e) {
			LOG.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeScript function. Error: "
							+ e.getMessage());
		}
	}
	
}