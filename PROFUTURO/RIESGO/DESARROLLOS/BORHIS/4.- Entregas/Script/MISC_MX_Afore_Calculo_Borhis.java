package com.afore.workflow;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsAforeWebService;
import com.afore.fmwk_webservice_getter.FMWK_Getter_WebService;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;
import com.olf.openjvs.Util;

public class MISC_MX_Afore_Calculo_Borhis implements IScript {
	
	// Basic util variables
		private String		sScriptName		=	this.getClass().getSimpleName();
		private UTIL_Log	_Log			=	new UTIL_Log(sScriptName);

	@Override
	public void execute(IContainerContext context) {
		
		_Log.markStartScript();
		
			try
			{
				FMWK_Getter_WebService executeWS = new FMWK_Getter_WebService();
				_Log.printMsg(EnumTypeMessage.INFO, "Obteniendo Calculo Borhis...");
				executeWS.getDataWS(EnumsAforeWebService.MX_WS_BORHIS.value());
			}
			catch(OException e){
				_Log.printMsg(EnumTypeMessage.ERROR, "OException: "+ e.getMessage());
				Util.exitFail();
			}finally{
				_Log.markEndScript();
			}
		
	}

}
