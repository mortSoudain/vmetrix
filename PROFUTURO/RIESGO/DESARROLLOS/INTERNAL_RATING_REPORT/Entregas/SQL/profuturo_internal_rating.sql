with variables as
(
	select
		TO_DATE('1900/01/01', 'YYYY/MM/DD') as fecha_nula
	from dual 
),
maxdates as (
	select
		max(ir.fecha) fecha
		,ir.party_id
	from user_mx_internal_rating ir
	group by ir.party_id
)
select
	ir.fecha
	,ir.party_id
	,ir.short_name
	,sc.category_name
	,ir.internal_rating
	,ir.score
	,ir.default_prob
	,ir.market_cap
	,ir.total_debt
from user_mx_internal_rating ir
	inner join maxdates max
		on max.party_id = ir.party_id
		and max.fecha = ir.fecha
	inner join user_mx_score_party sp 
		on sp.party_id = ir.party_id
	inner join user_mx_score_category sc
		on sc.category_id = sp.category_id
	inner join variables v
		on 1 = 1
where sp.fecha_hasta = v.fecha_nula