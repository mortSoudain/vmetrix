/*$Header: v 1.0, 31/May/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DRSK_BBG_Request.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script creado para la obtencion de metricas para riesgo,
 *							desde el servicio de bloomberg.
 *
 *							Se realiza una consulta al servicio de bloomberg,
 *							entregando los tickers asociados a las emisoras/contrapartes y
 *							las métricas a consultar.
 *
 * REVISION HISTORY
 * Date:					11/Jun/2018
 * Version/Autor:			Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				Se incluyen las columnas de market_cap y total debt
 *							Asociadas a tickers bloomberg:
 *							- CUR_MKT_CAP
 *							- BS_ST_DEBT
 *							- BS_LONG_TERM_BORROWINGS
 *                         
 ************************************************************************************/
package com.profuturo.ws.bloomberg;

import java.util.ArrayList;
import java.util.List;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.bloomberg.vm.services.datalicense.dlws.submit.RequestBBG;
import com.bloomberg.vm.services.datalicense.dlws.submit.WsCallBBGDL;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;

public class MISC_Afore_DRSK_BBG_Request implements IScript {
	
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	String sScriptName				=	this.getClass().getSimpleName();	   
	UTIL_Log _Log					=	new UTIL_Log(sScriptName);

	private String PROPERTIES_FILEPATH = "";
	private String PROPERTIES_FILENAME = "WSBloombergClient_DRSK.properties";

		@Override
		public void execute(IContainerContext context) {
			
			_Log.setDEBUG_STATUS(EnumStatus.ON);
			_Log.markStartScript();

			// Initialize variables
				initializeGlobalVariables();
			
			// Preparing request
				List<String> listFields			=	getFieldsList();
				List<String> listInstruments	=	getInstrumentsList();
					
			// Create Request
				String sRes = sendRequest(listFields,listInstruments);
				
			// Save Response Id on UserTable user_mx_score_category
				saveResponseId(sRes);

			_Log.markEndScript();
		}
		
		private void initializeGlobalVariables() {
			try {
				PROPERTIES_FILEPATH = UtilAfore.getVariableGlobal("FINDUR", "BLOOMBERG_DL", "properties_path");
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to set some of the global variables on initializeGlobalVariables function. Error :"
						+ e.getMessage());
			}
		}

		private void saveResponseId(String sRes) {
			
			String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
			Table tUserTable = Util.NULL_TABLE;
			
			// Load usertable
				try {
					tUserTable = Table.tableNew(sUserMxScoreCategory);
					StringBuilder sb2 = new StringBuilder ();
					sb2.append("\n select * from " +sUserMxScoreCategory);
					DBaseTable.execISql(tUserTable, sb2.toString());
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to load usertable user_mx_score_category on saveResponseId function. Error :"
							+ e.getMessage());
				}
				
			// Set Response Id
				try {
					int iFindedRow = tUserTable.unsortedFindInt("category_id", 4);
					tUserTable.setString("response_id", iFindedRow, sRes);					
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to set Response Id on usertable user_mx_score_category, on saveResponseId function. Error :"
							+ e.getMessage());
				}
			
			// Push Changes on UserTable into DB
				try {
					DBUserTable.clear(tUserTable);
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable clear previous values of usertable user_mx_score_category on DB, on saveResponseId function. Error :"
							+ e.getMessage());
				}
				
				try {
					DBUserTable.bcpIn(tUserTable);
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to push new values of usertable user_mx_score_category on DB, on saveResponseId function. Error :"
							+ e.getMessage());
				}
			
		}

		private String sendRequest(List<String> listFields, List<String> listInstruments) {
			
			String sRes = "";
		
			// Creating request and client objects
				RequestBBG request = new RequestBBG("BDP", listFields, listInstruments);
				WsCallBBGDL client = new WsCallBBGDL();
			
			// Execute request and get Response Id		
				try {
					sRes = client.callWSBBGDL_Request(request, PROPERTIES_FILEPATH,PROPERTIES_FILENAME);
				} catch (Exception e) {
					_Log.printMsg(EnumTypeMessage.ERROR, "Unable to make the request. Error : " + e.getMessage());
				} finally{
					_Log.printMsg(EnumTypeMessage.INFO, "Response Id: " + sRes);
				}
			
			return sRes;
		}

		private List<String> getInstrumentsList() {
			
			String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();
			Table tListTickers = Util.NULL_TABLE;
			List<String> listInstruments = new ArrayList<String>();
			
			// Get list of Instruments (Tickers)
				try {
					tListTickers = Table.tableNew("List Tickers");
					StringBuilder sb = new StringBuilder ();
					sb.append("\n select distinct");
					sb.append("\n 	up.ticker_bbg ");
					sb.append("\n from "+sUserMxScoreParty+" up ");
					sb.append("\n where up.category_id = 4 -- Bloomberg ");
					sb.append("\n and up.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
					DBaseTable.execISql(tListTickers, sb.toString());
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to load user_mx_score_party table on getInstrumentsList function. Error :"
							+ e.getMessage());
				}
			
			// Setting Instruments from Table to List
				try {
					
					for (int i=1; i<= tListTickers.getNumRows(); i++){
						String sTicker = tListTickers.getString("ticker_bbg", i);
						listInstruments.add(sTicker);
					}
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to get instruments from tListTickers table on getInstrumentsList function. Error :"
							+ e.getMessage());
				}
				
			_Log.printMsg(EnumTypeMessage.DEBUG, "List of Instruments : " + listInstruments.toString());
				
			return listInstruments;
			
		}

		private List<String> getFieldsList() {
			
			List<String> listFields = new ArrayList<String>();
			
			// Add fields
				
				// Internal Rating
				listFields.add("RSK_BB_ISSUER_DEFAULT");
				
				// Probabilidad Default
				listFields.add("BB_1YR_DEFAULT_PROB");
				
				// Market Cap
				listFields.add("CUR_MKT_CAP");
				
				// Total Debt
				listFields.add("BS_ST_DEBT");
				listFields.add("BS_LONG_TERM_BORROWINGS");
			
			_Log.printMsg(EnumTypeMessage.DEBUG, "List of Fields : " + listFields.toString());

			return listFields;
			
		}
}