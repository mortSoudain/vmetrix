/*$Header: v 1.2, 22/Aug/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DRSK_BBG_Response.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script creado para la obtencion de metricas para riesgo,
 *							desde el servicio de bloomberg.
 *
 *							Se realiza obtiene el numero de consulta almacenado por el script de request
 *							y se obtienen los datos que entrega el servicio a partir de este numero de consulta.
 *
 *							Los datos son almacenados en una usertable del sistema.
 *
 * REVISION HISTORY
 * Date:					11/Jun/2018
 * Version/Autor:			Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				Se incluyen las columnas de market_cap y total debt
 *							Asociadas a tickers bloomberg:
 *							- CUR_MKT_CAP
 *							- BS_ST_DEBT
 *							- BS_LONG_TERM_BORROWINGS
 *
 * Date:					19/Jun/2018
 * Version/Autor:			Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				Se incluye el manejo para la linea de auditoria que almacena
 *							el usuario que corre el workflow en columna personnel_id
 *
 * Date:					21/Aug/2018
 * Version/Autor:			1.1 - Daniel Inostroza A - VMetrix SpA
 * Description:				Se incluye validacion de valores (N.A y N.S) para el field BB_1YR_DEFAULT_PROB
 *
 * Date:					22/Aug/2018
 * Version/Autor:			1.2- Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				Se multiplica el campo default_prob por 100 para visualizacion porcentual.
 *                         
 ************************************************************************************/
package com.profuturo.ws.bloomberg;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.bloomberg.vm.services.datalicense.dlws.response.DetailRetrieveResponseBBG;
import com.bloomberg.vm.services.datalicense.dlws.response.InstrumentResponseBBG;
import com.bloomberg.vm.services.datalicense.dlws.response.ResponseBBG;
import com.bloomberg.vm.services.datalicense.dlws.submit.WsCallBBGDL;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.Workflow;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;

public class MISC_Afore_DRSK_BBG_Response extends Thread implements IScript {
	
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	String sScriptName				=	this.getClass().getSimpleName();	   
	UTIL_Log _Log					=	new UTIL_Log(sScriptName);
	
	private String PROPERTIES_FILEPATH = "";
	private String PROPERTIES_FILENAME = "WSBloombergClient_DRSK.properties";
	
	private int			iUserId			=	0;
	
	private int iToday		=	0;
	private String sToday	=	"";
	
	@Override
	public void execute(IContainerContext context) {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		_Log.markStartScript();
		    
    	// Initialize Global Varibales
			initializeGlobalVariables();
		
		// Get Reponse Id from UserTable	
			String sResponseId = getResponseIdFromUserTable();
			
		// Get response data from response id
			Table tData = getResponseDataFromResponseId(sResponseId);
			
		// If data was getted, tData have the registers
		// So we can, and we have to erase response_id from user_mx_score_category
			deleteResponseIdFromUserTable(sResponseId);

		// Setting final format for table (user_mx_internal_rating like)
			tData = formatTable(tData);

		// Load historical usertable, and paste new values
			Table tUserMxInternalRating = loadHistoricalAndUpdateValues(tData);
			
		// Clear previuos values on usertable, and push new ones
			clearPreviousAndPushNew(tUserMxInternalRating);
			
		_Log.markEndScript();

	}

	private void deleteResponseIdFromUserTable(String sResponseId) {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
		
		Table tUserMxScoreCategory = Util.NULL_TABLE;
		int iFindedRow = -1;

		try {
			tUserMxScoreCategory = Table.tableNew(sUserMxScoreCategory);
			String sQueryUserMxScoreCategory = "select * from " + sUserMxScoreCategory;
			DBaseTable.execISql(tUserMxScoreCategory, sQueryUserMxScoreCategory);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to load UserTable user_mx_score_category on deleteResponseIdFromUserTable function. Error :"
					+ e.getMessage());
		}
		
		try {
			iFindedRow = tUserMxScoreCategory.unsortedFindString("response_id", sResponseId, SEARCH_CASE_ENUM.CASE_INSENSITIVE);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to find stored reponse_id on deleteResponseIdFromUserTable function. Error :"
					+ e.getMessage());
		}
		
		try {
			tUserMxScoreCategory.setString("response_id", iFindedRow, "");
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set empty string on reponse_id column on deleteResponseIdFromUserTable function. Error :"
					+ e.getMessage());
		}
		
		// Clear previous and push new values
		clearPreviousAndPushNew(tUserMxScoreCategory);
		
	}

	private void initializeGlobalVariables() {
		
		Table tWorkflow = Util.NULL_TABLE;
		int iRowRunningJob = 0;
		
		try {
			tWorkflow = Workflow.getExecutionLog("15.- MISC - DRSK Bloomberg Response", OCalendar.today(), OCalendar.today());
			iRowRunningJob = tWorkflow.unsortedFindString("status_txt", "Executing", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to load tWorkflow table on initializeGlobalVariables function. Error :"
					+ e.getMessage());
		}
		
		try {
			iToday = OCalendar.today();
			sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH); 
			PROPERTIES_FILEPATH = UtilAfore.getVariableGlobal("FINDUR", "BLOOMBERG_DL", "properties_path");
			iUserId = tWorkflow.getInt("submitter", iRowRunningJob);	
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set some of the global variables on initializeGlobalVariables function. Error :"
					+ e.getMessage());
		}
	}

	private void clearPreviousAndPushNew(Table tUserMxInternalRating) {
		
		String sTableName = "";
		
		try {
			sTableName = tUserMxInternalRating.getTableName();
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable get table name, on clearPreviousAndPushNew function. Error :"
					+ e.getMessage());
		}
		
		try {
			DBUserTable.clear(tUserMxInternalRating);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to clear historical data for "+sTableName+", on clearPreviousAndPushNew function. Error :"
					+ e.getMessage());
		}
		
		try {
			DBUserTable.bcpIn(tUserMxInternalRating);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to push new data for "+sTableName+", on clearPreviousAndPushNew function. Error :"
					+ e.getMessage());
		}

	}

	private Table loadHistoricalAndUpdateValues(Table tData) {
		
		String sUserMxInternalRating = EnumsUserTables.USER_MX_INTERNAL_RATING.toString();
		Table tUserMxInternalRating = Util.NULL_TABLE;
		
		// Load Usertable internal rating, and paste new values
			
			try {
				tUserMxInternalRating = Table.tableNew(sUserMxInternalRating);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to create tUserMxInternalRating Table, on loadHistoricalAndUpdateValues function. Error :"
						+ e.getMessage());
			}
			
			String sQuery = "select * from " +sUserMxInternalRating;
			
			try {
				DBaseTable.execISql(tUserMxInternalRating, sQuery);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to load data on tUserMxInternalRating Table, on loadHistoricalAndUpdateValues function. Error :"
						+ e.getMessage());
			}
		
		// Update new values
			try {
				for (int i=1; i<=tUserMxInternalRating.getNumRows(); i++){
					int iParty = tUserMxInternalRating.getInt("party_id", i);
					int iFecha = tUserMxInternalRating.getDate("fecha", i);
					
					for (int j=1; j<=tData.getNumRows();j++){
						int jParty = tData.getInt("party_id", j);
						int jFecha = tData.getDate("fecha", j);
						
						if(iParty == jParty && iFecha == jFecha){
							tData.setInt("updated", j, 1);
							tUserMxInternalRating.setString("internal_rating", i, tData.getString("internal_rating", j));
							tUserMxInternalRating.setDouble("score", i, 0.0);
							tUserMxInternalRating.setDouble("default_prob", i, tData.getDouble("default_prob", j));
							tUserMxInternalRating.setDouble("market_cap", i, tData.getDouble("market_cap", j));
							tUserMxInternalRating.setDouble("total_debt", i, tData.getDouble("total_debt", j));
							tUserMxInternalRating.setInt("personnel_id", i, tData.getInt("personnel_id", j));
						} 
					}
				}
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to update some of the values on tUserMxInternalRating Table, on loadHistoricalAndUpdateValues function. Error :"
						+ e.getMessage());
			}
		
		// Insert values not updated
			try {
				tUserMxInternalRating.select(tData, "party_id, short_name, internal_rating, score, default_prob, market_cap, total_debt, fecha, personnel_id", "updated EQ 0");
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Unable to add new values on tUserMxInternalRating Table, on loadHistoricalAndUpdateValues function. Error :"
						+ e.getMessage());
			}
		
		return tUserMxInternalRating;
	}

	private Table formatTable(Table tData) {
		
		tData = getPartyIdFromTickerBloomberg (tData);
		
		try {
			tData.addCol("fecha", COL_TYPE_ENUM.COL_DATE_TIME);
			tData.addCol("short_name", COL_TYPE_ENUM.COL_STRING);
			tData.addCol("score", COL_TYPE_ENUM.COL_DOUBLE);
			tData.addCol("default_prob", COL_TYPE_ENUM.COL_DOUBLE);
			tData.addCol("market_cap", COL_TYPE_ENUM.COL_DOUBLE);
			tData.addCol("total_debt", COL_TYPE_ENUM.COL_DOUBLE);
			tData.addCol("updated", COL_TYPE_ENUM.COL_INT);
			tData.addCol("personnel_id", COL_TYPE_ENUM.COL_INT);
			tData.setColName("RSK_BB_ISSUER_DEFAULT", "internal_rating");
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to add cols on table tData, on formatTable function. Error :"
					+ e.getMessage());
		}
		
		// Set User id that runs workflow
		try {
			tData.setColValInt("personnel_id", iUserId);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set uUserId on table tData, on formatTable function. Error :"
					+ e.getMessage());
		}

		try {
			for (int i=1; i<= tData.getNumRows(); i++){
				tData.setDateTime("fecha", i, ODateTime.strToDateTime(sToday));
				tData.setString("short_name", i, Ref.getName(SHM_USR_TABLES_ENUM.PARTY_TABLE, tData.getInt("party_id", i)));
				
				// Default Prob String to Double
				
				String sDefaultProb = tData.getString("BB_1YR_DEFAULT_PROB", i);
				
				if (sDefaultProb != null && sDefaultProb.length() > 0 
				&& !sDefaultProb.equals("N.A.") && !sDefaultProb.equals("N.S.")) { //1.1 - Se agrega validacion por problema de parse.
					tData.setDouble("default_prob", i, Double.parseDouble(sDefaultProb)*100); //1.2 - Se agrega multiplicacion * 100 para visualizacion porcentual
				} else {
					tData.setDouble("default_prob", i, 0.0);
				}
				
				// Market Cap
				String sMarketCap = tData.getString("CUR_MKT_CAP", i);
				
				if (sMarketCap != null && sMarketCap.length() > 0 &&
						!sMarketCap.equals("N.A.") && !sMarketCap.equals("N.S.")){
					Double dMarketCap = Double.parseDouble(sMarketCap);
					tData.setDouble("market_cap", i, dMarketCap);
				} else {
					tData.setDouble("market_cap", i, 0.0);
				}
				
				
				// Total Debt
				
				String sTotalDebt1 = tData.getString("BS_ST_DEBT", i);
				String sTotalDebt2 = tData.getString("BS_LONG_TERM_BORROWINGS", i);
				
				if (sTotalDebt1 != null && sTotalDebt1.length() > 0 &&
						sTotalDebt2 != null && sTotalDebt2.length() > 0 &&
						!sTotalDebt1.equals("N.A.") && !sTotalDebt1.equals("N.S.")
						&& !sTotalDebt2.equals("N.A.") && !sTotalDebt2.equals("N.S.")){
					
					Double dTotalDebt1 = Double.parseDouble(sTotalDebt1);
					Double dTotalDebt2 = Double.parseDouble(sTotalDebt2);
					
					Double dTotalDebt = dTotalDebt1+dTotalDebt2; 
					
					tData.setDouble("total_debt", i, dTotalDebt);
				} else {
					tData.setDouble("total_debt", i, 0.0);
				}
				
				
			}
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set fecha, short_name or default_prob on table tData, on formatTable function. Error :"
					+ e.getMessage());
		}
		
		return tData;
	}

	private Table getResponseDataFromResponseId(String sResponseId) {
		
		Table tData = Util.NULL_TABLE;
		ResponseBBG response = null;
		WsCallBBGDL client   = new WsCallBBGDL();
		int iNewRow = 0;
		
		try {
			tData = Table.tableNew("tData");
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to create table tData on getResponseDataFromResponseId function. Error :"
					+ e.getMessage());
		}
		
		// Try to get data until reponse code = 0 (successful)
			do {			
				try {
					response = client.callWSBBGDL_Response(sResponseId, "BDP", PROPERTIES_FILEPATH,PROPERTIES_FILENAME);
				} catch (Exception e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"Unable to get response data on getResponseDataFromResponseId function. Error :"
							+ e.getMessage());
				}
				
				// If response is not successful, sleep plugin for a minute
				if (response.getResponseCode() != 0){
					try {
						Thread.sleep(60000);
						_Log.printMsg(EnumTypeMessage.INFO,
								"Response Code is not 0 (successful), so data unavailable. Sleep task for a minute. ");
					} catch (InterruptedException e) {
						_Log.printMsg(EnumTypeMessage.ERROR,
								"Failed to sleep thread on getResponseDataFromResponseId function. Error :"
								+ e.getMessage());
					}
				}
			} while (response.getResponseCode() != 0);
	
		// If there's data, put it on a Table
			try {
				tData.addCol("instruments", COL_TYPE_ENUM.COL_STRING);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"Failed to create column instruments for tData on on getResponseDataFromResponseId function. Error :"
						+ e.getMessage());
			}
			
			try {
				if (response.getResponseCode() == 0){
					
					for (int i=0; i<response.getListInstrument().size();i++){
						
						iNewRow = tData.addRow();
						InstrumentResponseBBG instrument = response.getListInstrument().get(i);
						tData.setString("instruments", iNewRow, instrument.getInstrument());
						_Log.printMsg(EnumTypeMessage.DEBUG, "Instrument: " +instrument.getInstrument());
						
						for( int j=0; j<instrument.getDataResponse().size();j++){
							
							DetailRetrieveResponseBBG data = instrument.getDataResponse().get(j);
							
							// If it's the first instrument, set fields as cols
							if(i==0)
								tData.addCol(data.getField(), COL_TYPE_ENUM.COL_STRING);
							
							tData.setString(data.getField(), iNewRow, data.getValue());
							_Log.printMsg(EnumTypeMessage.DEBUG, "Field: " +data.getField());
							_Log.printMsg(EnumTypeMessage.DEBUG, "Data: " +data.getValue());
						}
					}				
				}
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Failed to set data values for tData table, on getResponseDataFromResponseId function. Error :"
					+ e.getMessage());
		}
			



		return tData;
	}

	private String getResponseIdFromUserTable() {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();		
		Table tResponseId = Util.NULL_TABLE;
		String sResponseId = "";

		try {
			tResponseId = Table.tableNew("Response Id");
			StringBuilder sb = new StringBuilder ();
			sb.append("\n select ");
			sb.append("\n 	uc.response_id ");
			sb.append("\n from "+sUserMxScoreCategory+" uc ");
			sb.append("\n where uc.category_id = 4 -- Bloomberg ");
			DBaseTable.execISql(tResponseId, sb.toString());
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to load user_mx_score_category on getResponseIdFromUserTable function. Error :"
					+ e.getMessage());
		}

		try {
			sResponseId = tResponseId.getString("response_id", 1);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to get String Response_Id from tResponseId Table on getResponseIdFromUserTable function. Error :"
					+ e.getMessage());
		}
		
		if (sResponseId.equals("")){
			_Log.printMsg(EnumTypeMessage.ERROR,
					"There's no value setted on the reponse_id column of user_mx_score_category."
					+ "Please run the Request script to get this work right.");
			Util.exitFail();
		}
		
		return sResponseId;
	}

	private Table getPartyIdFromTickerBloomberg(Table tData) {
		
		String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();		
		Table tParty = Util.NULL_TABLE;
		
		try {
			tParty = Table.tableNew();
			String sQuery = "select party_id, ticker_bbg from " +sUserMxScoreParty+" up where up.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') and up.category_id=4";
			DBaseTable.execISql(tParty, sQuery);
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to load user_mx_score_party on getPartyIdFromTickerBloomberg function. Error :"
					+ e.getMessage());
		}
		
		try {
			tParty.select(tData, "*", "instruments EQ $ticker_bbg");
		} catch (OException e) {
			_Log.printMsg(EnumTypeMessage.ERROR,
					"Unable to set party_id on tData Table, on getPartyIdFromTickerBloomberg function. Error :"
					+ e.getMessage());
		}
		
//		try {
//			tData.select(tParty, "party_id", "ticker_bbg EQ $instruments");
//		} catch (OException e) {
//			_Log.printMsg(EnumTypeMessage.ERROR,
//					"Unable to set party_id on tData Table, on getPartyIdFromTickerBloomberg function. Error :"
//					+ e.getMessage());
//		}

		return tParty;
	}
}