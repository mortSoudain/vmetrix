Select distinct ab.internal_portfolio, 
   CASE WHEN ca.value is null THEN 'SIN CLASIFICACION DE CLASE DE ACTIVO' ELSE ca.value END as clase_activo_aux,
   h.ticker,
   ab.deal_tracking_num as deal_num,
   ab.tran_num,
   ab.ins_num,
   ab.toolset,
   ab.ins_type,
   ins.currency,
   ab.position,
   ab.mvalue,
   u.dias_por_vencer,
   u.factor_interes,
   u.fecha_vcto_cupon,
   u.plazo_emision,
   u.precio_limpio,
   u.precio_sucio,
   u.sobretasa,
   u.tasa_cupon,
   u.tasa_mercado,
   bu.value bursatilidad,
   ax.value anexo,
   pa.value region,
   ab.external_lentity contraparte_contrato,
   NVL((select h_sub.ticker from header h_sub, ab_tran ab_sub where h_sub.ins_num = ab_sub.ins_num and ab_sub.tran_num = sub.underlying_tran and ab_sub.tran_type = 2 and ab_sub.tran_status = 3) ,'NO APLICA') ticker_subyacente,
   p.notnl contract_size
from ab_tran ab
   inner join ab_tran ins on ab.ins_num = ins.ins_num and ins.tran_type = 2 and ins.tran_status = 3 -- holding, validated
   inner join header h on ab.ins_num = h.ins_num
   left join ab_tran_info ca on ins.tran_num = ca.tran_num  and ca.type_id = 20098 and (value like '%FUTURO%' or value is null)
   left join ab_tran_info pa on ab.tran_num = pa.tran_num  and pa.type_id = 20109
   left join ab_tran_info bu on ab.tran_num = bu.tran_num  and bu.type_id = 20104
   left join ab_tran_info ax on ab.tran_num = ax.tran_num  and ax.type_id = 20106
   left join tran_underlying_link sub on ab.ins_num = sub.ins_num,
   user_mx_vectorprecios_diario u,
   parameter p
where ab.tran_type = 0
   and ab.tran_status  = 3
   and ab.internal_portfolio in (20010,20011,20012,20013,20014,20015,20016)
   and ab.asset_type  = 2
   and ab.toolset in (5,20,28,34,41,50)
   and u.ticker = h.ticker
   and p.ins_num = ab.ins_num
   and p.param_seq_num = 0
order by 1,2,3,4

UNION ALL 
select
   ab.internal_portfolio
   ,'SIN CLASIFICACION DE CLASE DE ACTIVO' as clase_activo_aux
   ,h.ticker
   ,ab.deal_tracking_num as deal_num
   ,ab.tran_num
   ,ab.ins_num
   ,ab.toolset
   ,ab.ins_type
   ,ins.currency
   ,ab.position
   ,ab.mvalue
   ,u.dias_por_vencer
   ,u.factor_interes
   ,u.fecha_vcto_cupon
   ,u.plazo_emision
   ,u.precio_limpio
   ,u.precio_sucio
   ,u.sobretasa
   ,u.tasa_cupon
   ,u.tasa_mercado
   ,bu.value bursatilidad
   ,ax.value anexo
   ,pa.value region
   ,ab.external_lentity contraparte_contrato
   ,NVL((select h_sub.ticker from header h_sub, ab_tran ab_sub where h_sub.ins_num = ab_sub.ins_num and ab_sub.tran_num = sub.underlying_tran and ab_sub.tran_type = 2 and ab_sub.tran_status = 3) ,'NO APLICA') ticker_subyacente
   ,p.notnl contract_size
from ab_tran ab
   left join header h on h.ins_num = ab.ins_num
   left join ab_tran ins on ins.ins_num = ab.ins_num
   left join user_mx_vectorprecios_diario u on u.ticker = h.ticker
   left join ab_tran_info pa on ab.tran_num = pa.tran_num  and pa.type_id = 20109
   left join ab_tran_info bu on ab.tran_num = bu.tran_num  and bu.type_id = 20104
   left join ab_tran_info ax on ab.tran_num = ax.tran_num  and ax.type_id = 20106
   left join tran_underlying_link sub on ab.ins_num = sub.ins_num
   left join parameter p on p.ins_num= ab.ins_num and p.param_seq_num = 0
where ab.tran_type = 0
   and ab.tran_status  = 3
   and ab.internal_portfolio in (20010,20011,20012,20013,20014,20015,20016)
   and ab.asset_type  = 2
   and ab.toolset = 1
order by 1,2,3,4