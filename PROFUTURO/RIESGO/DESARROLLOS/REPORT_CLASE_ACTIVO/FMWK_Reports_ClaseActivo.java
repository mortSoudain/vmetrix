/**$Header: v1.2  04/12/2017 $*/
/*
 * File Name:               FMWK_Reports_ClaseActivo.java
 * Input File Name:         None
 * Author:                  Elias Perez Aguilera - VMetrix
 * Creation Date:           13/02/2017
 * 
 * REVISION HISTORY
 *  Release:				v1.1     
 *  Date:				    01-12-2017    
 *  Author:					Gustavo Rojas Arteaga - VMetrix
 *  Description:			Se trae toda la logica al plugin, solo el formateo se hace en el report builder	      
 *  			      
 *  Release:				v1.2     
 *  Date:				    04-12-2017    
 *  Author:					Gustavo Rojas Arteaga - VMetrix
 *  Description:			Se agregan instrumentos de derivados al query para poder ser utilizado como data source en el clon de derivados	   
 *  
 */

package com.afore.custom_reports;

import standard.include.JVS_INC_STD_Simulation;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsPortfolios;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.PFOLIO_RESULT_TYPE;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Reports_ClaseActivo implements IScript {
	
	private final String sPlugInName = this.getClass().getSimpleName();

	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _utilafore = new UTIL_Afore();
	
	private boolean bDerivativeReport = false;
	
	JVS_INC_STD_Simulation  m_INCSTDSimulation = new JVS_INC_STD_Simulation();
	
	@Override
	public void execute(IContainerContext context) throws OException {

		_Log.markStartScript();
		_Log.setDEBUG_STATUS(EnumStatus.ON);
    	
		int iPortfolio = 0;
		int iQuery = 0;
		
		Table tArgt               = Util.NULL_TABLE;
		Table tReturnt            = Util.NULL_TABLE;
		Table tResList            = Util.NULL_TABLE;
		Table tAllTrades          = Util.NULL_TABLE;
		Table tCreditRatings      = Util.NULL_TABLE;
		Table tPortfolio          = Util.NULL_TABLE;
		Table tTradesPortfolio    = Util.NULL_TABLE;
		Table tTradesClaseActivo  = Util.NULL_TABLE;
		Table tSimRes             = Util.NULL_TABLE;
		Table tTranResults        = Util.NULL_TABLE;
		//v1.1
		Table tNav        		  = Util.NULL_TABLE;

		tArgt.viewTable();
		try{
			
			tArgt    = context.getArgumentsTable();
    		tReturnt = context.getReturnTable();

    		//Check Report
    		String sReportDefName = tArgt.getTable("ReportDefinition", 1).getString("dxr_definition_name", 1);
    		if (sReportDefName.contains("Derivados"))
    			bDerivativeReport = true;
    		
    		_Log.printMsg(EnumTypeMessage.DEBUG, "Report Def Name : " + sReportDefName);
    		
    		if(tArgt.getColNum("QueryId") <= 0)
    			tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);
    			
    		_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": SIM Result Create");
			tResList = Sim.createResultListForSim ();

    		SimResultType srtBaseMTM   = SimResultType.create(PFOLIO_RESULT_TYPE.BASE_PV_RESULT.toString());
    		
    		SimResult.addResultForSim(tResList, srtBaseMTM);
    		
    		_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Getting Nav");
    		
    		//Getting NAV
    		tNav = getNav();
    		
    		_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Getting Trades");
    		tAllTrades = getTranVigentes();
			
			_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Getting Credit Rating");
    		
			if (bDerivativeReport){
				tCreditRatings = _utilafore.getTableMinRateForAllInsByCounterParty();
				tAllTrades.select(tCreditRatings, "value(rating_name)", "party_id EQ $contraparte_contrato"); 
			}else{
				tCreditRatings = _utilafore.getTableMinRateForAllIns();
				tAllTrades.select(tCreditRatings, "value(rating_name)", "ins_num EQ $ins_num"); 
			}

    		   		
    		
    		tPortfolio = Table.tableNew("Portfolios");
			tPortfolio.select(tAllTrades, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
			tPortfolio.sortCol("internal_portfolio");
			
			for (int iRowPortfolio = 1; iRowPortfolio <= tPortfolio.getNumRows(); iRowPortfolio++) {
				
				iPortfolio = tPortfolio.getInt("internal_portfolio", iRowPortfolio);
				
				Table tTradesSimulacion = Table.tableNew("Trade Simulacion");
				
				tTradesSimulacion.select(tAllTrades, "DISTINCT, deal_num, tran_num", "internal_portfolio EQ " + iPortfolio);
				
				iQuery = Query.tableQueryInsert(tTradesSimulacion, "tran_num");
				
				if(tArgt.getNumRows() < 1)
					tArgt.addRow();
				
				tArgt.setInt("QueryId", 1, iQuery);
				
				_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Running SIM Result for Portfolio " + iPortfolio);
				tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
	    		
	    		Query.clear(iQuery);
	    		
	    		tTranResults = SimResult.getTranResults(tSimRes);
	    		
	    		tTradesSimulacion.select(tTranResults, srtBaseMTM.getId()+"(base_mtm)", "deal_num EQ $deal_num AND deal_leg EQ 0");

	    		_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Setting MTM from Sim for Portfolio " + iPortfolio);
	    		tAllTrades.select(tTradesSimulacion, "base_mtm", "deal_num EQ $deal_num");

	    		if(Table.isTableValid(tTradesPortfolio )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTradesPortfolio.destroy();
	    		if(Table.isTableValid(tSimRes          )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tSimRes.destroy();
				if(Table.isTableValid(tTranResults     )  == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTranResults.destroy();
				if(Table.isTableValid(tTradesClaseActivo) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTradesClaseActivo.destroy();
			}
			_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Getting all trades tu return table");
			tReturnt.select(tAllTrades, "*", "internal_portfolio GT 0");

			_Log.printMsg(EnumTypeMessage.DEBUG,Util.timeGetServerTimeHMS() + ": Setting Nav");
			tReturnt.select(tNav, "current_nav", "internal_portfolio EQ $internal_portfolio");

		}catch (Throwable e){
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{
			if(Table.isTableValid(tResList         ) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tResList.destroy();
			if(Table.isTableValid(tAllTrades       ) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAllTrades.destroy();
			if(Table.isTableValid(tPortfolio       ) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tPortfolio.destroy();
			if(Table.isTableValid(tCreditRatings   ) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tCreditRatings.destroy();

			_Log.markEndScript();
		}
	}

	private Table getTranVigentes() throws OException {
		
		Table tReturn = Util.NULL_TABLE;
		tReturn       = Table.tableNew("Operaciones Vigentes");

		StringBuffer sb = new StringBuffer();
		
		
		sb.append("\nSelect distinct ab.internal_portfolio, ");
		sb.append("\n   CASE WHEN ca.value is null THEN 'SIN CLASIFICACION DE CLASE DE ACTIVO' ELSE ca.value END as clase_activo_aux,");
		sb.append("\n   h.ticker,");
		sb.append("\n   ab.deal_tracking_num as deal_num,");
		sb.append("\n   ab.tran_num,");
		sb.append("\n   ab.ins_num,");
		sb.append("\n   ab.ins_type,");
		sb.append("\n   ins.currency,");
		sb.append("\n   ab.position,");
		sb.append("\n   ab.mvalue,");
		sb.append("\n   u.dias_por_vencer,");
		sb.append("\n   u.factor_interes,");
		sb.append("\n   u.fecha_vcto_cupon,");
		sb.append("\n   u.plazo_emision,");
		sb.append("\n   u.precio_limpio,");
		sb.append("\n   u.precio_sucio,");
		sb.append("\n   u.sobretasa,");
		sb.append("\n   u.tasa_cupon,");
		sb.append("\n   u.tasa_mercado,");
		sb.append("\n   bu.value bursatilidad,");
		sb.append("\n   ax.value anexo,");
		sb.append("\n   pa.value region,");
		sb.append("\n   ab.external_lentity contraparte_contrato,");
		sb.append("\n   NVL((select h_sub.ticker from header h_sub, ab_tran ab_sub where h_sub.ins_num = ab_sub.ins_num and ab_sub.tran_num = sub.underlying_tran and ab_sub.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " and ab_sub.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + ") ,'NO APLICA') ticker_subyacente,");
		sb.append("\n   p.notnl contract_size");
		sb.append("\nfrom ab_tran ab");
		sb.append("\n   inner join ab_tran ins on ab.ins_num = ins.ins_num and ins.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " and ins.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n   inner join header h on ab.ins_num = h.ins_num");
		
		if (bDerivativeReport)
			sb.append("\n   left join ab_tran_info ca on ins.tran_num = ca.tran_num  and ca.type_id = " + EnumsTranInfoFields.MX_ALL_INS_CLASE_ACTIVO.toInt() + " and (value like '%FUTURO%' or value is null)");
		else
			sb.append("\n   left join ab_tran_info ca on ins.tran_num = ca.tran_num  and ca.type_id = " + EnumsTranInfoFields.MX_ALL_INS_CLASE_ACTIVO.toInt());
		
		sb.append("\n   left join ab_tran_info pa on ab.tran_num = pa.tran_num  and pa.type_id = " + EnumsTranInfoFields.MX_RISK_ISSUER_REGION.toInt());
		sb.append("\n   left join ab_tran_info bu on ab.tran_num = bu.tran_num  and bu.type_id = " + EnumsTranInfoFields.MX_RISK_TRAN_BURSATILIDAD.toInt());
		sb.append("\n   left join ab_tran_info ax on ab.tran_num = ax.tran_num  and ax.type_id = " + EnumsTranInfoFields.MX_RISK_TRAN_ANNEX.toInt());
		sb.append("\n   left join tran_underlying_link sub on ab.ins_num = sub.ins_num,");
		sb.append("\n   user_mx_vectorprecios_diario u,");
		sb.append("\n   parameter p");
		sb.append("\nwhere ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n   and ab.tran_status  = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n   and ab.internal_portfolio in (").append(EnumsPortfolios.MX_PORTFOLIO_SB1.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SB2.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SB3.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SB4.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SBP.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SCP.toInt())
       	                                .append(",").append(EnumsPortfolios.MX_PORTFOLIO_SLP.toInt())
       	                                .append(")");
		sb.append("\n   and ab.asset_type  = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt());
		sb.append("\n   and ab.toolset in (").append(TOOLSET_ENUM.BOND_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.MONEY_MARKET_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.EQUITY_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.BONDFUT_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.SWAP_TOOLSET.toInt()).append(",")
											.append(TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt()).append(")");
		sb.append("\n   and u.ticker = h.ticker");
		sb.append("\n   and p.ins_num = ab.ins_num");
		sb.append("\n   and p.param_seq_num = 0");
		sb.append("\norder by 1,2,3,4");

		if (bDerivativeReport){
			sb.append("\n UNION ALL  ");
			sb.append("\n select ");
			sb.append("\n    'SIN CLASIFICACION DE CLASE DE ACTIVO' as clase_activo_aux ");
			sb.append("\n    ,h.ticker ");
			sb.append("\n    ,ab.deal_tracking_num as deal_num ");
			sb.append("\n    ,ab.tran_num ");
			sb.append("\n    ,ab.ins_num ");
			sb.append("\n    ,ab.toolset ");
			sb.append("\n    ,ab.ins_type ");
			sb.append("\n    ,ins.currency ");
			sb.append("\n    ,ab.position ");
			sb.append("\n    ,ab.mvalue ");
			sb.append("\n    ,u.dias_por_vencer ");
			sb.append("\n    ,u.factor_interes ");
			sb.append("\n    ,u.fecha_vcto_cupon ");
			sb.append("\n    ,u.plazo_emision ");
			sb.append("\n    ,u.precio_limpio ");
			sb.append("\n    ,u.precio_sucio ");
			sb.append("\n    ,u.sobretasa ");
			sb.append("\n    ,u.tasa_cupon ");
			sb.append("\n    ,u.tasa_mercado ");
			sb.append("\n    ,bu.value bursatilidad ");
			sb.append("\n    ,ax.value anexo ");
			sb.append("\n    ,pa.value region ");
			sb.append("\n    ,ab.external_lentity contraparte_contrato ");
			sb.append("\n    ,NVL((select h_sub.ticker from header h_sub, ab_tran ab_sub where h_sub.ins_num = ab_sub.ins_num and ab_sub.tran_num = sub.underlying_tran and ab_sub.tran_type = 2 and ab_sub.tran_status = 3) ,'NO APLICA') ticker_subyacente ");
			sb.append("\n    ,p.notnl contract_size ");
			sb.append("\n from ab_tran ab ");
			sb.append("\n    left join header h on h.ins_num = ab.ins_num ");
			sb.append("\n    left join ab_tran ins on ins.ins_num = ab.ins_num ");
			sb.append("\n    left join user_mx_vectorprecios_diario u on u.ticker = h.ticker ");
			sb.append("\n    left join ab_tran_info pa on ab.tran_num = pa.tran_num  and pa.type_id = 20109 ");
			sb.append("\n    left join ab_tran_info bu on ab.tran_num = bu.tran_num  and bu.type_id = 20104 ");
			sb.append("\n    left join ab_tran_info ax on ab.tran_num = ax.tran_num  and ax.type_id = 20106 ");
			sb.append("\n    left join tran_underlying_link sub on ab.ins_num = sub.ins_num ");
			sb.append("\n    left join parameter p on p.ins_num= ab.ins_num and p.param_seq_num = 0 ");
			sb.append("\n where ab.tran_type = 0 ");
			sb.append("\n    and ab.tran_status  = 3 ");
			sb.append("\n    and ab.internal_portfolio in (20010,20011,20012,20013,20014,20015,20016) ");
			sb.append("\n    and ab.asset_type  = 2 ");
			sb.append("\n    and ab.toolset = 1 ");
			sb.append("\n order by 1,2,3,4 ");
		}
        	  
		_Log.printMsg(EnumTypeMessage.DEBUG,"\n SQL : "+sb.toString());
		
		try {
			
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
			
			if(iRet != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt() || tReturn.getNumRows() < 1){
				_Log.printMsg(EnumTypeMessage.ERROR,"No se pudo obtener las operaciones vigentes");
			}
		
		} catch (OException exception){
			
			_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
			_Log.printMsg(EnumTypeMessage.ERROR,"Exception: \n "+ exception.getMessage());
			tReturn.destroy();
			Util.exitFail();
		}
		
		return tReturn;
	}
	
	private Table getNav() throws OException
	{
		int iToday = OCalendar.today();
		Table tTotalNav = Table.tableNew();
		int iYesterday = OCalendar.getLgbd(iToday);
		tTotalNav = _utilafore.getAllTotalNAV(iToday);
		
		if (tTotalNav.getNumRows()==0)
			tTotalNav = _utilafore.getAllTotalNAV(iYesterday);
	
		return tTotalNav;
	}
	
}

