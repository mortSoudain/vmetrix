/*$Header: v 1.0, 22/Jun/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_SAS_to_CSV.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Junio 2018
 * Version:					1.0
 * Description:				Script diseñado para la generacion de archivos csv
 *							a partir de archivos sas7bdat en rutas específicas
 *							configuradas en user_configurable_variables
 *                       
 * REVISION HISTORY
 * Date:                   
 * Version/Autor:          
 * Description:            
 *                         
 ************************************************************************************/
package com.profuturo.servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.io.FilenameUtils;

import com.afore.enums.EnumTypeMessage;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.epam.parso.CSVDataWriter;
import com.epam.parso.SasFileReader;
import com.epam.parso.impl.CSVDataWriterImpl;
import com.epam.parso.impl.SasFileReaderImpl;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;

public class MISC_Afore_SAS_to_CSV implements IScript {
	
	// Basic util variables
		private String		sScriptName		=	this.getClass().getSimpleName();
		private UTIL_Afore	UtilAfore 		=	new UTIL_Afore();
		private UTIL_Log	_Log			=	new UTIL_Log(sScriptName);

	// Specific script variables
		private String SAS_FILEPATH 	= "";
		private String CSV_FILEPATH 	= "";
		private String TEMP_CSV_FILE	= "";

	@Override
	public void execute(IContainerContext context) {
		
		_Log.markStartScript();
			
			initializeGlobalVariables();
			
			// Get SAS directory filenames
				Table tFileNames = readFileNamesFromSasDirectory();
			
			// Generate csv file for each sas file on directory
				generateCsvFiles(tFileNames);
				
		_Log.markEndScript();
				
	}

	private void generateCsvFiles(Table tFileNames) {
		
		// Get info from SAS file, set it on Table object,
		// format table and generate csv files
			try {
				for(int i=1; i<= tFileNames.getNumRows(); i++){
					String sFileName = tFileNames.getString("filename", i);
					Table tFile = getTableFromSas(sFileName);
					tFile = formatTable(sFileName,tFile);
					generateCsvFile(sFileName,tFile);
				}
			} catch (OException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to generate some of csv files, on generateCsvFiles function. Error: "
								+ e.getMessage());
			}
	}

	private void generateCsvFile(String sFileName,Table tFile) {
		
		// Replace .sas7bdat extension to .csv and add csv path to generate files
			sFileName = sFileName.replace(".sas7bdat", ".csv");
			sFileName = CSV_FILEPATH + sFileName;
		
		// Generate CSV
			try {
				//tFile.printTableDumpToFile(sFileName);
				tFile.hideRowNames();
				tFile.commaSep();
				tFile.noFormatPrint();
				tFile.noFormatNumerical(); 
				
				tFile.printTableToFile(sFileName);
				
			} catch (OException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable generate csv file, on generateCsvFile function. Error: "
								+ e.getMessage());
			}
	}

	private Table formatTable(String sFileName, Table tFile) {
		
		int iNumRows = 0;
		
		// Change col names and delete the first row (which contains col names)
			try {
				for(int i=1; i<= tFile.getNumCols(); i++){
					tFile.setColName(i, tFile.getString(i, 1));
				}
				tFile.delRow(1);
			} catch (OException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable set col names, on formatTable function. Error: "
								+ e.getMessage());
			}
			
		// Get Num Rows
			try {
				iNumRows = tFile.getNumRows();
			} catch (OException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to get num rows for tFile Table, on formatTable function. Error: "
								+ e.getMessage());
			}
			
		if(sFileName.contains("runoff")){
		
			for(int i=1; i<=iNumRows;i++){
				
				String sFecha 	= "";
				String sSb4		= "";
				String sSb3		= "";
				String sSb2		= "";
				String sSb1		= "";
				String sSb0		= "";
				String sCp		= "";
				String sLp		= "";
				
				// Get fields info
					try {
						sFecha	= tFile.getString("FECHA", i);
						sSb4	= tFile.getString("SB4", i);
						sSb3	= tFile.getString("SB3", i);
						sSb2	= tFile.getString("SB2", i);
						sSb1	= tFile.getString("SB1", i);
						sSb0	= tFile.getString("SB0", i);
						sCp		= tFile.getString("CP", i);
						sLp		= tFile.getString("LP", i);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to get some of the fields of tFile Table for runoff file, on formatTable function. Error: "
										+ e.getMessage());
					}
				
				// Change spanish format to english date format
					if(sFecha.contains("ene")){
						sFecha=sFecha.replace("ene", "jan");
					} else if (sFecha.contains("ago")){
						sFecha=sFecha.replace("ago", "aug");
					} else if (sFecha.contains("abr")){
						sFecha=sFecha.replace("abr", "apr");
					}
					
				// Set new format to date
					try {
						int iFecha = OCalendar.parseString(sFecha);
						sFecha	= OCalendar.formatDateInt(iFecha, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to set some new format of date for runoff file, on formatTable function. Error: "
										+ e.getMessage());
					}
					
				// Set new format to double values
					if (sSb4 != null && sSb4.length() > 0) {
						Double dSb4 = Double.valueOf(sSb4);
						sSb4 = String.format("%.2f",dSb4);
					}
					
					if (sSb3 != null && sSb3.length() > 0) {
						Double dSb3 = Double.valueOf(sSb3);
						sSb3 = String.format("%.2f",dSb3);
					}
					
					if (sSb2 != null && sSb2.length() > 0) {
						Double dSb2 = Double.valueOf(sSb2);
						sSb2 = String.format("%.2f",dSb2);
					}
					
					if (sSb1 != null && sSb1.length() > 0) {
						Double dSb1 = Double.valueOf(sSb1);
						sSb1 = String.format("%.2f",dSb1);
					}
					
					if (sSb0 != null && sSb0.length() > 0) {
						Double dSb0 = Double.valueOf(sSb0);
						sSb0 = String.format("%.2f",dSb0);
					}
					
					if (sCp != null && sCp.length() > 0) {
						Double dCp = Double.valueOf(sCp);
						sCp = String.format("%.2f",dCp);
					}
					
					if (sLp != null && sLp.length() > 0) {
						Double dLp = Double.valueOf(sLp);
						sLp = String.format("%.2f",dLp);
					}
					
				// Set new values
					try {
						tFile.setString("FECHA",i,sFecha);
						tFile.setString("SB4",i,sSb4);
						tFile.setString("SB3",i,sSb3);
						tFile.setString("SB2",i,sSb2);
						tFile.setString("SB1",i,sSb1);
						tFile.setString("SB0",i,sSb0);
						tFile.setString("CP",i,sCp);
						tFile.setString("LP",i,sLp);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to set some new formatted values for runoff file, on formatTable function. Error: "
										+ e.getMessage());
					}
			}
				
		} else if(sFileName.contains("entradas_salidas")){
					
			for(int i=1; i<=iNumRows;i++){
				
				String sPeriodo			= "";
				//String sSiefore		= "";
				String sRecaudacion		= "";
				String sTraspasosIn		= "";
				String sRetiroRimt		= "";
				String sAniversarioIn	= "";
				String sAniversarioOut	= "";
				String sRetiroP			= "";
				String sSalidaTotal		= "";
				String sEntradaTotal	= "";
				String sNeto 			= "";

				// Get fields info
				try {
					sPeriodo		= tFile.getString("periodo", i);
					// sSiefore		= tFile.getString("siefore", i);
					sRecaudacion	= tFile.getString("recaudacion", i);
					sTraspasosIn	= tFile.getString("traspasos_in", i);
					sRetiroRimt		= tFile.getString("retiro_rimt", i);
					sAniversarioIn	= tFile.getString("aniversario_in", i);
					sAniversarioOut	= tFile.getString("aniversario_out", i);
					sRetiroP		= tFile.getString("retiro_p", i);
					sSalidaTotal	= tFile.getString("salida_total", i);
					sEntradaTotal	= tFile.getString("entrada_total", i);
					sNeto 			= tFile.getString("neto", i);
				} catch (OException e) {
					_Log.printMsg(
							EnumTypeMessage.ERROR,
							"Unable to get some of the fields of tFile Table for entradas_salidas file, on formatTable function. Error: "
									+ e.getMessage());
				}
				
				// Change spanish format to english date format
					if(sPeriodo.contains("ene")){
						sPeriodo=sPeriodo.replace("ene", "jan");
					} else if (sPeriodo.contains("ago")){
						sPeriodo=sPeriodo.replace("ago", "aug");
					} else if (sPeriodo.contains("abr")){
						sPeriodo=sPeriodo.replace("abr", "apr");
					}
				
				// Set new format to date
					try {
						int iFecha = OCalendar.parseString(sPeriodo);
						sPeriodo	= OCalendar.formatDateInt(iFecha, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
						sPeriodo	= sPeriodo.replace("/", "-");
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to set some new format of date for runoff file, on formatTable function. Error: "
										+ e.getMessage());
					}
					
				// Set new format to double values
					if (sRecaudacion != null && sRecaudacion.length() > 0) {
						Double dRecaudacion = Double.valueOf(sRecaudacion);
						sRecaudacion = String.format("%.12f",dRecaudacion);
					}

					if (sTraspasosIn != null && sTraspasosIn.length() > 0) {
						Double dTraspasosIn = Double.valueOf(sTraspasosIn);
						sTraspasosIn = String.format("%.12f",dTraspasosIn);
					}

					if (sRetiroRimt != null && sRetiroRimt.length() > 0) {
						Double dRetiroRimt = Double.valueOf(sRetiroRimt);
						sRetiroRimt = String.format("%.12f",dRetiroRimt);
					}

					if (sAniversarioIn != null && sAniversarioIn.length() > 0) {
						Double dAniversarioIn = Double.valueOf(sAniversarioIn);
						sAniversarioIn = String.format("%.12f",dAniversarioIn);
					}

					if (sAniversarioOut != null && sAniversarioOut.length() > 0) {
						Double dAniversarioOut = Double.valueOf(sAniversarioOut);
						sAniversarioOut = String.format("%.12f",dAniversarioOut);
					}

					if (sRetiroP != null && sRetiroP.length() > 0) {
						Double dRetiroP = Double.valueOf(sRetiroP);
						sRetiroP = String.format("%.12f",dRetiroP);
					}

					if (sSalidaTotal != null && sSalidaTotal.length() > 0) {
						Double dSalidaTotal = Double.valueOf(sSalidaTotal);
						sSalidaTotal = String.format("%.12f",dSalidaTotal);
					}

					if (sEntradaTotal != null && sEntradaTotal.length() > 0) {
						Double dEntradaTotal = Double.valueOf(sEntradaTotal);
						sEntradaTotal = String.format("%.12f",dEntradaTotal);
					}

					if (sNeto != null && sNeto.length() > 0) {
						Double dNeto = Double.valueOf(sNeto);
						sNeto = String.format("%.12f",dNeto);
					}
					
				// Set new values
					try {
						tFile.setString("periodo",i,sPeriodo);
						tFile.setString("recaudacion",i,sRecaudacion);
						tFile.setString("traspasos_in",i,sTraspasosIn);
						tFile.setString("retiro_rimt",i,sRetiroRimt);
						tFile.setString("aniversario_in",i,sAniversarioIn);
						tFile.setString("aniversario_out",i,sAniversarioOut);
						tFile.setString("retiro_p",i,sRetiroP);
						tFile.setString("salida_total",i,sSalidaTotal);
						tFile.setString("entrada_total",i,sEntradaTotal);
						tFile.setString("neto",i,sNeto);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to set some new formatted values for entradas_salidas file, on formatTable function. Error: "
										+ e.getMessage());
					}
				
			}
			
			
		} else if(sFileName.contains("afo_saldo_sie_edad")){
			
			for(int i=1; i<=iNumRows;i++){
				
//				String sAnios			= "";
				String sSb0				= "";
				String sSb1				= "";
				String sSb2				= "";
				String sSb3				= "";
				String sSb4				= "";
				String sCp				= "";
				String sLp				= "";
//				String sFechaFin		= "";
//				String sFechaEjecuta	= "";
//				String sJobFin 			= "";
				
				// Get fields info
					try {
						// sAnios		= tFile.getString("ANIOS", i);
						sSb0			= tFile.getString("SB0", i);
						sSb1			= tFile.getString("SB1", i);
						sSb2			= tFile.getString("SB2", i);
						sSb3			= tFile.getString("SB3", i);
						sSb4			= tFile.getString("SB4", i);
						sCp				= tFile.getString("CP", i);
						sLp				= tFile.getString("LP", i);
						// sFechaFin	= tFile.getString("FECHA_FIN", i);
						// sFechaEjecuta= tFile.getString("FECHA_EJECUTA", i);
						// sJobFin 		= tFile.getString("JOBFIN", i);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to get some of the fields of tFile Table for afo_saldo_sie_edad file, on formatTable function. Error: "
										+ e.getMessage());
					}
				
				// Set new format to double values
					if (sSb4 != null && sSb4.length() > 0) {
						Double dSb4 = Double.valueOf(sSb4);
						sSb4 = String.format("%.2f",dSb4);
					}
					
					if (sSb3 != null && sSb3.length() > 0) {
						Double dSb3 = Double.valueOf(sSb3);
						sSb3 = String.format("%.2f",dSb3);
					}
					
					if (sSb2 != null && sSb2.length() > 0) {
						Double dSb2 = Double.valueOf(sSb2);
						sSb2 = String.format("%.2f",dSb2);
					}
					
					if (sSb1 != null && sSb1.length() > 0) {
						Double dSb1 = Double.valueOf(sSb1);
						sSb1 = String.format("%.2f",dSb1);
					}
					
					if (sSb0 != null && sSb0.length() > 0) {
						Double dSb0 = Double.valueOf(sSb0);
						sSb0 = String.format("%.2f",dSb0);
					}
					
					if (sCp != null && sCp.length() > 0) {
						Double dCp = Double.valueOf(sCp);
						sCp = String.format("%.2f",dCp);
					}
					
					if (sLp != null && sLp.length() > 0) {
						Double dLp = Double.valueOf(sLp);
						sLp = String.format("%.2f",dLp);
					}
					
				// Set new values
					try {
						tFile.setString("SB4",i,sSb4);
						tFile.setString("SB3",i,sSb3);
						tFile.setString("SB2",i,sSb2);
						tFile.setString("SB1",i,sSb1);
						tFile.setString("SB0",i,sSb0);
						tFile.setString("CP",i,sCp);
						tFile.setString("LP",i,sLp);
					} catch (OException e) {
						_Log.printMsg(
								EnumTypeMessage.ERROR,
								"Unable to set some new formatted values for afo_saldo_sie_edad file, on formatTable function. Error: "
										+ e.getMessage());
					}
			}
		}
		
		return tFile;
	}

	private Table getTableFromSas(String sFileName) {
		
		Table tValues = Util.NULL_TABLE;
		Writer writer = new StringWriter();
		
		// Add directory path to filename
			sFileName = SAS_FILEPATH + sFileName;
		
		// Read SAS file info and set it into writer buffer
			try {
				File fSasFile = new File(sFileName);
			    InputStream targetStream = new FileInputStream(fSasFile);
				SasFileReader sasFileReader = new SasFileReaderImpl(targetStream);
				CSVDataWriter csvDataWriter = new CSVDataWriterImpl(writer);
				csvDataWriter.writeColumnNames(sasFileReader.getColumns());
				csvDataWriter.writeRowsArray(sasFileReader.getColumns(), sasFileReader.readAll());
			} catch (IOException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to read sas file, on getTableFromSas function. Error: "
								+ e.getMessage());
			}
			
		// Generate temp csv 
			try {
				PrintWriter out = new PrintWriter(TEMP_CSV_FILE);
				out.println(writer.toString());
				out.close();
			} catch (FileNotFoundException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to generate temp csv, on getTableFromSas function. Error: "
								+ e.getMessage());
			}
		
		// Set temp csv file info into Table
			try {
				tValues = Table.tableNew();
				tValues.inputFromCSVFile(TEMP_CSV_FILE);
			} catch (OException e) {
				_Log.printMsg(
						EnumTypeMessage.ERROR,
						"Unable to get temp csv info, on getTableFromSas function. Error: "
								+ e.getMessage());
			}
			
		// Delete temp csv
			File fTempFile = new File(TEMP_CSV_FILE);
			fTempFile.delete();
			
		return tValues;
	}

	private Table readFileNamesFromSasDirectory() {

		int iNewRow = 0;
		Table tFileNames = Util.NULL_TABLE;

		try {
			tFileNames = Table.tableNew();
			tFileNames.addCol("filename", COL_TYPE_ENUM.COL_STRING);
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to create tFileNames Table, on readFileNamesFromSasDirectory function. Error: "
							+ e.getMessage());
		}

		File folder = new File(SAS_FILEPATH);
		File[] listOfFiles = folder.listFiles();

		try {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					String sFileName = listOfFiles[i].getName();
					String sFileExtension = FilenameUtils
							.getExtension(sFileName);

					if (sFileExtension.equalsIgnoreCase("sas7bdat")) {
						iNewRow = tFileNames.addRow();
						tFileNames.setString("filename", iNewRow, sFileName);
					}
				}
			}
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable set some String sFileName on tFileNames Table, on readFileNamesFromSasDirectory function. Error: "
							+ e.getMessage());
		}

		return tFileNames;
	}

	private void initializeGlobalVariables() {
		
		try {
			SAS_FILEPATH = UtilAfore.getVariableGlobal("FINDUR",
					sScriptName, "sas_path");
			
			CSV_FILEPATH = UtilAfore.getVariableGlobal("FINDUR",
					sScriptName, "csv_path");
			
			TEMP_CSV_FILE = CSV_FILEPATH + "tempOutput.csv";
			
		} catch (OException e) {
			_Log.printMsg(
					EnumTypeMessage.ERROR,
					"Unable to set some of the global variables of the script, on initializeGlobalVariables function. Error: "
							+ e.getMessage());
		}
	}
}