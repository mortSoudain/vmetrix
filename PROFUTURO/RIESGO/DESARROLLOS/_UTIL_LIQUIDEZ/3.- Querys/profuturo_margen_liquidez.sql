-- CTE01: Trades asociados a Socios Liquidadores
-- 		Status: New (2), Proposed (7), Validated (3)
--		MValue >>> GenericFut (50), FinFut (41), BondFut (34)
with trades_sl as 
(
	select 	ab.tran_num
			,ab.ins_num
			,h.ticker
			,s.reference subyacente
			,ab.toolset
			,ab.ins_type
			,ab.currency
			,ab.internal_portfolio
			,ab.external_lentity
			,pa.notnl
			,f.name prince_inp_format
			,ab.mvalue
			,ab.price
	from ab_tran ab
			left join header h on h.ins_num = ab.ins_num
			left join ab_tran s on s.ins_num = ab.ins_num
			inner join parameter pa on pa.ins_num = s.ins_num and pa.param_seq_num = 0
			inner join misc_ins mi on mi.ins_num = s.ins_num
			inner join price_input_format f on f.id_number = mi.price_inp_format
	where ab.tran_type = 0
				and ab.tran_status in (2, 3, 7)
				and ab.asset_type = 2
				and ab.toolset in (50, 41, 34)
				and s.tran_type = 2
				and s.tran_status = 3
),
-- CTE02: Position asociados a Socios Liquidadores
--		Se separa por Siefore, moneda y socio liquidador para uso compartido con otros propositos
position_sl as 
(
	select 	t.ins_num
			,t.currency
			,t.internal_portfolio
			,t.external_lentity
			,sum(t.mvalue) ohd_position
	from trades_sl t
	group by t.ins_num, t.currency, t.internal_portfolio, t.external_lentity
),
-- CTE03: MtoM de la cartera asociada a Socios Liquidadores
--		Incorpora los redondeos de las monedas de origen. Cuadra con Base MtM nativo
base_mtm_sl as 
(
	select 	t.tran_num
			,t.ins_num
			,t.ticker
			,t.subyacente
			,t.toolset
			,t.ins_type
			,t.currency
			,t.internal_portfolio
			,t.external_lentity
			,t.mvalue
			,vt.precio_sucio_24h precio_ticker
			,vs.precio_sucio_24h precio_sub
			,t.notnl
			,t.price
			,fx.precio_sucio_24h
			,case 	when t.toolset = 50 
							then round(t.mvalue * vt.precio_sucio_24h/NVL(fx.precio_sucio_24h, 1)/100.0, c.round) * NVL(fx.precio_sucio_24h, 1)
						when t.toolset = 34
							then round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price * 100.0), c.round) * NVL(fx.precio_sucio_24h, 1)
						else round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price), c.round) * NVL(fx.precio_sucio_24h, 1)
					end ohd_base_mtm
	from trades_sl t
			left join currency c on c.id_number = t.currency
			left join user_mx_vectorprecios_diario vs on t.subyacente = vs.ticker
			left join user_mx_vectorprecios_diario vt on t.ticker = vt.ticker
			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		
																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	
),
-- CTE04: Agrupa MtoM por siefore, moneda y socio liquidador
resumen_mtm_sl as
(
	select 	internal_portfolio
			,currency
			,external_lentity
			,sum(ohd_base_mtm) ohd_total_mtm
	from base_mtm_sl
	group by internal_portfolio, currency, external_lentity
),
-- CTE05: Listado de escenarios de VaR (Matriz Esc Moviles) por Siefore y Socio
escenarios_sl as
(
	select 	row_number() over(partition by p.internal_portfolio, p.external_lentity order by sum(p.ohd_position * m.var_consar)) rec
			,m.id
			,p.internal_portfolio
			,p.currency
			,p.external_lentity
			,sum(p.ohd_position * m.var_consar) ohd_exposition
	from position_sl p
			inner join user_mx_varconsar_escenarios m
						on m.ins_num = p.ins_num
	group by m.id, p.internal_portfolio, p.currency, p.external_lentity
),
-- CTE06: Extrae escenario k configurado por siefore
esc_var as
(
	select 	p.id_number internal_portfolio
            ,to_number(valor) k
	from user_configurable_variables u
			left join portfolio p on p.name = u.variable
	where proceso = 'Escenarios_CONSAR'
),
-- CTE07: Retorna el Margen de Liquidez por Siefore, Moneda y Socio
--		Toma en cuenta posicion especifica por Socio Liquidador (se usa para computo de Margen de Liquidez oficial)
margen_liquidez_sl as
(
	select 	p.name siefore
			,c.name currency
			,s.short_name socio
			,u.k
			,d.business_date fecha_var
			,sum(case when e.rec = u.k then e.id else 0.0 end) escenario
			,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc
			,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var
			,-avg(e.ohd_exposition) ohd_var_condicional
			,r.ohd_total_mtm
			,case when r.ohd_total_mtm > -avg(e.ohd_exposition) then r.ohd_total_mtm else -avg(e.ohd_exposition) end ohd_margen_liq
	from esc_var u 
			inner join escenarios_sl e on e.internal_portfolio = u.internal_portfolio
			left join system_dates d on 1 = 1
			left join resumen_mtm_sl r on r.internal_portfolio = e.internal_portfolio and r.external_lentity = e.external_lentity
			left join currency c on c.id_number = e.currency
			left join portfolio p on p.id_number = e.internal_portfolio
			left join party s on s.party_id = e.external_lentity
	where e.rec <= u.k
	group by p.name, c.name, s.short_name, u.k, d.business_date, r.ohd_total_mtm
)
select *
from margen_liquidez_sl
order by currency, socio, siefore