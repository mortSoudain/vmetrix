-- Limite Liquidez en Chequera
-- CTE01: Cuentas de saldos para obtener saldos en chequeras y otros montos de cobros y pagos
with cuentas as
(
	select 	ajev.acs_posting_date
			,ajev.acs_fin_stmt_id
			,case	when ajev.acs_account_number = '2101-0002' then 'pago_mxn_0d'
					when ajev.acs_account_number = '1301-0002' then 'cobro_mxn_0d'
					else substr(ajev.acs_account_number, 1, 4) 
			end cuenta
			,round(ajev.acs_account_amount, 2) acs_account_amount
	from acs_journal_entries_dual_view ajev
	where ajev.acs_account_number in (	'1102-0001', 
										'1103-0001', 
										'1104-0001', 
										'1301-0002', '1301-0003', '1301-0004', '1301-0009', 
										'2101-0002', '2101-0003', '2101-0004', '2101-0009', 
										'2108-0001')
),
-- CTE02: Saldos contables para chequeras y montos a cobrar y pagar en MXN y USD
chequera as 
(
	select 	cfg.name siefore
			-- Disponible Chequera MXN - Inicio de dia: [1102 + 2101-0002 + 1301-0002]
			,sum(case 	when c.cuenta = '1102' and c.acs_posting_date = d.prev_business_date and c.acs_account_amount > 0 
						then c.acs_account_amount 
						else 0 
				end) ohd_saldo_1102
			,sum(case when c.cuenta = 'pago_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_2101_0002
			,sum(case when c.cuenta = 'cobro_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_1301_0002

			-- Disponible Chequera USD - Inicio de dia: [1103 - 2108]
			,sum(case 	when c.cuenta = '1103' then c.acs_account_amount 
						else 0 end
			) ohd_saldo_1103

			,sum(case 	when c.cuenta = '2108' then c.acs_account_amount 
						else 0 end
			) ohd_saldo_2108

			-- Cobrar USD: [1104]
			,sum(case when c.cuenta = '1104' then c.acs_account_amount else 0 end) ohd_saldo_1104
			-- Pagar USD: [0]
			,0.0 ohd_pagar_usd
			-- Cobrar MXN - 24 a 96h: 1301
			,sum(case when c.cuenta = '1301' then c.acs_account_amount else 0 end) ohd_saldo_1301
			-- Pagar MXN - 24 a 96h: 2101
			,sum(case when c.cuenta = '2101' then c.acs_account_amount else 0 end) ohd_saldo_2101

	from cuentas c
		left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id
		left join system_dates d on 1 = 1 
	group by cfg.name
),
-- CTE03: Pagos de Amortizaciones e intereses de instrumentos en cartera. Se obtiene a partir de los CASH generados en la ma�ana
--		Se filtran los CASH con asset_type igual a "Security Inventory Payment" (8)
amort_interest as 
(
	select 	p.name siefore
			,c.name currency
			,sum(v.position * NVL(fx.precio_sucio_24h, 1)) ohd_monto_venc
	from ab_tran v
			inner join system_dates d on d.business_date = v.trade_date 
			left join currency c on c.id_number = v.currency
			left join portfolio p on p.id_number = v.internal_portfolio

			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		
																				concat(concat(concat('*C_MXP', c.name), '_'), c.name))	
	where v.tran_type = 0
			and v.tran_status = 3
			and v.toolset = 10
			and v.asset_type = 8
			and d.business_date = v.settle_date
	group by p.name, c.name
),
-- CTE03: Movimientos de Cash que impactan el disponible en Chequera
movimientos as 
(
	-- FX (9) y Cash (10) se toma el position en vez del proceeds
	-- Status: New (2), Proposed (7), Validated (3)
	-- Cflow Comisiones SL (2031) debe ir con signo contrario. Lo ingresan desde el punto de vista del SL en Conta
	select  p.name siefore
	        ,c.name currency
	        ,sum(case when ab.toolset in (9, 10) 
	        			then (	case when c.name = 'USD'
	        							then (	case when ab.cflow_type in (271, 2031) 
	        										then -ab.position * v.precio_sucio_24h 
	        										else ab.position * v.precio_sucio_24h 
	        									end )
	        						when c.name = 'MXN'
	        							then (	case when ab.cflow_type in (271, 2031) 
	        										then -ab.position
	        										else ab.position
	        									end )
	        					end)
	        			else ab.proceeds 
	        	end) ohd_movimiento
	from ab_tran ab
	        inner join system_dates d
	                on ab.trade_date = d.business_date and ab.settle_date = d.business_date
	        left join portfolio p on p.id_number = ab.internal_portfolio
	        left join currency c on c.id_number = ab.currency
	        left join user_mx_vectorprecios_diario v on v.ticker = '*C_MXPUSD_FIX'
	where ab.tran_type = 0
	        and ab.tran_status in (2, 3, 7)
	        and (ab.cflow_type in (266, 271, 2017, 2031) or ab.toolset not in (6, 10))
	group by p.name, c.name
	order by p.name, c.name
)
select 	c.siefore
		,'Chequera MXN' currency
		,c.ohd_saldo_1102
		,c.ohd_saldo_2101_0002
		,c.ohd_saldo_1301_0002
		,'[1102 + 2101-0002 + 1301-0002]' form_cheq_inicio_dia
		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) ohd_disp_inicio_dia
		,NVL(mov.ohd_movimiento, 0) ohd_movimiento
		,'[1102 + 2101-0002 + 1301-0002 + Movimientos]' form_cheq_disponible
		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_disponible
		,c.ohd_saldo_1301
		,c.ohd_saldo_2101
from chequera c
		left join movimientos mov on mov.siefore = c.siefore and mov.currency = 'MXN'
		left join amort_interest a on a.siefore = c.siefore and a.currency = 'MXN'
