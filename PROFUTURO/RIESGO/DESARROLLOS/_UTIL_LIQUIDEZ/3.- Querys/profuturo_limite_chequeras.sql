-- Limite Liquidez en Chequera
-- CTE01: Cuentas de saldos para obtener saldos en chequeras y otros montos de cobros y pagos
with cuentas as
(
	select 	ajev.acs_posting_date
			,ajev.acs_fin_stmt_id
			,case	when ajev.acs_account_number = '2101-0002' then 'pago_mxn_0d'
					when ajev.acs_account_number = '1301-0002' then 'cobro_mxn_0d'
					else substr(ajev.acs_account_number, 1, 4) 
			end cuenta
			,round(ajev.acs_account_amount, 2) acs_account_amount
	from acs_journal_entries_dual_view ajev
	where ajev.acs_account_number in (	'1102-0001', 
										'1103-0001', 
										'1104-0001', 
										'1301-0002', '1301-0003', '1301-0004', '1301-0009', 
										'2101-0002', '2101-0003', '2101-0004', '2101-0009', 
										'2108-0001')
),
-- CTE02: Saldos contables para chequeras y montos a cobrar y pagar en MXN y USD
chequera as 
(
	select 	cfg.name siefore
			-- Disponible Chequera MXN - Inicio de dia: [1102 + 2101-0002 + 1301-0002]
			,sum(case 	when c.cuenta = '1102' and c.acs_posting_date = d.prev_business_date and c.acs_account_amount > 0 
						then c.acs_account_amount 
						else 0 
				end) ohd_saldo_1102
			,sum(case when c.cuenta = 'pago_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_2101_0002
			,sum(case when c.cuenta = 'cobro_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_1301_0002

			-- Disponible Chequera USD - Inicio de dia: [1103 - 2108]
			,sum(case 	when c.cuenta = '1103' then c.acs_account_amount 
						else 0 end
			) ohd_saldo_1103

			,sum(case 	when c.cuenta = '2108' then c.acs_account_amount 
						else 0 end
			) ohd_saldo_2108

			-- Cobrar USD: [1104]
			,sum(case when c.cuenta = '1104' then c.acs_account_amount else 0 end) ohd_saldo_1104
			-- Pagar USD: [0]
			,0.0 ohd_pagar_usd
			-- Cobrar MXN - 24 a 96h: 1301
			,sum(case when c.cuenta = '1301' then c.acs_account_amount else 0 end) ohd_saldo_1301
			-- Pagar MXN - 24 a 96h: 2101
			,sum(case when c.cuenta = '2101' then c.acs_account_amount else 0 end) ohd_saldo_2101

	from cuentas c
		left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id
		left join system_dates d on 1 = 1 
	group by cfg.name
),
-- CTE03: Pagos de Amortizaciones e intereses de instrumentos en cartera. Se obtiene a partir de los CASH generados en la ma�ana
--		Se filtran los CASH con asset_type igual a "Security Inventory Payment" (8)
amort_interest as 
(
	select 	p.name siefore
			,c.name currency
			,sum(v.position * NVL(fx.precio_sucio_24h, 1)) ohd_monto_venc
	from ab_tran v
			inner join system_dates d on d.business_date = v.trade_date 
			left join currency c on c.id_number = v.currency
			left join portfolio p on p.id_number = v.internal_portfolio

			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		
																				concat(concat(concat('*C_MXP', c.name), '_'), c.name))	
	where v.tran_type = 0
			and v.tran_status = 3
			and v.toolset = 10
			and v.asset_type = 8
			and d.business_date = v.settle_date
	group by p.name, c.name
),
-- CTE04: Movimientos de Cash que impactan el disponible en Chequera
movimientos as 
(
	-- FX (9) y Cash (10) se toma el position en vez del proceeds
	-- Status: New (2), Proposed (7), Validated (3)
	-- Cflow Comisiones SL (2031) debe ir con signo contrario. Lo ingresan desde el punto de vista del SL en Conta
	select  p.name siefore
	        ,c.name currency
	        ,sum(case when ab.toolset in (9, 10) 
	        			then (	case when c.name = 'USD'
	        							then (	case when ab.cflow_type in (271, 2031) 
	        										then -ab.position * v.precio_sucio_24h 
	        										else ab.position * v.precio_sucio_24h 
	        									end )
	        						when c.name = 'MXN'
	        							then (	case when ab.cflow_type in (271, 2031) 
	        										then -ab.position
	        										else ab.position
	        									end )
	        					end)
	        			else ab.proceeds 
	        	end) ohd_movimiento
	from ab_tran ab
	        inner join system_dates d
	                on ab.trade_date = d.business_date and ab.settle_date = d.business_date
	        left join portfolio p on p.id_number = ab.internal_portfolio
	        left join currency c on c.id_number = ab.currency
	        left join user_mx_vectorprecios_diario v on v.ticker = '*C_MXPUSD_FIX'
	where ab.tran_type = 0
	        and ab.tran_status in (2, 3, 7)
	        and (ab.cflow_type in (266, 271, 2017, 2031) or ab.toolset not in (6, 10))
	group by p.name, c.name
	order by p.name, c.name
),
-- CTE05: Limite operativo por Siefore (porcentaje del NAV)
limiteOP as
(
	select 	n.nav_portfolio siefore
			,n.nav_value
			,f.factor ohd_factor
			,n.nav_value * f.factor/100.0 ohd_limop
	from user_mx_historical_nav n
			inner join user_mx_chequera_nav_factor f
				on f.portfolio = n.nav_portfolio
),
-- CTE06: Trades asociados a Socios Liquidadores (COMPARTIDO)
-- 		Status: New (2), Proposed (7), Validated (3)
--		MValue >>> GenericFut (50), FinFut (41), BondFut (34)
trades_sl as 
(
	select 	ab.tran_num
			,ab.ins_num
			,h.ticker
			,s.reference subyacente
			,ab.toolset
			,ab.ins_type
			,ab.currency
			,ab.internal_portfolio
			,ab.external_lentity
			,pa.notnl
			,f.name prince_inp_format
			,ab.mvalue
			,ab.price
	from ab_tran ab
			left join header h on h.ins_num = ab.ins_num
			left join ab_tran s on s.ins_num = ab.ins_num
			inner join parameter pa on pa.ins_num = s.ins_num and pa.param_seq_num = 0
			inner join misc_ins mi on mi.ins_num = s.ins_num
			inner join price_input_format f on f.id_number = mi.price_inp_format
	where ab.tran_type = 0
				and ab.tran_status in (2, 3, 7)
				and ab.asset_type = 2
				and ab.toolset in (50, 41, 34)
				and s.tran_type = 2
				and s.tran_status = 3
),
-- CTE07: Position asociados a Socios Liquidadores (COMPARTIDO)
--		Se separa por Siefore, Moneda y socio liquidador para uso compartido con otros propositos
position_sl as 
(
	select 	t.ins_num
			,t.currency
			,t.internal_portfolio
			,t.external_lentity
			,sum(t.mvalue) ohd_position
	from trades_sl t
	group by t.ins_num, t.currency, t.internal_portfolio, t.external_lentity
),
-- CTE08: MtoM de la cartera asociada a Socios Liquidadores (COMPARTIDO)
--		Incorpora los redondeos de las monedas de origen. Cuadra con Base MtM nativo
base_mtm_sl as 
(
	select 	t.tran_num
			,t.ins_num
			,t.ticker
			,t.subyacente
			,t.toolset
			,t.ins_type
			,t.currency
			,t.internal_portfolio
			,t.external_lentity
			,t.mvalue
			,vt.precio_sucio_24h precio_ticker
			,vs.precio_sucio_24h precio_sub
			,t.notnl
			,t.price
			,fx.precio_sucio_24h
			,case 	when t.toolset = 50 
							then round(t.mvalue * vt.precio_sucio_24h/NVL(fx.precio_sucio_24h, 1)/100.0, c.round) * NVL(fx.precio_sucio_24h, 1)
						when t.toolset = 34
							then round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price * 100.0), c.round) * NVL(fx.precio_sucio_24h, 1)
						else round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price), c.round) * NVL(fx.precio_sucio_24h, 1)
					end ohd_base_mtm
	from trades_sl t
			left join currency c on c.id_number = t.currency
			left join user_mx_vectorprecios_diario vs on t.subyacente = vs.ticker
			left join user_mx_vectorprecios_diario vt on t.ticker = vt.ticker
			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		
																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	
),
-- CTE09: Agrupa MtoM por siefore y moneda (NO COMPARTIDO)
resumen_mtm_ccy as
(
	select 	internal_portfolio
			,currency
			,sum(ohd_base_mtm) ohd_total_mtm
	from base_mtm_sl
	group by internal_portfolio, currency
),
-- CTE10: Listado de escenarios de VaR (Matriz Esc Moviles) por Siefore y Moneda (NO COMPARTIDO)
escenarios_ccy as
(
	select 	row_number() over(partition by p.internal_portfolio, p.currency order by sum(p.ohd_position * m.var_consar)) rec
			,m.id
			,p.internal_portfolio
			,p.currency
			,sum(p.ohd_position * m.var_consar) ohd_exposition
	from position_sl p
			inner join user_mx_varconsar_escenarios m
						on m.ins_num = p.ins_num
	group by m.id, p.internal_portfolio, p.currency
),
-- CTE11: Extrae escenario k configurado por siefore (COMPARTIDO)
esc_var as
(
	select 	p.id_number internal_portfolio
            ,to_number(valor) k
	from user_configurable_variables u
			left join portfolio p on p.name = u.variable
	where proceso = 'Escenarios_CONSAR'
),
-- CTE12: Retorna el Margen de Liquidez por Siefore y Moneda (NO COMPARTIDO)
--		No toma en cuenta posicion especifica por Socio Liquidador (se usa para Limite Chequera)
margen_liquidez_ccy as
(
	select 	p.name siefore
			,c.name currency
			,u.k
			,d.business_date fecha_var
			,sum(case when e.rec = u.k then e.id else 0.0 end) escenario
			,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc
			,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var
			,-avg(e.ohd_exposition) ohd_var_condicional
			,r.ohd_total_mtm
			,case when r.ohd_total_mtm > -avg(e.ohd_exposition) then r.ohd_total_mtm else -avg(e.ohd_exposition) end ohd_margen_liq
	from esc_var u 
			inner join escenarios_ccy e on e.internal_portfolio = u.internal_portfolio
			left join system_dates d on 1 = 1
			left join resumen_mtm_ccy r on r.internal_portfolio = e.internal_portfolio and r.currency = e.currency
			left join currency c on c.id_number = e.currency
			left join portfolio p on p.id_number = e.internal_portfolio
	where e.rec <= u.k
	group by p.name, c.name, u.k, d.business_date, r.ohd_total_mtm
)
select 	c.siefore
		,'Chequera MXN' currency
		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) ohd_disp_inicio_dia
		,l.nav_value
		,l.ohd_factor 
		,l.ohd_limop
		,c.ohd_saldo_1301 ohd_cobros96h
		,c.ohd_saldo_2101 ohd_pagos96h
		,c.ohd_saldo_1301 + c.ohd_saldo_2101 ohd_neto96h
		,NVL(ml.ohd_margen_liq, 0) ohd_margen_liq_agg
		,c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0) ohd_req_obl
		,NVL(mov.ohd_movimiento, 0) ohd_movimiento
		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_disponible
		,case when l.ohd_limop > c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0)
				then l.ohd_limop 
				else c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0)
		end ohd_limite
from chequera c
		left join limiteOP l on l.siefore = c.siefore
		left join margen_liquidez_ccy ml on ml.siefore = c.siefore and ml.currency = 'MXN'
		left join movimientos mov on mov.siefore = c.siefore and mov.currency = 'MXN'
		left join amort_interest a on a.siefore = c.siefore and a.currency = 'MXN'

union

select 	c.siefore
		,'Chequera USD' currency
		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) ohd_disp_inicio_dia
		,l.nav_value
		,0.0 ohd_factor
		,0.0 ohd_limop
		,c.ohd_saldo_1104 ohd_cobros96h
		,c.ohd_pagar_usd ohd_pagos96h
		,c.ohd_saldo_1104 - c.ohd_pagar_usd ohd_neto96h
		,NVL(ml.ohd_margen_liq, 0) ohd_margen_liq_agg
		,c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0) ohd_req_obl
		,NVL(mov.ohd_movimiento, 0) ohd_movimiento
		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_disponible
		,case when c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0) < 0 
				then 0 
				else c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0)
		end ohd_limite
from chequera c
		left join limiteOP l on l.siefore = c.siefore
		left join margen_liquidez_ccy ml on ml.siefore = c.siefore and ml.currency = 'USD'
		left join movimientos mov on mov.siefore = c.siefore and mov.currency = 'USD'
		left join amort_interest a on a.siefore = c.siefore and a.currency = 'USD'