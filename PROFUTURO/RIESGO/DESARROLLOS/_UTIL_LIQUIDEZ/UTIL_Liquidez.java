/*$Header: v 1.2, 07/05/2018 $*/
/**
 * File Name:              UTIL_Liquidez.java
 * 
 * Author:                 Basthian Matthews - VMetrix International
 * Creation Date:          10 de Abril del 2018
 * Version:                1.0
 * Description:            Utilitario para la obtención de resultados de liquidez.
 *
 *
 * REVISION HISTORY
 * Date:					Mayo 2018
 * Version/Autor:			1.1 - Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				- Se cambian valores en duro en las querys por sus enumaradores
 *							- Se cambian joins entre tablas de campos string a campos int
 *							(se compara por id's en vez de nombres en caso de siefore y currency)
 *
 * Date:					Mayo 2018
 * Version/Autor:			1.2 - Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				- Se incluye una linea descriptiva en la definicion de las funciones
 *							que indica las columnas que trae la ejecución de la query
 *
 ************************************************************************************/ 

package com.afore.util.risk.query;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsUserCflowType;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class UTIL_Liquidez {
	
	private int iQueryID = 0;
	private static final String QUERY_INIT_WITH			=	"WITH";
	private static final String QUERY_SEPARATOR_COMMA 	=	",";
	//private static final String QUERY_UNION_ALL		=	"UNION ALL";
	
	public static final int OUT_Cheq_SaldosConta		=	1;
	public static final int OUT_Cheq_Disponible_MXN		=	2;
	public static final int OUT_Cheq_Disponible_USD		=	3;
	public static final int OUT_Cheq_LimiteOperativo	=	4;
	public static final int OUT_SL_Trades_SL			=	5;
	public static final int OUT_SL_Position_SL			=	6;
	public static final int OUT_SL_BaseMtoM_SL			=	7;
	public static final int OUT_SL_MargenLiquidez_SL	=	8;
	public static final int OUT_SL_MargenLiquidez_CCY	=	9;
	public static final int OUT_Cheq_Limites			=	10;
		
	/**
	 * @Type Implementacion de Util Liquidez con filtro de Query ID
	 * @Description Permite traspasar el QueryID proveniente de simulaciones para dar los<br>
	 * correspondientes filtros mediante la tabla query_result.<br>
	 * Cuando el input es cero(0) se calcula en base a toda la cartera.
	 */
	public UTIL_Liquidez(int iInput){
		iQueryID = iInput;
	} 
	/**
	 * @Type Implementacion de Util Liquidez sin filtros
	 * @Description Implementa el Util Liquidez para construccion de queries. Sin parametros significa procesar la cartera completa.
	 */
	public UTIL_Liquidez(){
		iQueryID = 0;
	} 
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CREACION DE TABLAS CTE ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @Type CTE Query para obtencion de cuentas en chequeras
	 * @Description Cuentas de saldos para obtener saldos en chequeras y otros montos de cobros y pagos
	 * @Columns
	 * 	     - acs_posting_date
	 *	<br> - acs_fin_stmt_id
	 *	<br> - cuenta
	 *	<br> - acs_account_amount
	 * @return String con formato SQL 
	 */
	protected String getCTE_Cheq_CuentasConta()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n cuentas as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	ajev.acs_posting_date ");
		sb.append("\n 			,ajev.acs_fin_stmt_id ");
		sb.append("\n 			,case	when ajev.acs_account_number = '2101-0002' then 'pago_mxn_0d' ");
		sb.append("\n 					when ajev.acs_account_number = '1301-0002' then 'cobro_mxn_0d' ");
		sb.append("\n 					else substr(ajev.acs_account_number, 1, 4)  ");
		sb.append("\n 			end cuenta ");
		sb.append("\n 			,round(ajev.acs_account_amount, 2) acs_account_amount ");
		sb.append("\n 	from acs_journal_entries_dual_view ajev ");
		sb.append("\n 	where ajev.acs_account_number in (	'1102-0001',  ");
		sb.append("\n 										'1103-0001',  ");
		sb.append("\n 										'1104-0001',  ");
		sb.append("\n 										'1301-0002', '1301-0003', '1301-0004', '1301-0009',  ");
		sb.append("\n 										'2101-0002', '2101-0003', '2101-0004', '2101-0009',  ");
		sb.append("\n 										'2108-0001') ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de saldos
	 * @Description Saldos contables para chequeras y montos a cobrar y pagar en MXN y USD
	 * @Columns
	 * 	     - siefore
	 *	<br> - ohd_saldo_1102
	 *	<br> - ohd_saldo_2101_0002
	 *	<br> - ohd_saldo_1301_0002
	 *	<br> - ohd_saldo_1103
	 *	<br> - ohd_saldo_2108
	 *	<br> - ohd_saldo_1104
	 *	<br> - ohd_pagar_usd
	 *	<br> - ohd_saldo_1301
	 *	<br> - ohd_saldo_2101
	 * @return String con formato SQL 
	 */
	protected String getCTE_Cheq_SaldosConta()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n chequera as  ");
		sb.append("\n ( ");
		sb.append("\n 	select 	cfg.portfolio_id siefore ");
		sb.append("\n 			-- Disponible Chequera MXN - Inicio de dia: [1102 + 2101-0002 + 1301-0002] ");
		sb.append("\n 			,sum(case 	when c.cuenta = '1102' and c.acs_posting_date = d.prev_business_date and c.acs_account_amount > 0  ");
		sb.append("\n 						then c.acs_account_amount  ");
		sb.append("\n 						else 0  ");
		sb.append("\n 				end) ohd_saldo_1102 ");
		sb.append("\n 			,sum(case when c.cuenta = 'pago_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_2101_0002 ");
		sb.append("\n 			,sum(case when c.cuenta = 'cobro_mxn_0d' then c.acs_account_amount else 0 end) ohd_saldo_1301_0002 ");
		sb.append("\n  ");
		sb.append("\n 			-- Disponible Chequera USD - Inicio de dia: [1103 - 2108] ");
		sb.append("\n 			,sum(case 	when c.cuenta = '1103' then c.acs_account_amount  ");
		sb.append("\n 						else 0 end ");
		sb.append("\n 			) ohd_saldo_1103 ");
		sb.append("\n  ");
		sb.append("\n 			,sum(case 	when c.cuenta = '2108' then c.acs_account_amount  ");
		sb.append("\n 						else 0 end ");
		sb.append("\n 			) ohd_saldo_2108 ");
		sb.append("\n  ");
		sb.append("\n 			-- Cobrar USD: [1104] ");
		sb.append("\n 			,sum(case when c.cuenta = '1104' then c.acs_account_amount else 0 end) ohd_saldo_1104 ");
		sb.append("\n 			-- Pagar USD: [0] ");
		sb.append("\n 			,0.0 ohd_pagar_usd ");
		sb.append("\n 			-- Cobrar MXN - 24 a 96h: 1301 ");
		sb.append("\n 			,sum(case when c.cuenta = '1301' then c.acs_account_amount else 0 end) ohd_saldo_1301 ");
		sb.append("\n 			-- Pagar MXN - 24 a 96h: 2101 ");
		sb.append("\n 			,sum(case when c.cuenta = '2101' then c.acs_account_amount else 0 end) ohd_saldo_2101 ");
		sb.append("\n  ");
		sb.append("\n 	from cuentas c ");
		sb.append("\n 		left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id ");
		sb.append("\n 		left join system_dates d on 1 = 1  ");
		sb.append("\n 	group by cfg.portfolio_id ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	
	/**
	 * @Type CTE Query para obtención de amortizaciones e intereses
	 * @Description Pagos de Amortizaciones e intereses de instrumentos en cartera.
	 * <br>Se obtiene a partir de los CASH generados en la mañana
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - ohd_monto_venc
	 * @return String con formato SQL 
	 */
	protected String getCTE_Cheq_Amortizaciones_Intereses()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n  amort_interest as   ");
		sb.append("\n  (  ");
		sb.append("\n   	select 	v.internal_portfolio siefore  ");
		sb.append("\n  			,v.currency currency  ");
		sb.append("\n  			,sum(v.position * NVL(fx.precio_sucio_24h, 1)) ohd_monto_venc  ");
		sb.append("\n  	from ab_tran v  ");
		sb.append("\n  			inner join system_dates d on d.business_date = v.trade_date   ");
		sb.append("\n  			left join currency c on c.id_number = v.currency    ");
		sb.append("\n  			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		  ");
		sb.append("\n  																				concat(concat(concat('*C_MXP', c.name), '_'), c.name))	  ");
		sb.append("\n  	where v.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + "  ");
		sb.append("\n  			and v.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "  ");
		sb.append("\n  			and v.toolset = " + TOOLSET_ENUM.CASH_TOOLSET.toInt() + "  ");
		sb.append("\n  			and v.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_SEC_INV_PYMT.toInt() + "  ");
		sb.append("\n  			and d.business_date = v.settle_date  ");
		sb.append("\n  	group by v.internal_portfolio, v.currency  ");
		sb.append("\n  ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de movimientos
	 * @Description Movimientos de Trading que impactan disponible en chequera
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - ohd_movimiento
	 * @return String con formato SQL 
	 */
	protected String getCTE_Cheq_Mov_Trading()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n movimientos as  ");
		sb.append("\n ( ");
		sb.append("\n 	-- FX (9) y Cash (10) se toma el position en vez del proceeds ");
		sb.append("\n 	-- Status: New (2), Proposed (7), Validated (3) ");
		sb.append("\n 	-- Cflow Comisiones SL (2031) debe ir con signo contrario. Lo ingresan desde el punto de vista del SL en Conta ");
		sb.append("\n 	select  ab.internal_portfolio siefore ");
		sb.append("\n 	        ,ab.currency currency ");
		sb.append("\n 	        ,sum(case when ab.toolset in (" + TOOLSET_ENUM.FX_TOOLSET.toInt() + ", " + TOOLSET_ENUM.CASH_TOOLSET.toInt() + ")  ");
		sb.append("\n 	        			then (	case when c.name = 'USD' ");
		sb.append("\n 	        							then (	case when ab.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_COMISIONES_SL.toInt() + ")  ");
		sb.append("\n 	        										then -ab.position * v.precio_sucio_24h  ");
		sb.append("\n 	        										else ab.position * v.precio_sucio_24h  ");
		sb.append("\n 	        									end ) ");
		sb.append("\n 	        						when c.name = 'MXN' ");
		sb.append("\n 	        							then (	case when ab.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_COMISIONES_SL.toInt() + ")  ");
		sb.append("\n 	        										then -ab.position ");
		sb.append("\n 	        										else ab.position ");
		sb.append("\n 	        									end ) ");
		sb.append("\n 	        					end) ");
		sb.append("\n 	        			else ab.proceeds  ");
		sb.append("\n 	        	end) ohd_movimiento ");
		sb.append("\n 	from ab_tran ab ");
		if (iQueryID>0)
			sb.append("\n		inner join query_result q on q.query_result = ab.tran_num and q.unique_id = " + iQueryID);
		sb.append("\n 	        inner join system_dates d ");
		sb.append("\n 	                on ab.trade_date = d.business_date and ab.settle_date = d.business_date ");
		sb.append("\n 	        left join currency c on c.id_number = ab.currency ");
		sb.append("\n 	        left join user_mx_vectorprecios_diario v on v.ticker = '*C_MXPUSD_FIX' ");
		sb.append("\n 	where ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " ");
		sb.append("\n 	        and ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() + ") ");
		sb.append("\n 	        and (ab.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_MOV_TRABAJADORES.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_MARGIN_CALL_AIMS.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_COMISION_SALDO.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_COMISIONES_SL.toInt() + ") or ab.toolset not in (" + TOOLSET_ENUM.LOANDEP_TOOLSET.toInt() + ", " + TOOLSET_ENUM.CASH_TOOLSET.toInt() + ")) ");
		sb.append("\n 	group by ab.internal_portfolio, ab.currency ");
		sb.append("\n 	order by ab.internal_portfolio, ab.currency ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de limites operativos
	 * @Description Limite operativo por Siefore (porcentaje del NAV)
	 * @Columns
	 * 	     - siefore
	 *	<br> - nav_value
	 *	<br> - ohd_factor
	 *	<br> - ohd_limop
	 * @return String con formato SQL 
	 */
	protected String getCTE_Cheq_LimiteOperativo()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n limiteOP as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	n.nav_portfolio_id siefore ");
		sb.append("\n 			,n.nav_value ");
		sb.append("\n 			,f.factor ohd_factor ");
		sb.append("\n 			,n.nav_value * f.factor/100.0 ohd_limop ");
		sb.append("\n 	from user_mx_historical_nav n ");
		sb.append("\n 			inner join user_mx_chequera_nav_factor f ");
		sb.append("\n 				on f.portfolio = n.nav_portfolio ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de trades asociados a socios liquidadores
	 * @Description Trades asociados a Socios Liquidadores
 	 * <br>Status: New (2), Proposed (7), Validated (3)
	 * <br>MValue >>> GenericFut (50), FinFut (41), BondFut (34)
	 * @Columns
	 * 	     - tran_num
	 *	<br> - ins_num
	 *	<br> - ticker
	 *	<br> - subyacente
	 *	<br> - toolset
	 *	<br> - ins_type
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - notnl
	 *	<br> - prince_inp_format
	 *	<br> - mvalue
	 *	<br> - price
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_Trades()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n trades_sl as  ");
		sb.append("\n ( ");
		sb.append("\n 	select 	ab.tran_num ");
		sb.append("\n 			,ab.ins_num ");
		sb.append("\n 			,h.ticker ");
		sb.append("\n 			,s.reference subyacente ");
		sb.append("\n 			,ab.toolset ");
		sb.append("\n 			,ab.ins_type ");
		sb.append("\n 			,ab.currency ");
		sb.append("\n 			,ab.internal_portfolio ");
		sb.append("\n 			,ab.external_lentity ");
		sb.append("\n 			,pa.notnl ");
		sb.append("\n 			,f.name prince_inp_format ");
		sb.append("\n 			,ab.mvalue ");
		sb.append("\n 			,ab.price ");
		sb.append("\n 	from ab_tran ab ");
		if (iQueryID>0)
			sb.append("\n		inner join query_result q on q.query_result = ab.tran_num and q.unique_id = " + iQueryID);
		sb.append("\n 			left join header h on h.ins_num = ab.ins_num ");
		sb.append("\n 			left join ab_tran s on s.ins_num = ab.ins_num ");
		sb.append("\n 			inner join parameter pa on pa.ins_num = s.ins_num and pa.param_seq_num = 0 ");
		sb.append("\n 			inner join misc_ins mi on mi.ins_num = s.ins_num ");
		sb.append("\n 			inner join price_input_format f on f.id_number = mi.price_inp_format ");
		sb.append("\n 	where ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() + " ");
		sb.append("\n 				and ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() + ") ");
		sb.append("\n 				and ab.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt() + " ");
		sb.append("\n 				and ab.toolset in (" + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ", " +  TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + ", " +  TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + ") ");
		sb.append("\n 				and s.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() + " ");
		sb.append("\n 				and s.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + " ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de position asociados a socios liquidadores y agrupado por moneda
	 * @Description Position agrupados por Siefore, Socio Liquidador y Moneda
 	 * <br>Se separa por Siefore, Moneda y socio liquidador para uso compartido con otros propositos
	 * @Columns
	 * 	     - ins_num
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - ohd_position
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_Position()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n position_sl as  ");
		sb.append("\n ( ");
		sb.append("\n 	select 	t.ins_num ");
		sb.append("\n 			,t.currency ");
		sb.append("\n 			,t.internal_portfolio ");
		sb.append("\n 			,t.external_lentity ");
		sb.append("\n 			,sum(t.mvalue) ohd_position ");
		sb.append("\n 	from trades_sl t ");
		sb.append("\n 	group by t.ins_num, t.currency, t.internal_portfolio, t.external_lentity ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de cartera asociada a Socios Liquidadores
	 * @Description MtoM de la cartera asociada a Socios Liquidadores
 	 * <br>Incorpora los redondeos de las monedas de origen. Cuadra con Base MtM nativo
	 * @Columns
	 * 	     - tran_num
	 *	<br> - ins_num
	 *	<br> - ticker
	 *	<br> - subyacente
	 *	<br> - toolset
	 *	<br> - ins_type
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - mvalue
	 *	<br> - precio_ticker
	 *	<br> - precio_sub
	 *	<br> - notnl
	 *	<br> - price
	 *	<br> - precio_sucio_24h
	 *	<br> - ohd_base_mtm
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_BaseMtoM()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n base_mtm_sl as  ");
		sb.append("\n ( ");
		sb.append("\n 	select 	t.tran_num ");
		sb.append("\n 			,t.ins_num ");
		sb.append("\n 			,t.ticker ");
		sb.append("\n 			,t.subyacente ");
		sb.append("\n 			,t.toolset ");
		sb.append("\n 			,t.ins_type ");
		sb.append("\n 			,t.currency ");
		sb.append("\n 			,t.internal_portfolio ");
		sb.append("\n 			,t.external_lentity ");
		sb.append("\n 			,t.mvalue ");
		sb.append("\n 			,vt.precio_sucio_24h precio_ticker ");
		sb.append("\n 			,vs.precio_sucio_24h precio_sub ");
		sb.append("\n 			,t.notnl ");
		sb.append("\n 			,t.price ");
		sb.append("\n 			,fx.precio_sucio_24h ");
		sb.append("\n 			,case 	when t.toolset = " + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + "  ");
		sb.append("\n 							then round(t.mvalue * vt.precio_sucio_24h/NVL(fx.precio_sucio_24h, 1)/100.0, c.round) * NVL(fx.precio_sucio_24h, 1) ");
		sb.append("\n 						when t.toolset = " +  TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + " ");
		sb.append("\n 							then round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price * 100.0), c.round) * NVL(fx.precio_sucio_24h, 1) ");
		sb.append("\n 						else round(t.mvalue * t.notnl * (NVL(vt.precio_sucio_24h, vs.precio_sucio_24h)/NVL(fx.precio_sucio_24h, 1) - t.price), c.round) * NVL(fx.precio_sucio_24h, 1) ");
		sb.append("\n 					end ohd_base_mtm ");
		sb.append("\n 	from trades_sl t ");
		sb.append("\n 			left join currency c on c.id_number = t.currency ");
		sb.append("\n 			left join user_mx_vectorprecios_diario vs on t.subyacente = vs.ticker ");
		sb.append("\n 			left join user_mx_vectorprecios_diario vt on t.ticker = vt.ticker ");
		sb.append("\n 			left join user_mx_vectorprecios_diario fx on fx.ticker in (concat(concat(concat('*C_MXP', c.name), '_'), 'FIX'), 		 ");
		sb.append("\n 																			concat(concat(concat('*C_MXP', c.name), '_'), c.name))	 ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de escenarios var moviles por siefore y moneda.
	 * @Description Listado de escenarios de VaR (Matriz Esc Moviles) por Siefore y Moneda
	 * @Columns
	 * 	     - rec
	 *	<br> - id
	 *	<br> - internal_portfolio
	 *	<br> - currency
	 *	<br> - ohd_exposition
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_Escenarios_CCY()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n escenarios_ccy as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	row_number() over(partition by p.internal_portfolio, p.currency order by sum(p.ohd_position * m.var_consar)) rec ");
		sb.append("\n 			,m.id ");
		sb.append("\n 			,p.internal_portfolio ");
		sb.append("\n 			,p.currency ");
		sb.append("\n 			,sum(p.ohd_position * m.var_consar) ohd_exposition ");
		sb.append("\n 	from position_sl p ");
		sb.append("\n 			inner join user_mx_varconsar_escenarios m ");
		sb.append("\n 						on m.ins_num = p.ins_num ");
		sb.append("\n 	group by m.id, p.internal_portfolio, p.currency ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de escenarios var moviles por siefore, socio liquidador y moneda.
	 * @Description Listado de escenarios de VaR (Matriz Esc Moviles) por Siefore, Socio Liquidador y Moneda
	 * @Columns
	 * 	     - rec
	 *	<br> - id
	 *	<br> - internal_portfolio
	 *	<br> - currency
	 *	<br> - external_lentity
	 *	<br> - ohd_exposition
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_Escenarios_SL()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n escenarios_sl as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	row_number() over(partition by p.internal_portfolio, p.external_lentity order by sum(p.ohd_position * m.var_consar)) rec ");
		sb.append("\n 			,m.id ");
		sb.append("\n 			,p.internal_portfolio ");
		sb.append("\n 			,p.currency ");
		sb.append("\n 			,p.external_lentity ");
		sb.append("\n 			,sum(p.ohd_position * m.var_consar) ohd_exposition ");
		sb.append("\n 	from position_sl p ");
		sb.append("\n 			inner join user_mx_varconsar_escenarios m ");
		sb.append("\n 						on m.ins_num = p.ins_num ");
		sb.append("\n 	group by m.id, p.internal_portfolio, p.currency, p.external_lentity ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de resumen de cartera asociada a Siefore y Moneda
	 * @Description Agrupa MtoM por siefore y moneda
	 * @Columns
	 * 	     - internal_portfolio
	 *	<br> - currency
	 *	<br> - ohd_total_mtm
	 */
	protected String getCTE_SL_ResumenMtoM_CCY()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n resumen_mtm_ccy as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	internal_portfolio ");
		sb.append("\n 			,currency ");
		sb.append("\n 			,sum(ohd_base_mtm) ohd_total_mtm ");
		sb.append("\n 	from base_mtm_sl ");
		sb.append("\n 	group by internal_portfolio, currency ");
		sb.append("\n ) ");
		
		return sb.toString();
	}	
	
	/**
	 * @Type CTE Query para obtención de resumen de cartera asociada a Socios Liquidadores, Siefore y Moneda
	 * @Description Agrupa MtoM por siefore, socio liquidador y moneda
	 * @Columns
	 * 	     - internal_portfolio
	 *	<br> - currency
	 *	<br> - external_lentity
	 *	<br> - ohd_total_mtm
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_ResumenMtoM_SL()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n resumen_mtm_sl as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	internal_portfolio ");
		sb.append("\n 			,currency ");
		sb.append("\n 			,external_lentity ");
		sb.append("\n 			,sum(ohd_base_mtm) ohd_total_mtm ");
		sb.append("\n 	from base_mtm_sl ");
		sb.append("\n 	group by internal_portfolio, currency, external_lentity ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	

	/**
	 * @Type CTE Query para obtención de valor k por cada siefore
	 * @Description Extrae escenario k configurado por siefore 
	 * @Columns
	 * 	     - internal_portfolio
	 *	<br> - k
	 * @return String con formato SQL 
	 */
	protected String getCTE_Esc_K_VaR()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n esc_var as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	p.id_number internal_portfolio ");
		sb.append("\n             ,to_number(valor) k ");
		sb.append("\n 	from user_configurable_variables u ");
		sb.append("\n 			left join portfolio p on p.name = u.variable ");
		sb.append("\n 	where proceso = 'Escenarios_CONSAR' ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de Margen de Liquidez
	 * @Description Retorna el Margen de Liquidez por Siefore, Moneda y Socio Liquidador
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - k
	 *	<br> - fecha_var
	 *	<br> - escenario
	 *	<br> - fecha_esc
	 *	<br> - ohd_var
	 *	<br> - ohd_var_condicional
	 *	<br> - ohd_total_mtm
	 *	<br> - ohd_margen_liq
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_MargenLiquidez_CCY()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n margen_liquidez_ccy as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	e.internal_portfolio siefore ");
		sb.append("\n 			,e.currency currency ");
		sb.append("\n 			,u.k ");
		sb.append("\n 			,d.business_date fecha_var ");
		sb.append("\n 			,sum(case when e.rec = u.k then e.id else 0.0 end) escenario ");
		sb.append("\n 			,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc ");
		sb.append("\n 			,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var ");
		sb.append("\n 			,-avg(e.ohd_exposition) ohd_var_condicional ");
		sb.append("\n 			,r.ohd_total_mtm ");
		sb.append("\n 			,case when r.ohd_total_mtm > -avg(e.ohd_exposition) then r.ohd_total_mtm else -avg(e.ohd_exposition) end ohd_margen_liq ");
		sb.append("\n 	from esc_var u  ");
		sb.append("\n 			inner join escenarios_ccy e on e.internal_portfolio = u.internal_portfolio ");
		sb.append("\n 			left join system_dates d on 1 = 1 ");
		sb.append("\n 			left join resumen_mtm_ccy r on r.internal_portfolio = e.internal_portfolio and r.currency = e.currency ");
		sb.append("\n 	where e.rec <= u.k ");
		sb.append("\n 	group by e.internal_portfolio, e.currency, u.k, d.business_date, r.ohd_total_mtm ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query para obtención de Margen de Liquidez
	 * @Description Retorna el Margen de Liquidez por Siefore y Moneda
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - socio
	 *	<br> - k
	 *	<br> - fecha_var
	 *	<br> - escenario
	 *	<br> - fecha_esc
	 *	<br> - ohd_var
	 *	<br> - ohd_var_condicional
	 *	<br> - ohd_total_mtm
	 *	<br> - ohd_margen_liq
	 * @return String con formato SQL 
	 */
	protected String getCTE_SL_MargenLiquidez_SL()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("\n margen_liquidez_sl as ");
		sb.append("\n ( ");
		sb.append("\n 	select 	e.internal_portfolio siefore ");
		sb.append("\n 			,e.currency currency ");
		sb.append("\n 			,e.external_lentity socio ");
		sb.append("\n 			,u.k ");
		sb.append("\n 			,d.business_date fecha_var ");
		sb.append("\n 			,sum(case when e.rec = u.k then e.id else 0.0 end) escenario ");
		sb.append("\n 			,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc ");
		sb.append("\n 			,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var ");
		sb.append("\n 			,-avg(e.ohd_exposition) ohd_var_condicional ");
		sb.append("\n 			,r.ohd_total_mtm ");
		sb.append("\n 			,case when r.ohd_total_mtm > -avg(e.ohd_exposition) then r.ohd_total_mtm else -avg(e.ohd_exposition) end ohd_margen_liq ");
		sb.append("\n 	from esc_var u  ");
		sb.append("\n 			inner join escenarios_sl e on e.internal_portfolio = u.internal_portfolio ");
		sb.append("\n 			left join system_dates d on 1 = 1 ");
		sb.append("\n 			left join resumen_mtm_sl r on r.internal_portfolio = e.internal_portfolio and r.external_lentity = e.external_lentity ");
		sb.append("\n 	where e.rec <= u.k ");
		sb.append("\n 	group by e.internal_portfolio, e.currency, e.external_lentity, u.k, d.business_date, r.ohd_total_mtm ");
		sb.append("\n ) ");
		
		return sb.toString();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// OUTPUT SECTION ///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * @param	iTipo Corresponde al tipo de output que se espera obtener:
	 *  		<br> - OUT_Cheq_SaldosConta
	 *  		<br> - OUT_Cheq_Disponible_MXN
	 * 		 	<br> - OUT_Cheq_Disponible_USD
	 *  		<br> - OUT_Cheq_LimiteOperativo
	 *  		<br> - OUT_SL_Trades_SL
	 *  		<br> - OUT_SL_Position_SL
	 *  		<br> - OUT_SL_BaseMtoM_SL
	 *  		<br> - OUT_SL_MargenLiquidez_SL
	 * 		 	<br> - OUT_SL_MargenLiquidez_CCY
	 *  		<br> - OUT_Cheq_Limites
	 * @return	Retorna un objeto de tipo Table con los registros según el output seleccionado.
	 * @throws	OException 
	 * @example	El ejemplo de uso :
	 * 				<br>
	 * 				<br> Table tTest = Table.tableNew();
	 * 				<br> tTest = UTIL_Liquidez.getOutput(UTIL_Liquidez.OUT_Cheq_LimiteOperativo);
	 */
	public Table getOutput(int iTipo) throws OException
	{		
		Table tReturn = Table.tableNew();
		String sSql = "";
		
		 switch (iTipo) {
			case OUT_Cheq_SaldosConta:
				tReturn.setTableName("OUT_Cheq_SaldosConta");
				sSql = getOutput_Cheq_SaldosConta();
				break;
			case OUT_Cheq_Disponible_MXN:
				tReturn.setTableName("OUT_Cheq_Disponible_MXN");
				sSql = getOutput_Cheq_Disponible_MXN();
				break;
			case OUT_Cheq_Disponible_USD:
				tReturn.setTableName("OUT_Cheq_Disponible_USD");
				sSql = getOutput_Cheq_Disponible_USD();
				break;
			case OUT_Cheq_LimiteOperativo:
				tReturn.setTableName("OUT_Cheq_LimiteOperativo");
				sSql = getOutput_Cheq_LimiteOperativo();
				break;
			case OUT_SL_Trades_SL:
				tReturn.setTableName("OUT_SL_Trades_SL");
				sSql = getOutput_SL_Trades_SL();
				break;
			case OUT_SL_Position_SL:
				tReturn.setTableName("OUT_SL_Position_SL");
				sSql = getOutput_SL_Position_SL();
				break;
			case OUT_SL_BaseMtoM_SL:
				tReturn.setTableName("OUT_SL_BaseMtoM_SL");
				sSql = getOutput_SL_BaseMtoM_SL();
				break;
			case OUT_SL_MargenLiquidez_SL:
				tReturn.setTableName("OUT_SL_MargenLiquidez_SL");
				sSql = getOutput_SL_MargenLiquidez_SL();
				break;
			case OUT_SL_MargenLiquidez_CCY:
				tReturn.setTableName("OUT_SL_MargenLiquidez_CCY");
				sSql = getOutput_SL_MargenLiquidez_CCY();
				break;
			case OUT_Cheq_Limites:
				tReturn.setTableName("OUT_Cheq_Limites");
				sSql = getOutput_Cheq_Limites();
				break;
			default:
				break;
		}
		
		DBaseTable.execISql(tReturn, sSql);

		return tReturn;
	}
	
	/**
	 * @Type Output Saldos de Chequeras
	 * @Description Query de Salida de los saldos en las chequeras<br>
	 * @Columns
	 * 	     - siefore
	 *	<br> - ohd_saldo_1102
	 *	<br> - ohd_saldo_2101_0002
	 *	<br> - ohd_saldo_1301_0002
	 *	<br> - ohd_saldo_1103
	 *	<br> - ohd_saldo_2108
	 *	<br> - ohd_saldo_1104
	 *	<br> - ohd_pagar_usd
	 *	<br> - ohd_saldo_1301
	 *	<br> - ohd_saldo_2101
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_Cheq_SaldosConta() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_Cheq_CuentasConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_SaldosConta()).append("\n");
		//Output
		sb.append("\n select siefore, ohd_saldo_1102, ohd_saldo_2101_0002, ohd_saldo_1301_0002, ohd_saldo_1103, ohd_saldo_2108, ohd_saldo_1104, ohd_pagar_usd, ohd_saldo_1301, ohd_saldo_2101 from chequera ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Chequera disponible en MXN
	 * @Description Query de Salida que  entrega monto disponible al inicio de dia,
	 * <br> y monto disponible vigente en MXN
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - ohd_saldo_1102
	 *	<br> - ohd_saldo_2101_0002
	 *	<br> - ohd_saldo_1301_0002
	 *	<br> - form_cheq_inicio_dia
	 *	<br> - ohd_cheq_inicio_dia
	 *	<br> - ohd_movimiento
	 *	<br> - form_cheq_disponible
	 *	<br> - ohd_cheq_disponible
	 *	<br> - ohd_saldo_1301
	 *	<br> - ohd_saldo_2101
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_Cheq_Disponible_MXN() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_Cheq_CuentasConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_SaldosConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Amortizaciones_Intereses()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Mov_Trading()).append("\n");
		//Output
		sb.append("\n  select c.siefore  ");
		sb.append("\n  		,"+EnumsCurrency.MX_CURRENCY_MXN.toInt()+" currency  ");
		sb.append("\n  		,c.ohd_saldo_1102  ");
		sb.append("\n  		,c.ohd_saldo_2101_0002  ");
		sb.append("\n  		,c.ohd_saldo_1301_0002  ");
		sb.append("\n  		,'[1102 + 2101-0002 + 1301-0002]' form_cheq_inicio_dia  ");
		sb.append("\n  		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) ohd_cheq_inicio_dia  ");
		sb.append("\n  		,NVL(mov.ohd_movimiento, 0) ohd_movimiento  ");
		sb.append("\n  		,'[1102 + 2101-0002 + 1301-0002 + Movimientos]' form_cheq_disponible  ");
		sb.append("\n  		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_cheq_disponible  ");
		sb.append("\n  		,c.ohd_saldo_1301  ");
		sb.append("\n  		,c.ohd_saldo_2101  ");
		sb.append("\n  from chequera c  ");
		sb.append("\n  		left join movimientos mov on mov.siefore = c.siefore and mov.currency = "+EnumsCurrency.MX_CURRENCY_MXN.toInt()+" ");
		sb.append("\n 			left join amort_interest a on a.siefore = c.siefore and a.currency = "+EnumsCurrency.MX_CURRENCY_MXN.toInt()+" ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Chequera disponible en USD
	 * @Description Query de Salida que  entrega monto disponible al inicio de dia,
	 * <br> y monto disponible vigente en USD
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - ohd_saldo_1103
	 *	<br> - ohd_saldo_2108
	 *	<br> - form_cheq_inicio_dia
	 *	<br> - ohd_cheq_inicio_dia
	 *	<br> - ohd_movimiento
	 *	<br> - form_cheq_disponible
	 *	<br> - ohd_cheq_disponible
	 *	<br> - ohd_saldo_1104
	 *	<br> - ohd_pagar_usd
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_Cheq_Disponible_USD() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_Cheq_CuentasConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_SaldosConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Amortizaciones_Intereses()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Mov_Trading()).append("\n");
		//Output
		sb.append("\n select c.siefore  ");
		sb.append("\n  		,"+ EnumsCurrency.MX_CURRENCY_USD.toInt() +" currency  ");
		sb.append("\n  		,c.ohd_saldo_1103  ");
		sb.append("\n  		,c.ohd_saldo_2108  ");
		sb.append("\n  		,'[1103 - 2108]' form_cheq_inicio_dia  ");
		sb.append("\n  		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) ohd_cheq_inicio_dia  ");
		sb.append("\n  		,NVL(mov.ohd_movimiento, 0) ohd_movimiento  ");
		sb.append("\n  		,'[1103 - 2108 + Movimientos]' form_cheq_disponible  ");
		sb.append("\n  		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_cheq_disponible  ");
		sb.append("\n  		,c.ohd_saldo_1104  ");
		sb.append("\n  		,0 ohd_pagar_usd  ");
		sb.append("\n  from chequera c  ");
		sb.append("\n  		left join movimientos mov on mov.siefore = c.siefore and mov.currency = "+EnumsCurrency.MX_CURRENCY_USD.toInt()+" ");
		sb.append("\n  		left join amort_interest a on a.siefore = c.siefore and a.currency = "+EnumsCurrency.MX_CURRENCY_USD.toInt()+" ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Limite Operativo para Chequeras
	 * @Description Query de Salida con los limites operativos para chequeras por siefore<br>
	 * @Columns
	 * 	     - siefore
	 *	<br> - nav_value
	 *	<br> - ohd_factor
	 *	<br> - ohd_limop
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_Cheq_LimiteOperativo() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_Cheq_LimiteOperativo()).append("\n");
		//Output
		sb.append("\n select siefore, nav_value, ohd_factor, ohd_limop from limiteOP ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Trades SL
	 * @Description Query de Salida con los Trades asociados a Socios Liquidadores<br>
	 * @Columns
	 * 	     - tran_num
	 *	<br> - ins_num
	 *	<br> - ticker
	 *	<br> - subyacente
	 *	<br> - toolset
	 *	<br> - ins_type
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - notnl
	 *	<br> - prince_inp_format
	 *	<br> - mvalue
	 *	<br> - price
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_SL_Trades_SL() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_SL_Trades()).append("\n");
		//Output
		sb.append("\n select * from trades_sl");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Position asociada a socios liquidadores y moneda
	 * @Description Query de Salida con las Position asociada a Socios Liquidadores y Moneda<br>
	 * @Columns
	 * 	     - ins_num
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - ohd_position
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_SL_Position_SL() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_SL_Trades()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Position()).append("\n");
		//Output
		sb.append("\n select * from position_sl ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output MtoM asociados a socios liquidadores
	 * @Description Query de Salida con valores MtoM de la cartera asociada a Socios Liquidadores<br>
	 * @Columns
	 * 	     - tran_num
	 *	<br> - ins_num
	 *	<br> - ticker
	 *	<br> - subyacente
	 *	<br> - toolset
	 *	<br> - ins_type
	 *	<br> - currency
	 *	<br> - internal_portfolio
	 *	<br> - external_lentity
	 *	<br> - mvalue
	 *	<br> - precio_ticker
	 *	<br> - precio_sub
	 *	<br> - notnl
	 *	<br> - price
	 *	<br> - precio_sucio_24h
	 *	<br> - ohd_base_mtm
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_SL_BaseMtoM_SL() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_SL_Trades()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_BaseMtoM()).append("\n");
		//Output
		sb.append("\n select * from base_mtm_sl ");
		
		return sb.toString();
	}


		/**
	 * @Type Output Margen de liquidez
	 * @Description Query de Salida de Margen de Liquidez por Siefore, Socio Liquidador y Moneda <br>
	 * cuya aplicación es para el Margen de Liquidez oficial
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - socio
	 *	<br> - k
	 *	<br> - fecha_var
	 *	<br> - escenario
	 *	<br> - fecha_esc
	 *	<br> - ohd_var
	 *	<br> - ohd_var_condicional
	 *	<br> - ohd_total_mtm
	 *	<br> - ohd_margen_liq
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_SL_MargenLiquidez_SL() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_SL_Trades()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_BaseMtoM()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_ResumenMtoM_SL()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Escenarios_SL()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Esc_K_VaR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_MargenLiquidez_SL()).append("\n");
		//Output
		sb.append("\n select * from margen_liquidez_sl ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Margen de liquidez
	 * @Description Query de Salida de Margen de Liquidez por Siefore y Moneda,<br>
	 * cuya aplicación es para el límite de Chequera
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - k
	 *	<br> - fecha_var
	 *	<br> - escenario
	 *	<br> - fecha_esc
	 *	<br> - ohd_var
	 *	<br> - ohd_var_condicional
	 *	<br> - ohd_total_mtm
	 *	<br> - ohd_margen_liq
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_SL_MargenLiquidez_CCY() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_SL_Trades()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_BaseMtoM()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_ResumenMtoM_SL()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Escenarios_SL()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Esc_K_VaR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_ResumenMtoM_CCY()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Escenarios_CCY()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_MargenLiquidez_CCY()).append("\n");
		//Output
		sb.append("\n select * from margen_liquidez_ccy ");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Limites Chequeras
	 * @Description Query que retorna el reporte de Limites en Chequera
	 * @Columns
	 * 	     - siefore
	 *	<br> - currency
	 *	<br> - ohd_disp_inicio_dia
	 *	<br> - nav_value
	 *	<br> - ohd_factor
	 *	<br> - ohd_limop
	 *	<br> - ohd_cobros96h
	 *	<br> - ohd_pagos96h
	 *	<br> - ohd_neto96h
	 *	<br> - ohd_margen_liq_agg
	 *	<br> - ohd_req_obl
	 *	<br> - ohd_movimiento
	 *	<br> - ohd_disponible
	 *	<br> - ohd_limite
	 * @return String con formato SQL que entrega los resultados:<br>
	 */	
	public String getOutput_Cheq_Limites() throws OException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_Cheq_CuentasConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_SaldosConta()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Amortizaciones_Intereses()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_Mov_Trading()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Cheq_LimiteOperativo()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Trades()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_BaseMtoM()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_ResumenMtoM_CCY()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_Escenarios_CCY()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Esc_K_VaR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_SL_MargenLiquidez_CCY()).append("\n");
		//Output
		sb.append("\n select 	c.siefore ");
		sb.append("\n 		,"+ EnumsCurrency.MX_CURRENCY_MXN.toInt()+" currency ");
		sb.append("\n 		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) ohd_disp_inicio_dia ");
		sb.append("\n 		,l.nav_value ");
		sb.append("\n 		,l.ohd_factor  ");
		sb.append("\n 		,l.ohd_limop ");
		sb.append("\n 		,c.ohd_saldo_1301 ohd_cobros96h ");
		sb.append("\n 		,c.ohd_saldo_2101 ohd_pagos96h ");
		sb.append("\n 		,c.ohd_saldo_1301 + c.ohd_saldo_2101 ohd_neto96h ");
		sb.append("\n 		,NVL(ml.ohd_margen_liq, 0) ohd_margen_liq_agg ");
		sb.append("\n 		,c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0) ohd_req_obl ");
		sb.append("\n 		,NVL(mov.ohd_movimiento, 0) ohd_movimiento ");
		sb.append("\n 		,c.ohd_saldo_1102 + c.ohd_saldo_2101_0002 + c.ohd_saldo_1301_0002 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_disponible ");
		sb.append("\n 		,case when l.ohd_limop > c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0) ");
		sb.append("\n 				then l.ohd_limop  ");
		sb.append("\n 				else c.ohd_saldo_1301 + c.ohd_saldo_2101 + NVL(ml.ohd_margen_liq, 0) ");
		sb.append("\n 		end ohd_limite ");
		sb.append("\n from chequera c ");
		sb.append("\n 		left join limiteOP l on l.siefore = c.siefore ");
		sb.append("\n 		left join margen_liquidez_ccy ml on ml.siefore = c.siefore and ml.currency = "+ EnumsCurrency.MX_CURRENCY_MXN.toInt()+" ");
		sb.append("\n 		left join movimientos mov on mov.siefore = c.siefore and mov.currency = "+ EnumsCurrency.MX_CURRENCY_MXN.toInt()+" ");
		sb.append("\n 		left join amort_interest a on a.siefore = c.siefore and a.currency = "+ EnumsCurrency.MX_CURRENCY_MXN.toInt()+" ");
		sb.append("\n  ");
		sb.append("\n union ");
		sb.append("\n  ");
		sb.append("\n select 	c.siefore ");
		sb.append("\n 		,"+ EnumsCurrency.MX_CURRENCY_USD.toInt()+" currency ");
		sb.append("\n 		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) ohd_disp_inicio_dia ");
		sb.append("\n 		,l.nav_value ");
		sb.append("\n 		,0.0 ohd_factor ");
		sb.append("\n 		,0.0 ohd_limop ");
		sb.append("\n 		,c.ohd_saldo_1104 ohd_cobros96h ");
		sb.append("\n 		,c.ohd_pagar_usd ohd_pagos96h ");
		sb.append("\n 		,c.ohd_saldo_1104 - c.ohd_pagar_usd ohd_neto96h ");
		sb.append("\n 		,NVL(ml.ohd_margen_liq, 0) ohd_margen_liq_agg ");
		sb.append("\n 		,c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0) ohd_req_obl ");
		sb.append("\n 		,NVL(mov.ohd_movimiento, 0) ohd_movimiento ");
		sb.append("\n 		,c.ohd_saldo_1103 - c.ohd_saldo_2108 + NVL(a.ohd_monto_venc, 0) + NVL(mov.ohd_movimiento, 0) ohd_disponible ");
		sb.append("\n 		,case when c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0) < 0  ");
		sb.append("\n 				then 0  ");
		sb.append("\n 				else c.ohd_saldo_1104 - c.ohd_pagar_usd + NVL(ml.ohd_margen_liq, 0) ");
		sb.append("\n 		end ohd_limite ");
		sb.append("\n from chequera c ");
		sb.append("\n 		left join limiteOP l on l.siefore = c.siefore ");
		sb.append("\n 		left join margen_liquidez_ccy ml on ml.siefore = c.siefore and ml.currency = "+ EnumsCurrency.MX_CURRENCY_USD.toInt()+" ");
		sb.append("\n 		left join movimientos mov on mov.siefore = c.siefore and mov.currency = "+ EnumsCurrency.MX_CURRENCY_USD.toInt()+" ");
		sb.append("\n 		left join amort_interest a on a.siefore = c.siefore and a.currency = "+ EnumsCurrency.MX_CURRENCY_USD.toInt()+" ");
		
		return sb.toString();
	}
}