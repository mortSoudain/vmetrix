-- Limite por Operador - Derivados
with transactions as 
(
	select 	z.personnel_id
			,ab.tran_num
			,z.tran_num_o
			,ab.ins_num
			,h.ticker
			,ab.toolset
			,ab.ins_type
			,ab.trade_date
			,ab.trade_time
			,ab.buy_sell
			,ab.internal_portfolio
			,ab.position
			,abs(ab.proceeds) ohd_proceeds
			,sum(abs(
				CASE   
					WHEN (ab.toolset = 1)	THEN p.notnl
											ELSE ab.proceeds
				END
			)) 
				over (partition by z.personnel_id order by ab.trade_time ROWS between UNBOUNDED PRECEDING and CURRENT ROW) ohd_acumulado
	from ab_tran ab
			left join header h on h.ins_num = ab.ins_num
			left join (
							select 	x.tran_num
									,x.tran_num_o
									,hi.personnel_id
							from ab_tran_history hi
									inner join (
													select 	e.tran_num 
															,e.tran_num_o
															,max(h.version_number) n
													from ab_tran_history h
																left join (
																				select 	tran_num
																						,min(para_tran_num) tran_num_o
																				from ab_tran_event
																				group by tran_num
																			) e
																	on e.tran_num_o = h.tran_num 
													where h.tran_status in (2,10)
													group by e.tran_num_o, e.tran_num
												) x
										on x.tran_num_o = hi.tran_num and x.n = hi.version_number
						) z
				on z.tran_num = ab.tran_num 
			left join parameter p on p.ins_num = ab.ins_num and p.pay_rec=1
	where ab.tran_type = 0	
			and ab.tran_status = 3
			and ab.toolset in (1, 34, 41, 50)
			and ab.trade_date = to_date('$$fecha_trade$$')
	order by z.personnel_id, ab.trade_time
)
select 	t.personnel_id
		,concat(concat(p.first_name, ' '), p.last_name) name
		,t.tran_num
		,t.tran_num_o
		,t.ins_num
		,t.ticker
		,t.toolset
		,t.ins_type
		,t.trade_date
		,t.trade_time
		,t.buy_sell
		,t.internal_portfolio
		,t.position
		,t.ohd_proceeds
		,t.ohd_acumulado
		,to_number(cv.valor) ohd_limite
		,case when abs(t.ohd_acumulado) <= to_number(cv.valor) then 'OK' else 'ALERTA' end status
from transactions t
		left join user_configurable_variables cv 
				on cv.proceso = 'limite_operador_derivados' and cv.variable = 'limite'
		left join personnel p on t.personnel_id = p.id_number