-- Reporte Resumen de Operaciones V2.0
select 	ab.trade_date
		,ab.tran_num
		,ab.ins_num
		,ab.toolset
		,ab.ins_type
		,case when substr(ab.reference, 1, 13) = 'Cash for Deal' then hu.ticker else h.ticker end ticker
		,case when substr(ab.reference, 1, 13) = 'Cash for Deal' then 'Vencimiento' 
				when ab.buy_sell = 0 then 'Compra'
				when ab.buy_sell = 1 then 'Venta'
				else 'NA' 
		end operacion
		,ab.internal_portfolio
		,c.name currency
		,case when ab.toolset = 10 then ab.position else ab.proceeds end ohd_proceeds
		,NVL(v.precio_sucio, 1) ohd_fx_mxn
		,case when ab.toolset = 10 then ab.position * NVL(v.precio_sucio, 1) else ab.proceeds * NVL(v.precio_sucio, 1) end ohd_settle_proceeds
		,ab.tran_status
from ab_tran ab
		left join header h on h.ins_num = ab.ins_num
		left join currency c on c.id_number = ab.currency
		left join ab_tran abu on to_char(abu.tran_num) = replace(ab.reference, 'Cash for Deal: ', '')
		left join header hu on hu.ins_num = abu.ins_num
		left join user_mx_vectorprecios_diario v 
				on v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name)
						or v.ticker = concat(concat('*C_MXP', c.name), '_FIX')
where ab.asset_type in (2, 8)
		and ab.tran_type = 0
		and ab.tran_status in (3, 4, 22)
		and (ab.toolset not in (6, 10) or (ab.toolset = 10 and substr(ab.reference, 1, 13) = 'Cash for Deal'))
		--and ab.trade_date = DATE '2018-04-27'
		and ab.trade_date = to_date('$$fecha_trading$$') 