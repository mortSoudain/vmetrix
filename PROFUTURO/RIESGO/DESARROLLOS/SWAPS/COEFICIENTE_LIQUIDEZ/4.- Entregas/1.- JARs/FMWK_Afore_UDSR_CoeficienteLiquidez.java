/**
File Name:              FMWK_Afore_UDSR_CoeficienteLiquidez.java

Author:                 Gustavo Rojas - VMetrix International SpA 
Creation Date:          Noviembre 2017

REVISION HISTORY  		
Version	:				v1.1
Date	:				24-04-2018
Author	:				Gustavo Rojas Arteaga - VMetrix
Descrip	:				Se incorpora Util VaRConsar


Main Script:            This is the Main Script
Parameter Script:       None
Display Script:         None

Description:            This script calculate Coeficiente Liquidez for limit and report.


Assumptions:            None

Instructions:

Script Category:

Uses EOD Results:       Yes

Which EOD Results are used: None

When can the script be run: SIM

************************************************************************************/
package com.afore.udsr.risk;

import standard.include.JVS_INC_STD_UserSimRes;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserCflowType;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.afore.util.risk.query.UTIL_VaRConsar;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.PluginCategory;
import com.olf.openjvs.PluginType;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.ScriptAttributes;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_FORMAT_BASE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.PFOLIO_RESULT_TYPE;
import com.olf.openjvs.enums.SCRIPT_CATEGORY_ENUM;
import com.olf.openjvs.enums.SCRIPT_TYPE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;
import com.olf.openjvs.enums.USER_RESULT_OPERATIONS;
@ScriptAttributes(allowNativeExceptions=false)
@PluginCategory(SCRIPT_CATEGORY_ENUM.SCRIPT_CAT_SIM_RESULT)
@PluginType(SCRIPT_TYPE_ENUM.MAIN_SCRIPT)

public class FMWK_Afore_UDSR_CoeficienteLiquidez implements IScript
{
	//Tabla de Queries
	private final static String USER_TABLE_ACC = "user_mx_activos_alta_calidad";
	
	private final String sPlugInName = this.getClass().getSimpleName();
	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _Lib = new UTIL_Afore();
	UTIL_VaRConsar _VaR;
	private JVS_INC_STD_UserSimRes m_INCSTDUserSimRes;
	private int iToday;
	private int iQuery;
	private Table tTranMTM = Util.NULL_TABLE;
	private Table tFxCurve = Util.NULL_TABLE;
	private Table tReturnt = Util.NULL_TABLE;
	private Table tArgt    = Util.NULL_TABLE;
	
	public FMWK_Afore_UDSR_CoeficienteLiquidez(){
		m_INCSTDUserSimRes = new JVS_INC_STD_UserSimRes();
	 }

    public void execute(IContainerContext context) throws OException
    {
    	_Log.setDEBUG_STATUS(EnumStatus.OFF);
    	_Log.markStartScript();
    	try{
    		tReturnt = context.getReturnTable();
    		tArgt    = context.getArgumentsTable();
    		
    		iToday = OCalendar.today();
    		
    		iQuery = 0;
    		
	    	int tmp = m_INCSTDUserSimRes.USR_RunMode(tArgt);
	
	    	if( tmp == USER_RESULT_OPERATIONS.USER_RES_OP_CALCULATE.toInt())
	    	{
	    		compute_result();
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_FORMAT.toInt())
	    	{
	    		format_result();
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_DWEXTRACT.toInt())
	    	{

	    		dw_extract_result();
	    	}
	    	else
	    	{
	    		m_INCSTDUserSimRes.USR_Fail("Incorrect operation code", tArgt);
		    }
	    		
    	}catch (OException e){
    			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{

    		_Log.markEndScript();
    	}
    	return;// main/0
    }

    
	private void compute_result() throws OException 
	{
		//Add Query column for custom treatment
		tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);
		
		int iFind = tArgt.getTable("sim_results", 1).getTable("result_class", 4).unsortedFindInt("result_type", PFOLIO_RESULT_TYPE.FX_RESULT.toInt());

		tTranMTM = tArgt.getTable("sim_results", 1).getTable("result_class", 1);
		tFxCurve = tArgt.getTable("sim_results", 1).getTable("result_class", 4).getTable("result", iFind);

		
		//Add Portfolio, ExternalBU(Socio Liquidador) and ins_type
		doAddPortfolio(tTranMTM);
		
		Table tPidByPortfolio = Table.tableNew();
		Table tActivosAltaCalidad = Table.tableNew();
		
		tPidByPortfolio = getPIDValuesByPortfolio();
		tActivosAltaCalidad = getAACValues();
		
		int iLimite = Str.strToInt(_Lib.getVariableGlobal("FINDUR", sPlugInName, "limite"));
		
		tReturnt.addCol("fecha", COL_TYPE_ENUM.COL_INT);
		
		tReturnt.select(tPidByPortfolio, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
		
		tReturnt.select(tPidByPortfolio, "SUM, pid", "internal_portfolio EQ $internal_portfolio");
		tReturnt.select(tActivosAltaCalidad, "SUM, aac", "internal_portfolio EQ $internal_portfolio");

		tReturnt.addFormulaColumn("iif(COL('aac') != 0, (COL('pid')/COL('aac'))*100, 0.0)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "coef_liquidez");
		
		tReturnt.addFormulaColumn(iLimite+"-COL('coef_liquidez')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "precaucion");
		tReturnt.addFormulaColumn("iif(COL('coef_liquidez')<" + iLimite/100 + ",1, 0)", COL_TYPE_ENUM.COL_INT.toInt(), "res");
		
		
		tReturnt.addCol("cumplimiento", COL_TYPE_ENUM.COL_STRING);
		for(int i=1;i<=tReturnt.getNumRows();i++)
			if (tReturnt.getInt("res", i)==0)
				tReturnt.setString("cumplimiento", i, "OK");
			else
				tReturnt.setString("cumplimiento", i, "EXCESO");
		
		tReturnt.setColValInt("fecha", iToday);
		tReturnt.colHide("res");
	}
	
	// *****************************************************************************
	void format_result() throws OException
	{
	   // Set column titles
		
		tReturnt.setColTitle( "fecha",  			"Fecha");
		tReturnt.setColTitle( "internal_portfolio",	"Portafolio");
		tReturnt.setColTitle( "pid",				"PID");
		tReturnt.setColTitle( "aac",				"AAC");
		tReturnt.setColTitle( "coef_liquidez",		"Coeficiente\nLiquidez %");
		tReturnt.setColTitle( "limite",				"Limite %");
		tReturnt.setColTitle( "precaucion",			"Precaucion %");
		tReturnt.setColTitle( "cumplimiento",		"Cumplimiento");
		
		tReturnt.setColFormatAsDate("fecha", DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		tReturnt.setColFormatAsRef("internal_portfolio", SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE);
		tReturnt.setColFormatAsNotnl("pid", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tReturnt.setColFormatAsNotnl("aac", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		
		tReturnt.setColFormatAsNotnl("coef_liquidez", Util.RATE_WIDTH, 2, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tReturnt.setColFormatAsNotnl("precaucion", Util.RATE_WIDTH, 2, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
	}
	
	void dw_extract_result() throws OException
	{
	}
	
	
	private Table getPIDValuesByPortfolio()  throws OException
	{

		Table tPidValues 	= Table.tableNew("Resultados PID por Portafolio");
		Table tAIM 			= Table.tableNew("AIM Requerido");
		int iYesterday = OCalendar.getLgbd(iToday);
		
		//Getting Socios
		Table tSocioLiquidador = getSociosLiquidadores();
		
		Table tCalculateCVarByQuery = getVarConsarByQuery(); 
		//tCalculateCVarByQuery.addFormulaColumn("COL('cvar_bbva')+COL('cvar_santander')+COL('cvar_hsbc')+COL('cvar_gssl')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "cvar_consar");

		//Aim Requerido
		Table tAimRequerido = Table.tableNew();
		String sSql = "Select portfolio, party, aim_collateral+aim_efectivo as aim_requerido FROM " + EnumsUserTables.USER_MX_MARGINCALL_HISTORY.toString() + " WHERE fecha = TO_DATE('" + OCalendar.formatDateInt(iYesterday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH) + "','DD-MM-YYYY')";
		//OConsole.oprint("\nSQL: " + sSql);
		try
		{
			int iRet = DBaseTable.execISql(tAimRequerido, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		tAIM.select(tSocioLiquidador, "DISTINCT, party_id", "party_id GT 0");
		tAIM.select(tAimRequerido, "portfolio(internal_portfolio),aim_requerido(aim)", "party EQ $party_id");
		
		//delete null rows 
		tAIM.deleteWhereValue("internal_portfolio", 0);

		tPidValues.select(tTranMTM, "DISTINCT, internal_portfolio","deal_num GT 0");
		tPidValues.select(tTranMTM, "SUM,"+PFOLIO_RESULT_TYPE.BASE_PV_RESULT.toInt()+"(mtm)","internal_portfolio EQ $internal_portfolio");
		tPidValues.select(tCalculateCVarByQuery, "cvar_consar","internal_portfolio EQ $internal_portfolio");
		tPidValues.select(tAIM, "SUM,aim","internal_portfolio EQ $internal_portfolio");

		tPidValues.addFormulaColumn("iif(COL('mtm')+COL('aim')>0 ,0, COL('mtm')+COL('aim'))", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "min_mtm_aim");
		tPidValues.addFormulaColumn("COL('min_mtm_aim')+COL('cvar_consar')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "pid");
		
		return tPidValues;
	}
	
	
	private Table getAACValues() throws OException
	{
		//Return Values by Portfoliio
		int i, iPortfolio;
		Table tAAC = Table.tableNew("Tabla Resultados AAC");

		Table tAllAacTransByMTM = Table.tableNew();
		Table tPortfolio = Table.tableNew("All Portfolios");
		
		tAllAacTransByMTM = getAACTrans();
		
		tPortfolio.select(tAllAacTransByMTM, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
		tAAC.select(tAllAacTransByMTM, "DISTINCT, internal_portfolio", "internal_portfolio GT 0");
		
		tAAC.select(tAllAacTransByMTM, "SUM, monto(aac)", "internal_portfolio EQ $internal_portfolio");
		
		
		for(i=1;i<=tPortfolio.getNumRows();i++)
		{
			iPortfolio = tPortfolio.getInt("internal_portfolio", i);
			Table tCashflows = getCashPositionByPortfolio(iPortfolio);
		
			tAAC.select(tCashflows, "SUM,position(aac)", "internal_portfolio EQ $internal_portfolio");
		}

		
		return tAAC;
		
	}
	
	private Table getAACTrans() throws OException
	{
		Table tAac = Table.tableNew("All AAC");
		Table tReturn = Table.tableNew("All AAC with MTM");
		Table tUserTableAAC = getACCInstruments();
		Table tReportosFwdProceeds = getRepoValues();
		int i;
		
		tUserTableAAC.addCol("ins_type_id", COL_TYPE_ENUM.COL_INT);
		
		for(i=1;i<=tUserTableAAC.getNumRows();i++)
			tUserTableAAC.setInt("ins_type_id", i, Ref.getValue(SHM_USR_TABLES_ENUM.INS_TYPE_TABLE, tUserTableAAC.getString("ins_type", i)));
		
		
		String sSql = "Select tran_num, deal_tracking_num deal_num from ab_tran ab"
				+ " where ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "," + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + "," + TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() +")"
				+ " and ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt()
				+ " and ab.ins_type in (" + _Lib.getSQLStringFromCol(tUserTableAAC, 3) + ")";
		
		try
		{
			int iRet = DBaseTable.execISql(tAac, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}


		tAac.select(tTranMTM, "*", "deal_num EQ $deal_num");
		
		tReturn.select(tAac, "DISTINCT, deal_num", "deal_num GT 0");
		
		tReturn.select(tAac, "deal_num, ins_num, ins_type,internal_portfolio, "+PFOLIO_RESULT_TYPE.BASE_PV_RESULT.toInt()+"(mtm),currency_id(currency)", "deal_num EQ $deal_num");
		tReturn.select(tUserTableAAC, "factor", "ins_type_id EQ $ins_type");
		
		tReturn.select(tFxCurve, "result(fx_tc)", "id EQ $currency");
		
		//Incorpora Reportos remplazando el MTM por el FwdProceeds
		tReturn.select(tReportosFwdProceeds, "fwd_proceeds(mtm)", "deal_num EQ $deal_num");

		tReturn.addFormulaColumn("COL('mtm')*COL('factor')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "monto");

		tReturn.deleteWhereValue("ins_num", 0);;
		
		return tReturn;
		
	}
	
	private Table getACCInstruments() throws OException
	{
		Table tResult = Table.tableNew("Instrumentos AAC");
		String sSql = "Select instrument_type ins_type, factor from " +USER_TABLE_ACC;
		
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tResult, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		return tResult;
	}
	
	
	private Table getSociosLiquidadores() throws OException
	{
		Table tResult = Table.tableNew("Socios Liquidadores");
		String sSql = "Select distinct portfolio internal_portfolio, party party_id from " + EnumsUserTables.USER_MX_MARGINCALL_HISTORY.toString();
		
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tResult, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		return tResult;
	}
	
	
	private void doAddPortfolio(Table tTranMTM) throws OException
	{
		Table tTmp = Table.tableNew();
		String sSql = "\nSelect deal_tracking_num deal_num, tran_num, ins_type, internal_portfolio, external_bunit" +
					  "\nfrom ab_tran where tran_status in ( " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "," +
													  TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + "," +
													  TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() + ")" +
						"\nand tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tTmp, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		tTranMTM.select(tTmp, "tran_num, external_bunit, internal_portfolio", "deal_num EQ $deal_num");
	}
	
	public Table getCashPositionByPortfolio(int iPortfolio) throws OException{
		
	    Table tReturn = Util.NULL_TABLE;
		tReturn       = Table.tableNew("Position by Instrument");

		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT ab.ins_num , ab.internal_portfolio, CAST(TRUNC(CAST(SUM(ab.position) AS float), 0) AS float) as position, h.ticker");
		sb.append("\n FROM ab_tran ab , header h");
		sb.append("\n WHERE ab.internal_portfolio in ("+iPortfolio+")");
		sb.append("\n AND h.ins_num = ab.ins_num");
		sb.append("\n AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n AND ab.tran_status in (").append(TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt())
		   						 .append(",").append(TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt())
 								 .append(",").append(TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt())
		   						 .append(")");
		sb.append("\n AND ab.toolset in ("+TOOLSET_ENUM.CASH_TOOLSET.toInt()+","+TOOLSET_ENUM.FX_TOOLSET.toInt()+")");
		sb.append("\n AND ab.cflow_type in ("+ EnumsUserCflowType.MX_CFLOW_TYPE_AIM_EFECTIVO.toInt()+",")
									.append(EnumsUserCflowType.MX_CFLOW_TYPE_AIM_EFECTIVO.toInt()+",")
									.append(EnumsUserCflowType.MX_CFLOW_TYPE_INTERES_SL.toInt()+",")
									.append(EnumsUserCflowType.MX_CFLOW_TYPE_SALDO_INICIAL.toInt()+",")
									.append(EnumsUserCflowType.MX_CFLOW_TYPE_MOV_CHEQUERAS.toInt()+")");
		sb.append("\n GROUP BY ab.ins_num,ab.internal_portfolio,h.ticker");

		//OConsole.oprint("\nSQL:\n" + sb.toString());
		
		try {
			
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tReturn, sb.toString());
		
		} catch (OException exception){
			
			_Log.printMsg(EnumTypeMessage.ERROR,"ExecISql failed: \n "+ sb.toString());
			_Log.printMsg(EnumTypeMessage.ERROR,"Exception: \n "+ exception.getMessage());
			tReturn.destroy();
			Util.exitFail();
		}
		
		return tReturn;
	}
	
	private Table getVarConsarByQuery() throws OException
	{
		Table tReturn = Table.tableNew();
		String sSql = "";

		iQuery = Query.tableQueryInsert(tTranMTM, "tran_num");
		
		_VaR = new UTIL_VaRConsar(iQuery);

		sSql = _VaR.getOutput_CVAR_SL();
		
		_Log.printMsg(EnumTypeMessage.DEBUG, "Sql:\n" + sSql);
		
		try
		{
			int iRet = DBaseTable.execISql(tReturn,sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		Query.clear(iQuery);
		
		return tReturn;
				
	}
	
	
	private Table getRepoValues() throws OException
	{
		Table tRepoVal = Table.tableNew("Reportos with Fwd Proceeds values");
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nselect ab.deal_tracking_num deal_num, aux.fwd_proceeds "); 
		sb.append("\nfrom ab_tran ab ");
		sb.append("\n   inner join repo_aux_table aux on ab.tran_num = aux.repo_tran_num ");
		sb.append("\nwhere ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "," + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + ") ");
		sb.append("\nand ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\nand ab.toolset = " + TOOLSET_ENUM.REPO_TOOLSET.toInt());
		
		try
		{
			int iRet = DBaseTable.execISql(tRepoVal, sb.toString());
			
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		return tRepoVal;
	}
}