/**
File Name:              UTIL_VaRConsar.java

Author:                 Gustavo Rojas - VMetrix International 
Creation Date:          Marzo 2018

REVISION HISTORY  
Date:                   
Description:            


Main Script:            This is the Main Script
Parameter Script:       None
Display Script:         None

Description:            Utilitario para calculos de VaR Consar que utiliza queries de forma modular.


Assumptions:            None

Instructions:

Script Category:		Util
************************************************************************************/
package com.afore.util.risk.query;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsUserCflowType;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.OException;
import com.olf.openjvs.Str;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class UTIL_VaRConsar {
	private UTIL_Log _Log = new UTIL_Log(this.getClass().getSimpleName());
	private int iQueryID = 0;
	private static final String QUERY_INIT_WITH  = "WITH";
	private static final String QUERY_SEPARATOR_COMMA  = ",";
	private static final String QUERY_UNION_ALL  = "UNION ALL";
	
	private static final String DEBUG_SQL_FILENAME_START = "SqlFile_";
	private static final String DEBUG_SQL_FILENAME_END = ".sql";
	private static final String DEBUG_SQL_PATH = "X:/OpenLink/customSQL/Report Builder/output";
	
	private static final int INS_C_MXPUSD_FIX = 21047;
	private static final int INS_C_MXPCAD_CAD = 28723;
	private static final int INS_C_MXPEUR_EUR = 28724;
	
	/**
	 * @Type Implementacion de Clase Var con filtro de Query ID
	 * @Description Permite traspasar el QueryID proveniente de simulaciones para dar los<br>
	 * correspondientes filtros mediante la tabla query_result.<br>
	 * Cuando el input es cero(0) se calcula en base a toda la cartera.
	 */
	public UTIL_VaRConsar(int iInput){
		_Log.setDEBUG_STATUS(EnumStatus.OFF);
		iQueryID = iInput;
	} 
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CREACION DE TABLAS CTE ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @Type CTE Query cuentas contables
	 * @Description
	 * Extraer cuentas de saldos para medir posicion en divisas USD, CAD, EUR, MXN (sin Factor de Riesgo).<br>
	 * Se actualiza despues de ejecutar la Contabilidad
	 * @return String con formato SQL 
	 */
	protected String getCTE_VarCuentas()
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("\ncuentas as ");
		sb.append("\n(");
		sb.append("\nselect 	ajev.acs_posting_date");
		sb.append("\n 	,ajev.acs_fin_stmt_id");
		sb.append("\n	,ajev.acs_account_number");
		sb.append("\n	,round(ajev.acs_account_amount, 2) acs_account_amount");
		sb.append("\nfrom acs_journal_entries_dual_view ajev");
		sb.append("\nwhere acs_account_number in (	'7115-0001', '7115-0090', '7115-0003',");
		sb.append("\n	'7116-0001', '7116-0090', '7116-0003',");
		sb.append("\n	'7118-0001', '7118-0090', '7118-0003',");
		sb.append("\n	'1211-0002', '1211-0090', '1211-0003'");
		sb.append("\n )");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query saldos contables
	 * @Description Aritmetica de cuentas para extraer posicion en Divisas<br>
	 * Usando este calculo Riesgo Cuadra con evaluadora
	 * @return String con formato SQL 
	 */
	protected String getCTE_VarSaldos()
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\nsaldos as");
		sb.append("\n(");
		sb.append("\nselect cfg.portfolio_id internal_portfolio");
		sb.append("\n,sum(case when c.acs_account_number = '7115-0001' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '7116-0001' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	- sum(case when c.acs_account_number = '7118-0001' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '1211-0002' then round(c.acs_account_amount, 2) else 0 end)/tcusd.precio_sucio_24h as ohd_saldo_usd");
		sb.append("\n,sum(case when c.acs_account_number = '7115-0090' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '7116-0090' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	- sum(case when c.acs_account_number = '7118-0090' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '1211-0090' then round(c.acs_account_amount, 2) else 0 end)/tccad.precio_sucio_24h as ohd_saldo_cad");
		sb.append("\n,sum(case when c.acs_account_number = '7115-0003' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '7116-0003' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	- sum(case when c.acs_account_number = '7118-0003' then round(c.acs_account_amount, 2) else 0 end) ");
		sb.append("\n	+ sum(case when c.acs_account_number = '1211-0003' then round(c.acs_account_amount, 2) else 0 end)/tceur.precio_sucio_24h as ohd_saldo_eur");
		sb.append("\nfrom cuentas c");
		sb.append("\n	left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id");
		sb.append("\n	left join user_mx_vectorprecios_diario tcusd on tcusd.ticker = '*C_MXPUSD_FIX'");
		sb.append("\n	left join user_mx_vectorprecios_diario tccad on tccad.ticker = '*C_MXPCAD_CAD'");
		sb.append("\n	left join user_mx_vectorprecios_diario tceur on tceur.ticker = '*C_MXPEUR_EUR'");
		sb.append("\ngroup by cfg.portfolio_id, tcusd.precio_sucio_24h, tccad.precio_sucio_24h, tceur.precio_sucio_24h");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query saldos contables para divisas CAD - EUR - USD
	 * @Description Aritmetica de cuentas para extraer posicion en Divisas<br>
	 * Usando este calculo Riesgo Cuadra con evaluadora
	 * @return String con formato SQL 
	 */
	protected String getCTE_VarSaldosCash()
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\nsaldos as");
		sb.append("\n(");
		sb.append("\nselect 	cfg.portfolio_id internal_portfolio");
		sb.append("\n,case 	when substr(c.acs_account_number, 6, 4) = '0090' then '" + EnumsCurrency.MX_CURRENCY_CAD.toString() + "'");
		sb.append("\n		when substr(c.acs_account_number, 6, 4) = '0003' then '" + EnumsCurrency.MX_CURRENCY_EUR.toString() + "'");
		sb.append("\n		else '" + EnumsCurrency.MX_CURRENCY_USD.toString() + "'");
		sb.append("\nend as currency");
		sb.append("\n,case 	when substr(c.acs_account_number, 6, 4) = '0090' then " + EnumsCurrency.MX_CURRENCY_CAD.toInt());
		sb.append("\n		when substr(c.acs_account_number, 6, 4) = '0003' then " + EnumsCurrency.MX_CURRENCY_EUR.toInt());
		sb.append("\n		else " + EnumsCurrency.MX_CURRENCY_USD.toInt());
		sb.append("\nend as currency_id");
		sb.append("\n,c.acs_account_number");
		sb.append("\n,sum(round(c.acs_account_amount, 2)) as ohd_saldo");
		sb.append("\nfrom cuentas c");
		sb.append("\n		left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id");
		sb.append("\ngroup by cfg.portfolio_id, c.acs_account_number");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	
	
	/**
	 * @Type CTE Query Detalle para el calculo del VaR
	 * @Description Aritmetica de cuentas para extraer posicion en Divisas<br>
	 * Usando este calculo Riesgo Cuadra con evaluadora
	 * @return String con formato SQL 
	 */
	protected String getCTE_VarDetalleCash()
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\ndetalle as");
		sb.append("\n(");
		sb.append("\nselect s.internal_portfolio");
		sb.append("\n		,s.currency");
		sb.append("\n		,s.currency_id");
		sb.append("\n		,s.acs_account_number");
		sb.append("\n		,s.ohd_saldo");
		sb.append("\n		,v.precio_sucio_24h ohd_fx_mxn");
		sb.append("\n		,case when substr(s.acs_account_number, 1, 4) = '1211' then s.ohd_saldo/v.precio_sucio_24h else s.ohd_saldo end ohd_saldo_ajustado");
		sb.append("\nfrom saldos s");
		sb.append("\n		left join user_mx_vectorprecios_diario v ");
		sb.append("\n				on v.ticker = concat(concat(concat('*C_MXP', s.currency), '_'), s.currency)");
		sb.append("\n						or v.ticker = concat(concat('*C_MXP', s.currency), '_FIX')");
		sb.append("\nunion");
		sb.append("\nselect s.internal_portfolio");
		sb.append("\n		,s.currency");
		sb.append("\n		,s.currency_id");
		sb.append("\n		,'Dividendo*' acs_account_number");
		sb.append("\n		,s.saldo");
		sb.append("\n		,v.precio_sucio_24h ohd_fx_mxn");
		sb.append("\n		,s.saldo ohd_saldo_ajustado");
		sb.append("\nfrom user_mx_ajuste_cash_varconsar s");
		sb.append("\n		left join user_mx_vectorprecios_diario v ");
		sb.append("\n				on v.ticker = concat(concat(concat('*C_MXP', s.currency), '_'), s.currency)");
		sb.append("\n						or v.ticker = concat(concat('*C_MXP', s.currency), '_FIX')");
		sb.append("\nunion");
		sb.append("\nselect  ab.internal_portfolio");
		sb.append("\n        ,c.name currency");
		sb.append("\n		 ,ab.currency currency_id");
		sb.append("\n        ,'Dividendos' acs_account_number");
		sb.append("\n        ,round(sum(case when c.name = '" + EnumsCurrency.MX_CURRENCY_CAD.toString() + "' then ab.position * (1.0 - to_number(cv.valor)) else ab.position end), 2) ohd_saldo");
		sb.append("\n        ,v.precio_sucio_24h");
		sb.append("\n        ,round(sum(case when c.name = '" + EnumsCurrency.MX_CURRENCY_CAD.toString() + "' then ab.position * (1.0 - to_number(cv.valor)) else ab.position end), 2) ohd_saldo_ajustado");
		sb.append("\nfrom ab_tran ab");
		sb.append("\n        left join parameter pa on pa.ins_num = ab.ins_num");
		sb.append("\n        left join currency c on c.id_number = pa.currency");
		sb.append("\n        left join user_configurable_variables cv on cv.proceso = 'Resultados_Contables' and cv.variable = 'iva_cad'");
		sb.append("\n        left join user_mx_vectorprecios_diario v ");
		sb.append("\n                        on v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name)");
		sb.append("\n                                        or v.ticker = concat(concat('*C_MXP', c.name), '_FIX')");
		sb.append("\nwhere ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n        and ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n        and ab.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_DIV_EFECTIVO.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_DERECHOS_CKD_FIBRAS.toInt() + ")");
		sb.append("\n        and ab.settle_date > (select max(acs_posting_date) from acs_journal_entries)"); 
		sb.append("\n        and c.name <> '" + EnumsCurrency.MX_CURRENCY_MXN.toString() + "'");
		sb.append("\ngroup by ab.internal_portfolio, c.name, ab.currency, v.precio_sucio_24h");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	
	
	/**
	 * @Type Query Position sin CASH
	 * @Description Postion para todos los instrumentos salvo Cash <br>
	 * El query considera los estados Validates, New y Simulated, sin embargo,
	 * si se ejecuta a nivel de simulacion, dependerá del Trade Query.<br>
	 * Se filtran los instrumentos de tipo EQT-SIEFORE 
	 * @return String con formato SQL 
	 */
	protected String getCTE_Position_Part_NOCASH()
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nselect	ab.ins_num");
		sb.append("\n	,h.ticker");
		sb.append("\n	,ab.toolset");
		sb.append("\n	,ab.ins_type");
		sb.append("\n	,ab.internal_portfolio");
		sb.append("\n	,CAST(SUM(ab.mvalue) as float) ohd_position");
		sb.append("\nfrom ab_tran ab");
		sb.append("\n	left join header h on h.ins_num = ab.ins_num");
		//TODO: Debug Query ID
		if (iQueryID>0)
			sb.append("\n	inner join query_result q on q.query_result = ab.tran_num and q.unique_id = " + iQueryID);
		sb.append("\nwhere ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n	and ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "," +
														   TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + "," +
														   TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() + ")");
		sb.append("\n	and ab.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt());
		sb.append("\n	and ab.toolset in (" + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," +
										 	   TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," +
										 	   TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + "," +
										 	   TOOLSET_ENUM.BOND_TOOLSET.toInt() + "," +
										 	   TOOLSET_ENUM.MONEY_MARKET_TOOLSET.toInt() + "," +
										 	   TOOLSET_ENUM.EQUITY_TOOLSET.toInt() + ")");
		sb.append("\n	and ab.ins_type not in (" + EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt() + ")");
		sb.append("\ngroup by ab.ins_num,h.ticker,ab.toolset,ab.ins_type,ab.internal_portfolio");
		
		return sb.toString();
	}
	
	/**
	 * 
	 * @Type Subquery de Posicion solo los CASH <br>
	 * @Description Cash Position (Contabilidad)<br>
	 * Se suma la posicion en divisas y los dividendos <br>
	 * <b>Para mapear con Factores de Riesgo, se usan Holding de Monedas (Perpetual):</b><br>
	 * *C_MXPUSD_FIX (21047)<br>
	 * *C_MXPCAD_CAD (28723)<br>
	 * *C_MXPEUR_EUR (28724)<br>
	 * <b>Los dividendos incluisdos son:</b><br>
	 * Dividendo en Efectivo (263)<br>
	 * Derechos por CKDs o Fibras (2025)<p>
	 * Al dividendo en CAD, se resta un impuesto de 15% (viene de user table, puede variar)
	 * @return String con formato SQL
	 * @throws OException 
	 */
	protected String getCTE_Position_Part_CASH() throws OException
	{
		
		StringBuffer sb = new StringBuffer();
		

		sb.append("\nselect  ab.ins_num");
		sb.append("\n	,ab.reference ticker");
		sb.append("\n	,ab.toolset");
		sb.append("\n	,ab.ins_type");
		sb.append("\n	,s.internal_portfolio");
		sb.append("\n	,case   when ab.ins_num = " + INS_C_MXPUSD_FIX + " then trunc(s.ohd_saldo_usd + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)");
		sb.append("\n		when ab.ins_num = " + INS_C_MXPCAD_CAD + " then trunc(s.ohd_saldo_cad + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)");
		sb.append("\n		when ab.ins_num = " + INS_C_MXPEUR_EUR + " then trunc(s.ohd_saldo_eur + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)");
		sb.append("\n	end ohd_position");
		sb.append("\nfrom ab_tran ab");
		sb.append("\n	left join saldos s on 1 = 1");
		sb.append("\n	left join user_mx_ajuste_cash_varconsar a on a.internal_portfolio = s.internal_portfolio and a.ins_num = ab.ins_num");
		//TODO: Debug Query ID
		if (iQueryID>0)
			sb.append("\n	inner join query_result q on q.query_result = ab.tran_num and q.unique_id = " + iQueryID);
		sb.append("\n	left join (");
		sb.append("\n		select  ab.internal_portfolio");
		sb.append("\n			,pa.currency");
		sb.append("\n			,round(sum(case when pa.currency = '" + EnumsCurrency.MX_CURRENCY_CAD.toInt() + "' then ab.position * (1.0 - to_number(cv.valor)) else ab.position end), 2) ohd_dividendo ");
		sb.append("\n		from ab_tran ab");
		sb.append("\n			left join parameter pa on pa.ins_num = ab.ins_num");
		sb.append("\n			left join user_configurable_variables cv on cv.proceso = 'Resultados_Contables' and cv.variable = 'iva_cad'");
		sb.append("\n		where ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n		and ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt());
		sb.append("\n		and ab.cflow_type in (" + EnumsUserCflowType.MX_CFLOW_TYPE_DIV_EFECTIVO.toInt() + ", " + EnumsUserCflowType.MX_CFLOW_TYPE_DERECHOS_CKD_FIBRAS.toInt() + ")");
		sb.append("\n		and ab.settle_date > (select max(acs_posting_date) from acs_journal_entries) ");
		sb.append("\n		group by ab.internal_portfolio, pa.currency");
		sb.append("\n	) d ");
		sb.append("\n	on d.internal_portfolio = s.internal_portfolio and d.currency = ab.currency");
		sb.append("\nwhere ab.ins_num in (" + INS_C_MXPUSD_FIX + ", " + INS_C_MXPCAD_CAD + ", " + INS_C_MXPEUR_EUR + ")");
		sb.append("\n	and ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() );
		sb.append("\n	and ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt());

		return sb.toString();
	}
	
	/**
	 * @Type CTE Query Position TOTAL
	 * @Description Union de los metodos de posicion <br>
	 * getCTE_Position_Part_NOCASH() <br>UNION ALL <br>getCTE_Position_Part_CASH()
	 * @return String con formato SQL 
	 * @throws OException 
	 * @param utiliza la tabla de transacciones que indican el unverso de operaciones a evaluar.
	 */
	protected String getCTE_Position() throws OException
	{
		
		StringBuffer sb = new StringBuffer();
			
		//Table tSiefore = getSiefore();
		
		sb.append("\nposition as (");
		sb.append(getCTE_Position_Part_NOCASH());
		sb.append("\n"+QUERY_UNION_ALL);
		sb.append(getCTE_Position_Part_CASH());

		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query Position of just derivatives toolsets
	 * @Description Metodo de captura de posicion para derivados <br>
	 * GenericFut (50), FinFut (41), BondFut (34)
	 * @return String con formato SQL 
	 * @throws OException 
	 * @param utiliza la tabla de transacciones que indican el unverso de operaciones a evaluar.
	 */
	protected String getCTE_PositionDerivatives() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nposition as (");
		sb.append("\nselect    ab.ins_num");
		sb.append("\n         ,h.ticker");
		sb.append("\n         ,ab.toolset");
		sb.append("\n         ,ab.ins_type");
		sb.append("\n         ,ab.currency");
		sb.append("\n         ,ab.external_lentity");
		sb.append("\n         ,ab.internal_portfolio");
		sb.append("\n         ,p.name siefore");
		sb.append("\n         ,CAST(SUM(ab.mvalue) as float) ohd_position");
		sb.append("\n               from ab_tran ab");
		//TODO: Debug Query ID
		if (iQueryID>0)
			sb.append("\n	inner join query_result q on q.query_result = ab.tran_num and q.unique_id = " + iQueryID);

		sb.append("\n     left join header h on h.ins_num = ab.ins_num");
		sb.append("\n     left join portfolio p on p.id_number = ab.internal_portfolio");
		sb.append("\n     left join currency c on c.id_number = ab.currency");
		sb.append("\n     left join user_mx_vectorprecios_diario v ");
		sb.append("\n              on v.ticker = concat(concat('*C_MXP', c.name), '_FIX') or v.ticker = concat(concat(concat('*C_MXP', c.name), '_'), c.name)");
		sb.append("\nwhere ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
		sb.append("\n		and ab.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + "," +
													   TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt() + "," +
				   									   TRAN_STATUS_ENUM.TRAN_STATUS_PROPOSED.toInt() + ")");
		sb.append("\n	and ab.asset_type = " + ASSET_TYPE_ENUM.ASSET_TYPE_TRADING.toInt());
		sb.append("\n	and ab.toolset in (" + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," +
				 							 TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," +
											 TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ")");
		sb.append("\n	and ab.ins_type not in (" + EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt() + ")");
		sb.append("\ngroup by ab.ins_num");
		sb.append("\n   ,h.ticker");
		sb.append("\n   ,ab.toolset");
		sb.append("\n   ,ab.ins_type");
		sb.append("\n   ,ab.currency");
		sb.append("\n   ,ab.external_lentity");
		sb.append("\n   ,ab.internal_portfolio");
		sb.append("\n   ,p.name");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	
	/**
	 * @Type CTE Query Escenarios Moviles
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Moviles <br>
	 * Computa para la posicion entregada por la CTE <b>getCTE_Position()</b>
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosMoviles()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\nemov as");
		sb.append("\n(");
		sb.append("\nselect row_number() over(partition by p.internal_portfolio order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n		,'Limite VaR CONSAR' as name ");
		sb.append("\n		,'Esc. Moviles' as matriz");
		sb.append("\n		,1 as matriz_id");
		sb.append("\n		,m.id");
		sb.append("\n		,p.internal_portfolio");
		sb.append("\n		,SUM(p.ohd_position * m.var_consar) ohd_exposition");
		sb.append("\nfrom position p");
		sb.append("\n		inner join user_mx_varconsar_escenarios m");
		sb.append("\n					on m.ins_num = p.ins_num");
		sb.append("\ngroup by m.id, p.internal_portfolio");
		sb.append("\n)");
	
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query Escenarios Moviles
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Moviles <br>
	 * Computa para la posicion entregada por la CTE <b>getCTE_Position()</b>
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosVaRMarginal()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\nescenarios as");
		sb.append("\n(");
		sb.append("\nselect row_number() over(partition by p.internal_portfolio order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n		,m.id");
		sb.append("\n		,p.internal_portfolio");
		sb.append("\n		,SUM(p.ohd_position * m.var_consar) ohd_exposition");
		sb.append("\nfrom position p");
		sb.append("\n		inner join user_mx_varconsar_escenarios m");
		sb.append("\n					on m.ins_num = p.ins_num");
		sb.append("\ngroup by m.id, p.internal_portfolio");
		sb.append("\norder by SUM(p.ohd_position * m.var_consar)");
		sb.append("\n)");
	
		return sb.toString();
	}
	
	
	/**
	 * @Type CTE Query Escenarios Moviles Sin Derivados
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Moviles <br>
	 * Computa para la posicion entregada por la CTE <b>getCTE_Position()</b> pero eliminando <br>
	 * los instrumentos derivados: BondFut (34), FinFut (41), GenericFut (50)
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosMoviles_SD() throws OException
	{
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nemov_sd as");
		sb.append("\n(");
		sb.append("\n	select 	row_number() over(partition by p.internal_portfolio order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n			,m.id");
		sb.append("\n			,p.internal_portfolio");
		sb.append("\n			,SUM(p.ohd_position * m.var_consar) ohd_exposition_sd");
		sb.append("\n	from position p");
		sb.append("\n			inner join user_mx_varconsar_escenarios m");
		sb.append("\n						on m.ins_num = p.ins_num");
		sb.append("\n	where p.toolset not in (" + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ", " + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + ")");
		sb.append("\n	group by m.id, p.internal_portfolio");
		sb.append("\n)");
		
		return sb.toString();
		
	}
	
	
	/**
	 * @Type CTE Query Escenarios Fijos
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Fijos <br>
	 * Computa para la posicion entregada por la CTE <b>getCTE_Position()</b> 
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosFijos()
	{
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("\nefij as");
		sb.append("\n(");
		sb.append("\n	select 	row_number() over(partition by p.internal_portfolio order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n			,'Diferencial VaR Condicional' as name ");
		sb.append("\n			,'Esc. Fijos' as matriz");
		sb.append("\n			,0 as matriz_id");
		sb.append("\n			,m.id");
		sb.append("\n			,p.internal_portfolio");
		sb.append("\n			,SUM(p.ohd_position * m.var_consar) ohd_exposition");
		sb.append("\n	from position p");
		sb.append("\n			inner join user_mx_varconsar_esc_stress m");
		sb.append("\n						on m.ins_num = p.ins_num");
		sb.append("\n	group by m.id, p.internal_portfolio");
		sb.append("\n)");
		
		return sb.toString();
		
	}
	
	/**
	 * @Type CTE Query Escenarios Fijos Sin Derivados
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Fijos <br>
	 * Computa para la posicion entregada por la CTE <b>getCTE_Position()</b> pero eliminando <br>
	 * los instrumentos derivados: BondFut (34), FinFut (41), GenericFut (50)
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosFijos_SD()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\nefij_sd as");
		sb.append("\n(");
		sb.append("\n	select 	row_number() over(partition by p.internal_portfolio order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n			,m.id");
		sb.append("\n			,p.internal_portfolio");
		sb.append("\n			,SUM(p.ohd_position * m.var_consar) ohd_exposition_sd");
		sb.append("\n	from position p");
		sb.append("\n			inner join user_mx_varconsar_esc_stress m");
		sb.append("\n						on m.ins_num = p.ins_num");
		sb.append("\n	where p.toolset not in (" + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ", " + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + ")");
		sb.append("\n	group by m.id, p.internal_portfolio");
		sb.append("\n)");
		
		return sb.toString();
		
	}
	
	/**
	 * @Type CTE Query Resumen de Escenarios VaR
	 * @Description Tabla descriptiva de cada limite asociado al VaR CONSAR <br>
	 * Valor del escenario k a utilizar para cada limite y siefore <br>
	 * Valor del limite % a utilizar para cada limite y siefore
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosVAR()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\nesc_var as");
		sb.append("\n(");
		sb.append("\nselect c1.variable siefore");
		sb.append("\n	,p.id_number internal_portfolio");
		sb.append("\n	,c2.proceso");
		sb.append("\n	,c1.valor k");
		sb.append("\n	,c2.valor limite");
		sb.append("\nfrom user_configurable_variables c1 ");
		sb.append("\n	inner join user_configurable_variables c2 on c1.variable = c2.variable");
		sb.append("\n	inner join portfolio p on c1.variable = p.name");
		sb.append("\nwhere c1.proceso = 'Escenarios_CONSAR'");
		sb.append("\n	and c2.proceso in ('Limite VaR CONSAR', 'Diferencial VaR Condicional', 'Limite CVaR')");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query Resumen de Escenarios VaR Marginal
	 * @Description Tabla descriptiva de cada limite asociado al VaR CONSAR <br>
	 * Valor del escenario k a utilizar para cada limite y siefore <br>
	 * Valor del limite % a utilizar para cada limite y siefore
	 * @return String con formato SQL 
	 */
	protected String getCTE_VaR_VARMarginal()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\nvar as");
		sb.append("\n(");
		sb.append("\nselect e.internal_portfolio");
		sb.append("\n	,p.name siefore");
		sb.append("\n	,e.rec k");
		sb.append("\n	,e.id escenario");
		sb.append("\n	,e.ohd_exposition ohd_var");
		sb.append("\nfrom escenarios e");
		sb.append("\n	inner join portfolio p on e.internal_portfolio = p.id_number and p.portfolio_type = 0");
		sb.append("\n	inner join user_configurable_variables u ");
		sb.append("\n		on u.variable = p.name");
		sb.append("\nwhere u.proceso = 'Escenarios_CONSAR'");
		sb.append("\n		and e.rec = cast(u.valor as int)");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query VaR Activo
	 * @Description Tabla con el computo del Activo VaR <br>
	 * Al valor del NAV, se resta el MtoM de los instrumentos CKDs: <br>
	 * EQT-CKD-KCALL (1000043), EQT-CKD-PREFUND (1000044) y bono '91_AGSACB_08' (caso especial)
	 * @return String con formato SQL 
	 */
	protected String getCTE_ActiveVAR()
	{
		
		StringBuffer sb = new StringBuffer();

		sb.append("\navar as");
		sb.append("\n(");
		sb.append("\nselect n.nav_portfolio_id internal_portfolio");
		sb.append("\n	,n.nav_value");
		sb.append("\n	,NVL(m.ohd_mtom_ckd, 0) ohd_mtom_ckd");
		sb.append("\n	,n.nav_value - NVL(m.ohd_mtom_ckd, 0) ohd_activo_var");
		sb.append("\nfrom user_mx_historical_nav n");
		sb.append("\n	left join (");
		sb.append("\n		select 	p.internal_portfolio");
		sb.append("\n			,sum(p.ohd_position * v.precio_sucio_24h) ohd_mtom_ckd");
		sb.append("\n		from position p");
		sb.append("\n			inner join user_mx_vectorprecios_diario v ");
		sb.append("\n				on v.ticker = p.ticker");
		sb.append("\n		where p.ins_type in (" + EnumsInstrumentsMX.MX_EQT_CKDKCALL.toInt() + ", " + EnumsInstrumentsMX.MX_EQT_CKDPREFUND.toInt() + ") or p.ticker = '91_AGSACB_08'");
		sb.append("\n		group by p.internal_portfolio");
		sb.append("\n		) m");
		sb.append("\n	on m.internal_portfolio = n.nav_portfolio_id");
		sb.append("\n)");
		
		return sb.toString();
	}
	
	/**
	 * @Type CTE Query Escenarios Moviles por socio liquidador
	 * @Description 1000 escenarios de Riesgo por Siefore usando Matriz de Escenarios Moviles <br>
	 * Computa para la posicion entregada por la CTE de position de derivados <b>getCTE_PositionDerivatives()</b>
	 * Actuelmente Existen los siguientes Socios Liquidadores para Derivados:
	 * - Legal Entity: BBVA (20722)
	 * - Legal Entity: SANTANDER (20724)
	 * - Legal Entity: HSBC (20062)
	 * - Legal Entity:GS SL (20729)
	 * @return String con formato SQL 
	 */
	protected String getCTE_EscenariosBySocioLiquidador() throws OException
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\nesc_sl as");
		sb.append("\n(");
		sb.append("\nselect row_number() over(partition by p.internal_portfolio, p.external_lentity order by SUM(p.ohd_position * m.var_consar)) rec");
		sb.append("\n     ,m.id");
		sb.append("\n     ,p.internal_portfolio");
		sb.append("\n     ,p.currency");
		sb.append("\n     ,p.external_lentity");
		sb.append("\n     ,pf.name siefore");
		sb.append("\n     ,SUM(p.ohd_position * m.var_consar) ohd_exposition");
		sb.append("\nfrom position p");
		sb.append("\n      inner join user_mx_varconsar_escenarios m");
		sb.append("\n      	on m.ins_num = p.ins_num");
		sb.append("\n      inner join portfolio pf");
		sb.append("\n      	on pf.id_number = p.internal_portfolio");
		sb.append("\ngroup by m.id, p.internal_portfolio, p.currency, p.external_lentity, pf.name");
		sb.append("\n)");
	
		return sb.toString();
	}
	
	
	
	/**
	 * @Type CTE Query Metricas de riesgo
	 * @Description almacenadas y mantenidas en la user table user_mx_metricas <br>
	 * @return String con formato SQL 
	 */
	protected String getCTE_Metricas() throws OException
	{
		StringBuffer sb = new StringBuffer();

		sb.append("\nmetricas as ");
		sb.append("\n( ");
		sb.append("\nselect met.metric_id, met.metric_name, met.result_type, met.metric_trigger ");
		sb.append("\n,CASE WHEN LOWER(met.metric_name)  like '%var consar%' THEN 1   ");
		sb.append("\n          WHEN LOWER(met.metric_name)  like '%var condicional%' THEN 2 ");
		sb.append("\n          WHEN LOWER(met.metric_name)  like '%var stress%' THEN 3 ");
		sb.append("\n          WHEN LOWER(met.metric_name)  like '%activo var%' THEN 4 ELSE 0 ");
		sb.append("\n          END tipo_var ");
		sb.append("\n,CASE WHEN LOWER(met.metric_name)  like '%fijos%' THEN 1   ");
		sb.append("\n          WHEN LOWER(met.metric_name)  like '%viles%' THEN 2 ELSE 0 ");
		sb.append("\n          END tipo_esc ");
		sb.append("\n,CASE WHEN LOWER(met.metric_name)  like '%con derivados%' THEN 1   ");
		sb.append("\n           WHEN LOWER(met.metric_name)  like '%sin derivados%' THEN 2 ELSE 0 ");
		sb.append("\n          END tipo_der ");
		sb.append("\nfrom user_mx_risk_metric met ");
		sb.append("\nwhere met.result_type = 'VAR' ");
		sb.append("\n)");
		
		return sb.toString();
		
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// OUTPUT SECTION ///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * @Type Output VaRConsar con escenarios Moviles
	 * @Description Query de Salida para Computar VaR Consar de escenarios Moviles <br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 * - VAR<br>
	 * - VAR_SD<br>
	 * - CVAR<br>
	 * - CVAR_SD<br>
	 */
	public String getOutput_VarConsar_Movil() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosVAR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_ActiveVAR()).append("\n");
		//Output
		sb.append("\nselect e.name ");
		sb.append("\n	,e.matriz");
		sb.append("\n	,e.internal_portfolio");
		sb.append("\n	,u.k");
		sb.append("\n	,d.business_date fecha_var");
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario");
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc");
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var");
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_var_sd");
		sb.append("\n	,-avg(e.ohd_exposition) ohd_cvar");
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_cvar_sd");
		sb.append("\n	,-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd)) ohd_cvar_dif");
		sb.append("\n	,n.ohd_activo_var ");
		sb.append("\n	,trunc(-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd))/n.ohd_activo_var, 8) ohd_uso");
		sb.append("\n	,u.limite");
		sb.append("\nfrom esc_var u");
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso");
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec");
		sb.append("\n	left join system_dates d on 1 = 1");
		sb.append("\n	left join avar n on n.internal_portfolio = e.internal_portfolio");
		sb.append("\nwhere e.rec <= u.k");
		sb.append("\ngroup by e.name, e.matriz, e.internal_portfolio, u.k, d.business_date, n.ohd_activo_var, u.limite");
		
		
		doPrintSqlFile(sb.toString(),"VarConsar_Movil");
			
		
		return sb.toString();
	}
	
	
	/**
	 * @Type Output VaRConsar con escenarios Fijos
	 * @Description Query de Salida para Computar VaR Consar de escenarios Fijos <br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 * - VAR<br>
	 * - VAR_SD<br>
	 * - CVAR<br>
	 * - CVAR_SD<br>
	 */
	public String getOutput_VarConsar_Fijo() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosVAR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_ActiveVAR()).append("\n");
		//Output
		sb.append("\nselect e.name ");
		sb.append("\n	,e.matriz");
		sb.append("\n	,e.internal_portfolio");
		sb.append("\n	,u.k");
		sb.append("\n	,d.business_date fecha_var");
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario");
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc");
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var");
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_var_sd");
		sb.append("\n	,-avg(e.ohd_exposition) ohd_cvar");
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_cvar_sd");
		sb.append("\n	,-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd)) ohd_cvar_dif");
		sb.append("\n	,n.ohd_activo_var ");
		sb.append("\n	,trunc(-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd))/n.ohd_activo_var, 8) ohd_uso");
		sb.append("\n	,u.limite");
		sb.append("\nfrom esc_var u");
		sb.append("\n	inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso");
		sb.append("\n	inner join efij_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec");
		sb.append("\n	left join system_dates d on 1 = 1");
		sb.append("\n	left join avar n on n.internal_portfolio = e.internal_portfolio");
		sb.append("\nwhere e.rec <= u.k");
		sb.append("\ngroup by e.name, e.matriz, e.internal_portfolio, u.k, d.business_date, n.ohd_activo_var, u.limite");
		
		doPrintSqlFile(sb.toString(),"VarConsar_Fijo");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Escenarios Moviles
	 * @Description Query de Salida de los Escenarios Moviles <br>
	 * @return String con formato SQL<br>
	 */
	public String getOutput_EscenariosMoviles() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles()).append("\n");
		//Output
		sb.append("\nselect * from emov ");
		
		doPrintSqlFile(sb.toString(),"EscenariosMoviles");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Escenarios Moviles sin derivados
	 * @Description Query de Salida de los Escenarios Moviles sin derivados<br>
	 * @return String con formato SQL<br>
	 */
	public String getOutput_EscenariosMoviles_SD() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles_SD()).append("\n");
		//Output
		sb.append("\nselect * from emov_sd ");
		
		doPrintSqlFile(sb.toString(),"EscenariosMoviles_SD");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Escenarios Fijos
	 * @Description Query de Salida de los Escenarios Fijos <br>
	 * @return String con formato SQL<br>
	 */
	public String getOutput_EscenariosFijos() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos()).append("\n");
		//Output
		sb.append("\nselect * from efij ");
		
		doPrintSqlFile(sb.toString(),"EscenariosFijos");
		
		
		return sb.toString();
	}
	
	/**
	 * @Type Output Escenarios Fijos sin derivados
	 * @Description Query de Salida de los Escenarios Fijos sin derivados<br>
	 * @return String con formato SQL<br>
	 */
	public String getOutput_EscenariosFijos_SD() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos_SD()).append("\n");
		//Output
		sb.append("\nselect * from efij_sd ");
		
		doPrintSqlFile(sb.toString(),"EscenariosFijos_SD");
		
		
		return sb.toString();
	}
	
	
	/**
	 * @Type Output VaR Divisas
	 * @Description Query de Salida de calculo de VaR por divisas<br>
	 * @return String con formato SQL<br>
	 */
	public String getOutput_VaRDivisas() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldosCash()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarDetalleCash()).append("\n");
		//Output
		sb.append("\nselect 	internal_portfolio");
		sb.append("\n	,currency");
		sb.append("\n	,currency_id");
		sb.append("\n	,acs_account_number");
		sb.append("\n	,ohd_saldo");
		sb.append("\n	,ohd_fx_mxn");
		sb.append("\n	,ohd_saldo_ajustado");
		sb.append("\nfrom detalle ");
		sb.append("\nunion");
		sb.append("\nselect 	s.internal_portfolio");
		sb.append("\n	,s.currency");
		sb.append("\n	,s.currency_id");
		sb.append("\n		,'[CALCULADO]' acs_account_number");
		sb.append("\n		,sum(case when substr(s.acs_account_number, 1, 4) in ('7115', '7116', '1211', 'Divi') then s.ohd_saldo_ajustado else 0 end)");
		sb.append("\n			- sum(case when substr(s.acs_account_number, 1, 4) in ('7118') then s.ohd_saldo_ajustado else 0 end) as ohd_saldo");
		sb.append("\n		,max(s.ohd_fx_mxn) as ohd_fx_mxn");
		sb.append("\n		,sum(case when substr(s.acs_account_number, 1, 4) in ('7115', '7116', '1211', 'Divi') then s.ohd_saldo_ajustado else 0 end)");
		sb.append("\n			- sum(case when substr(s.acs_account_number, 1, 4) in ('7118') then s.ohd_saldo_ajustado else 0 end) as ohd_saldo_ajustado");
		sb.append("\nfrom detalle s");
		sb.append("\n		left join user_mx_vectorprecios_diario v ");
		sb.append("\n					on v.ticker = concat(concat(concat('*C_MXP', s.currency), '_'), s.currency)");
		sb.append("\n							or v.ticker = concat(concat('*C_MXP', s.currency), '_FIX')");
		sb.append("\ngroup by s.internal_portfolio, s.currency, s.currency_id");
		
		doPrintSqlFile(sb.toString(),"VaR_Divisas");
		
		
		return sb.toString();
	}
	
	
	
	/**
	 * @Type Output VaRConsar Completo con escenarios fijos y moviles para reporte de limites
	 * @Description Query de Salida para Computar VaR Consar en todos sus escenarios<br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 * - VAR<br>
	 * - VAR_SD<br>
	 * - CVAR<br>
	 * - CVAR_SD<br>
	 * - ACTIVOVAR<br>
	 */
	public String getOutput_AllVarConsarLimit() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosVAR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_ActiveVAR()).append("\n");
		//Output
		
		sb.append("\nselect	e.name ");
		sb.append("\n	,e.matriz ");
		sb.append("\n	,e.internal_portfolio ");
		sb.append("\n	,u.k ");
		sb.append("\n	,d.business_date fecha_var ");
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario ");
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc ");
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var ");
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_var_sd ");
		sb.append("\n	,-avg(e.ohd_exposition) ohd_cvar ");
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_cvar_sd ");
		sb.append("\n	,-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd)) ohd_cvar_dif ");
		sb.append("\n	,-sum(case when e.rec = 1 then e.ohd_exposition else 0.0 end) ohd_var_stress ");
		sb.append("\n	,n.ohd_activo_var ");
		sb.append("\n	,trunc(-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end)/n.ohd_activo_var, 8) ohd_uso ");
		sb.append("\n	,u.limite ");
		sb.append("\nfrom esc_var u ");
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso ");
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec ");
		sb.append("\n	left join system_dates d on 1 = 1 ");
		sb.append("\n	left join avar n on n.internal_portfolio = e.internal_portfolio ");
		sb.append("\nwhere e.rec <= u.k ");
		sb.append("\ngroup by e.name, e.matriz, e.internal_portfolio, u.k, d.business_date, n.ohd_activo_var, u.limite ");
		
		
		sb.append("\nUNION ALL ");
		
		sb.append("\nselect e.name ");
		sb.append("\n	,e.matriz");
		sb.append("\n	,e.internal_portfolio");
		sb.append("\n	,u.k");
		sb.append("\n	,d.business_date fecha_var");
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario");
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc");
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var");
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_var_sd");
		sb.append("\n	,-avg(e.ohd_exposition) ohd_cvar");
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_cvar_sd");
		sb.append("\n	,-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd)) ohd_cvar_dif");
		sb.append("\n	,-sum(case when e.rec = 1 then e.ohd_exposition else 0.0 end) ohd_var_stress ");
		sb.append("\n	,n.ohd_activo_var ");
		sb.append("\n	,trunc(-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd))/n.ohd_activo_var, 8) ohd_uso");
		sb.append("\n	,u.limite");
		sb.append("\nfrom esc_var u");
		sb.append("\n	inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso");
		sb.append("\n	inner join efij_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec");
		sb.append("\n	left join system_dates d on 1 = 1");
		sb.append("\n	left join avar n on n.internal_portfolio = e.internal_portfolio");
		sb.append("\nwhere e.rec <= u.k");
		sb.append("\ngroup by e.name, e.matriz, e.internal_portfolio, u.k, d.business_date, n.ohd_activo_var, u.limite");
		
		sb.append("\nUNION ALL ");

		sb.append("\nselect	'Limite CVaR' name");
		sb.append("\n	,e.matriz");
		sb.append("\n	,e.internal_portfolio");
		sb.append("\n	,u.k");
		sb.append("\n	,d.business_date fecha_var");
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario");
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc");
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_var");
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_var_sd");
		sb.append("\n	,-avg(e.ohd_exposition) ohd_cvar");
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_cvar_sd");
		sb.append("\n	,-(avg(e.ohd_exposition) - avg(s.ohd_exposition_sd)) ohd_cvar_dif");
		sb.append("\n	,-sum(case when e.rec = 1 then e.ohd_exposition else 0.0 end) ohd_var_stress ");
		sb.append("\n	,n.ohd_activo_var");
		sb.append("\n	,trunc(-avg(e.ohd_exposition)/n.ohd_activo_var, 8) ohd_uso");
		sb.append("\n	,u.limite");
		sb.append("\nfrom esc_var u");
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and u.proceso = 'Limite CVaR'");
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec");
		sb.append("\n	left join system_dates d on 1 = 1");
		sb.append("\n	left join avar n on n.internal_portfolio = e.internal_portfolio");
		sb.append("\nwhere e.rec <= u.k");
		sb.append("\ngroup by e.name, e.matriz, e.internal_portfolio, u.k, d.business_date, n.ohd_activo_var, u.limite");
		
		doPrintSqlFile(sb.toString(),"getOutput_AllVarConsarLimit");
		
		return sb.toString();
	}
	
	/**
	 * @Type Output VaRConsar Completo con escenarios fijos y moviles para su almacenamiento historico
	 * @Description Query de Salida para Computar VaR Consar en todos sus escenarios<br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 * - VAR<br>
	 * - VAR_SD<br>
	 * - CVAR<br>
	 * - CVAR_SD<br>
	 * - ACTIVOVAR<br>
	 */
	public String getOutput_VarConsarSaveMetricas() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosFijos_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosMoviles_SD()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosVAR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_ActiveVAR()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Metricas()).append("\n");
		//Output
		
		//1 VaR MOV
		sb.append("\nselect	e.internal_portfolio ");
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n  	left join metricas m on m.tipo_var = 1 and m.tipo_esc = 2 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//2 CVaR MOV
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n  ,u.k "); 
		sb.append("\n  ,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-avg(e.ohd_exposition) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n  left join metricas m on m.tipo_var = 2 and m.tipo_esc = 2 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//3 VaR MOV SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n  inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n  left join metricas m on m.tipo_var = 1 and m.tipo_esc = 2 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//4 CVaR MOV SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n  inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n  left join metricas m on m.tipo_var = 2 and m.tipo_esc = 2 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//5 VaR FIJ
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when e.rec = u.k then e.ohd_exposition else 0.0 end) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n	inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n  left join metricas m on m.tipo_var = 1 and m.tipo_esc = 1 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//6 CVaR FIJ 
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-avg(e.ohd_exposition) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n	inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n  left join metricas m on m.tipo_var = 2 and m.tipo_esc = 1 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//7 VaR FIJ SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when s.rec = u.k then s.ohd_exposition_sd else 0.0 end) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n  inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n	inner join efij_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n  left join metricas m on m.tipo_var = 1 and m.tipo_esc = 1 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//8 CVaR FIJ SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-avg(s.ohd_exposition_sd) ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n  inner join efij e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n	inner join efij_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n  left join metricas m on m.tipo_var = 2 and m.tipo_esc = 1 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//9 VaR STRESS MOV 
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,'1' k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = 1 then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = 1 then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when e.rec = 1 then e.ohd_exposition else 0.0 end) ohd_result_value ");
		sb.append("\nfrom emov e ");  
		sb.append("\n   left join metricas m on m.tipo_var = 3 and m.tipo_esc = 2 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec = 1 ");
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//10 VaR Stress MOV SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,'1' k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = 1 then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = 1 then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when s.rec = 1 then s.ohd_exposition_sd else 0.0 end) ohd_result_value ");
		sb.append("\nfrom emov e ");
		sb.append("\n	inner join emov_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n  left join metricas m on m.tipo_var = 3 and m.tipo_esc = 2 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec = 1 "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//11 VaR Stress FIJ 
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,'1' k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = 1 then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = 1 then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when e.rec = 1 then e.ohd_exposition else 0.0 end) ohd_result_value ");
		sb.append("\nfrom efij e ");  
		sb.append("\n  left join metricas m on m.tipo_var = 3 and m.tipo_esc = 1 and m.tipo_der = 1 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec = 1 "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//12 VaR Stress FIJ SD
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,'1' k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = 1 then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = 1 then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,-sum(case when s.rec = 1 then s.ohd_exposition_sd else 0.0 end) ohd_result_value ");
		sb.append("\nfrom efij e ");
		sb.append("\n	inner join efij_sd s on s.internal_portfolio = e.internal_portfolio and s.rec = e.rec "); 
		sb.append("\n left join metricas m on m.tipo_var = 3 and m.tipo_esc = 1 and m.tipo_der = 2 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\nwhere e.rec = 1 ");
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name,d.business_date ");
		
		sb.append("\nUNION ALL ");
		
		//13 Activo VaR 
		sb.append("\nselect	e.internal_portfolio "); 
		sb.append("\n  ,m.metric_id ");
		sb.append("\n  ,m.metric_name ");
		sb.append("\n	,u.k "); 
		sb.append("\n	,d.business_date fecha_var "); 
		sb.append("\n	,sum(case when e.rec = u.k then e.id else 0.0 end) escenario "); 
		sb.append("\n	,d.business_date - sum(case when e.rec = u.k then e.id else 0.0 end) fecha_esc "); 
		sb.append("\n	,n.ohd_activo_var ohd_result_value ");
		sb.append("\nfrom esc_var u "); 
		sb.append("\n	inner join emov e on e.internal_portfolio = u.internal_portfolio and e.name = u.proceso "); 
		sb.append("\n  left join metricas m on m.tipo_var = 4 and m.tipo_esc = 0 and m.tipo_der = 0 ");
		sb.append("\n	left join system_dates d on 1 = 1 "); 
		sb.append("\n  left join avar n on n.internal_portfolio = e.internal_portfolio "); 
		sb.append("\nwhere e.rec <= u.k "); 
		sb.append("\ngroup by e.internal_portfolio, m.metric_id, m.metric_name, u.k, d.business_date, n.ohd_activo_var ");


		doPrintSqlFile(sb.toString(),"getOutput_VarConsarSaveMetricas");
		
		return sb.toString();
	}
	
	
	/**
	 * @Type Output VaRConsar Marginal
	 * @Description Query de Salida para Computar VaR Marginal con escenarios moviles unicamente<br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 */
	public String getOutput_VaRMarginal() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_VarCuentas()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VarSaldos()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_Position()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosVaRMarginal()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_VaR_VARMarginal()).append("\n");
		//Output
		sb.append("\nselect 	v.internal_portfolio");
		sb.append("\n	,p.ins_num");
		sb.append("\n	,p.ticker");
		sb.append("\n	,p.ohd_position");
		sb.append("\n	,p.ohd_position + 1 ohd_new_position");
		sb.append("\n	,-v.ohd_var ohd_var");
		sb.append("\n	,-m.var_consar ohd_unit_var");
		sb.append("\n	,-p.ohd_position * m.var_consar ohd_var_ins");
		sb.append("\n	,-(v.ohd_var + m.var_consar) ohd_new_var");
		sb.append("\n	,m.var_consar/v.ohd_var ohd_margin_var");
		sb.append("\n	,u.valor");
		sb.append("\nfrom var v");
		sb.append("\n	left join position p on p.internal_portfolio = v.internal_portfolio ");
		sb.append("\n	inner join user_mx_varconsar_escenarios m on m.ins_num = p.ins_num and m.id = v.escenario ");
		sb.append("\n	inner join user_configurable_variables u on u.proceso = 'limite_var_marginal' and u.variable = v.siefore");
		sb.append("\norder by v.internal_portfolio");
		
		doPrintSqlFile(sb.toString(),"VarMarginal");
		
		return sb.toString();
	}
	
	
	/**
	 * @Type Output CVaR por Socio Liquidador
	 * @Description Query de Salida para Computar CVaR por socio liquidador con escenarios moviles unicamente<br>
	 * @return String con formato SQL que entrega los resultados:<br>
	 */
	public String getOutput_CVAR_SL() throws OException
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(QUERY_INIT_WITH);
		sb.append(this.getCTE_PositionDerivatives()).append(QUERY_SEPARATOR_COMMA).append("\n");
		sb.append(this.getCTE_EscenariosBySocioLiquidador()).append("\n");
		//Output
		
		sb.append("\nselect sl.siefore,sl.internal_portfolio, sl.external_lentity, sl.currency ");
		sb.append("\n	,-sum(case when sl.rec = c.valor then sl.ohd_exposition else 0.0 end) ohd_var_consar");
		sb.append("\n	,-avg(sl.ohd_exposition) ohd_cvar_consar");
		sb.append("\nfrom esc_sl sl ");
		sb.append("\n  left join user_configurable_variables c");
		sb.append("\n       on c.proceso = 'Escenarios_CONSAR' and c.variable = sl.siefore");       
		sb.append("\nwhere sl.rec = c.valor");
		sb.append("\ngroup by sl.siefore,sl.internal_portfolio, sl.external_lentity, sl.currency");
		
		doPrintSqlFile(sb.toString(),"CVaR_SocioLiquidador");
		
		return sb.toString();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// PRIVATE METHODS SECTION  //////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	
	private void doPrintSqlFile(String sSql, String sTitulo) throws OException
	{
		if (_Log.getDEBUG_STATUS()==EnumStatus.ON)
		{
			Path path = Paths.get(DEBUG_SQL_PATH);
			String sSqlFile = path.toString() + "\\" + DEBUG_SQL_FILENAME_START + sTitulo + DEBUG_SQL_FILENAME_END;
			_Log.printMsg(EnumTypeMessage.DEBUG, "Creando Archivo SQL: " + sTitulo);
			_Log.printMsg(EnumTypeMessage.DEBUG, "File: " + sSqlFile);
			
			Str.printToFile(sSqlFile, sSql);
			
		}
	}

}
