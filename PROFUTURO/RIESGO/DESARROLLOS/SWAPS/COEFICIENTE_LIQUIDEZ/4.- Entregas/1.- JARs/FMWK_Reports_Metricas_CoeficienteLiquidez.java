/**$Header: v1.0  31/01/2018 $*/
/*
 * File Name:               FMWK_Reports_Metricas_CoeficienteLiquidez.java
 * Input File Name:         None
 * Author:                  Gustavo Rojas Arteaga - VMetrix
 * Creation Date:           31/01/2018
 * 
 * REVISION HISTORY
 *  Release:				     
 *  Date:				        
 *  Author:					
 *  Description:			     
 *  			      
 *  
 */
package com.afore.reportes.metricas;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserSimResultType;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class FMWK_Reports_Metricas_CoeficienteLiquidez implements IScript
{

	private final String sPlugInName = this.getClass().getSimpleName();

	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_Afore _utilafore = new UTIL_Afore();
	
	@Override
	public void execute(IContainerContext context) throws OException 
	{
		_Log.markStartScript();
		_Log.setDEBUG_STATUS(EnumStatus.OFF);
		
		Table tArgt			= Util.NULL_TABLE;
		Table tReturnt		= Util.NULL_TABLE;
		Table tResList		= Util.NULL_TABLE;
		Table tSimRes		= Util.NULL_TABLE;
		Table tTrans		= Util.NULL_TABLE;
		Table tGenResults	= Util.NULL_TABLE;
		Table tCoefiLiqui	= Util.NULL_TABLE;

		int iQuery, iFecha;
		
		try{
			
			tArgt    = context.getArgumentsTable();
    		tReturnt = context.getReturnTable();
    		
    		iFecha = OCalendar.today();
    		
    		if(tArgt.getColNum("QueryId") <= 0)
    			tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);

			tResList = Sim.createResultListForSim ();
    		SimResultType usrCoef   = SimResultType.create(EnumsUserSimResultType.MX_SIM_RISK_COEFICIENTE_LIQUIDEZ.toString());
    		SimResult.addResultForSim(tResList, usrCoef);
    		
    		tTrans = getAllTrades();
    		
			iQuery = Query.tableQueryInsert(tTrans, "tran_num");
			
			if(tArgt.getNumRows() < 1)
				tArgt.addRow();
			
			tArgt.setInt("QueryId", 1, iQuery);
			
			tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
    		
    		Query.clear(iQuery);
    		
    		tGenResults	= SimResult.getGenResults(tSimRes);
    		tCoefiLiqui	= SimResult.getGenResultTables(tGenResults, EnumsUserSimResultType.MX_SIM_RISK_COEFICIENTE_LIQUIDEZ.toInt()).getTable("results", 1);
    		
    		tReturnt.addCol("fecha", COL_TYPE_ENUM.COL_INT);
    		
    		tReturnt.select(tCoefiLiqui, "*", "internal_portfolio GT 0");
    		
    		tReturnt.setColValInt("fecha", iFecha);
    		
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally
		{
			_Log.markEndScript();
		}

	}

	private Table getAllTrades() throws OException
	{
		Table tTmp = Table.tableNew();
		String sSql = "\nselect ab.deal_tracking_num deal_num, ab.tran_num, ab.ins_num, ab.ins_type, ab.internal_portfolio, ins.external_bunit" +
				"\nfrom ab_tran ab, ab_tran ins" +
				"\nwhere ab.ins_num = ins.ins_num" +
				"\nand ab.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +
				"\nand ins.tran_status = " + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() +
				"\nand ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() +
				"\nand ins.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() +
				//"\nand ab.toolset in (" + TOOLSET_ENUM.REPO_TOOLSET.toInt() + "," + TOOLSET_ENUM.BOND_TOOLSET.toInt() + "," + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + ")";
				"\nand ab.toolset in (" + TOOLSET_ENUM.BOND_TOOLSET.toInt() + "," + TOOLSET_ENUM.BONDFUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.FIN_FUT_TOOLSET.toInt() + "," + TOOLSET_ENUM.GENERIC_FUTURE_TOOLSET.toInt() + "," + TOOLSET_ENUM.SWAP_TOOLSET.toInt() + ")";
		
		_Log.printMsg(EnumTypeMessage.DEBUG, "Sql:" + sSql);	
		try
		{
			@SuppressWarnings("unused")
			int iRet = DBaseTable.execISql(tTmp, sSql);
		}catch(OException e)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}
		
		return tTmp;
	}
}

