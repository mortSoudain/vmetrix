/*$Header: v 1.0 - 24/01/2018 $*/
/**
 * 
 * File Name:              FMWK_Reports_Plugin_Flujos_Futuros.java
 * Author:                 Gustavo Rojas - VMetrix SpA
 * Creation Date:          Enero 2018
 * Version:                1.0
 * Description:            Reporte tipo plugin para Report Builder con flujos futuros de hoy al infinito... 
 *
 * 
 * REVISION HISTORY
 * Date:		             
 * Version/Autor:          
 * Description:            
 * 
 * 
 *************************************************************************************/

package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.Index;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Query;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Sim;
import com.olf.openjvs.SimResult;
import com.olf.openjvs.SimResultType;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.BUY_SELL_ENUM;
import com.olf.openjvs.enums.CFLOW_TYPE;
import com.olf.openjvs.enums.COL_FORMAT_BASE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CONF_INT_EXT;
import com.olf.openjvs.enums.EVENT_TYPE_ENUM;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.PFOLIO_RESULT_TYPE;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;



public class FMWK_Reports_Plugin_Flujos_Futuros implements IScript
{
	private final String sGENTEC_WILDCARD = "GENTEC%";
	protected String sScriptName = this.getClass().getSimpleName();
	protected UTIL_Log _Log = new UTIL_Log(sScriptName);
	protected UTIL_Afore _Util = new UTIL_Afore();

	public void execute(IContainerContext context) throws OException
	{
		_Log.markStartScript();

		Table tReturnt = context.getReturnTable();

    	tReturnt.addCol("deal_num", COL_TYPE_ENUM.COL_INT, "Deal Num");
    	tReturnt.addCol("moneda", COL_TYPE_ENUM.COL_INT, "Divisa");
    	tReturnt.addCol("internal_portfolio", COL_TYPE_ENUM.COL_INT, "Portfolio");
    	tReturnt.addCol("buy_sell", COL_TYPE_ENUM.COL_INT, "Compra/Venta");
    	tReturnt.addCol("external_bunit", COL_TYPE_ENUM.COL_INT, "Contraparte");
    	tReturnt.addCol("cflow_type", COL_TYPE_ENUM.COL_INT, "Tipo Movimiento");
    	tReturnt.addCol("ins_type", COL_TYPE_ENUM.COL_INT, "Tipo Instrumento");
    	tReturnt.addCol("reference", COL_TYPE_ENUM.COL_STRING, "Reference");
    	tReturnt.addCol("trade_date", COL_TYPE_ENUM.COL_INT, "Trade Date");
    	tReturnt.addCol("cflow_date", COL_TYPE_ENUM.COL_INT, "Settlement Date");
    	tReturnt.addCol("ticker", COL_TYPE_ENUM.COL_STRING, "Ticker");
    	tReturnt.addCol("mvalue", COL_TYPE_ENUM.COL_DOUBLE, "Posicion Vigente");
    	tReturnt.addCol("position", COL_TYPE_ENUM.COL_DOUBLE, "Posicion Operada");
    	tReturnt.addCol("amount_known_flow", COL_TYPE_ENUM.COL_DOUBLE, "Monto Origen Conocido Pay/Rec");
    	tReturnt.addCol("amount_proj_flow", COL_TYPE_ENUM.COL_DOUBLE, "Monto Origen Proj Pay/Rec");
    	tReturnt.addCol("base_amount_known_flow", COL_TYPE_ENUM.COL_DOUBLE, "Monto Base Conocido Pay/Rec");
    	tReturnt.addCol("base_amount_proj_flow", COL_TYPE_ENUM.COL_DOUBLE, "Monto Base Proj Pay/Rec");
    	tReturnt.addCol("flow_known_proj", COL_TYPE_ENUM.COL_STRING, "Flujo Conocido/Estimado");
    	tReturnt.addCol("tc", COL_TYPE_ENUM.COL_DOUBLE, "TC");
				
		getDataDisplay(tReturnt, context);


     	_Log.markEndScript();
	}
	
	
	
	private void getDataDisplay(Table tReturnt, IContainerContext context) throws OException {
		
		int iDealNum;
		int iMoneda;
		int iPortfolio;
		int iCompraVenta;
		int iContraparte;
		int iCflowType;
		int iInsType;
		String sReference;
		int iTradeDate;
		int iCflowDate;
		String sTicker;
		double dMvalue;
		double dPosition;
		double dComisionIva = 0;
		double dBaseComisionIva = 0;
		double dAmountKnownFlow;
		double dSettleFX;
		double dAmountProjFlow = 0;
		double dBaseAmountKnownFlow;
		double dBaseAmountProjFlow = 0;
		String sFlowKnownProj = "CONOCIDO";
//		int iTranStatus;
		double dTC = 0;
		int iRowNew;
		
		Table tInst;
		Table tTodayCashFlow;
		Table tFutureCashFlow;
		Table tCurveSpotMxn;
		Table tTemp;
		
		tInst = getTickerForInstrument();
		
		tTodayCashFlow = getTodayCashFlow(context);
		tTodayCashFlow.setTableName("TodayCashFlow");
		tTodayCashFlow.select(tInst, "ticker", "ins_num EQ $ins_num");
		
		getImportNetoEquity(tTodayCashFlow, "today");
		
		int iIndexId = Ref.getValue(SHM_USR_TABLES_ENUM.INDEX_TABLE, "SPOT_FX.MXN");
		tCurveSpotMxn = Index.getOutput(iIndexId);
		tCurveSpotMxn.setColName("Price (Mid)", "price_mid");
		
		tTodayCashFlow.addCol("currency_name", COL_TYPE_ENUM.COL_STRING);
		tTodayCashFlow.copyColFromRef("currency_id", "currency_name", SHM_USR_TABLES_ENUM.CURRENCY_TABLE);
		
		tTodayCashFlow.select(tCurveSpotMxn, "price_mid", "Spot EQ $currency_name");
		tTodayCashFlow.addFormulaColumn("iif(COL('toolset') == "+TOOLSET_ENUM.CASH_TOOLSET.toInt()+", 1 / COL('price_mid'), COL('settle_fx'))"
                                           , COL_TYPE_ENUM.COL_DOUBLE.toInt(), "price_fx");
		
		for (int iRow = 1; iRow <= tTodayCashFlow.getNumRows(); iRow++) {
			
			if(tTodayCashFlow.getInt("toolset", iRow) == TOOLSET_ENUM.REPO_TOOLSET.toInt()
					&& tTodayCashFlow.getInt("event_date", iRow) > OCalendar.today())
				continue;
			
			iDealNum = tTodayCashFlow.getInt("deal_num", iRow);
			iMoneda = tTodayCashFlow.getInt("currency_id", iRow);
			iPortfolio = tTodayCashFlow.getInt("internal_portfolio", iRow);
			iCompraVenta = tTodayCashFlow.getInt("buy_sell", iRow);
			iContraparte = tTodayCashFlow.getInt("external_bunit", iRow);
			iCflowType = tTodayCashFlow.getInt("cflow_type", iRow);
			iInsType = tTodayCashFlow.getInt("ins_type", iRow);
			sReference = tTodayCashFlow.getString("reference", iRow);
			iTradeDate = tTodayCashFlow.getInt("trade_date", iRow);
			iCflowDate = tTodayCashFlow.getInt("event_date", iRow);
			sTicker = tTodayCashFlow.getInt("toolset", iRow) != TOOLSET_ENUM.REPO_TOOLSET.toInt() 
				      ? tTodayCashFlow.getString("ticker", iRow) 
				      : "";
			dMvalue = tTodayCashFlow.getDouble("mvalue", iRow);
			dPosition = tTodayCashFlow.getDouble("tran_position", iRow);
			
			dComisionIva = 0;
			dBaseComisionIva = 0;
			
			if(tTodayCashFlow.getInt("toolset", iRow) == TOOLSET_ENUM.EQUITY_TOOLSET.toInt()){
				dComisionIva = tTodayCashFlow.getDouble("comision_iva", iRow);
				dBaseComisionIva = tTodayCashFlow.getDouble("base_comision_iva", iRow);
			}
			
			dAmountKnownFlow = tTodayCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
								tTodayCashFlow.getDouble("net_settle_amount", iRow) + dComisionIva:
								tTodayCashFlow.getDouble("net_settle_amount", iRow) - dComisionIva;
			
			dSettleFX = tTodayCashFlow.getDouble("price_fx", iRow);
			
			dAmountProjFlow = 0;

			dBaseAmountKnownFlow = iMoneda == EnumsCurrency.MX_CURRENCY_MXN.toInt() ? dAmountKnownFlow : dAmountKnownFlow * dSettleFX;
			
			dBaseAmountKnownFlow = tTodayCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
									dBaseAmountKnownFlow + dBaseComisionIva:
									dBaseAmountKnownFlow - dBaseComisionIva;
			
			dBaseAmountProjFlow = 0;
			sFlowKnownProj = "CONOCIDO";
			
			dTC = iMoneda == EnumsCurrency.MX_CURRENCY_MXN.toInt() ? 1 : dSettleFX;
			
			iRowNew = tReturnt.addRow();
			
			tReturnt.setInt("deal_num", iRowNew, iDealNum);
			tReturnt.setInt("moneda", iRowNew, iMoneda);
			tReturnt.setInt("internal_portfolio", iRowNew, iPortfolio);
			tReturnt.setInt("buy_sell", iRowNew, iCompraVenta);
			tReturnt.setInt("external_bunit", iRowNew, iContraparte);
			tReturnt.setInt("cflow_type", iRowNew, iCflowType);
			tReturnt.setInt("ins_type", iRowNew, iInsType);
			tReturnt.setString("reference", iRowNew, sReference);
			tReturnt.setInt("trade_date", iRowNew, iTradeDate);
			tReturnt.setInt("cflow_date", iRowNew, iCflowDate);
			tReturnt.setString("ticker", iRowNew, sTicker);
			tReturnt.setDouble("mvalue", iRowNew, dMvalue);
			tReturnt.setDouble("position", iRowNew, dPosition);
			tReturnt.setDouble("amount_known_flow", iRowNew, dAmountKnownFlow);
			tReturnt.setDouble("amount_proj_flow", iRowNew, dAmountProjFlow);
			tReturnt.setDouble("base_amount_known_flow", iRowNew, dBaseAmountKnownFlow);
			tReturnt.setDouble("base_amount_proj_flow", iRowNew, dBaseAmountProjFlow);
			tReturnt.setString("flow_known_proj", iRowNew, sFlowKnownProj);
			tReturnt.setDouble("tc", iRowNew, dTC);
		}
	
		tFutureCashFlow = getFutureCashFlow(context);
		tFutureCashFlow.setTableName("FutureCashFlow");
		tFutureCashFlow.select(tInst, "ticker", "ins_num EQ $ins_num");
		tFutureCashFlow.addFormulaColumn("iif(COL('known_cflow') != 0.0, 1, 0)", COL_TYPE_ENUM.COL_INT.toInt(), "flag_known_proj");
		
		getImportNetoEquity(tFutureCashFlow, "future");
		
		for (int iRow = 1; iRow <= tFutureCashFlow.getNumRows(); iRow++) {
			
			if(tFutureCashFlow.getInt("toolset", iRow) == TOOLSET_ENUM.CASH_TOOLSET.toInt()
					&& tFutureCashFlow.getInt("trade_date", iRow) < tFutureCashFlow.getInt("cflow_date", iRow))
				continue;
			
			iDealNum = tFutureCashFlow.getInt("deal_num", iRow);
			iMoneda = tFutureCashFlow.getInt("currency", iRow);
			iPortfolio = tFutureCashFlow.getInt("internal_portfolio", iRow);
			iCompraVenta = tFutureCashFlow.getInt("buy_sell", iRow);
			iContraparte = tFutureCashFlow.getInt("external_bunit", iRow);
			iCflowType = tFutureCashFlow.getInt("cflow_type", iRow);
			iInsType = tFutureCashFlow.getInt("ins_type", iRow);
			sReference = tFutureCashFlow.getString("reference", iRow);
			iTradeDate = tFutureCashFlow.getInt("trade_date", iRow);
			iCflowDate = tFutureCashFlow.getInt("cflow_date", iRow);
			sTicker = tFutureCashFlow.getInt("toolset", iRow) != TOOLSET_ENUM.REPO_TOOLSET.toInt() 
					     ? tFutureCashFlow.getString("ticker", iRow) 
					     : "";
			dMvalue = tFutureCashFlow.getDouble("mvalue", iRow);
			dPosition = tFutureCashFlow.getDouble("position", iRow);
			
			dComisionIva = 0;
			dBaseComisionIva = 0;
			
			if(tFutureCashFlow.getInt("toolset", iRow) == TOOLSET_ENUM.EQUITY_TOOLSET.toInt()){
				dComisionIva = tFutureCashFlow.getDouble("comision_iva", iRow);
				dBaseComisionIva = tFutureCashFlow.getDouble("base_comision_iva", iRow);
			}
			
			if (tFutureCashFlow.getInt("flag_known_proj", iRow) == 1){
				
				dAmountKnownFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
									tFutureCashFlow.getDouble("known_cflow", iRow) + dComisionIva:
									tFutureCashFlow.getDouble("known_cflow", iRow) - dComisionIva;

				dAmountProjFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
									tFutureCashFlow.getDouble("proj_cflow", iRow):
									tFutureCashFlow.getDouble("proj_cflow", iRow);
				
				
				dBaseAmountKnownFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
										tFutureCashFlow.getDouble("base_known_cflow", iRow) + dBaseComisionIva:
										tFutureCashFlow.getDouble("base_known_cflow", iRow) - dBaseComisionIva;
				
				dBaseAmountProjFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
										tFutureCashFlow.getDouble("base_proj_cflow", iRow):
										tFutureCashFlow.getDouble("base_proj_cflow", iRow);
										
				dTC = tFutureCashFlow.getDouble("base_known_cflow", iRow) / tFutureCashFlow.getDouble("known_cflow", iRow);
				
			}else {
			
				dAmountKnownFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
											tFutureCashFlow.getDouble("known_cflow", iRow):
											tFutureCashFlow.getDouble("known_cflow", iRow);
				
				dAmountProjFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
											tFutureCashFlow.getDouble("proj_cflow", iRow) + dComisionIva:
											tFutureCashFlow.getDouble("proj_cflow", iRow) - dComisionIva;
				
				
				dBaseAmountKnownFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
											tFutureCashFlow.getDouble("base_known_cflow", iRow):
											tFutureCashFlow.getDouble("base_known_cflow", iRow);
				
				dBaseAmountProjFlow = tFutureCashFlow.getInt("buy_sell", iRow) == BUY_SELL_ENUM.BUY.toInt() ? 
											tFutureCashFlow.getDouble("base_proj_cflow", iRow) + dBaseComisionIva:
											tFutureCashFlow.getDouble("base_proj_cflow", iRow) - dBaseComisionIva;
				
				dTC = tFutureCashFlow.getDouble("base_proj_cflow", iRow) / tFutureCashFlow.getDouble("proj_cflow", iRow);
			}

			sFlowKnownProj = tFutureCashFlow.getInt("flag_known_proj", iRow)==1?"CONOCIDO":"ESTIMADO";
			
			iRowNew = tReturnt.addRow();
			
			tReturnt.setInt("deal_num", iRowNew, iDealNum);
			tReturnt.setInt("moneda", iRowNew, iMoneda);
			tReturnt.setInt("internal_portfolio", iRowNew, iPortfolio);
			tReturnt.setInt("buy_sell", iRowNew, iCompraVenta);
			tReturnt.setInt("external_bunit", iRowNew, iContraparte);
			tReturnt.setInt("cflow_type", iRowNew, iCflowType);
			tReturnt.setInt("ins_type", iRowNew, iInsType);
			tReturnt.setString("reference", iRowNew, sReference);
			tReturnt.setInt("trade_date", iRowNew, iTradeDate);
			tReturnt.setInt("cflow_date", iRowNew, iCflowDate);
			tReturnt.setString("ticker", iRowNew, sTicker);
			tReturnt.setDouble("mvalue", iRowNew, dMvalue);
			tReturnt.setDouble("position", iRowNew, dPosition);
			tReturnt.setDouble("amount_known_flow", iRowNew, dAmountKnownFlow);
			tReturnt.setDouble("amount_proj_flow", iRowNew, dAmountProjFlow);
			tReturnt.setDouble("base_amount_known_flow", iRowNew, dBaseAmountKnownFlow);
			tReturnt.setDouble("base_amount_proj_flow", iRowNew, dBaseAmountProjFlow);
			tReturnt.setString("flow_known_proj", iRowNew, sFlowKnownProj);
			tReturnt.setDouble("tc", iRowNew, dTC);
		}

		
		tTemp = Table.tableNew("TEMP");
		tTemp.select(tReturnt, "DISTINCT,*", "deal_num GT 0");
		
		tReturnt.clearDataRows();
		tReturnt.select(tTemp, "*", "deal_num GT 0");
		
		if(Table.isTableValid(tTodayCashFlow) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTodayCashFlow.destroy();		
		if(Table.isTableValid(tFutureCashFlow) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tFutureCashFlow.destroy();	
		if(Table.isTableValid(tCurveSpotMxn) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tCurveSpotMxn.destroy();
		if(Table.isTableValid(tInst) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tInst.destroy();
		if(Table.isTableValid(tTemp) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tTemp.destroy();
	}
	
	
	private Table getPortfolio() throws OException {
		
		String what = "name as Portfolio";
		String from = "portfolio";
		String where = "id_number > 0";
		
		Table tPortfolio = Table.tableNew();
		
		DBaseTable.loadFromDbWithSQL(tPortfolio, what, from, where);
		
		tPortfolio.hideColNames();
		
		return tPortfolio;
	}
	
	private Table getTickerForInstrument() throws OException {
		
		String sWhat;
		String sFrom;
		String sWhere;
		
		Table tInst = Table.tableNew("Instrument Ticker");
		sWhat =  "DISTINCT ab.ins_num, CASE WHEN h.ticker is not null THEN h.ticker ELSE hr.ticker END as ticker";
		sFrom =  "ab_tran ab, ab_tran abr, header h, header hr";
		sWhere = "ab.tran_group = abr.tran_group "
			  + "and ab.ins_num = h.ins_num "
			  + "and abr.ins_num = hr.ins_num "
			  + "and abr.toolset != "+TOOLSET_ENUM.REPO_TOOLSET.toInt()+" "
			  + "and ab.tran_status in ("+TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()
                                  + "," + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt()
			                      + "," + TRAN_STATUS_ENUM.TRAN_STATUS_MATURED.toInt()
			                      + "," + TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt()
			                      +") "
			  + "and ab.tran_type = "+TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt();
		
		DBaseTable.loadFromDbWithSQL(tInst, sWhat, sFrom, sWhere);
		
		return tInst;
	}
	
	private void getImportNetoEquity(Table tOperaciones, String sType) throws OException {
		
		Table tEquity = Table.tableNew("Operaciones Equity");
		Table tEquityComision = Table.tableNew("Operaciones Equity");
		
		tEquity.select(tOperaciones, "tran_num", "toolset EQ " + TOOLSET_ENUM.EQUITY_TOOLSET.toInt());
		
		if(tEquity.getNumRows() >= 1){
			
			tEquity.hideColNames();
			String sTransNums = tEquity.exportCSVString().replace("\n", ",").substring(0, tEquity.exportCSVString().length()-1);
			
			String sQuery = "select distinct tran_num, CAST(value as FLOAT) as monto_comision from ab_tran_info_view "
					+ "where tran_num in ("+sTransNums+")"
					+ " and type_name = '"+EnumsTranInfoFields.MX_ALL_MONTO_COMISION.toString()+"'";
			
			try{
				DBaseTable.execISql( tEquityComision, sQuery );
			}
			catch( OException oex ){
				_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Event info from database, " + oex.getMessage() );
			}
			
			double dIva  = Str.strToDouble(_Util.getVariableGlobal("FINDUR", "Calculo_Comision", "iva"));
			tEquityComision.addCol("iva", COL_TYPE_ENUM.COL_DOUBLE);
			tEquityComision.setColValDouble("iva", dIva);
			
			tEquityComision.select(tOperaciones, "buy_sell,fx_rate", "tran_num EQ $tran_num");
			tEquityComision.group("tran_num");
			tEquityComision.distinctRows();
			tEquityComision.addFormulaColumn("COL('monto_comision') * COL('iva') / 100", COL_TYPE_ENUM.COL_DOUBLE.jvsValue(), "monto_iva");
			tEquityComision.addFormulaColumn("COL('monto_comision') + COL('monto_iva')", COL_TYPE_ENUM.COL_DOUBLE.jvsValue(), "comision_iva");
			
			tEquityComision.addCol("base_comision_iva", COL_TYPE_ENUM.COL_DOUBLE);
			
			if(sType.equals("today"))
				tEquityComision.mathMultCol("comision_iva", "settle_fx", "base_comision_iva");
			else 
				tEquityComision.mathDivCol( "comision_iva", "fx_rate", "base_comision_iva");
			
			tOperaciones.select(tEquityComision, "comision_iva, base_comision_iva", "tran_num EQ $tran_num");
			
//			TODO Comentar
//			tEquityComision.viewTable();
		}
		
		if (Table.isTableValid(tEquity) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tEquity.destroy();
		if (Table.isTableValid(tEquityComision) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tEquityComision.destroy();
	}
	

	private Table getTodayCashFlow(IContainerContext context) throws OException{
		

		Table argt = context.getArgumentsTable();

		String errorMessages = "";
		int i = 0;
		int iCurrencyQID = 0;
		int iEventNumQID = 0;
		//int iPaymentEndDate = 0;
		int iPaymentStartDate = 0;
		int iRetval = 0;
		int iErrors = 0;
		int exit_fail = 0;
		int numRows;

		String sFrom = null;
		String sWhat = null;
		String sWhere = null;
		String queryTableName;

		Table tCurrency = Util.NULL_TABLE;
		Table tDistinctEventNum = Util.NULL_TABLE;
		Table tEvent = Util.NULL_TABLE;
		Table tEventSource = Util.NULL_TABLE;
		Table tExtBunit = Util.NULL_TABLE;
		Table tIndex = Util.NULL_TABLE;
		Table tIndexDef = Util.NULL_TABLE;
		Table tIndexText = Util.NULL_TABLE;
		Table tIndexVersion = Util.NULL_TABLE;
		Table tInsNum = Util.NULL_TABLE;
		Table tOutput = Util.NULL_TABLE;
		Table tOutputPortfolio = Util.NULL_TABLE;
		Table tParameter = Util.NULL_TABLE;
		Table tProfile = Util.NULL_TABLE;
		Table tSettle = Util.NULL_TABLE;
		Table tSettleId = Util.NULL_TABLE;
		Table tSettleName = Util.NULL_TABLE;
		Table tPymtFreq = Util.NULL_TABLE;
		Table tErrorRpt = Util.NULL_TABLE;
		Table tPortfolio = Util.NULL_TABLE;
		Table tTemp = Util.NULL_TABLE;

		//==============================================================================
		// Initialization (formerly performed in param script)                         =
		//==============================================================================

		// set up start and end dates (they are the same)
		//===============================================
		iPaymentStartDate = OCalendar.today();
		//iPaymentEndDate   = OCalendar.today() + 1800;

		// load all external business units from database
		//===============================================
		tExtBunit = Table.tableNew();
		sWhat  = " SELECT business_unit.party_id, party.short_name ";
		sFrom  = " FROM business_unit, party ";
		sWhere = " WHERE business_unit.party_id = party.party_id and party.int_ext = " + CONF_INT_EXT.EXTERNAL.toInt();
		try{
			iRetval = DBaseTable.execISql( tExtBunit, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load ExtBunit info from database, " + oex.getMessage() );
		}

		tExtBunit.group( "short_name");

		// loading all currencies from the currency table
		//===============================================
		tCurrency = Table.tableNew();
		try{
			iRetval = DBaseTable.execISql( tCurrency, " SELECT id_number FROM currency WHERE id_number >= 0 " );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load currency info from database, " + oex.getMessage() );
		}

		// Load all portfolios, or only those portfolios selected by param script
		// ======================================================================
		if(argt.getNumRows() > 0 && argt.getColNum( "internal_portfolio") > 0)
		{
			tPortfolio = Table.tableNew();
			tPortfolio.addCol( "id", COL_TYPE_ENUM.COL_INT);
			argt.copyCol( "internal_portfolio", tPortfolio, "id");
		} else
		{
			tPortfolio = Table.tableNew();
			Ref.loadFromRef(tPortfolio, SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE);
		}

		//=======================================================================================================================================================================
		// Query Database table: ab_tran_event_view
		//=======================================================================================================================================================================

		tExtBunit.setColName( 1, "external_bunit");
		tCurrency.setColName( 1, "currency_id");

		// Query ab_tran_event_view
		//=========================

		tEvent = Table.tableNew("Event");

		tEvent.addCol( "portfolio_id",         COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "tran_num",             COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "deal_tracking_num",    COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "tran_status",          COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "ins_type",             COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "event_num",            COL_TYPE_ENUM.COL_INT64 );
		tEvent.addCol( "ins_num",              COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "internal_conf_status", COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "document_num",         COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "event_date",           COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "toolset",              COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "trade_date",           COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "tran_position",        COL_TYPE_ENUM.COL_DOUBLE );
		tEvent.addCol( "pymt_type",            COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "tran_price",           COL_TYPE_ENUM.COL_DOUBLE );
		tEvent.addCol( "external_bunit",       COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "internal_bunit",       COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "param_seq_num",        COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "profile_seq_num",      COL_TYPE_ENUM.COL_INT );
		tEvent.addCol( "ins_class",            COL_TYPE_ENUM.COL_INT );

		int partyIdQid = 0;
		
		try {
			partyIdQid = Query.tableQueryInsert( tExtBunit, "external_bunit", "query_result_plugin" );
		}catch( Throwable oex ){
			oex.getMessage();
		}

		queryTableName = Query.getResultTableForId(partyIdQid);
		if ( queryTableName == null && partyIdQid > 0 )
		{
			queryTableName = "query_result_plugin";
			_Log.printMsg(EnumTypeMessage.ERROR, "Query id " + partyIdQid
					+ " does not have a query result table. Default " + queryTableName + " table will be used.");
		}

		sWhat =  " SELECT "
				+ "ab_tran_event_view.portfolio_id, ab_tran_event_view.tran_num, ab_tran_event_view.deal_tracking_num, "
				+ "ab_tran_event_view.tran_status, ab_tran_event_view.ins_type, ab_tran_event_view.event_num, "
				+ "ins_parameter.ins_num, ab_tran_event_view.internal_conf_status, ab_tran_event_view.document_num, "
				+ "ab_tran_event_view.event_date, ab_tran_event_view.toolset, ab_tran_event_view.trade_date,  "
				+ "ab_tran_event_view.tran_position, ab_tran_event_view.pymt_type, ab_tran_event_view.tran_price, "
				+ "ab_tran_event_view.external_bunit, ab_tran_event_view.internal_bunit, "
				+ "ab_tran_event_view.ins_para_seq_num param_seq_num, ab_tran_event_view.ins_seq_num profile_seq_num, "
				+ "ab_tran_event_view.ins_class, ins_parameter.pymt_period, "
				+ "ab_tran.reference, ab_tran.buy_sell, ab_tran.cflow_type, ab_tran.mvalue, ab_tran.position, "
				+ "security_tran_aux_data.settle_fx ";
		sFrom = " FROM ins_parameter "
				+ "inner join ab_tran_event_view on  ins_parameter.ins_num = ab_tran_event_view.ins_num "
				                              + "and ins_parameter.param_seq_num = ab_tran_event_view.ins_para_seq_num "
				+ "inner join query_result_plugin on ab_tran_event_view.external_bunit = query_result "
				+ "inner join ab_tran on ab_tran.tran_num = ab_tran_event_view.tran_num "
				+ "left join security_tran_aux_data on security_tran_aux_data.tran_num = ab_tran_event_view.tran_num ";
		sWhere = " WHERE ab_tran_event_view.event_date >= '"
				+ OCalendar.formatJdForDbAccess(iPaymentStartDate) + "'"
//				+ OCalendar.formatJdForDbAccess(iPaymentStartDate) + "' and '"
//				+ OCalendar.formatJdForDbAccess(iPaymentEndDate) + "'"
				//+ " and ab_tran_event_view.event_type = " + EVENT_TYPE_ENUM.EVENT_TYPE_CASH_SETTLE.toInt()
				+ " AND ((ab_tran_event_view.event_type = " + EVENT_TYPE_ENUM.EVENT_TYPE_CASH_SETTLE.toInt() + ") OR (ab_tran_event_view.tran_reference LIKE '" + sGENTEC_WILDCARD + "' AND ab_tran_event_view.event_type in (" + EVENT_TYPE_ENUM.EVENT_TYPE_EQUITY_DELIVERY.toInt() + " ))) \n"
				+ " and ab_tran_event_view.tran_status in (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() 
													   + "," + TRAN_STATUS_ENUM.TRAN_STATUS_MATURED.toInt()
													   + "," + TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt()
													   + "," + TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt()
													   + ") "
				+ " and ab_tran_event_view.toolset != " + TOOLSET_ENUM.COM_FUT_TOOLSET.toInt()
				+ " and ab_tran_event_view.toolset != " + TOOLSET_ENUM.COM_OPT_FUT_TOOLSET.toInt()
				+ " and ab_tran_event_view.toolset != " + TOOLSET_ENUM.DEPFUT_TOOLSET.toInt()
				+ " and ab_tran_event_view.toolset != " + TOOLSET_ENUM.OPT_RATE_FUT_TOOLSET.toInt()
				+ " and ab_tran_event_view.pymt_type != " + CFLOW_TYPE.BROKER_FEE_CFLOW.toInt()
				+ " and unique_id = " + partyIdQid;
		try{
			iRetval = DBaseTable.execISql( tEvent, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Event info from database, " + oex.getMessage() );
		}
		Query.clear( partyIdQid );
		tEvent.group( "tran_num, event_date");

		// Create tErrorRpt
		//=================
		tErrorRpt = tEvent.cloneTable();
		tErrorRpt.setTableTitle( "Settlement Error Report");

		// Place erroneous deals into tErrorRpt, then remove the deals from tEvent.
		//========================================================================

		for (i=1; i<=tEvent.getNumRows()-1; i++)
		{
			if (tEvent.getInt( "tran_num", i) == tEvent.getInt( "tran_num", i+1))                                           //Are there two sides to this deal ?
			{

				if (tEvent.getInt( "internal_conf_status", i) == 1 || tEvent.getInt( "internal_conf_status", i+1) == 1)   //Is one of the sides UNKNOWN ?
				{
					if (tEvent.getInt( "pymt_period", i) == tEvent.getInt( "pymt_period", i+1))                            //Are payment freqs. the same
					{
						// Add both sides of this deal to tErrorRpt
						tEvent.copyRowAdd( i,   tErrorRpt);
						tEvent.copyRowAdd( i+1, tErrorRpt);
						iErrors = 1;

						// Delete both sides of this deal from tEvent
						tEvent.delRow( i);
						tEvent.delRow( i);
						i-- ;
						if(i > 1)
							i--;
					}
				}

			}
			else if (tEvent.getInt( "internal_conf_status", i) == 1)         // Delete one-sided deals that have status UNKNOWN
			{
				// add to tErrorRpt
				tEvent.copyRowAdd( i,   tErrorRpt);
				iErrors = 1;

				// delete events that have payment status = unknown
				tEvent.delRow( i);
				i--;
			}
		}

		//See if query is empty
		//======================

		if(tEvent.getNumRows() < 1)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, "No deals were found in the query.");

//			iRetval = GenerateEmptyReports(tErrorRpt, iPaymentStartDate, sFileName, error_log_file, iErrors);

			if(iRetval==1 || iErrors == 1){
				exit_fail = 1;
				errorMessages += "Error value returned from:\n" +
					"m_INCStandard.STD_OutputCrystalWithSubreports -> GenerateCrystal -> GenerateEmptyReports .\n";
			}

			// Clean up
			tExtBunit.destroy();
			tCurrency.destroy();
			tErrorRpt.destroy();
			tEvent.destroy();
			tPymtFreq.destroy();

//			m_INCStandard.Print(error_log_file, "END", "*** End of " + sFileName + " script ***\n");
			if(exit_fail == 1)
				throw new OException( errorMessages );
			return Util.NULL_TABLE;
		}

		//=======================================================================================================================================================================
		// Obtain settlement details for each cash event
		//=======================================================================================================================================================================

		// Get all distinct event numbers for each cash settlement
		//========================================================

		tDistinctEventNum = Table.tableNew();
		tDistinctEventNum.addCol( "event_num", COL_TYPE_ENUM.COL_INT64);
		tEvent.copyColDistinct( "event_num", tDistinctEventNum, "event_num");


		iCurrencyQID = Query.tableQueryInsert(tCurrency, "currency_id", "query_result_plugin");
		iEventNumQID = Query.tableQueryInsert(tDistinctEventNum, "event_num", "query_result64_plugin");

		String currencyQueryTableName = Query.getResultTableForId(iCurrencyQID);
		String eventQueryTableName = Query.getResultTableForId(iEventNumQID);

		if ( currencyQueryTableName == null && iCurrencyQID > 0 )
		{
			currencyQueryTableName = "query_result_plugin";
			_Log.printMsg(EnumTypeMessage.ERROR, "Query id " + iCurrencyQID
					+ " does not have a query result table. Default " + currencyQueryTableName + " table will be used.");
		}
		if ( eventQueryTableName == null && iEventNumQID > 0 )
		{
			eventQueryTableName = "query_result64_plugin";
			_Log.printMsg(EnumTypeMessage.ERROR, "Query id " + iEventNumQID
					+ " does not have a query result table. Default " + eventQueryTableName + " table will be used.");
		}
		
		tSettle = Table.tableNew();
		tSettle.addCol("settle_id", COL_TYPE_ENUM.COL_INT);
		tSettle.addCol("event_num", COL_TYPE_ENUM.COL_INT64);
		tSettle.addCol("currency_id", COL_TYPE_ENUM.COL_INT);
		tSettle.addCol("settle_amount", COL_TYPE_ENUM.COL_DOUBLE);
		sWhat = " SELECT ext_settle_id settle_id, event_num, currency_id, settle_amount " ;
		sFrom = " FROM ab_tran_event_settle ";
		sWhere = " WHERE event_num in (select qr.query_result from " + eventQueryTableName + " qr where qr.unique_id = "
			+ iEventNumQID + ") and currency_id in (select qr.query_result from " + currencyQueryTableName + " qr where unique_id = " + iCurrencyQID + ") ";
		try{
			iRetval = DBaseTable.execISql( tSettle, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Settlement info from database, " + oex.getMessage() );
		}

		tEventSource = Table.tableNew();
		sWhat = " SELECT event_num, para_tran_num, event_source ";
		sFrom = " FROM ab_tran_event, " + eventQueryTableName;
		sWhere = " WHERE event_num > 0 AND event_num = query_result AND unique_id = " + iEventNumQID;
		try{
			iRetval = DBaseTable.execISql( tEventSource, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Event Source info from database, " + oex.getMessage() );
		}

		tEvent.select( tSettle, "*", "event_num EQ $event_num");
		tEvent.select( tEventSource, "para_tran_num, event_source", "event_num EQ $event_num");

		tInsNum = Table.tableNew();
		tInsNum.addCol( "ins_num", COL_TYPE_ENUM.COL_INT);
		tEvent.copyColDistinct( "ins_num", tInsNum, "ins_num");

		Query.clear(iCurrencyQID);
		Query.clear(iEventNumQID);

		//=======================================================================================================================================================================
		// Initialize tables {parameter, profile}
		//=======================================================================================================================================================================

		// INITIALIZE Table parameter
		tParameter = Table.tableNew();
		tParameter.addCol( "ins_num", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "param_seq_num", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "cflow_type", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "proj_index", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "currency", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "fx_flt", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "pay_rec", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "notnl", COL_TYPE_ENUM.COL_DOUBLE);
		tParameter.addCol( "unit", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "avg_period", COL_TYPE_ENUM.COL_INT);
		tParameter.addCol( "reset_roll_conv", COL_TYPE_ENUM.COL_INT);

		int insNumQid = Query.tableQueryInsert( tInsNum, "ins_num", "query_result_plugin" );
		queryTableName = Query.getResultTableForId(insNumQid);
		if ( queryTableName == null && insNumQid > 0 )
		{
			queryTableName = "query_result_plugin";
			_Log.printMsg(EnumTypeMessage.ERROR, "Query id " + insNumQid
					+ " does not have a query result table. Default " + queryTableName + " table will be used.");
		}
		sWhat = " SELECT ins_num, param_seq_num, cflow_type, proj_index, currency, fx_flt, pay_rec, notnl,unit, avg_period, reset_roll_conv ";
		sFrom = " FROM parameter, " + queryTableName;
		sWhere = " WHERE ins_num = query_result AND unique_id = " + insNumQid;
		try{
			DBaseTable.execISql( tParameter, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Parameter info from database, " + oex.getMessage() );
		}
		tParameter.group( "ins_num, param_seq_num");

		// INITIALIZE Table profile
		tProfile = Table.tableNew();
		tProfile.addCol( "ins_num", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "param_seq_num", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "profile_seq_num", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "notnl", COL_TYPE_ENUM.COL_DOUBLE);
		tProfile.addCol( "rate", COL_TYPE_ENUM.COL_DOUBLE);
		tProfile.addCol( "float_spread", COL_TYPE_ENUM.COL_DOUBLE);
		tProfile.addCol( "rate_status", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "start_date", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "end_date", COL_TYPE_ENUM.COL_INT);
		tProfile.addCol( "pymt_date", COL_TYPE_ENUM.COL_INT);

		sWhat = " SELECT ins_num, param_seq_num, profile_seq_num, notnl, rate, float_spread, rate_status, start_date, end_date, pymt_date ";
		sFrom = " FROM profile, " + queryTableName + " ";
		sWhere = " WHERE rate_status < 3 AND ins_num = query_result AND unique_id = " + insNumQid;
		try{
			DBaseTable.execISql( tProfile, sWhat + sFrom + sWhere );
		}
		catch( OException oex ){
			_Log.printMsg(EnumTypeMessage.ERROR, "OException at execute(), failed to load Profile info from database, " + oex.getMessage() );
		}
		Query.clear( insNumQid );
		tProfile.mathAddCol( "rate", "float_spread", "rate");

		numRows = tProfile.getNumRows();
		for(i = 1; i <= numRows ; i++)
		{
			if(tProfile.getDouble( "notnl", i) < 0)
				tProfile.setDouble( "rate", i, tProfile.getDouble( "rate", i) * -1 );
		}

		sWhat = "proj_index(index_id), fx_flt" ;
		sWhere = "ins_num EQ $ins_num and param_seq_num EQ $param_seq_num" ;
		tProfile.select( tParameter, sWhat, sWhere);

		tEvent.addCol( "index_id",  COL_TYPE_ENUM.COL_INT);
		tEvent.addCol( "commodity", COL_TYPE_ENUM.COL_STRING);

		// Get Index
		//==========
		sWhat = "proj_index(index_id)" ;
		sWhere = "ins_num EQ $ins_num and param_seq_num EQ $param_seq_num" ;
		tEvent.select( tParameter, sWhat, sWhere);

		// Get Fixed Price
		//================
		sWhat = "start_date, end_date, rate(fixed_price), notnl, index_id, fx_flt";
		sWhere = "ins_num EQ $ins_num and param_seq_num EQ $param_seq_num and profile_seq_num EQ $profile_seq_num and fx_flt EQ 0" ; //Fixed side
		tEvent.select( tProfile, sWhat, sWhere);

		// Get Float Price
		//================
		sWhat = "start_date, end_date, rate(float_price), notnl, index_id, fx_flt";
		sWhere = "ins_num EQ $ins_num and param_seq_num EQ $param_seq_num and profile_seq_num EQ $profile_seq_num and fx_flt EQ 1" ; // Float side
		tEvent.select( tProfile, sWhat, sWhere);

		// Get Commodity
		//==============
		sWhat = "commodity";
		sWhere = "index_id EQ $index_id";
		tEvent.select( tIndexDef, sWhat, sWhere);

		// Loop for specific toolsets where tran_price is fixed_price and tran_position is notnl,
		// set "commodity" equal to proj_index where deal side is floating, N/A if fixed

		numRows = tEvent.getNumRows();
		for(i = 1; i <= numRows; i++)
		{
			if(tEvent.getInt( "toolset", i) == TOOLSET_ENUM.ENERGY_SWAPTION_TOOLSET.toInt() 
					|| tEvent.getInt( "toolset", i) == TOOLSET_ENUM.SWAPTION_TOOLSET.toInt()
					|| tEvent.getInt( "toolset", i) == TOOLSET_ENUM.COM_OPT_TOOLSET.toInt() 
					|| tEvent.getInt( "toolset", i) == TOOLSET_ENUM.OPTION_TOOLSET.toInt())
			{
				tEvent.setDouble( "fixed_price", i, tEvent.getDouble( "tran_price", i));
			}

			if(tEvent.getInt( "toolset", i) == TOOLSET_ENUM.ENERGY_SWAPTION_TOOLSET.toInt() 
					|| tEvent.getInt( "toolset", i) == TOOLSET_ENUM.SWAPTION_TOOLSET.toInt()
					|| tEvent.getInt( "toolset", i) == TOOLSET_ENUM.OPTION_TOOLSET.toInt())
			{
				tEvent.setDouble( "notnl", i, tEvent.getDouble( "tran_position", i));
			}

			if(tEvent.getInt( "fx_flt", i) == 1)
			{
				tEvent.setString( "commodity", i, Table.formatRefInt(tEvent.getInt( "index_id", i), SHM_USR_TABLES_ENUM.INDEX_TABLE));   // Set proj_index -> commodity
			}
			else
				tEvent.setString( "commodity", i, "N/A");   // Fixed leg has no index associated with it
		}

		tParameter.destroy();
		tProfile.destroy();


		//=======================================================================================================================================================================
		// Initialize table {tOutput}
		//=======================================================================================================================================================================

		tOutput = tDistinctEventNum.copyTable();

		tOutput.addCol( "internal_portfolio", COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "side",               COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "settle_id",          COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "external_bunit",     COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "trade_date",         COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "event_date",         COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "commodity",          COL_TYPE_ENUM.COL_STRING);
		tOutput.addCol( "deal_num",           COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "tran_status",        COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "ins_type",           COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "fixed_price",        COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "float_price",        COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "notnl",              COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "settle_amount",      COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "transaction_fee",    COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "premium",            COL_TYPE_ENUM.COL_DOUBLE);
		tOutput.addCol( "currency_id",        COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "net_settle_amount",  COL_TYPE_ENUM.COL_DOUBLE);

		//======================
		//=  Populate tOutput  =
		//======================

		// Get cash settlement belong to transaction fee (event_source from physcash table)
		//=================================================================================
		sWhat = "portfolio_id(internal_portfolio), settle_id, external_bunit, trade_date, event_date, document_num, "
			+ "commodity, deal_tracking_num(deal_num), tran_num, param_seq_num(side), ins_type, tran_status, "
			+ "fixed_price, float_price, fx_flt,  notnl, tran_position, tran_price, "
			+ "currency_id, settle_amount(transaction_fee), ins_class,"
			+ "reference, buy_sell, cflow_type, mvalue, position, settle_fx, ins_num, toolset" ;
		sWhere = "event_num EQ $event_num and pymt_type NE " + CFLOW_TYPE.PREMIUM_CFLOW.toInt()
		+ " and event_source EQ 2" ; //event_source from Physcash table

		tOutput.select( tEvent, sWhat, sWhere);

		// Get cash settlement belong to settle amount (event_source not from physcash table)
		//===================================================================================
		sWhat = "portfolio_id(internal_portfolio), settle_id, external_bunit, trade_date, event_date, document_num, "
			+  "commodity, deal_tracking_num(deal_num), tran_num, param_seq_num(side), ins_type, tran_status, "
			+  " fixed_price, float_price, fx_flt, notnl, tran_position, tran_price, "
			+  "currency_id, settle_amount, ins_class, "
			+  "reference, buy_sell, cflow_type, mvalue, position, settle_fx, ins_num, toolset" ;
		sWhere = "event_num EQ $event_num and pymt_type NE " + CFLOW_TYPE.PREMIUM_CFLOW.toInt()
		+ " and event_source NE 2" ; //event_source not from Physcash table

		tOutput.select( tEvent, sWhat, sWhere);

		// Get cash settlement belong to premium (pymt_type is PREMIUM)
		//=============================================================
		sWhat = "portfolio_id(internal_portfolio), settle_id, external_bunit, trade_date, event_date, document_num, "
			+  "commodity, deal_tracking_num(deal_num), tran_num, param_seq_num(side), ins_type, tran_status, "
			+  "fixed_price, float_price, fx_flt,  notnl, tran_position, tran_price, "
			+  "currency_id, settle_amount(premium), ins_class, "
			+  "reference, buy_sell, cflow_type, mvalue, position, settle_fx, ins_num, toolset" ;
		sWhere = "event_num EQ $event_num and pymt_type EQ " + CFLOW_TYPE.PREMIUM_CFLOW.toInt();

		tOutput.select( tEvent, sWhat, sWhere);
		tOutput.group( "tran_num");

		numRows = tOutput.getNumRows();
		for(i = 1; i <= numRows; i++)
		{
			if(tOutput.getInt( "ins_class", i) > 0)
			{
				tOutput.setDouble( "notnl",       i, tOutput.getDouble( "tran_position", i));
				tOutput.setDouble( "fixed_price", i, tOutput.getDouble( "tran_price",    i));
			}
		}

		// Sum multiple cash-settlement events from the sub-leg level (for tOutputBySide)
		//===============================================================================
		tTemp = Table.tableNew();
		tTemp.addCol( "tran_num",      COL_TYPE_ENUM.COL_INT);
		tTemp.addCol( "side",          COL_TYPE_ENUM.COL_INT);
		tTemp.addCol( "tran_price",    COL_TYPE_ENUM.COL_DOUBLE);
		tTemp.addCol( "tran_position", COL_TYPE_ENUM.COL_DOUBLE);
		tTemp.addCol( "mvalue", COL_TYPE_ENUM.COL_DOUBLE);
		tTemp.addCol( "event_date", COL_TYPE_ENUM.COL_INT);
		tOutput.copyCol( "tran_num",      tTemp, "tran_num");
		tOutput.copyCol( "side",          tTemp, "side");
		tOutput.copyCol( "tran_price",    tTemp, "tran_price");      // Don't sum tran_price at the sub-leg level.
		tOutput.copyCol( "tran_position", tTemp, "tran_position");   // Don't sum tran_position at the sub-leg level.
		tOutput.copyCol( "mvalue", tTemp, "mvalue"); 
		tOutput.copyCol( "event_date",    tTemp, "event_date");
		tTemp.group( "tran_num, side, tran_price, tran_position, mvalue, event_date");
		tTemp.distinctRows();

		sWhat = " SUM, internal_portfolio, side, settle_id, external_bunit, trade_date, "
				+ "event_date, commodity, deal_num, tran_status, ins_type, fixed_price, "
				+ "float_price, settle_amount, transaction_fee, premium, currency_id, "
				+ "net_settle_amount, document_num, tran_num, fx_flt, ins_class, "
		        + "reference, buy_sell, cflow_type, settle_fx, ins_num, toolset" ;
		tTemp.select( tOutput, sWhat, "tran_num EQ $tran_num AND side EQ $side AND event_date EQ $event_date");
		tOutput.destroy();

		tOutput = tTemp.copyTable();
		tTemp.destroy();

		//=======================================================================================================================================================================
		// Obtain net_settle_amounts for tOutput
		//=======================================================================================================================================================================

		// Obtain net_settle_amount for tOutput
		//=====================================
		tOutput.mathAddCol( "settle_amount", "premium", "net_settle_amount");
		tOutput.mathAddCol( "net_settle_amount", "transaction_fee", "net_settle_amount");

		//=======================================================================================================================================================================
		// Formatting
		//=======================================================================================================================================================================

		// Format tOutput
		//===============
//		tOutput.setColFormatAsRef( "external_bunit", SHM_USR_TABLES_ENUM.PARTY_TABLE );
//		tOutput.setColFormatAsRef( "settle_id",      SHM_USR_TABLES_ENUM.SETTLE_INSTRUCTIONS_TABLE );
//		tOutput.setColFormatAsDate( "trade_date");
//		tOutput.setColFormatAsRef( "currency_id",    SHM_USR_TABLES_ENUM.CURRENCY_TABLE );
//		tOutput.setColFormatAsRef( "ins_type",       SHM_USR_TABLES_ENUM.INSTRUMENTS_TABLE );
//		tOutput.setColFormatAsRef( "tran_status",    SHM_USR_TABLES_ENUM.TRANS_STATUS_TABLE );
//		tOutput.setColFormatAsRef( "fx_flt",         SHM_USR_TABLES_ENUM.FX_FLT_TABLE );
//		tOutput.setColFormatAsDate( "event_date");
//		tOutput.setColFormatAsNotnlAcct( "net_settle_amount", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
//		tOutput.group( "currency_id, event_date, settle_id, deal_num");

		tSettleName.destroy();
		tSettleId.destroy();
		tSettle.destroy();
		tEventSource.destroy();
		tInsNum.destroy();
		tIndex.destroy();
		tIndexDef.destroy();
		tIndexVersion.destroy();
		tIndexText.destroy();
		tEvent.destroy();
		tDistinctEventNum.destroy();
//		tOutput.destroy();
		tPymtFreq.destroy();
		tErrorRpt.destroy();
		tExtBunit.destroy();
		tCurrency.destroy();
		tPortfolio.destroy();
		tOutputPortfolio.destroy();

		if(exit_fail == 1)
		   throw new OException( errorMessages );
		
		getOperacionesNewForToday(tOutput);

		return tOutput;
		
	}
	
	private Table getFutureCashFlow(IContainerContext context) throws OException {

		int iSimFail = 0;
		int iExitFail = 0;
		int iQueryId = 0;
		int iColNum;
		
		//int iEndCflowDate   = OCalendar.today() + 1800;
		
		String sWhat;
		String sFrom;
		String sWhere;
		String sQueryTableName;
		
		Table tTran = Util.NULL_TABLE;
		Table tAllDeals = Util.NULL_TABLE;
		Table tOutput = Util.NULL_TABLE;
		Table tKnown = Util.NULL_TABLE;
		Table tProj = Util.NULL_TABLE;
		Table tResultList = Util.NULL_TABLE;
		Table tSimResults = Util.NULL_TABLE;
		Table tTranResults = Util.NULL_TABLE;
		Table tGenResults = Util.NULL_TABLE;
		Table tFxResults = Util.NULL_TABLE;
		Table tArgt = context.getArgumentsTable();
		
		/*** Check to see if Report Manager is being run ***/
		iExitFail = 0;

		tAllDeals = Table.tableNew("all_deals");
		sWhat  = "ab_tran.tran_num";
		sFrom  = "ab_tran, trans_type";
		sWhere = "ab_tran.tran_type = trans_type.id_number " +
				"and trans_type.trade_flag = 1 " +
				"and ab_tran.tran_type != " + TRAN_TYPE_ENUM.TRAN_TYPE_REPO_COLL.jvsValue() + " " +
				"and "
				+ "(( ab_tran.buy_sell = " + BUY_SELL_ENUM.BUY.toInt()
				+ "   and ab_tran.tran_status in ("+TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt()
				                               +","+TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()+") "
				+ " )or( "
				+ "   ab_tran.buy_sell = " + BUY_SELL_ENUM.SELL.toInt()
				+ "   and ab_tran.tran_status in ("+TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt()
				                               +","+TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt()
				                               +","+TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt()+") "
				+ "))";

		DBaseTable.loadFromDbWithSQL(tAllDeals, sWhat, sFrom, sWhere);

		if(tAllDeals.getNumRows() == 0)
		{
			_Log.printMsg(EnumTypeMessage.ERROR, "There are no New or Validated trades in the database.");

			tAllDeals.destroy();
			iSimFail = 1;
		}
		else
		{
			iQueryId = Query.tableQueryInsert(tAllDeals, 1);
			tAllDeals.destroy();

			if(tArgt.getNumRows() == 0)
			{
				tArgt.addCol( "update_criteria", COL_TYPE_ENUM.COL_TABLE);
				tArgt.addRow();
				Sim.createRevalTable(tArgt);
			}
			
			if(tArgt.getColNum("QueryId") < 1 )
				tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);
			
			tArgt.setInt( "QueryId", 1, iQueryId);
		}

		if(iSimFail == 0)
		{
			sQueryTableName = Query.getResultTableForId(iQueryId);
			if ( sQueryTableName == null && iQueryId > 0 )
			{
				sQueryTableName = "query_result";
				_Log.printMsg(EnumTypeMessage.ERROR, "Query id " + iQueryId
						   + " does not have a query result table. Default " + sQueryTableName + " table will be used.");
			}
			tTran = Table.tableNew();
			tTran.addCol("tran_num", COL_TYPE_ENUM.COL_INT);
			tTran.addCol("deal_tracking_num", COL_TYPE_ENUM.COL_INT);
			tTran.addCol("external_bunit", COL_TYPE_ENUM.COL_INT);
			tTran.addCol("toolset", COL_TYPE_ENUM.COL_INT);

			sWhat = "select ab_tran.tran_num, ab_tran.deal_tracking_num, ab_tran.external_bunit, "
					+ "ab_tran.toolset, ab_tran.internal_portfolio, "
					+ "ab_tran.buy_sell, ab_tran.cflow_type, ab_tran.ins_type, ab_tran.reference, "
					+ "ab_tran.trade_date, ab_tran.ins_num, ab_tran.mvalue, ab_tran.position"
					+ ",security_tran_aux_data.settle_fx, ab_tran.tran_status ";
			sFrom = "from ab_tran "
					+ " inner join " + sQueryTableName + " on ab_tran.tran_num = query_result and unique_id = " + iQueryId
					+ " left join security_tran_aux_data on security_tran_aux_data.tran_num = ab_tran.tran_num ";

			DBaseTable.execISql( tTran, sWhat + sFrom);
			
			/**************     Set up an run the Simulation Results      *********************/
			tResultList = Sim.createResultListForSim();
			SimResult.addResultForSim(tResultList, SimResultType.create("PFOLIO_RESULT_TYPE.CFLOW_PROJECTED_BY_DEAL_RESULT"));
			SimResult.addResultForSim(tResultList, SimResultType.create("PFOLIO_RESULT_TYPE.CFLOW_FUTURE_BY_DEAL_RESULT"));

			tSimResults = Sim.runRevalByQidFixed(tArgt, tResultList, Ref.getLocalCurrency());
			if(tSimResults ==null || Table.isTableValid(tSimResults) == 0)
			{
				_Log.printMsg(EnumTypeMessage.ERROR, "Simulation returned an invalid table");
				iSimFail  = 1;
				iExitFail = 1;
			}
			else
			{
				tTranResults = SimResult.getTranResults(tSimResults);
				tGenResults = SimResult.getGenResults(tSimResults);

				tFxResults = SimResult.findGenResultTable(tGenResults, PFOLIO_RESULT_TYPE.FX_RESULT.toInt(), -2, -2, -2);

				tKnown = SimResult.findGenResultTable(tGenResults, PFOLIO_RESULT_TYPE.CFLOW_FUTURE_BY_DEAL_RESULT.toInt(), -2, -2, -2);
				tProj = SimResult.findGenResultTable(tGenResults, PFOLIO_RESULT_TYPE.CFLOW_PROJECTED_BY_DEAL_RESULT.toInt(), -2, -2, -2);

				/* get the currency for each param */
				Index.tableColConvertIndexToCurrency(tTranResults, "disc_idx", "disc_idx");
				iColNum = tTranResults.getColNum( "disc_idx");

				tTranResults.setColName( iColNum, "currency");

				if (((tKnown != null) && Table.isTableValid(tKnown) == 1) || ((tProj != null) && Table.isTableValid(tProj) == 1))
				{
					tKnown.select( tTranResults, "currency", "deal_num EQ $deal_num AND deal_leg EQ $deal_leg ");
					if (Table.isTableValid(tProj) == 1)
					tProj.select( tTranResults, "currency", "deal_num EQ $deal_num AND deal_leg EQ $deal_leg");
				}
			}
		}

		/********** Create Output Table - This is used by the Daily, Monthly and Yearly Views **********/
		tOutput = Table.tableNew();
		tOutput.addCol( "ref_date", COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "cflow_date", COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "deal_num", COL_TYPE_ENUM.COL_INT);
		tOutput.addCol( "toolset", " \nToolset", SHM_USR_TABLES_ENUM.TOOLSETS_TABLE);
		tOutput.addCol( "external_bunit", " \nCounterparty", SHM_USR_TABLES_ENUM.PARTY_TABLE);
		tOutput.addCol( "known_cflow", "Known Cash\nFlows", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "proj_cflow", "Proj Cash\nFlows", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "total_cflow", "Total Cash\nFlows", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "currency", " \nCcy", SHM_USR_TABLES_ENUM.CURRENCY_TABLE);
		tOutput.addCol( "base_known_cflow", "Known Cash\nFlows(Base)", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "base_proj_cflow", "Proj Cash\nFlows(Base)", Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "base_total_cflow", "Total Cash\nFlows(Base)",  Util.NOTNL_WIDTH, Util.NOTNL_PREC, COL_FORMAT_BASE_ENUM.BASE_NONE.toInt());
		tOutput.addCol( "base_currency", "Base\nCcy", SHM_USR_TABLES_ENUM.CURRENCY_TABLE);
		tOutput.addCol( "tran_status", "Estado\nOperacion", SHM_USR_TABLES_ENUM.TRANS_STATUS_TABLE);

		tOutput.setColFormatAsDate( "ref_date");
		tOutput.setColFormatAsDate( "cflow_date");
		
		Table tTemp = Table.tableNew();
		if (tKnown != null && Table.isTableValid(tKnown) == 1 && tKnown.getNumRows() > 0)
			tTemp.select( tKnown, "deal_num, cflow_date, currency", "deal_num GT 0");
		if (tProj != null && Table.isTableValid(tProj) == 1 && tProj.getNumRows() > 0)
			tTemp.select( tProj, "deal_num, cflow_date, currency", "deal_num GT 0");
		if (tTemp.getNumRows() > 0)
			tOutput.select( tTemp, "DISTINCT, deal_num, cflow_date, currency", "deal_num GT 0");// AND cflow_date LE " + iEndCflowDate);

		tTemp.destroy();

		if(iSimFail == 0)
		{
			tOutput.select( tTran, "external_bunit, toolset, internal_portfolio, buy_sell, "
					+ "cflow_type, ins_type, reference, trade_date, mvalue, position, "
					+ "ins_num, tran_num, settle_fx, tran_status", "deal_tracking_num EQ $deal_num");
			tOutput.select( tKnown, "SUM, cflow(known_cflow)", "deal_num EQ $deal_num AND cflow_date EQ $cflow_date AND currency EQ $currency");
			tOutput.select( tProj, "SUM, cflow(proj_cflow)", "deal_num EQ $deal_num AND cflow_date EQ $cflow_date AND currency EQ $currency");

			/* Convert the cash flows to the Base Currency*/
			tOutput.addCol( "fx_rate", COL_TYPE_ENUM.COL_DOUBLE);
			tOutput.select( tFxResults, "result(fx_rate)", "id EQ $currency");
			tOutput.mathDivCol( "known_cflow", "fx_rate", "base_known_cflow");
			tOutput.mathDivCol( "proj_cflow", "fx_rate", "base_proj_cflow");
			//tOutput.delCol( "fx_rate");

			tOutput.setColValInt( "base_currency", Ref.getLocalCurrency());

			tOutput.mathAddCol( "known_cflow", "proj_cflow", "total_cflow");
			tOutput.mathAddCol( "base_known_cflow", "base_proj_cflow", "base_total_cflow");
		}

		/********************       Script Clean Up       *****************/
		if (tResultList   != Util.NULL_TABLE && Table.isTableValid(tResultList) == 1) 
				tResultList.destroy();
		if (tSimResults   != Util.NULL_TABLE && Table.isTableValid(tSimResults) == 1) 
			tSimResults.destroy();
		if (tTran          != Util.NULL_TABLE && Table.isTableValid(tTran) == 1) 
			tTran.destroy();

		if(iExitFail != 0)
			throw new OException("Plugin ended with failed status");
		
		return tOutput;
	}
	
	private void getOperacionesNewForToday(Table tOperaciones) throws OException{
		
		StringBuffer sbQuery = new StringBuffer(); 
		Table tOpNew = Table.tableNew("OP NEW");
		Table tFxResults = Table.tableNew();
		
		try {
			
			sbQuery.append("SELECT ab_tran.deal_tracking_num deal_num,\n")
                   .append("    ab_tran.tran_num,\n")
                   .append("    ab_tran.ins_num,\n")
                   .append("    ab_tran.tran_status,\n")
                   .append("    ab_tran.toolset,\n")
                   .append("    ab_tran.currency currency_id,\n")
                   .append("    ab_tran.internal_portfolio ,\n")
                   .append("    ab_tran.buy_sell,\n")
                   .append("    ab_tran.external_bunit,\n")
                   .append("    ab_tran.cflow_type,\n")
                   .append("    ab_tran.ins_type,\n")
                   .append("    ab_tran.reference,\n")
                   .append("    ab_tran.trade_date,\n")
                   .append("    ab_tran.settle_date event_date,\n")
                   .append("    ab_tran.position tran_position,\n")
                   .append("    ab_tran.mvalue,\n")
//                 .append("    security_tran_aux_data.net_proceeds,\n")
//                 .append("    ab_tran.proceeds,\n")
//                 .append("    repo_aux_table.settle_proceeds,\n")
                   .append("    CAST((CASE ab_tran.toolset WHEN 11 \n")
                   .append("           THEN repo_aux_table.settle_proceeds \n")
                   .append("           ELSE security_tran_aux_data.net_proceeds END) \n")
                   .append("        AS FLOAT) net_settle_amount,\n")
                   .append("    security_tran_aux_data.settle_fx,\n")
//                 .append("    CAST((security_tran_aux_data.net_proceeds * security_tran_aux_data.settle_fx) AS FLOAT) settle_proceeds,\n")
                   .append("    CAST ((CASE ab_tran.toolset WHEN 11 \n")
                   .append("            THEN repo_aux_table.fwd_proceeds \n")
                   .append("            ELSE (security_tran_aux_data.net_proceeds * security_tran_aux_data.settle_fx) END)\n")
                   .append("        AS FLOAT) settle_proceeds\n")
                   .append("FROM ab_tran\n")
                   .append("    LEFT JOIN security_tran_aux_data on ab_tran.tran_num = security_tran_aux_data.tran_num\n")
                   .append("    LEFT JOIN repo_aux_table on ab_tran.tran_num = repo_aux_table.repo_tran_num\n")
                   .append("WHERE ab_tran.tran_status = ").append(TRAN_STATUS_ENUM.TRAN_STATUS_NEW.toInt()).append("\n")//2 -- status new
                   .append("    AND ab_tran.settle_date = '").append(OCalendar.formatJdForDbAccess(OCalendar.today())).append("'\n")
                   .append("    AND ab_tran.tran_type = ").append(TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt());
			
			DBaseTable.execISql( tOpNew, sbQuery.toString());
			
			for (int i = 1; i <= tOpNew.getNumRows(); i++) {
				
				int iRow = tOperaciones.addRow();
				
				tOperaciones.setInt("deal_num", iRow, tOpNew.getInt("deal_num", i));
				tOperaciones.setInt("tran_num", iRow, tOpNew.getInt("tran_num", i));
				tOperaciones.setInt("ins_num", iRow, tOpNew.getInt("ins_num", i));
				tOperaciones.setInt("tran_status", iRow, tOpNew.getInt("tran_status", i));
				tOperaciones.setInt("toolset", iRow, tOpNew.getInt("toolset", i));
				tOperaciones.setInt("currency_id", iRow, tOpNew.getInt("currency_id", i));
				tOperaciones.setInt("internal_portfolio", iRow, tOpNew.getInt("internal_portfolio", i));
				tOperaciones.setInt("external_bunit", iRow, tOpNew.getInt("external_bunit", i));
				tOperaciones.setInt("cflow_type", iRow, tOpNew.getInt("cflow_type", i));
				tOperaciones.setInt("ins_type", iRow, tOpNew.getInt("ins_type", i));
				tOperaciones.setString("reference", iRow, tOpNew.getString("reference", i));
				tOperaciones.setInt("trade_date", iRow, tOpNew.getInt("trade_date", i));
				tOperaciones.setInt("event_date", iRow, tOpNew.getInt("event_date", i));
				tOperaciones.setDouble("tran_position", iRow, tOpNew.getDouble("tran_position", i));
				tOperaciones.setDouble("mvalue", iRow, tOpNew.getDouble("mvalue", i));
				tOperaciones.setDouble("net_settle_amount", iRow, tOpNew.getDouble("net_settle_amount", i));
				tOperaciones.setDouble("settle_fx", iRow, tOpNew.getDouble("settle_fx", i));
				tOperaciones.setDouble("settle_proceeds", iRow, tOpNew.getDouble("settle_proceeds", i));
				
			}
			
			/* Convert the cash flows to the Base Currency*/
			tOperaciones.addCol( "fx_rate", COL_TYPE_ENUM.COL_DOUBLE);
			tOperaciones.setColValDouble("fx_rate", 1);
			tOperaciones.select( tFxResults, "rate(fx_rate)", "id EQ $currency_id");
			
		}catch (Throwable e){
			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{
			tFxResults = Index.getOutput("SPOT_FX.MXN");
			tFxResults.addCol("id", COL_TYPE_ENUM.COL_INT);
			tFxResults.addCol("rate", COL_TYPE_ENUM.COL_DOUBLE);
			for(int i=1;i<=tFxResults.getNumRows();i++)
			{
				tFxResults.setInt("id", i, Ref.getValue(SHM_USR_TABLES_ENUM.CURRENCY_TABLE, tFxResults.getString("Spot", i)));
				
			}
			tFxResults.setColName("Price (Mid)", "rate");

			tOpNew.destroy();
			tFxResults.destroy();
		}
	}
}
