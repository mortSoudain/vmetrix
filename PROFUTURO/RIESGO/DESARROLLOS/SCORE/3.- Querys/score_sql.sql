   with   
   variables as   
   (   
   	select   
   		sys.business_date fecha,   
   		DATE '1900-01-01' nulo   
   	from dual   
   	left join system_dates sys   
   		on 1 = 1   
   ),   
   score_pd as (   
   	select   
   		sp.party_id   
       	,sk.factor_k + sum(scatal.valor) as ohd_score
       	,CASE WHEN(scateg.category_id = 1) -- Estados y Municipios
			THEN 1.0 / (1.0 + EXP( -(500.0 - (sk.factor_k + sum(scatal.valor))) * (LN (2.0) / 40.0 )))
			ELSE 1.0 / (1.0 + EXP(-53.43832926+(0.124601632 * (sk.factor_k + sum(scatal.valor)))))
		END AS ohd_default_prob
   	from user_mx_score_party sp   
   		left join user_mx_score_matriz sm   
   			on sm.party_id = sp.party_id   
   		left join user_mx_score_category scateg   
   			on scateg.category_id = sp.category_id   
   		left join user_mx_score_catalogo scatal   
   			on scatal.category_id = scateg.category_id   
       	left join user_mx_score_k sk   
       		on sk.category_id = sp.category_id   
   		left join variables v   
   			on 1 = 1   
   	where   
   		-- Segunda condicion de join entre tablas user_mx_score_matriz y user_mx_score_catalogo   
   			sm.variable_id = scatal.variable_id   
   		-- Condicion de categorias solo Estados y municipios o Project finance   
   			and scateg.category_id in (1,2)   
   		-- Manejo de fechas (ultimo registro) para las tablas  
   			and v.fecha >= sp.fecha_desde and (v.fecha < sp.fecha_hasta or v.nulo = sp.fecha_hasta)   
   			and v.fecha >= sm.fecha_desde and (v.fecha < sm.fecha_hasta or v.nulo = sm.fecha_hasta)   
   			and v.fecha >= scatal.fecha_desde and (v.fecha < scatal.fecha_hasta or v.nulo = scatal.fecha_hasta)   
   			and v.fecha >= sk.fecha_desde and (v.fecha < sk.fecha_hasta or v.nulo = sk.fecha_hasta)   
   		-- Manejo de rangos de valores   
   			and sm.valor >= scatal.cota_min   
   			and sm.valor < scatal.cota_max   
       group by sp.party_id, sk.factor_k, scateg.category_id
   )         
   select   
   	sp.party_id   
   	,p.short_name 
   	,sp.ohd_score   
   	,sp.ohd_default_prob    
   	,scalif.calif_global as internal_rating
   	,0.0 as ohd_market_cap
   	,0.0 as ohd_total_debt
   	,v.fecha   
   from score_pd sp 
     inner join party p 
       on p.party_id = sp.party_id 
   	left join user_mx_score_calificacion scalif   
   		on 1 = 1   
   	left join variables v   
   		on 1 = 1   
   where   
   	-- Manejo de fechas (ultimo registro) para la tabla de calificaciones  
   		v.fecha >= scalif.fecha_desde and (v.fecha < scalif.fecha_hasta or v.nulo = scalif.fecha_hasta)   
   	-- Manejo de rangos de valores   
   		and sp.ohd_default_prob >= scalif.cota_min   
   		and sp.ohd_default_prob < scalif.cota_max   