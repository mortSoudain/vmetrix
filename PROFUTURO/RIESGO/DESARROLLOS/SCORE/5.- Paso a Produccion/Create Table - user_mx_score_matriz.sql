-- Creacion de User Tables

-- user_mx_score_matriz

create table TST_GRUPO_163961_01AUG16.user_mx_score_matriz
(
 party_id int ,
 variable_id int ,
 fecha_desde date ,
 fecha_hasta date ,
 valor double precision ,
 personnel_id int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_matriz');
