-- Creacion de User Tables

-- user_mx_score_calificacion

create table TST_GRUPO_163961_01AUG16.user_mx_score_calificacion
(
 calif_global varchar2(255) ,
 fecha_desde date ,
 fecha_hasta date ,
 cota_min double precision ,
 cota_max double precision ,
 probabilidad_default double precision ,
 personnel_id int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_calificacion');