-- Creacion de User Tables

-- user_mx_score_party

create table TST_GRUPO_163961_01AUG16.user_mx_score_party
(
 party_id int ,
 category_id int ,
 ticker_bbg varchar2(255) ,
 fecha_desde date ,
 fecha_hasta date ,
 personnel_id int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_party');