-- Creacion de User Tables

-- user_mx_score_catalogo

create table TST_GRUPO_163961_01AUG16.user_mx_score_catalogo
(
 catalogo_id int ,
 category_id int ,
 variable_id int ,
 cota_min double precision ,
 cota_max double precision ,
 valor double precision ,
 fecha_desde date ,
 fecha_hasta date ,
 personnel_id int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_catalogo');