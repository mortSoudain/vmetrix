-- Creacion de User Tables

-- user_mx_score_k

create table TST_GRUPO_163961_01AUG16.user_mx_score_k
(
 category_id int ,
 fecha_desde date ,
 fecha_hasta date ,
 factor_k int ,
 personnel_id int 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_k');
