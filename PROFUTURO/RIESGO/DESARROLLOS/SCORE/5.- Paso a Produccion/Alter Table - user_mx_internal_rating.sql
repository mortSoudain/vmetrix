-- Alter Table

-- user_mx_internal_rating

ALTER TABLE TST_GRUPO_163961_01AUG16.user_mx_internal_rating ADD score double precision;
ALTER TABLE TST_GRUPO_163961_01AUG16.user_mx_internal_rating ADD fecha date;
ALTER TABLE TST_GRUPO_163961_01AUG16.user_mx_internal_rating ADD market_cap double precision;
ALTER TABLE TST_GRUPO_163961_01AUG16.user_mx_internal_rating ADD total_debt double precision;
ALTER TABLE TST_GRUPO_163961_01AUG16.user_mx_internal_rating ADD personnel_id int;