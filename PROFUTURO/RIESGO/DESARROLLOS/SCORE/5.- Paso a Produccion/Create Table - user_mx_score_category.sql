-- Creacion de User Tables

-- user_mx_score_category

create table TST_GRUPO_163961_01AUG16.user_mx_score_category
(
 category_id int ,
 category_name varchar2(255) ,
 response_id varchar2(255) 
);

execute TST_GRUPO_163961_01AUG16.grant_priv_on_user_table ('user_mx_score_category');
