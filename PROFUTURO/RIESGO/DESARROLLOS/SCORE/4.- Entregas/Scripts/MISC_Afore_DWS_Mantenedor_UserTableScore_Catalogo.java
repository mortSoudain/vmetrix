/*$Header: v 1.1, 03/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Mantenedor_UserTableScore_Catalogo.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de un mantenedor para la usertable user_mx_score_catalogo
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   	Julio 2018
 * Version/Autor:          	1.1 Basthian Matthews - VMetrix International
 * Description:            	Se agrega control de seguridad (security group) para que solo los usuarios
 * 							PROSPERA puedan guardar loa cambios en la usertable.    
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;

public class MISC_Afore_DWS_Mantenedor_UserTableScore_Catalogo implements IScript {
	
	//Declare utils
	protected String sScriptName	=	this.getClass().getSimpleName();
	protected UTIL_Log _Log			=	new UTIL_Log(sScriptName);
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;
	
    private Table tblProperties			=	Util.NULL_TABLE;
    private Table tblGlobalProperties	=	Util.NULL_TABLE;
	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;
	
	private String sCellName			=	"";
	private int iSelectedRow			=	0;		
	private String sSelectedCategory	=	"";
	private int iSelectedCategory		=	0;
	private int iSelectedVariable		=	1;

	private int iToday		=	0;
	private String sToday	=	"";
	ODateTime oToday		=	null;
	
	// User
	private String		sUserName		=	"";
	private int			iUserId			=	0;
            
	public void execute(IContainerContext context) throws OException {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		oToday = ODateTime.strToDateTime(sToday);
		
		iUserId		=	Ref.getUserId();
		sUserName		=	Ref.getUserName();

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
				
				// Setting initial data
					setDataOnTable(tDisplay);
										
				// Set table formats
					setTableFormat();
			
				// Apply filter on start
					Table tCategory = getComboBoxCategory();
					filterDealsByCategory(tDisplay, tCategory.getInt("category_id", 1));
					
				// FINALLY, Setting table properties
					setTableProperties();
					
				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					callback_event		=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
					tReturnt			=	UserDataWorksheet.getData();
					sCellName			=	tReturnt.scriptDataGetCallbackName();
					iSelectedRow		=	tReturnt.scriptDataGetCallbackRow();
					sSelectedCategory 	=	tReturnt.scriptDataGetWidgetString("filterCategory");
					iSelectedCategory	=	getCategoryIdFromCategoryName(sSelectedCategory);
					String sSelectedVariable 	=	tReturnt.scriptDataGetWidgetString("filterVariable");

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						
		    			if (sCellName.equals("btnNuevoRegistro")){
							iSelectedVariable	=	Integer.valueOf(sSelectedVariable.substring(1));
		    				
		    				int iNewRow = tReturnt.addRow();
		    				
		    				tReturnt.setInt("category_id",iNewRow, iSelectedCategory);
		    				tReturnt.setString("category_name",iNewRow, sSelectedCategory);
		    				tReturnt.setInt("variable_id",iNewRow, iSelectedVariable);
		    				tReturnt.setString("variable",iNewRow, sSelectedVariable);
		    				tReturnt.setInt("udw_insert", iNewRow, 1);
		    			}
		    			
		    			if (sCellName.equals("btnBorrarRegistro")){
		    				tReturnt.setInt("udw_delete", iSelectedRow, 1);
		    			}
		    			
						Table tSecurityGroupsTable = Ref.retrievePersonnelSecurityGroups(iUserId);
						int iSecurityGroupRiskManager = tSecurityGroupsTable.unsortedFindString("name", "PROSPERA", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
						
						if(iSecurityGroupRiskManager!= -1){
			    			if (sCellName.equals("btnGuardarCambios")){
			    				
			    				if (saveTable(tReturnt)==true)
			    					Ask.ok("Se Guardaron correctamente las propiedades de metricas.");
			    				else
			    					Ask.ok("No se pudo guardar.");
			    				
			    				// Filter data on table display
			    				filterDealsByCategory(tReturnt, iSelectedCategory);
			    				
			    			}
						} else {
	    					Ask.ok("El usuario "+sUserName+" no tiene permisos para realizar modificaciones.\nLos cambios no fueron almacenados.");
						}
						
						break;
					case UDW_COMBO_BOX_EVENT:
						
		    			if (sCellName.equals("filterCategory")){
							filterDealsByCategory(tReturnt,iSelectedCategory);
		    			}
					
						break;
					case UDW_TED_EVENT:
						
						String sVariablePattern	=	"^(x|X)([1-9]+[0-9]*)$";
					    Pattern pVariable				=	Pattern.compile(sVariablePattern);
					    Matcher mVariable				=	pVariable.matcher(sSelectedVariable);
					    
					    if(!mVariable.find()){
	    					Ask.ok("La variable ingresada no corresponde con el formato esperado.\n"
	    							+ "Intente con el formato X+'El numero de la variable'. Ej. X1,X4,X10,etc.");
	    					
	    					tReturnt.scriptDataSetWidgetString("filterVariable", "X1");
					    }
					    
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}


	private int getCategoryIdFromCategoryName(String sSelectedCategory) throws OException {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
		
		Table tAux = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select ");
		sb.append("\n 	category_id ");
		sb.append("\n 	,category_name ");
		sb.append("\n from " +sUserMxScoreCategory);
		DBaseTable.execISql(tAux, sb.toString());
		
		int iFindedCategory = tAux.unsortedFindString("category_name", sSelectedCategory, SEARCH_CASE_ENUM.CASE_INSENSITIVE);
		int iResultId = tAux.getInt("category_id", iFindedCategory);
		
		return iResultId;
	}

	private void filterDealsByCategory(Table tReturnt, int iSelectedCategory) throws OException {
		
		//Show all data before filter
		tReturnt.rowAllShow();
		
		for(int i=1; i<= tReturnt.getNumRows(); i++){
			int iCategoryId = tReturnt.getInt("category_id", i);
			
			if(iCategoryId != iSelectedCategory){
				tReturnt.rowHide(i);
			}
		}
		
		// Refresh display in case of update
		UserDataWorksheet.setRefreshDataTable(tReturnt);
		
	}

	private boolean saveTable(Table tReturnt) throws OException {
		
		String sUserMxScoreCatalogo = EnumsUserTables.USER_MX_SCORE_CATALOGO.toString();
		
		int iResult = 0;
		
		Table tUserMxScoreK = Table.tableNew(sUserMxScoreCatalogo);
		String sQuery = "select * from " + sUserMxScoreCatalogo;
		DBaseTable.execISql(tUserMxScoreK, sQuery);
		
		Table tTableInserted = Table.tableNew();		
		Table tTableUpdated = Table.tableNew();		
		Table tTableDeleted = Table.tableNew();		

		tTableInserted.select(tReturnt, "*", "udw_insert EQ 1");
		tTableUpdated.select(tReturnt, "*", "udw_update EQ 1");
		tTableDeleted.select(tReturnt, "*", "udw_delete EQ 1");

		tUserMxScoreK = deleteRegisters(tUserMxScoreK,tTableDeleted);
		tUserMxScoreK = updateRegisters(tUserMxScoreK,tTableUpdated);
		tUserMxScoreK = insertRegisters(tUserMxScoreK,tTableInserted);
		
		//Delete previous info on db
			DBUserTable.clear(tUserMxScoreK);
		
		// Push user table with modifications
			iResult = DBUserTable.bcpIn(tUserMxScoreK);
		
		// Update display
			setDataOnTable(tReturnt);

		if (iResult==1) return true;
		else return false;
	}

	private Table deleteRegisters(Table tUserMxScoreK, Table tTableDeleted) throws OException {
		
		// Identify registers on historical usertable that are going to be deleted
		// Filter them by the selected registers on TableDeleted and field fecha_hasta = 0(null)
		
		for (int j = 1; j<= tTableDeleted.getNumRows(); j++){
			int jCatalogoId = tTableDeleted.getInt("catalogo_id", j);
			
			for (int i = 1 ; i<= tUserMxScoreK.getNumRows(); i++){
				int iFechaHasta = tUserMxScoreK.getDate("fecha_hasta", i);
				int iCatalogoId = tUserMxScoreK.getInt("catalogo_id", i);
				
				if(jCatalogoId == iCatalogoId &&
						iFechaHasta==0){
					tUserMxScoreK.setDateTime("fecha_hasta", i, oToday);
				}
			}
		}
		
		return tUserMxScoreK;
	}


	private Table updateRegisters(Table tUserMxScoreK, Table tTableUpdated) throws OException {
		
		for (int j = 1; j<= tTableUpdated.getNumRows(); j++){
			int jCatalogoId = tTableUpdated.getInt("catalogo_id", j);
			
			for (int i = 1 ; i<= tUserMxScoreK.getNumRows(); i++){
				int iFechaHasta = tUserMxScoreK.getDate("fecha_hasta", i);
				int iCatalogoId = tUserMxScoreK.getInt("catalogo_id", i);
				
				if(jCatalogoId == iCatalogoId &&
						iFechaHasta==0){
					tUserMxScoreK.setDateTime("fecha_hasta", i, oToday);
				}
			}
		
			int iNewRow = tUserMxScoreK.addRow();
			
			tUserMxScoreK.setInt("catalogo_id", iNewRow, iNewRow);
			tUserMxScoreK.setInt("category_id", iNewRow, tTableUpdated.getInt("category_id", j));
			tUserMxScoreK.setString("category_name", iNewRow, tTableUpdated.getString("category_name", j));
			tUserMxScoreK.setInt("variable_id", iNewRow, tTableUpdated.getInt("variable_id", j));
			tUserMxScoreK.setString("variable", iNewRow, tTableUpdated.getString("variable", j));
			tUserMxScoreK.setDouble("cota_min", iNewRow, tTableUpdated.getDouble("cota_min", j));
			tUserMxScoreK.setDouble("cota_max", iNewRow, tTableUpdated.getDouble("cota_max", j));
			tUserMxScoreK.setDouble("valor", iNewRow, tTableUpdated.getDouble("valor", j));
			tUserMxScoreK.setDateTime("fecha_desde", iNewRow, oToday);
			tUserMxScoreK.setInt("personnel_id", iNewRow, Ref.getUserId());
			
		}
		
		return tUserMxScoreK;
		
	}

	private Table insertRegisters(Table tUserMxScoreK, Table tTableInserted) throws OException {
		
		for (int j = 1; j<= tTableInserted.getNumRows(); j++){
			int iNewRow = tUserMxScoreK.addRow();
			
			tUserMxScoreK.setInt("catalogo_id", iNewRow, iNewRow);
			tUserMxScoreK.setInt("category_id", iNewRow, tTableInserted.getInt("category_id", j));
			tUserMxScoreK.setString("category_name", iNewRow, tTableInserted.getString("category_name", j));
			tUserMxScoreK.setInt("variable_id", iNewRow, tTableInserted.getInt("variable_id", j));
			tUserMxScoreK.setString("variable", iNewRow, tTableInserted.getString("variable", j));
			tUserMxScoreK.setDouble("cota_min", iNewRow, tTableInserted.getDouble("cota_min", j));
			tUserMxScoreK.setDouble("cota_max", iNewRow, tTableInserted.getDouble("cota_max", j));
			tUserMxScoreK.setDouble("valor", iNewRow, tTableInserted.getDouble("valor", j));
			tUserMxScoreK.setDateTime("fecha_desde", iNewRow, oToday);
			tUserMxScoreK.setInt("personnel_id", iNewRow, Ref.getUserId());

		}
		
		return tUserMxScoreK;
	}

	private void setTableFormat() throws OException {
		
		tDisplay.setColTitle("catalogo_id", "Catalogo\nId");
		tDisplay.setColTitle("category_id", "Category\nId");
		tDisplay.setColTitle("category_name", "Category");
		tDisplay.setColTitle("variable_id", "Variable\nId");
		tDisplay.setColTitle("variable", "Variable");
		tDisplay.setColTitle("cota_min", "Cota\nMin");
		tDisplay.setColTitle("cota_max", "Cota\nMax");
		tDisplay.setColTitle("valor", "Valor");
		
		tDisplay.colHide("catalogo_id");
		tDisplay.colHide("category_id");
		tDisplay.colHide("category_name");
		tDisplay.colHide("variable_id");

		
	}

	private void setDataOnTable(Table tDisplay) throws OException {
		
		String sUserMxScoreCatalogo = EnumsUserTables.USER_MX_SCORE_CATALOGO.toString();		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();		
		tDisplay.clearDataRows();
		
		Table tTable = Table.tableNew();
		
		// Get the last registers
		StringBuilder sb = new StringBuilder();
		sb.append("\n   select  ");
		sb.append("\n   	catal.catalogo_id ");
		sb.append("\n   	,catal.category_id  ");
		sb.append("\n   	,categ.category_name  ");
		sb.append("\n   	,catal.variable_id  ");
		sb.append("\n   	,'X' || catal.variable_id as variable  ");
		sb.append("\n   	,catal.cota_min  ");
		sb.append("\n   	,catal.cota_max  ");
		sb.append("\n   	,catal.valor  ");
		sb.append("\n   from "+sUserMxScoreCatalogo+" catal  ");
		sb.append("\n   	inner join "+sUserMxScoreCategory+" categ  ");
		sb.append("\n   		on categ.category_id = catal.category_id  ");
		sb.append("\n   where catal.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD')   ");
		DBaseTable.execISql(tTable, sb.toString());
		
		// Setting registers on display table
		tDisplay.select(tTable, "catalogo_id, category_id, category_name, variable_id, variable, cota_min, cota_max, valor", "category_id GT 0");
		
		tDisplay.group("variable_id,cota_min");
		tDisplay.groupBy();

		// Refresh display in case of update
		UserDataWorksheet.setRefreshDataTable(tDisplay);
		
	}

	private void setTableProperties() throws OException {
		
		// Initialize properties tables
			tblProperties			=	UserDataWorksheet.initProperties();
			tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
		// Setting col properties
			UserDataWorksheet.setColReadOnly("catalogo_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("category_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("category_name", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("variable_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("variable", tDisplay, tblProperties);

		// Setting UDW properties
			UserDataWorksheet.setDisableSave(tblGlobalProperties);
			UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
			UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
			UserDataWorksheet.setDisableReload(tblGlobalProperties);
			UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
	}

	private void addVisualElements() throws OException {
		
		// Creating display table
			tDisplay = Table.tableNew();
			
			Table tCategory = getComboBoxCategory();
			
			tDisplay.scriptDataAddWidget("labelCategory", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=28, y=42, h=20, w=90",
					"label=Filtrar Categoria:");
			
			tDisplay.scriptDataAddWidget("filterCategory", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
					"x=113, y=44, h=25, w=170",
					"label="+tCategory.getString("category_name", 1), tCategory);
			
			tDisplay.scriptDataAddWidget("labelVariable", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=26, y=70, h=20, w=90",
					"label=Insertar Variable:");
			
			tDisplay.scriptDataAddWidget("filterVariable", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_TED_WIDGET.toInt(), 
					"x=113, y=72, h=25, w=170",
					"label=X1");
		
			tDisplay.scriptDataAddWidget("btnNuevoRegistro", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=293,y=72,h=20,w=100",
					"label=Insertar");

			tDisplay.scriptDataAddWidget("btnBorrarRegistro", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=405,y=72,h=20,w=100",
					"label=Borrar");
		
			tDisplay.scriptDataAddWidget("btnGuardarCambios", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=517,y=72,h=20,w=100",
					"label=Guardar Cambios");
	
			tDisplay.scriptDataMoveListBox("top=100,left=4,right=5,bottom=5");
		
	}

	private Table getComboBoxCategory() throws OException {
		
		String sUserMxScoreCatalogo = EnumsUserTables.USER_MX_SCORE_CATALOGO.toString();		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();	
		
		Table tCategory = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select distinct ");
		sb.append("\n 	categ.category_id ");
		sb.append("\n 	,categ.category_name ");
		sb.append("\n from "+sUserMxScoreCategory+" categ ");
		sb.append("\n 	join "+sUserMxScoreCatalogo+" catal on catal.category_id = categ.category_id ");
		DBaseTable.execISql(tCategory, sb.toString());
		
		tCategory.sortCol("category_id");
		
		return tCategory;
	}
}
