/*$Header: v 1.1, 03/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Mantenedor_UserTableScore_InternalRating.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Junio 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de un mantenedor para tickers bloomberg sin informacion de user_mx_internal_rating
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   	Julio 2018
 * Version/Autor:          	1.1 Basthian Matthews - VMetrix International
 * Description:            	Se agrega control de seguridad (security group) para que solo los usuarios
 * 							PROSPERA puedan guardar loa cambios en la usertable.          
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;
import com.afore.enums.EnumStatus;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;

public class MISC_Afore_DWS_Mantenedor_UserTableScore_InternalRating implements IScript {
	
	//Declare utils
	protected String sScriptName	=	this.getClass().getSimpleName();
	protected UTIL_Log _Log			=	new UTIL_Log(sScriptName);
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;
	
    private Table tblProperties			=	Util.NULL_TABLE;
    private Table tblGlobalProperties	=	Util.NULL_TABLE;
	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;
	private String sCellName			=	null;
	
	// User
	private String		sUserName		=	"";
	private int			iUserId			=	0;

	private int iToday		=	0;
	private String sToday	=	null;
	ODateTime oToday		= null;
            
	public void execute(IContainerContext context) throws OException {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		iUserId			=	Ref.getUserId();
		sUserName		=	Ref.getUserName();
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		oToday = ODateTime.strToDateTime(sToday);

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
				
				// Setting initial data
					setDataOnTable(tDisplay);
										
				// Set table formats
					setTableFormat();
				
				// FINALLY, Setting table properties
					setTableProperties();

				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					callback_event	=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
					tReturnt		=	UserDataWorksheet.getData();
					sCellName		=	tReturnt.scriptDataGetCallbackName();

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:

						Table tSecurityGroupsTable = Ref.retrievePersonnelSecurityGroups(iUserId);
						int iSecurityGroupRiskManager = tSecurityGroupsTable.unsortedFindString("name", "PROSPERA", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
						
						if(iSecurityGroupRiskManager!= -1){
			    			if (sCellName.equals("btnGuardarCambios")){
			    				
			    				if (saveTable(tReturnt)==true)
			    					Ask.ok("Se Guardaron correctamente las propiedades de metricas.");
			    				else
			    					Ask.ok("No se pudo guardar.");
			    				
			    			}
						} else {
	    					Ask.ok("El usuario "+sUserName+" no tiene permisos para realizar modificaciones.\nLos cambios no fueron almacenados.");
						}

						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private boolean saveTable(Table tReturnt) throws OException {
		
		int iResult = 0;
		
		Table tHistoricalUserTable = Table.tableNew("user_mx_internal_rating");
		String sQuery = "select * from user_mx_internal_rating";
		DBaseTable.execISql(tHistoricalUserTable, sQuery);
		
		Table tTableInserted = Table.tableNew();		
		Table tTableUpdated = Table.tableNew();		
		Table tTableDeleted = Table.tableNew();		

		tTableInserted.select(tReturnt, "*", "udw_insert EQ 1");
		tTableUpdated.select(tReturnt, "*", "udw_update EQ 1");
		tTableDeleted.select(tReturnt, "*", "udw_delete EQ 1");

		tHistoricalUserTable = updateRegisters(tHistoricalUserTable,tTableUpdated);
		
		//Delete previous info on db
			DBUserTable.clear(tHistoricalUserTable);
		
		// Push user table with modifications
			iResult = DBUserTable.bcpIn(tHistoricalUserTable);
		
		// Update display
			setDataOnTable(tReturnt);

		if (iResult==1) return true;
		else return false;
	}

	private Table updateRegisters(Table tHistoricalUserTable, Table tTableUpdated) throws OException {	
		
		tTableUpdated.addCol("updated", COL_TYPE_ENUM.COL_INT);
		tTableUpdated.addCol("personnel_id", COL_TYPE_ENUM.COL_INT);
		tTableUpdated.setColValInt("personnel_id", iUserId);
		
		for (int j = 1; j<= tTableUpdated.getNumRows(); j++){
			int jPartyId = tTableUpdated.getInt("party_id", j);
			int jFecha = tTableUpdated.getDate("fecha", j);

									
			for (int i = 1 ; i<= tHistoricalUserTable.getNumRows(); i++){
				int iPartyId = tHistoricalUserTable.getInt("party_id", i);
				int iFecha = tHistoricalUserTable.getDate("fecha", i);


				if(jPartyId == iPartyId && jFecha == iFecha){
					
						tTableUpdated.setInt("updated", j, 1);
						tHistoricalUserTable.setInt("party_id", i, tTableUpdated.getInt("party_id", j));
						tHistoricalUserTable.setString("short_name", i, tTableUpdated.getString("short_name", j));
						tHistoricalUserTable.setString("internal_rating", i, tTableUpdated.getString("internal_rating", j));
						tHistoricalUserTable.setDouble("default_prob", i, tTableUpdated.getDouble("default_prob", j));
						tHistoricalUserTable.setDouble("score", i, tTableUpdated.getDouble("score", j));
						tHistoricalUserTable.setDouble("market_cap", i, tTableUpdated.getDouble("market_cap", j));
						tHistoricalUserTable.setDouble("total_debt", i, tTableUpdated.getDouble("total_debt", j));
						tHistoricalUserTable.setInt("personnel_id", i, tTableUpdated.getInt("personnel_id", j));
						tHistoricalUserTable.setDateTime("fecha", i, oToday);						
				}
			}
		
		}
		tHistoricalUserTable.select(tTableUpdated, "party_id, short_name, internal_rating, default_prob, market_cap, total_debt, fecha, personnel_id", "updated EQ 0");

		return tHistoricalUserTable;
	}

	private void setTableFormat() throws OException {
		
		tDisplay.setColTitle("party_id", "Party\nId");
		tDisplay.setColTitle("short_name", "Short\nName");
		tDisplay.setColTitle("internal_rating", "Internal\nRating");
		tDisplay.setColTitle("default_prob", "Probabilidad\nDefault");
		tDisplay.setColTitle("score", "Score");
		tDisplay.setColTitle("market_cap", "Market\nCap");
		tDisplay.setColTitle("total_debt", "Total\nDebt");
		tDisplay.setColTitle("fecha", "Fecha");

		tDisplay.colHide("party_id");
		tDisplay.colHide("fecha");
	
	}

	private void setDataOnTable(Table tDisplay) throws OException {
		
		tDisplay.clearDataRows();
		
		Table tTable = Table.tableNew();
		
		// Get the last registers
		StringBuilder sb = new StringBuilder();
		sb.append("\n with maxdates as ( ");
		sb.append("\n 	select ");
		sb.append("\n 		max(ir.fecha) fecha ");
		sb.append("\n 		,ir.party_id ");
		sb.append("\n 	from user_mx_internal_rating ir ");
		sb.append("\n 	group by ir.party_id ");
		sb.append("\n ) ");
		sb.append("\n  select  ");
		sb.append("\n  	ir.party_id  ");
		sb.append("\n  	,ir.short_name  ");
		sb.append("\n  	,ir.internal_rating  ");
		sb.append("\n  	,ir.default_prob  ");
		sb.append("\n  	,ir.score ");
		sb.append("\n  	,ir.market_cap ");
		sb.append("\n  	,ir.total_debt ");
		sb.append("\n  	,max.fecha  ");
		sb.append("\n  from user_mx_internal_rating ir  ");
		sb.append("\n  	inner join user_mx_score_party up  ");
		sb.append("\n  		on up.party_id = ir.party_id  ");
		sb.append("\n  	inner join maxdates max ");
		sb.append("\n 		on max.party_id = ir.party_id ");
		sb.append("\n 		and max.fecha = ir.fecha ");
		sb.append("\n  where ");
		sb.append("\n  	up.category_id = 4 -- Bloomberg  ");
		sb.append("\n  	and ir.internal_rating = ' ' ");
		sb.append("\n  	and up.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
		DBaseTable.execISql(tTable, sb.toString());
		
		// Setting registers on display table
		tDisplay.select(tTable, "party_id, short_name, internal_rating, default_prob, score, market_cap, total_debt, fecha", "party_id GT 0");
		
		tDisplay.sortCol("party_id");
		
		// Refresh display in case of update
		UserDataWorksheet.setRefreshDataTable(tDisplay);
		
	}

	private void setTableProperties() throws OException {
		
		// Initialize properties tables
			tblProperties			=	UserDataWorksheet.initProperties();
			tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
//		// Setting col properties
			UserDataWorksheet.setColReadOnly("party_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("short_name", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("fecha", tDisplay, tblProperties);

		// Setting UDW properties
			UserDataWorksheet.setDisableSave(tblGlobalProperties);
			UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
			UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
			UserDataWorksheet.setDisableReload(tblGlobalProperties);
			UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
	}

	private void addVisualElements() throws OException {
		
		// Creating display table
			tDisplay = Table.tableNew();

		// Setting visual elements		
			tDisplay.scriptDataAddWidget("btnGuardarCambios", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=113,y=44,h=20,w=100",
					"label=Guardar Cambios");
	
			tDisplay.scriptDataMoveListBox("top=100,left=4,right=5,bottom=5");
		
	}
}
