/*$Header: v 1.1, 03/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Mantenedor_UserTableScore_K.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de un mantenedor para la usertable user_mx_score_k
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   	Julio 2018
 * Version/Autor:          	1.1 Basthian Matthews - VMetrix International
 * Description:            	Se agrega control de seguridad (security group) para que solo los usuarios
 * 							PROSPERA puedan guardar loa cambios en la usertable.         
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;
import com.afore.enums.EnumStatus;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;

public class MISC_Afore_DWS_Mantenedor_UserTableScore_K implements IScript {
	
	
	//Declare utils
	protected String sScriptName	=	this.getClass().getSimpleName();
	protected UTIL_Log _Log			=	new UTIL_Log(sScriptName);
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;
	
    private Table tblProperties			=	Util.NULL_TABLE;
    private Table tblGlobalProperties	=	Util.NULL_TABLE;
	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;
	private String sCellName			=	null;
	private int iSelectedRow			=	0;		

	private int iToday		=	0;
	private String sToday	=	null;
	ODateTime oToday		= null;
	
	// User
	private String		sUserName		=	"";
	private int			iUserId			=	0;
            
	public void execute(IContainerContext context) throws OException {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		oToday = ODateTime.strToDateTime(sToday);
		
		iUserId			=	Ref.getUserId();
		sUserName		=	Ref.getUserName();

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
				
				// Setting initial data
					setDataOnTable(tDisplay);
										
				// Set table formats
					setTableFormat();
				
				// FINALLY, Setting table properties
					setTableProperties();

				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					callback_event	=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
					tReturnt		=	UserDataWorksheet.getData();
					sCellName		=	tReturnt.scriptDataGetCallbackName();
					iSelectedRow	=	tReturnt.scriptDataGetCallbackRow();

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:

						Table tSecurityGroupsTable = Ref.retrievePersonnelSecurityGroups(iUserId);
						int iSecurityGroupRiskManager = tSecurityGroupsTable.unsortedFindString("name", "PROSPERA", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
						
						if(iSecurityGroupRiskManager!= -1){
			    			if (sCellName.equals("btnGuardarCambios")){
			    				
			    				if (saveTable(tReturnt)==true)
			    					Ask.ok("Se Guardaron correctamente las propiedades de metricas.");
			    				else
			    					Ask.ok("No se pudo guardar.");
			    				
			    			}
						} else {
	    					Ask.ok("El usuario "+sUserName+" no tiene permisos para realizar modificaciones.\nLos cambios no fueron almacenados.");
						}

						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}


	private boolean saveTable(Table tReturnt) throws OException {
		
		String sUserMxScoreK = EnumsUserTables.USER_MX_SCORE_K.toString();		
		
		int iResult = 0;
		
		Table tUserMxScoreK = Table.tableNew(sUserMxScoreK);
		String sQuery = "select * from " + sUserMxScoreK;
		DBaseTable.execISql(tUserMxScoreK, sQuery);
		
		Table tTableInserted = Table.tableNew();		
		Table tTableUpdated = Table.tableNew();		
		Table tTableDeleted = Table.tableNew();		

		tTableInserted.select(tReturnt, "*", "udw_insert EQ 1");
		tTableUpdated.select(tReturnt, "*", "udw_update EQ 1");
		tTableDeleted.select(tReturnt, "*", "udw_delete EQ 1");

		tUserMxScoreK = updateRegisters(tUserMxScoreK,tTableUpdated);
		
		//Delete previous info on db
			DBUserTable.clear(tUserMxScoreK);
		
		// Push user table with modifications
			iResult = DBUserTable.bcpIn(tUserMxScoreK);
		
		// Update display
			setDataOnTable(tReturnt);

		if (iResult==1) return true;
		else return false;
	}
	
	private Table updateRegisters(Table tUserMxScoreK, Table tTableUpdated) throws OException {
		
		for (int j = 1; j<= tTableUpdated.getNumRows(); j++){
			int jCategoryId = tTableUpdated.getInt("category_id", j);
			
			for (int i = 1 ; i<= tUserMxScoreK.getNumRows(); i++){
				int iFechaHasta = tUserMxScoreK.getDate("fecha_hasta", i);
				int iCategoryId = tUserMxScoreK.getInt("category_id", i);
				
				if(jCategoryId == iCategoryId && iFechaHasta==0){
					tUserMxScoreK.setDateTime("fecha_hasta", i, oToday);
				}
			}
			
			int iNewRow = tUserMxScoreK.addRow();
			
			tUserMxScoreK.setInt("category_id", iNewRow, tTableUpdated.getInt("category_id", j));
			tUserMxScoreK.setDateTime("fecha_desde", iNewRow, oToday);
			tUserMxScoreK.setInt("factor_k", iNewRow, tTableUpdated.getInt("factor_k", j));
			tUserMxScoreK.setInt("personnel_id", iNewRow, Ref.getUserId());
			
		}
		
		return tUserMxScoreK;
		
	}

	private void setTableFormat() throws OException {

		tDisplay.setColTitle("category_id", "Category\nId");
		tDisplay.setColTitle("category_name", "Category\nName");
		tDisplay.setColTitle("factor_k", "Factor K");
		
		tDisplay.colHide("category_id");
		
	}

	private void setDataOnTable(Table tDisplay) throws OException {
		
		String sUserMxScoreK = EnumsUserTables.USER_MX_SCORE_K.toString();				
		String sUserMxScoreCategory= EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();				
		tDisplay.clearDataRows();
		
		Table tTable = Table.tableNew();
		
		// Get the last registers
		StringBuilder sb = new StringBuilder();
		sb.append("\n select  ");
		sb.append("\n 	k.category_id ");
		sb.append("\n 	,cat.category_name ");
		sb.append("\n 	,k.factor_k  ");
		sb.append("\n from "+sUserMxScoreK+" k  ");
		sb.append("\n 	join "+sUserMxScoreCategory+" cat on cat.category_id = k.category_id ");
		sb.append("\n where k.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
		DBaseTable.execISql(tTable, sb.toString());
		
		// Setting registers on display table
		tDisplay.select(tTable, "category_id, category_name, factor_k", "category_id GT 0");
		
		tDisplay.sortCol("category_id");
		
		// Refresh display in case of update
		UserDataWorksheet.setRefreshDataTable(tDisplay);
		
	}

	private void setTableProperties() throws OException {
		
		// Initialize properties tables
			tblProperties			=	UserDataWorksheet.initProperties();
			tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
		// Setting col properties
			UserDataWorksheet.setColReadOnly("category_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("category_name", tDisplay, tblProperties);

		// Setting UDW properties
			UserDataWorksheet.setDisableSave(tblGlobalProperties);
			UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
			UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
			UserDataWorksheet.setDisableReload(tblGlobalProperties);
			UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
	}

	private void addVisualElements() throws OException {
		
		// Creating display table
			tDisplay = Table.tableNew();

		// Setting visual elements		
			tDisplay.scriptDataAddWidget("btnGuardarCambios", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=113,y=44,h=20,w=100",
					"label=Guardar Cambios");
	
			tDisplay.scriptDataMoveListBox("top=100,left=4,right=5,bottom=5");
		
	}
}
