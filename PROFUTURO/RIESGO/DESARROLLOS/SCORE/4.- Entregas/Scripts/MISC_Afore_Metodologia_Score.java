/*$Header: v 1.0, 24/May/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_Metodologia_Score.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script creado para el calculo de valores Score, Probabilidad Default y Calificacion Global
 * 							de emisoras de tipo Estado y Municipios y Project Finance.
 * 							
 *                       
 * REVISION HISTORY
 * Date:					19/Jun/2018
 * Version/Autor:			Basthian Matthews Sanhueza - VMetrix SpA
 * Description:				- Se incluye logica para incluir la informacion del usuario
 *							en la columna personnel_id
 *							- Se cambia la logica para el calculo de score de los emisores Project Finance
 *                         
 ************************************************************************************/
package com.profuturo.servicios;

import com.afore.enums.EnumStatus;
import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.Workflow;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;

public class MISC_Afore_Metodologia_Score implements IScript {
	
	// Declare utils and general variables
		String sScriptName	=	this.getClass().getSimpleName();	   
		UTIL_Log _Log		=	new UTIL_Log(sScriptName);

	@Override
	public void execute(IContainerContext context) throws OException {
			
			_Log.markStartScript();
			_Log.setDEBUG_STATUS(EnumStatus.OFF);
			Table tHistoricalValues	=	Util.NULL_TABLE;
			Table tNewValues		=	Util.NULL_TABLE;
			
			String sUserMxInternalRating = EnumsUserTables.USER_MX_INTERNAL_RATING.toString();			
			
		// Getting User that runs workflow
			Table tWorkflow = Workflow.getExecutionLog("13.- MISC - Calculo Metodologia Score", OCalendar.today(), OCalendar.today());
			int iRowRunningJob = tWorkflow.unsortedFindString("status_txt", "Executing", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
			int iUserId = tWorkflow.getInt("submitter", iRowRunningJob);			

		// Creating tables
			try {
				tHistoricalValues 	=	Table.tableNew(sUserMxInternalRating);
				tNewValues			=	Table.tableNew("New Values");
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at execute(), failed to create tHistoricalValues or tNewValues table, "
						+ e.getMessage());
			}
			
		// Setting query filters
			String sUserMxScoreK = EnumsUserTables.USER_MX_SCORE_K.toString();		
			String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();		
			String sUserMxScoreCalificacion = EnumsUserTables.USER_MX_SCORE_CALIFICACION.toString();		
			String sUserMxScoreCatalogo = EnumsUserTables.USER_MX_SCORE_CATALOGO.toString();		
			String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();		
			String sUserMxScoreMatriz = EnumsUserTables.USER_MX_SCORE_MATRIZ.toString();
		
		// Prepare querys for new values and historical userteable
			String sQuery = "select party_id, short_name, internal_rating, default_prob, score, fecha, market_cap, total_debt, personnel_id from " +sUserMxInternalRating;
			StringBuilder sb = new StringBuilder ();

			sb.append("\n    with    ");
			sb.append("\n    variables as    ");
			sb.append("\n    (    ");
			sb.append("\n     select    ");
			sb.append("\n       sys.business_date fecha,    ");
			sb.append("\n       DATE '1900-01-01' nulo    ");
			sb.append("\n     from dual    ");
			sb.append("\n     left join system_dates sys    ");
			sb.append("\n       on 1 = 1    ");
			sb.append("\n    ),    ");
			sb.append("\n    score_pd as (    ");
			sb.append("\n     select    ");
			sb.append("\n       sp.party_id    ");
			sb.append("\n         ,sk.factor_k + sum(scatal.valor) as ohd_score ");
			sb.append("\n         ,CASE WHEN(scateg.category_id = 1) -- Estados y Municipios ");
			sb.append("\n       THEN 100.0 / (1.0 + EXP( -(500.0 - (sk.factor_k + sum(scatal.valor))) * (LN (2.0) / 40.0 ))) ");
			sb.append("\n       ELSE 100.0 / (1.0 + EXP(-53.43832926+(0.124601632 * (sk.factor_k + sum(scatal.valor))))) ");
			sb.append("\n     END AS ohd_default_prob ");
    		sb.append("\n     from "+sUserMxScoreParty+" sp   "); 
      		sb.append("\n       left join "+sUserMxScoreMatriz+" sm   ");
			sb.append("\n         on sm.party_id = sp.party_id    ");
			sb.append("\n       left join "+sUserMxScoreCategory+" scateg   ");
			sb.append("\n         on scateg.category_id = sp.category_id    ");
			sb.append("\n       left join "+sUserMxScoreCatalogo+" scatal   ");
			sb.append("\n         on scatal.category_id = scateg.category_id    ");
			sb.append("\n         left join "+sUserMxScoreK+" sk   "); 
			sb.append("\n           on sk.category_id = sp.category_id    ");
			sb.append("\n       left join variables v    ");
			sb.append("\n         on 1 = 1    ");
			sb.append("\n     where    ");
			sb.append("\n       -- Segunda condicion de join entre tablas user_mx_score_matriz y user_mx_score_catalogo    ");
			sb.append("\n         sm.variable_id = scatal.variable_id    ");
			sb.append("\n       -- Condicion de categorias solo Estados y municipios o Project finance    ");
			sb.append("\n         and scateg.category_id in (1,2)    ");
			sb.append("\n       -- Manejo de fechas (ultimo registro) para las tablas   ");
			sb.append("\n         and v.fecha >= sp.fecha_desde and (v.fecha < sp.fecha_hasta or v.nulo = sp.fecha_hasta)    ");
			sb.append("\n         and v.fecha >= sm.fecha_desde and (v.fecha < sm.fecha_hasta or v.nulo = sm.fecha_hasta)    ");
			sb.append("\n         and v.fecha >= scatal.fecha_desde and (v.fecha < scatal.fecha_hasta or v.nulo = scatal.fecha_hasta)    ");
			sb.append("\n         and v.fecha >= sk.fecha_desde and (v.fecha < sk.fecha_hasta or v.nulo = sk.fecha_hasta)    ");
			sb.append("\n       -- Manejo de rangos de valores    ");
			sb.append("\n         and sm.valor >= scatal.cota_min    ");
			sb.append("\n         and sm.valor < scatal.cota_max    ");
			sb.append("\n        group by sp.party_id, sk.factor_k, scateg.category_id ");
			sb.append("\n    )          ");
			sb.append("\n    select    ");
			sb.append("\n     sp.party_id    ");
			sb.append("\n     ,p.short_name  ");
			sb.append("\n     ,sp.ohd_score    ");
			sb.append("\n     ,sp.ohd_default_prob     ");
			sb.append("\n     ,scalif.calif_global as internal_rating ");
			sb.append("\n     ,0.0 as ohd_market_cap ");
			sb.append("\n     ,0.0 as ohd_total_debt ");
			sb.append("\n     ,v.fecha    ");
			sb.append("\n    from score_pd sp  ");
			sb.append("\n      inner join party p  ");
			sb.append("\n        on p.party_id = sp.party_id  ");
			sb.append("\n     left join "+sUserMxScoreCalificacion+" scalif   "); 
			sb.append("\n       on 1 = 1    ");
			sb.append("\n     left join variables v    ");
			sb.append("\n       on 1 = 1    ");
			sb.append("\n    where    ");
			sb.append("\n     -- Manejo de fechas (ultimo registro) para la tabla de calificaciones   ");
			sb.append("\n       v.fecha >= scalif.fecha_desde and (v.fecha < scalif.fecha_hasta or v.nulo = scalif.fecha_hasta)    ");
			sb.append("\n     -- Manejo de rangos de valores    ");
			sb.append("\n       and sp.ohd_default_prob >= scalif.cota_min    ");
			sb.append("\n       and sp.ohd_default_prob < scalif.cota_max    ");

		// Load both tables
			try {
				DBaseTable.execISql(tHistoricalValues, sQuery);
				DBaseTable.execISql(tNewValues, sb.toString());
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at execute(), failed to load query return values on tables, "
						+ e.getMessage());
			}
						
		// Add col to set userid value
			tNewValues.addCol("personnel_id", COL_TYPE_ENUM.COL_INT);
			tNewValues.setColValInt("personnel_id", iUserId);
			
		// Add new and updated values
			tHistoricalValues.addCol("update", COL_TYPE_ENUM.COL_INT);
			tNewValues.addCol("update", COL_TYPE_ENUM.COL_INT);
			// Select updated values
			tHistoricalValues.select(tNewValues, "party_id, short_name, internal_rating, default_prob, score, fecha, market_cap, total_debt, personnel_id", "party_id EQ $party_id AND fecha EQ $fecha");
			// Set updated
			tHistoricalValues.setColValInt("update", 1);
			// Identify updated values (matched) on new values table
			tNewValues.select(tHistoricalValues, "update", "party_id EQ $party_id AND fecha EQ $fecha");
			// Select new values
			tHistoricalValues.select(tNewValues, "party_id, short_name, internal_rating, default_prob, score, fecha, market_cap, total_debt, personnel_id", "update EQ 0");
			tHistoricalValues.delCol("update");
			
		// Get validity status of usertable
			int iValidTable = 0;
				try {
					iValidTable = DBUserTable.userTableIsValid(tHistoricalValues.getTableName());
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"OException at execute(), failed to get validity status for table, "
							+ e.getMessage());
				}
				
		// If Usertable exists on DB
			if (iValidTable == 1){					
				//Clean data (just for precaution because table was just created so no data should be there)
				try {
					DBUserTable.clear(tHistoricalValues);
				} catch (OException e) {
					_Log.printMsg(EnumTypeMessage.ERROR,
							"OException at pushUserTablesToDB(), failed to clear the table, "
							+ e.getMessage());
				}
		    }
						
		// Push the memory table(with new loaded info) to the user table with same name
			try {
				DBUserTable.bcpIn(tHistoricalValues);
			} catch (OException e) {
				_Log.printMsg(EnumTypeMessage.ERROR,
						"OException at pushUserTablesToDB(), failed to push usertable, "
						+ e.getMessage());
			}
						
		_Log.markEndScript();

		}
}