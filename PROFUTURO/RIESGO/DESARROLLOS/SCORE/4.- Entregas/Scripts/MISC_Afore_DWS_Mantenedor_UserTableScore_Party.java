/*$Header: v 1.1, 03/Jul/2018 $*/
/***********************************************************************************
 * File Name:				MISC_Afore_DWS_Mantenedor_UserTableScore_Party.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Mayo 2018
 * Version:					1.0
 * Description:				Script de tipo User Data Worksheet creado para la implementacion
 * 							de un mantenedor para la usertable user_mx_score_party
 * 							
 *                       
 * REVISION HISTORY
 * Date:                   	Julio 2018
 * Version/Autor:          	1.1 Basthian Matthews - VMetrix International
 * Description:            	Se agrega control de seguridad (security group) para que solo los usuarios
 * 							PROSPERA puedan guardar loa cambios en la usertable.         
 *                         
 ************************************************************************************/
package com.profuturo.misc_dws;
import com.afore.enums.EnumStatus;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Ask;
import com.olf.openjvs.DBUserTable;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;

public class MISC_Afore_DWS_Mantenedor_UserTableScore_Party implements IScript {
	
	//Declare utils
	protected String sScriptName	=	this.getClass().getSimpleName();
	protected UTIL_Log _Log			=	new UTIL_Log(sScriptName);
	UTIL_Afore UtilAfore			=	new UTIL_Afore();
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;
	
    private Table tblProperties			=	Util.NULL_TABLE;
    private Table tblGlobalProperties	=	Util.NULL_TABLE;
	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;
	
	private String sCellName			=	null;
	private int iSelectedRow			=	0;		
	private String sSelectedParty		=	null;
	private int iSelectedParty			=	0;
	private String sSelectedCategory	=	null;
	private int iSelectedCategory		=	0;

	private int iToday		=	0;
	private String sToday	=	null;
	ODateTime oToday		=	null;
	
	// User
	private String		sUserName		=	"";
	private int			iUserId			=	0;
            
	public void execute(IContainerContext context) throws OException {
		
		_Log.setDEBUG_STATUS(EnumStatus.ON);
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		oToday = ODateTime.strToDateTime(sToday);
		
		iUserId			=	Ref.getUserId();
		sUserName		=	Ref.getUserName();

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				// Adding visual elements
					addVisualElements();
				
				// Setting initial data
					setDataOnTable(tDisplay);
										
				// Set table formats
					setTableFormat();
					
				// FINALLY, Setting table properties
					setTableProperties();
					
				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
					callback_event		=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
					tReturnt			=	UserDataWorksheet.getData();
					sCellName			=	tReturnt.scriptDataGetCallbackName();
					iSelectedRow		=	tReturnt.scriptDataGetCallbackRow();
					sSelectedParty		=	tReturnt.scriptDataGetWidgetString("filterParty");
					iSelectedParty		=	Ref.getValue(SHM_USR_TABLES_ENUM.PARTY_TABLE, sSelectedParty);
					
					sSelectedCategory	=	tReturnt.scriptDataGetWidgetString("filterCategory");
					iSelectedCategory	=	getCategoryIdFromCategoryName(sSelectedCategory);

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						
		    			if (sCellName.equals("btnNuevoRegistro")){
		    				
		    				int iFoundParty = tReturnt.unsortedFindInt("party_id", iSelectedParty);
		    				
		    				if(iFoundParty == -1){
			    				int iNewRow = tReturnt.addRow();
			    				
			    				tReturnt.setInt("party_id",iNewRow, iSelectedParty);
			    				tReturnt.setString("party_name",iNewRow, sSelectedParty);
			    				tReturnt.setInt("category_id",iNewRow, iSelectedCategory);
			    				tReturnt.setString("category_name",iNewRow, sSelectedCategory);
			    				tReturnt.setInt("udw_insert", iNewRow, 1);
		    				} else {
	    						Ask.ok("La contraparte que intentas ingresar ya se encuentra en la tabla\n"
	    								+ "Si quieres actualizar la categoria de la contraparte, debes borrarla e ingresarla nuevamente");
		    				}
		    			}
		    			
		    			if (sCellName.equals("btnBorrarRegistro")){
		    				tReturnt.setInt("udw_delete", iSelectedRow, 1);
		    			}
		    			
						Table tSecurityGroupsTable = Ref.retrievePersonnelSecurityGroups(iUserId);
						int iSecurityGroupRiskManager = tSecurityGroupsTable.unsortedFindString("name", "PROSPERA", SEARCH_CASE_ENUM.CASE_INSENSITIVE);
						
						if(iSecurityGroupRiskManager!= -1){
			    			if (sCellName.equals("btnGuardarCambios")){
			    				
			    				if (saveTable(tReturnt)==true){
			    					Ask.ok("Se Guardaron correctamente las propiedades de metricas.");
			    					
			    					Table menu_select = Table.tableNew();
				                    menu_select.addCol("col1", COL_TYPE_ENUM.COL_INT);
				                    menu_select.addRow();
				                    menu_select.setInt(1,1,1);

				                    tReturnt.scriptDataSetWidgetMenu("filterParty", getComboBoxParty(), menu_select);

			    				}
			    				else
			    					Ask.ok("No se pudo guardar.");
			    			}
						} else {
	    					Ask.ok("El usuario "+sUserName+" no tiene permisos para realizar modificaciones.\nLos cambios no fueron almacenados.");
						}

						break;

					default:
						break;
				}
				break;
			default:
				break;
		}
	}


	private int getCategoryIdFromCategoryName(String sSelectedCategory) throws OException {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
		
		Table tAux = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select ");
		sb.append("\n 	category_id ");
		sb.append("\n 	,category_name ");
		sb.append("\n from " +sUserMxScoreCategory);
		DBaseTable.execISql(tAux, sb.toString());
		
		int iFindedCategory = tAux.unsortedFindString("category_name", sSelectedCategory, SEARCH_CASE_ENUM.CASE_INSENSITIVE);
		int iResultId = tAux.getInt("category_id", iFindedCategory);
		
		return iResultId;
	}

	private boolean saveTable(Table tReturnt) throws OException {
		
		String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();		
		int iResult = 0;
		
		Table tUserMxScoreK = Table.tableNew(sUserMxScoreParty);
		String sQuery = "select * from " +sUserMxScoreParty;
		DBaseTable.execISql(tUserMxScoreK, sQuery);
		
		Table tTableInserted = Table.tableNew();		
		Table tTableUpdated = Table.tableNew();		
		Table tTableDeleted = Table.tableNew();		

		tTableInserted.select(tReturnt, "*", "udw_insert EQ 1");
		tTableUpdated.select(tReturnt, "*", "udw_update EQ 1");
		tTableDeleted.select(tReturnt, "*", "udw_delete EQ 1");

		tUserMxScoreK = deleteRegisters(tUserMxScoreK,tTableDeleted);
		tUserMxScoreK = updateRegisters(tUserMxScoreK,tTableUpdated);
		tUserMxScoreK = insertRegisters(tUserMxScoreK,tTableInserted);
		
		//Delete previous info on db
			DBUserTable.clear(tUserMxScoreK);
		
		// Push user table with modifications
			iResult = DBUserTable.bcpIn(tUserMxScoreK);
		
		// Update display
			setDataOnTable(tReturnt);

		if (iResult==1) return true;
		else return false;
	}

	private Table deleteRegisters(Table tUserMxScoreK, Table tTableDeleted) throws OException {
		
		// Identify registers on historical usertable that are going to be deleted
		// Filter them by the selected registers on TableDeleted and field fecha_hasta = 0(null)
		
		for (int j = 1; j<= tTableDeleted.getNumRows(); j++){
			int jPartyId = tTableDeleted.getInt("party_id", j);
			
			for (int i = 1 ; i<= tUserMxScoreK.getNumRows(); i++){
				int iFechaHasta = tUserMxScoreK.getDate("fecha_hasta", i);
				int iPartyId = tUserMxScoreK.getInt("party_id", i);
				
				if(jPartyId == iPartyId &&
						iFechaHasta==0){
					tUserMxScoreK.setDateTime("fecha_hasta", i, oToday);
				}
			}
		}
		
		return tUserMxScoreK;
	}


	private Table updateRegisters(Table tUserMxScoreK, Table tTableUpdated) throws OException {
		
		for (int j = 1; j<= tTableUpdated.getNumRows(); j++){
			int jPartyId = tTableUpdated.getInt("party_id", j);
			
			for (int i = 1 ; i<= tUserMxScoreK.getNumRows(); i++){
				int iFechaHasta = tUserMxScoreK.getDate("fecha_hasta", i);
				int iPartyId = tUserMxScoreK.getInt("party_id", i);
				
				if(jPartyId == iPartyId &&
						iFechaHasta==0){
					tUserMxScoreK.setDateTime("fecha_hasta", i, oToday);
				}
			}
		
			int iNewRow = tUserMxScoreK.addRow();
			
			tUserMxScoreK.setInt("party_id", iNewRow, tTableUpdated.getInt("party_id", j));
			tUserMxScoreK.setInt("category_id", iNewRow, tTableUpdated.getInt("category_id", j));
			tUserMxScoreK.setString("category_name", iNewRow, tTableUpdated.getString("category_name", j));
			tUserMxScoreK.setString("ticker_bbg", iNewRow, tTableUpdated.getString("ticker_bbg", j));
			tUserMxScoreK.setDateTime("fecha_desde", iNewRow, oToday);
			tUserMxScoreK.setInt("personnel_id", iNewRow, Ref.getUserId());
			
		}
		
		return tUserMxScoreK;
		
	}

	private Table insertRegisters(Table tUserMxScoreK, Table tTableInserted) throws OException {
		
		for (int j = 1; j<= tTableInserted.getNumRows(); j++){
			int iNewRow = tUserMxScoreK.addRow();
			
			tUserMxScoreK.setInt("party_id", iNewRow, tTableInserted.getInt("party_id", j));
			tUserMxScoreK.setInt("category_id", iNewRow, tTableInserted.getInt("category_id", j));
			tUserMxScoreK.setString("category_name", iNewRow, tTableInserted.getString("category_name", j));
			tUserMxScoreK.setString("ticker_bbg", iNewRow, tTableInserted.getString("ticker_bbg", j));
			tUserMxScoreK.setDateTime("fecha_desde", iNewRow, oToday);
			tUserMxScoreK.setInt("personnel_id", iNewRow, Ref.getUserId());

		}
		
		return tUserMxScoreK;
	}

	private void setTableFormat() throws OException {
		
		tDisplay.setColTitle("party_id", "Party\nId");
		tDisplay.setColTitle("party_name", "Party");
		tDisplay.setColTitle("category_id", "Category\nId");
		tDisplay.setColTitle("category_name", "Category");
		tDisplay.setColTitle("ticker_bbg", "Ticker\nBloomberg");
		
		tDisplay.colHide("party_id");
		tDisplay.colHide("category_id");

	}

	private void setDataOnTable(Table tDisplay) throws OException {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
		String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();

		tDisplay.clearDataRows();
		
		Table tTable = Table.tableNew();
		
		// Get the last registers
		StringBuilder sb = new StringBuilder();
		sb.append("\n  select  ");
		sb.append("\n  	sp.party_id  ");
		sb.append("\n  	,p.short_name as party_name  ");
		sb.append("\n  	,c.category_id  ");
		sb.append("\n  	,c.category_name  ");
		sb.append("\n  	,sp.ticker_bbg  ");
		sb.append("\n  from "+sUserMxScoreParty+" sp  ");
		sb.append("\n  	inner join "+sUserMxScoreCategory+" c  ");
		sb.append("\n  		on c.category_id = sp.category_id  ");
		sb.append("\n  	inner join party p ");
		sb.append("\n  		on p.party_id = sp.party_id ");
		sb.append("\n  where  ");
		sb.append("\n  sp.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
		DBaseTable.execISql(tTable, sb.toString());
		
		// Setting registers on display table
		tDisplay.select(tTable, "party_id, party_name, category_id, category_name, ticker_bbg", "category_id GT 0");
		
		// Setting sort order
		tDisplay.group("category_id, party_id");
		tDisplay.groupBy();

		// Refresh display in case of update
		UserDataWorksheet.setRefreshDataTable(tDisplay);
		
	}

	private void setTableProperties() throws OException {
		
		// Initialize properties tables
			tblProperties			=	UserDataWorksheet.initProperties();
			tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
			
		// Setting col properties
			UserDataWorksheet.setColReadOnly("party_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("party_name", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("category_id", tDisplay, tblProperties);
			UserDataWorksheet.setColReadOnly("category_name", tDisplay, tblProperties);

		// Setting UDW properties
			UserDataWorksheet.setDisableSave(tblGlobalProperties);
			UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
			UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
			UserDataWorksheet.setDisableReload(tblGlobalProperties);
			UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			
	}

	private void addVisualElements() throws OException {
		
		// Creating display table
			tDisplay = Table.tableNew();
			
			Table tParty = getComboBoxParty();
			Table tCategory = getComboBoxCategory();
			
			tDisplay.scriptDataAddWidget("labelParty", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=38, y=42, h=20, w=90",
					"label=Insertar Party:");
			
			tDisplay.scriptDataAddWidget("filterParty", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
					"x=113, y=44, h=25, w=170",
					"label="+tParty.getString("party_name", 1), tParty);
			
			tDisplay.scriptDataAddWidget("labelCategory", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
					"x=285, y=42, h=20, w=90",
					"label=Categoria:");
			
			tDisplay.scriptDataAddWidget("filterCategory", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(),
					"x=340, y=44, h=25, w=170",
					"label="+tCategory.getString("category_name", 1), tCategory);
			
			tDisplay.scriptDataAddWidget("btnNuevoRegistro", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=523,y=44,h=20,w=100",
					"label=Insertar");

			tDisplay.scriptDataAddWidget("btnBorrarRegistro", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=635,y=44,h=20,w=100",
					"label=Borrar");
			
			tDisplay.scriptDataAddWidget("btnGuardarCambios", 
					SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
					"x=747,y=44,h=20,w=100",
					"label=Guardar Cambios");
	
			tDisplay.scriptDataMoveListBox("top=100,left=4,right=5,bottom=5");
			
		
	}


	private Table getComboBoxParty() throws OException {
		
		String sUserMxScoreParty = EnumsUserTables.USER_MX_SCORE_PARTY.toString();
		
		Table tParty = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select distinct ");
		sb.append("\n 	p.party_id ");
		sb.append("\n 	,p.short_name as party_name");
		sb.append("\n from party p ");
		sb.append("\n 	left join "+sUserMxScoreParty+" sp ");
		sb.append("\n 		on sp.party_id = p.party_id ");
		sb.append("\n 		and sp.fecha_hasta = TO_DATE('1900/01/01', 'YYYY/MM/DD') ");
		sb.append("\n where (sp.party_id is null) ");
		sb.append("\n and p.party_class = 0 -- Only legal entities ");
		sb.append("\n and p.int_ext = 1 -- External parties ");
		sb.append("\n and p.party_status = 1 ");
		DBaseTable.execISql(tParty, sb.toString());
		
		tParty.sortCol("party_id");
		
		return tParty;
	}
	
	private Table getComboBoxCategory() throws OException {
		
		String sUserMxScoreCategory = EnumsUserTables.USER_MX_SCORE_CATEGORY.toString();
		
		Table tCategory = Table.tableNew();
		StringBuilder sb = new StringBuilder();
		sb.append("\n select distinct ");
		sb.append("\n 	categ.category_id ");
		sb.append("\n 	,categ.category_name ");
		sb.append("\n from "+sUserMxScoreCategory+" categ ");
		DBaseTable.execISql(tCategory, sb.toString());
		
		tCategory.sortCol("category_id");
		
		return tCategory;
	}

}
