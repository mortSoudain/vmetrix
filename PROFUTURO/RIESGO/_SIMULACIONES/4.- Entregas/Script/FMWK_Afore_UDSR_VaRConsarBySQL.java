/**
File Name:              FMWK_Afore_UDSR_VaRConsarBySQL.java

Author:                 Gustavo Rojas - VMetrix International 
Creation Date:          Marzo 2018

REVISION HISTORY  
Date:                   
Description:            


Main Script:            This is the Main Script
Parameter Script:       None
Display Script:         None

Description:            Se utiliza el Query de Victor Rebolledo que calcula VaR via SQL


Assumptions:            None

Instructions:

Script Category:		SIM Result


************************************************************************************/

package com.afore.udsr.risk;

import standard.include.JVS_INC_STD_UserSimRes;

import com.afore.enums.EnumTypeMessage;
import com.afore.log.UTIL_Log;
import com.afore.util.risk.UTIL_VaR;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OException;
import com.olf.openjvs.PluginCategory;
import com.olf.openjvs.PluginType;
import com.olf.openjvs.ScriptAttributes;
import com.olf.openjvs.Table;
import com.olf.openjvs.enums.SCRIPT_CATEGORY_ENUM;
import com.olf.openjvs.enums.SCRIPT_TYPE_ENUM;
import com.olf.openjvs.enums.USER_RESULT_OPERATIONS;
@ScriptAttributes(allowNativeExceptions=false)
@PluginCategory(SCRIPT_CATEGORY_ENUM.SCRIPT_CAT_SIM_RESULT)
@PluginType(SCRIPT_TYPE_ENUM.MAIN_SCRIPT)

public class FMWK_Afore_UDSR_VaRConsarBySQL implements IScript
{

	private final String sPlugInName = this.getClass().getSimpleName();
	UTIL_Log _Log = new UTIL_Log(sPlugInName);
	UTIL_VaR _UtilVaR = new UTIL_VaR();
	private JVS_INC_STD_UserSimRes m_INCSTDUserSimRes;
	public FMWK_Afore_UDSR_VaRConsarBySQL(){
		m_INCSTDUserSimRes = new JVS_INC_STD_UserSimRes();
	 }

    public void execute(IContainerContext context) throws OException
    {
    	_Log.markStartScript();
    	try{
    		Table tReturnt = context.getReturnTable();
    		Table tArgt    = context.getArgumentsTable();
    		
	    	// Call the virtual functions according to action type
	    	int tmp = m_INCSTDUserSimRes.USR_RunMode(tArgt);
	
	    	if( tmp == USER_RESULT_OPERATIONS.USER_RES_OP_CALCULATE.toInt())
	    	{
	    		compute_result(tArgt, tReturnt);
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_FORMAT.toInt())
	    	{
	    		format_result(tReturnt);
	    	}
	    	else if(tmp == USER_RESULT_OPERATIONS.USER_RES_OP_DWEXTRACT.toInt())
	    	{
	    		//m_INCSTDUserSimRes.USR_DefaultExtract(argt, returnt);
	    		dw_extract_result(tArgt, tReturnt);
	    	}
	    	else
	    	{
	    		m_INCSTDUserSimRes.USR_Fail("Incorrect operation code", tArgt);
		    }
	    		
    	}catch (OException e){
    			_Log.printMsg(EnumTypeMessage.ERROR, e.getMessage());
		}finally{
    		_Log.markEndScript();
    	}
    	return;// main/0
    }
    
	private void compute_result(Table tArgt, Table tReturnt) throws OException 
	{

		Table tVarConsar			= Table.tableNew();
		
		tVarConsar = _UtilVaR.getVarConsarTable();
		
		tReturnt.select(tVarConsar, "*", "internal_portfolio GT 0");
		tReturnt.colHide("siefore");
		
	}
	
	// *****************************************************************************
	void format_result(Table tReturnt) throws OException
	{
	   // Set viewer column titles
		tReturnt.setColTitle( "id", 			"Scenario\nNumber");
		tReturnt.setColTitle( "return",  		"Return");
		tReturnt.setColTitle( "portfolio", 		"Portfolio");


	}
	
	void dw_extract_result(Table tArgt, Table tReturnt) throws OException
	{
	}
	
	

}
