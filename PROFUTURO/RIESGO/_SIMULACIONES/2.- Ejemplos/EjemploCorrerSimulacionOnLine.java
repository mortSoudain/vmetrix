if (tArgt.getColNum("QueryId")<1)
      tArgt.addCol("QueryId", COL_TYPE_ENUM.COL_INT);


// Run the simulation for trades queried
Table tResList = Sim.createResultListForSim();

SimResultType simResultTypeMTM = SimResultType.create(PFOLIO_RESULT_TYPE.PV_RESULT.toString());
SimResultType simResultTypeCalcPrice = SimResultType.create(PFOLIO_RESULT_TYPE.CALC_PRICE_RESULT.toString());

SimResult.addResultForSim(tResList, simResultTypeMTM);
SimResult.addResultForSim(tResList, simResultTypeCalcPrice);

      int iQuery = Query.tableQueryInsert(tAllTrades, "tran_num");

// The QueryId is now set into argt to prepare for the execution of a simulation.
tArgt.setInt("QueryId", 1, iQuery);

tSimRes = Sim.runRevalByQidFixed (tArgt, tResList, Ref.getLocalCurrency ());
Query.clear(iQuery);

// Extract the results from the simulation table for further processing
Table tRes = SimResult.getTranResults(tSimRes);

tReturnt.select(tRes, Str.intToStr(PFOLIO_RESULT_TYPE.CALC_PRICE_RESULT.toInt()) + "(indep_price)", "deal_num EQ $deal_num and deal_leg EQ 0");
