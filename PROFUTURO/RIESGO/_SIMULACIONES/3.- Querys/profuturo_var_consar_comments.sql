-- ### Límites VaR CONSAR ### --
-- Posicion en instrumentos financieros
-- Status: New (2), Proposed (7), Validated (3)

-- CTE01: Extraer cuentas de saldos para medir posicion en divisas 
-- 		USD, CAD, EUR, MXN (sin Factor de Riesgo)
-- 		Se actualiza despues de ejecutar la Contabilidad
with

--[CASH] cuentas as 
--[CASH] (
--[CASH] 	select 	ajev.acs_posting_date
--[CASH] 			,ajev.acs_fin_stmt_id
--[CASH] 			,ajev.acs_account_number
--[CASH] 			,round(ajev.acs_account_amount, 2) acs_account_amount
--[CASH] 	from acs_journal_entries_dual_view ajev
--[CASH] 	where acs_account_number in (	'7115-0001', '7115-0090', '7115-0003',  -- Saldo inicial
--[CASH] 									'7116-0001', '7116-0090', '7116-0003',	-- Por recibir
--[CASH] 									'7118-0001', '7118-0090', '7118-0003',  -- Por pagar
--[CASH] 									'1211-0002', '1211-0090', '1211-0003'	-- AIM (MXN)
--[CASH] 								)
--[CASH] ),
--[CASH] -- CTE02: Aritmetica de cuentas para extraer posicion en Divisas
--[CASH] --		Usando este calculo Riesgo Cuadra con evaluadora
--[CASH] saldos as
--[CASH] (
--[CASH] 	select 	cfg.name siefore
--[CASH] 			,sum(case when c.acs_account_number = '7115-0001' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '7116-0001' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				- sum(case when c.acs_account_number = '7118-0001' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '1211-0002' then round(c.acs_account_amount, 2) else 0 end)/tcusd.precio_sucio_24h as ohd_saldo_usd
--[CASH] 
--[CASH] 			,sum(case when c.acs_account_number = '7115-0090' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '7116-0090' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				- sum(case when c.acs_account_number = '7118-0090' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '1211-0090' then round(c.acs_account_amount, 2) else 0 end)/tccad.precio_sucio_24h as ohd_saldo_cad
--[CASH] 
--[CASH] 			,sum(case when c.acs_account_number = '7115-0003' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '7116-0003' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				- sum(case when c.acs_account_number = '7118-0003' then round(c.acs_account_amount, 2) else 0 end) 
--[CASH] 				+ sum(case when c.acs_account_number = '1211-0003' then round(c.acs_account_amount, 2) else 0 end)/tceur.precio_sucio_24h as ohd_saldo_eur
--[CASH] 	from cuentas c
--[CASH] 			left join user_siefore_cfg cfg on cfg.acs_fin_stmt_id = c.acs_fin_stmt_id
--[CASH] 			left join user_mx_vectorprecios_diario tcusd on tcusd.ticker = '*C_MXPUSD_FIX'
--[CASH] 			left join user_mx_vectorprecios_diario tccad on tccad.ticker = '*C_MXPCAD_CAD'
--[CASH] 			left join user_mx_vectorprecios_diario tceur on tceur.ticker = '*C_MXPEUR_EUR'
--[CASH] 	group by cfg.name, tcusd.precio_sucio_24h, tccad.precio_sucio_24h, tceur.precio_sucio_24h
--[CASH] ),

-- CTE03: Posicion de los instrumentos financieros. Se toma
--		MValue >>> GenericFut (50), FinFut (41), BondFut (34), Bond (5), MoneyMarket (20), Equity (28)
-- 		Saldos Contables >>> Posicion de divisas directo de la Contabilidad, mas algunos Cash (10)

position as
(
				select 	ab.ins_num
						,h.ticker
						,ab.toolset
						,ab.ins_type
						,p.name siefore
--[SOCIOS_LIQUIDADORES]	,ab.external_bunit
						,CAST(SUM(ab.mvalue) as float) ohd_position
				from ab_tran ab
						left join header h on h.ins_num = ab.ins_num
						left join portfolio p on p.id_number = ab.internal_portfolio
--[SOCIOS_LIQUIDADORES]	inner join user_mx_socios_liquidadores sl on ab.external_bunit = sl.id_findur
				where ab.tran_type = 0
						and ab.tran_status in (2, 3, 7)
						and ab.asset_type = 2
						and ab.toolset in (50, 41, 34, 5, 20, 28)
				group by ab.ins_num
						,h.ticker
						,ab.toolset
						,ab.ins_type
						,p.name
--[SOCIOS_LIQUIDADORES]	,ab.external_bunit

--[CASH] 	union all 
--[CASH] 
--[CASH] 	-- Cash Position (Contabilidad)
--[CASH] 	-- Se suma la posicion en divisas y los dividendos 
--[CASH] 	-- 		Para mapear con Factores de Riesgo, se usan Holding de Monedas (Perpetual)
--[CASH] 	--		*C_MXPUSD_FIX (21047)
--[CASH] 	--		*C_MXPCAD_CAD (28723)
--[CASH] 	--		*C_MXPEUR_EUR (28724)
--[CASH] 	select  ab.ins_num
--[CASH] 	        ,ab.reference ticker
--[CASH] 	        ,ab.toolset
--[CASH] 	        ,ab.ins_type
--[CASH] 	        ,s.siefore
--[CASH]--[SOCIOS_LIQUIDADORES]	,ab.external_bunit
--[CASH] 	        ,case   when ab.ins_num = 21047 then trunc(s.ohd_saldo_usd + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)
--[CASH] 	                        when ab.ins_num = 28723 then trunc(s.ohd_saldo_cad + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)
--[CASH] 	                        when ab.ins_num = 28724 then trunc(s.ohd_saldo_eur + NVL(d.ohd_dividendo, 0) + to_number(NVL(a.saldo, 0)), 0)
--[CASH] 	        end ohd_position
--[CASH] 	from ab_tran ab
--[CASH] 	        left join saldos s on 1 = 1
--[CASH] 	        left join user_mx_ajuste_cash_varconsar a on a.siefore = s.siefore and a.ins_num = ab.ins_num
--[CASH] 	        left join currency c on c.id_number = ab.currency
--[CASH] 	        left join (
--[CASH] 	        				-- Dividendos
--[CASH] 	        				-- 		Dividendo en Efectivo (263)
--[CASH] 	        				--		Derechos por CKDs o Fibras (2025)
--[CASH] 	        				-- Al dividendo en CAD, se resta un inpuesto de 15% (viene de user table, puede variar)
--[CASH] 	                        select  p.name siefore
--[CASH] 	                                ,c.name currency
--[CASH] 	                                ,round(sum(case when c.name = 'CAD' then ab.position * (1.0 - to_number(cv.valor)) else ab.position end), 2) ohd_dividendo 
--[CASH] 	                        from ab_tran ab
--[CASH] 	                                left join parameter pa on pa.ins_num = ab.ins_num
--[CASH] 	                                left join currency c on c.id_number = pa.currency
--[CASH] 	                                left join portfolio p on p.id_number = ab.internal_portfolio
--[CASH] 	                                left join user_configurable_variables cv on cv.proceso = 'Resultados_Contables' and cv.variable = 'iva_cad'
--[CASH] 	                        where ab.tran_type = 0
--[CASH] 	                                and ab.tran_status = 3
--[CASH] 	                                and ab.cflow_type in (263, 2025)
--[CASH] 	                                and ab.settle_date > (select max(acs_posting_date) from acs_journal_entries) 
--[CASH] 	                        group by p.name, c.name
--[CASH] 	                ) d 
--[CASH] 	                on d.siefore = s.siefore and d.currency = c.name
--[CASH] --[SOCIOS_LIQUIDADORES] inner join user_mx_socios_liquidadores sl on ab.external_bunit = sl.id_findur
--[CASH] 	where ab.ins_num in (21047, 28723, 28724)
--[CASH] 	        and ab.tran_status = 3 
--[CASH] 	        and ab.tran_type = 2
),

emov as
(
	select 	row_number() over(partition by p.siefore --[SOCIOS_LIQUIDADORES] , p.external_bunit
	 order by SUM(p.ohd_position * m.var_consar)) rec
		,'Escenarios Moviles' as escenario 
		,m.id
		,p.siefore
		--[SOCIOS_LIQUIDADORES] , p.external_bunit
		,SUM(p.ohd_position * m.var_consar) exposition
	from position p
			inner join user_mx_varconsar_escenarios m
						on m.ins_num = p.ins_num
	group by m.id, p.siefore
	--[SOCIOS_LIQUIDADORES] , p.external_bunit
),

emov_sd as
(
	select 	row_number() over(partition by p.siefore --[SOCIOS_LIQUIDADORES] , p.external_bunit
	 order by SUM(p.ohd_position * m.var_consar)) rec
		,'Escenarios Moviles Sin Derivados' as escenario 
		,m.id
		,p.siefore
		--[SOCIOS_LIQUIDADORES] , p.external_bunit
		,SUM(p.ohd_position * m.var_consar) exposition
	from position p
			inner join user_mx_varconsar_escenarios m
						on m.ins_num = p.ins_num
	where p.toolset not in (34, 41, 50)
	group by m.id, p.siefore
	--[SOCIOS_LIQUIDADORES] , p.external_bunit
),

efij as
(
	select 	row_number() over(partition by p.siefore --[SOCIOS_LIQUIDADORES] , p.external_bunit
	 order by SUM(p.ohd_position * m.var_consar)) rec
		,'Escenarios Fijos' as escenario 
		,m.id
		,p.siefore
		--[SOCIOS_LIQUIDADORES] , p.external_bunit
		,SUM(p.ohd_position * m.var_consar) exposition
	from position p
			inner join user_mx_varconsar_esc_stress m
						on m.ins_num = p.ins_num
	group by m.id, p.siefore
	--[SOCIOS_LIQUIDADORES] , p.external_bunit
),

efij_sd as 
(
	select 	row_number() over(partition by p.siefore --[SOCIOS_LIQUIDADORES] , p.external_bunit
	 order by SUM(p.ohd_position * m.var_consar)) rec
		,'Escenarios Fijos Sin Derivados' as escenario 
		,m.id
		,p.siefore
		--[SOCIOS_LIQUIDADORES] , p.external_bunit
		,SUM(p.ohd_position * m.var_consar) exposition
	from position p
			inner join user_mx_varconsar_esc_stress m
						on m.ins_num = p.ins_num
	where p.toolset not in (34, 41, 50)
	group by m.id, p.siefore
	--[SOCIOS_LIQUIDADORES] , p.external_bunit
),
-- CTE09: Tabla con el computo del Activo VaR
--		Al valor del NAV, se resta el MtoM de los instrumentos CKDs
--		ins_type: EQT-CKD-KCALL (1000043), EQT-CKD-PREFUND (1000044) y bono '91_AGSACB_08' (caso especial)
avar as
(
	select 	n.nav_portfolio siefore
			,n.nav_value
			,NVL(m.ohd_mtom_ckd, 0) ohd_mtom_ckd
			,n.nav_value - NVL(m.ohd_mtom_ckd, 0) ohd_activo_var
	from user_mx_historical_nav n
			left join (
							select 	p.siefore
									,sum(p.ohd_position * v.precio_sucio_24h) ohd_mtom_ckd
							from position p
									inner join user_mx_vectorprecios_diario v 
										on v.ticker = p.ticker
							where p.ins_type in (1000043, 1000044) or p.ticker = '91_AGSACB_08'
							group by p.siefore
						) m
			on m.siefore = n.nav_portfolio
)

--[ACTVAR] select * from avar

--[NO-ACTVAR]										select 	e1.escenario
--[NO-ACTVAR]												,u.metrica
--[NO-ACTVAR]												,e1.siefore
--[NO-ACTVAR] --[SOCIOS_LIQUIDADORES] 						,e1.external_bunit
--[NO-ACTVAR]												,u.k
--[NO-ACTVAR]												,d.business_date fecha_var
--[NO-ACTVAR]												,sum(case when e1.rec = u.k then e1.id else 0.0 end) id_escenario
--[NO-ACTVAR]												,d.business_date - sum(case when e1.rec = u.k then e1.id else 0.0 end) fecha_esc
--[NO-ACTVAR]												,CAST(
--[NO-ACTVAR]													case	
--[NO-ACTVAR]														when u.metrica = 'VaR' then -sum(case when e1.rec = u.k then e1.exposition else 0.0 end)
--[NO-ACTVAR]														when u.metrica = 'CVaR' then -avg(e1.exposition)
--[NO-ACTVAR]														when u.metrica = 'CVaR_Diff' then -(avg(e1.exposition) - avg(e2.exposition))
--[NO-ACTVAR]														else 0.0
--[NO-ACTVAR]													end
--[NO-ACTVAR]												as float) as valor
--[NO-ACTVAR]										from user_mx_conf_limites u

--[NO-ACTVAR] --[Escenarios_Moviles]--[Con_Derivados]	inner join emov e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[Escenarios_Moviles]--[Sin_Derivados]	inner join emov_sd e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[Escenarios_Fijos]--[Con_Derivados]		inner join efij e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[Escenarios_Fijos]--[Sin_Derivados]		inner join efij_sd e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[Escenarios_Moviles]--[Con_Derivados]	inner join emov e2 on e2.siefore = u.siefore and e2.rec = e1.rec
--[NO-ACTVAR] --[Escenarios_Moviles]--[Sin_Derivados]	inner join emov_sd e2 on e2.siefore = u.siefore and e2.rec = e1.rec
--[NO-ACTVAR] --[Escenarios_Fijos]--[Con_Derivados]		inner join efij e2 on e2.siefore = u.siefore and e2.rec = e1.rec
--[NO-ACTVAR] --[Escenarios_Fijos]--[Sin_Derivados]		inner join efij_sd e2 on e2.siefore = u.siefore and e2.rec = e1.rec
--[NO-ACTVAR] --[CVaR_Diff]	--[Escenarios_Moviles]		inner join emov e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[CVaR_Diff]	--[Escenarios_Moviles]		inner join emov_sd e2 on e2.siefore = e1.siefore and e2.rec = e1.rec
--[NO-ACTVAR] --[CVaR_Diff]	--[Escenarios_Fijos]		inner join efij e1 on e1.siefore = u.siefore
--[NO-ACTVAR] --[CVaR_Diff]	--[Escenarios_Fijos]		inner join efij_sd e2 on e2.siefore = e1.siefore and e2.rec = e1.rec
--[NO-ACTVAR]	
--[NO-ACTVAR]											left join system_dates d on 1 = 1
--[NO-ACTVAR]											left join avar n on n.siefore = e1.siefore

--[NO-ACTVAR]										where e1.rec <= u.k
--[NO-ACTVAR] --[VaR]									and u.metrica in ('VaR')						
--[NO-ACTVAR] --[CVaR]									and u.metrica in ('CVaR')					
--[NO-ACTVAR] --[CVaR_Diff]								and u.metrica in ('CVaR_Diff')
--[NO-ACTVAR] 										group by e1.escenario, u.metrica, e1.siefore, u.k, d.business_date
--[NO-ACTVAR] --[SOCIOS_LIQUIDADORES] 				,e1.external_bunit