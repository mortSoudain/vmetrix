/*$Header: v 1.0, 21/Feb/2018 $*/
/***********************************************************************************
 * File Name:				FMWK_Reports_Balanza_SAT.java
 * 
 * Author:					Basthian Matthews Sanhueza - VMetrix SpA
 * Creation Date:			Febrero 2018
 * Version:					1.0
 * Description:				Script diseñado para la obtencion de las balanzas por portfolio
 * 							y seleccion de fechas de inicio y término.
 * 							
 * 							Ademas, el plugin genera un archivo XML y XLS con respecto
 * 							a la informacion de balanza obtenida
 *         
 ************************************************************************************/

package com.afore.custom_reports;

import java.io.IOException;

import com.afore.enums.EnumTypeMessage;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.afore.util.UTIL_Afore_Accounting;
import com.olf.openjvs.Ask;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.DBaseTable;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.ODateTime;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.SystemUtil;
import com.olf.openjvs.Table;
import com.olf.openjvs.UserDataWorksheet;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.ENUM_UDW_ACTION;
import com.olf.openjvs.enums.ENUM_UDW_EVENT;
import com.olf.openjvs.enums.SCRIPT_PANEL_WIDGET_TYPE_ENUM;

public class FMWK_Reports_Balanza_SAT implements IScript {
		
	String sScriptName = this.getClass().getSimpleName();	   
	UTIL_Afore UtilAfore = new UTIL_Afore();
	UTIL_Log LOG = new UTIL_Log(sScriptName);
	
	ENUM_UDW_ACTION	callback_type;
	ENUM_UDW_EVENT	callback_event;

	private Table tDisplay				=	Util.NULL_TABLE;
	private Table tReturnt				=	Util.NULL_TABLE;

    private String sSelectedStartDate	=	null;
    private String sSelectedEndDate		=	null;
    private String sSelectedPortfolio	=	null;
    private String sSelectedSendType	=	null;
    private String sSettedVersion		=	null;
	private String sCellName			=	null;

	private int iToday		=	0;
	private String sToday	=	null;
            
	public void execute(IContainerContext context) throws OException {
		
		iToday = OCalendar.today();
		sToday = OCalendar.formatDateInt(iToday, DATE_FORMAT.DATE_FORMAT_DEFAULT);

		callback_type = ENUM_UDW_ACTION.fromInt(UserDataWorksheet.getCallbackAction());
				
		switch(callback_type){
		
			case UDW_INIT_ACTION:
				
				//Initializing Script
					initializeScript();
				
				// Adding visual elements
					addVisualElements();
				
				// Setting initial data
					setDataOnTable(tDisplay);

				// Format data
					formatData(tDisplay);
				
				// FINALLY, Setting table properties
					setTableProperties();

				break;
				
			case UDW_EDIT_ACTION:
				
				// Getting callback info
				callback_event	=	ENUM_UDW_EVENT.fromInt(UserDataWorksheet.getCallbackEvent());
				tReturnt		=	UserDataWorksheet.getData();
				sCellName		=	tReturnt.scriptDataGetCallbackName();
				
				sSelectedPortfolio	=	tReturnt.scriptDataGetWidgetString("Portfolio");

				switch(callback_event){
					case UDW_CUSTOM_BUTTON_EVENT:
						if(sCellName.equals("btnConsultar")){
							
							setDataOnTable(tReturnt);
							// Format data
							formatData(tReturnt);
						} else if(sCellName.equals("btnGenerarReporte")){
							try {
								generarReportes();
							} catch (IOException e) {
								LOG.printMsg(EnumTypeMessage.ERROR, "Unable to generate reports.");
							}
						}
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}

	private void initializeScript() {
		// Creating display table
			try {
				tDisplay = Table.tableNew("tDisplay");
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to create table tDisplay. " +e.getMessage());
			}
		
		// Setting tDisplay columns 
			try {
				tDisplay.addCol("account_number", COL_TYPE_ENUM.COL_STRING, "Numero\nde Cuenta" );
				tDisplay.addCol("account_name", COL_TYPE_ENUM.COL_STRING, "Nombre\nde Cuenta" );
				tDisplay.addCol("starting_balance", COL_TYPE_ENUM.COL_STRING, "Saldo\nInicial" );
				tDisplay.addCol("debit_amount", COL_TYPE_ENUM.COL_STRING, "Debe" );
				tDisplay.addCol("credit_amount", COL_TYPE_ENUM.COL_STRING, "Haber" );
				tDisplay.addCol("final_balance", COL_TYPE_ENUM.COL_STRING, "Saldo\nFinal" );
				tDisplay.addCol("portfolio", COL_TYPE_ENUM.COL_STRING, "Portfolio" );
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set tDisplay columns. " +e.getMessage());
			}
	}
	
	private void generarReportes() throws OException, IOException{
	
		if(sSelectedPortfolio.equals("TODOS")){
			
			String sPortafolioQuery = "SELECT name as portfolio FROM portfolio where portfolio_type =0";
			Table tPortfolio = Table.tableNew();
			DBaseTable.execISql(tPortfolio, sPortafolioQuery);
			
			String sPortfolio = "";
			
			for(int i=1; i<=tPortfolio.getNumRows(); i++){
				sPortfolio = tPortfolio.getString("portfolio", i);
				generarReporte(sPortfolio);
			}
		} else {
			generarReporte(sSelectedPortfolio);
		}
	}

	private void generarReporte(String sPortfolio) throws OException, IOException {
		
		// Getting today month and year
			sSelectedEndDate	=	tReturnt.scriptDataGetWidgetString("EndDate");
			int iEndDate = OCalendar.parseString(sSelectedEndDate);
			
			int iMonth = OCalendar.getMonth(iEndDate);
			int iYear = OCalendar.getYear(iEndDate);
			String sMonth = Str.intToStr(iMonth);
			String sYear = Str.intToStr(iYear);
			
			if(sMonth.length()==1){
				sMonth="0"+sMonth;
			}
		
		// Getting configurable variables to get path for report files
			String sPath = UtilAfore.getVariableGlobal("FINDUR", "FMWK_Reports_Cuentas_Balanza_SAT", "path");
			
		// Getting RFC from portfolio
			String sQueryRFC = 
					"\n SELECT a.irs_terminal_num as RFC  "+
							"\n FROM party_address a  "+
							"\n 	 	JOIN party p ON (a.party_id = p.party_id AND p.int_ext = 0)  "+
							"\n 	 	JOIN portfolio on (p.default_portfolio_id = portfolio.id_number)  "+
							"\n 	 WHERE portfolio.name = '"+sPortfolio+"' ";
			Table tRFC = Table.tableNew();
			DBaseTable.execISql(tRFC, sQueryRFC);
			String sRFC = tRFC.getString(1, 1);
			
		// Getting send type and version
			sSelectedSendType	=	tReturnt.scriptDataGetWidgetString("TipoEnvio");
			sSettedVersion		=	tReturnt.scriptDataGetWidgetString("Version");
			if(sSelectedSendType.equals("Normal")){
				sSelectedSendType = "N";
			} else {
				sSelectedSendType = "C";
			}

		// Setting filenames
			String sFileName = sPath+sRFC+sYear+sMonth+"BN";
			String sFileExtensionXML = ".xml";
			String sFileExtensionXLS = ".xls";
			
		//Setting output table
			Table tOutput = Table.tableNew();
			tOutput.select(tReturnt, "account_number,"
					+ "account_name,"
					+ "starting_balance,"
					+ "debit_amount,"
					+ "credit_amount,"
					+ "final_balance"
					, "portfolio EQ "+sPortfolio);
			
		// Generating String with XML format and saving file
			genXmlOutput(tOutput, sRFC, sMonth, sYear, sSelectedSendType, sSettedVersion, sFileName+sFileExtensionXML);
			
		// Generating and opening XLS
			genXlsOutput(tOutput, sRFC, sMonth, sYear, sSelectedSendType, sSettedVersion, sFileName+sFileExtensionXLS);
			
		//Dispose Tables memory
			tRFC.destroy();
			tOutput.destroy();
		
	}

	private void genXlsOutput(Table tOutput, String sRFC, String sMonth,
			String sYear, String sSendType,String sVersion, String sFullFilePath) throws OException {
		
		Table tParameters = Table.tableNew("Parametros Crystal");
		int iNewRow = tParameters.addRow();
		
		tParameters.addCol("varRFC", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varTipoEnvio", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varVersion", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varMes", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varAnio", COL_TYPE_ENUM.COL_STRING);
		tParameters.addCol("varFecha", COL_TYPE_ENUM.COL_DATE_TIME);
		tParameters.addCol("varGeneradoPor", COL_TYPE_ENUM.COL_STRING);
		
		tParameters.setString("varRFC", iNewRow, sRFC);
		tParameters.setString("varTipoEnvio", iNewRow, sSendType);
		tParameters.setString("varVersion", iNewRow, sVersion);
		tParameters.setString("varMes", iNewRow, sMonth);
		tParameters.setString("varAnio",iNewRow, sYear);
		tParameters.setDateTime("varFecha", iNewRow, ODateTime.strToDateTime(sToday));
		tParameters.setString("varGeneradoPor", iNewRow, Ref.getUserName());
		
		String sTemplatesCrystalPath = Crystal.getRptDir();
		String sReportTemplateName = "ReporteBalanzaSAT.rpt";
		String sFullTemplatePath = sTemplatesCrystalPath + "\\" + sReportTemplateName;
		
		//Exporting excel
		Crystal.tableExportCrystalReport(tOutput, 
				sFullTemplatePath, 
				CRYSTAL_EXPORT_TYPES.MS_EXCEL, 
				sFullFilePath, 
				CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR,
				tParameters);
		
		//Opening Report
		SystemUtil.createProcess(sFullFilePath);
		
		//Dispose Memory
		tParameters.destroy();
		
	}

	private void genXmlOutput(Table tOutput, String sRFC, String sMonth,
			String sYear, String sSendType, String sVersion, String sFullFilePath) throws OException {
		
		String sVersionUnderscore = sVersion.replace(".", "_");
		
		StringBuilder sb = new StringBuilder();		
		
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" charset=\"UTF-8\"?>\n")
		.append("<BCE:Balanza Version=\""+sVersion+"\" RFC=\""+sRFC+"\" Mes=\""+sMonth+"\" Anio=\""+sYear+"\" TipoEnvio=\""+sSendType+"\" xmlns:BCE=\"http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/BalanzaComprobacion\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/BalanzaComprobacion http://www.sat.gob.mx/esquemas/ContabilidadE/"+ sVersionUnderscore +"/BalanzaComprobacion/BalanzaComprobacion_"+ sVersionUnderscore +".xsd\">\n");
		for (int i=1 ; i <= tOutput.getNumRows(); i++){
			sb.append("<BCE:Ctas ")
			.append("NumCta=\"").append(tOutput.getString("account_number", i)).append("\" ")
			.append("SaldoIni=\"").append(String.format("%.2f", Str.strToDouble(tOutput.getString("starting_balance", i)))).append("\" ")
			.append("Debe=\"").append(String.format("%.2f", Str.strToDouble(tOutput.getString("debit_amount", i)))).append("\" ")
			.append("Haber=\"").append(String.format("%.2f", Str.strToDouble(tOutput.getString("credit_amount", i)))).append("\" ")
			.append("SaldoFin=\"").append(String.format("%.2f", Str.strToDouble(tOutput.getString("final_balance", i)))).append("\"/>\n");	
		}
		sb.append("</BCE:Balanza>\n");
		Str.printToFile(sFullFilePath, sb.toString());
		
	}


	private void setDataOnTable(Table tTable) throws OException {
		
		// Getting input's data
			sSelectedStartDate	=	tTable.scriptDataGetWidgetString("StartDate");
			sSelectedEndDate	=	tTable.scriptDataGetWidgetString("EndDate");
			sSelectedPortfolio	=	tTable.scriptDataGetWidgetString("Portfolio");

		// Format selected date to int
			int iSelectedStartDate = ODateTime.strDateTimeToDate(sSelectedStartDate);
			int iSelectedEndDate = ODateTime.strDateTimeToDate(sSelectedEndDate);
		
		//Clear previous balance
			tTable.clearDataRows();
		//Parametros para Balanza
			int ACC_LEVEL = 4;
			int GEN_SUMMARY = 1; //YES
			int DEL_ZEROS= 1; //YES
			UTIL_Afore_Accounting _AforeAcc = new UTIL_Afore_Accounting();
			
			if(sSelectedPortfolio.equals("TODOS")){
				
				String sPortafolioQuery = "SELECT name as portfolio FROM portfolio where portfolio_type = 0";
				Table tPortfolio = Table.tableNew();
				DBaseTable.execISql(tPortfolio, sPortafolioQuery);
				
				String sPortfolio = "";
				
				for(int i=1; i<=tPortfolio.getNumRows(); i++){
					sPortfolio = tPortfolio.getString("portfolio", i);
					
					Table tData = Table.tableNew("tData");
					tData = _AforeAcc.getAccBalance(iSelectedStartDate, iSelectedEndDate, sPortfolio, ACC_LEVEL, GEN_SUMMARY, DEL_ZEROS);
					
					tData.addCol("portfolio", COL_TYPE_ENUM.COL_STRING);
					tData.setColValString("portfolio", sPortfolio);
					
					tTable.select(tData, "account_number,"
							+ "account_name,"
							+ "starting_balance,"
							+ "debit_amount,"
							+ "credit_amount,"
							+ "final_balance,"
							+ "portfolio", "account_number GT 0");
					tData.destroy();
				}
			} else {
				Table tData = Table.tableNew("tData");
				tData = _AforeAcc.getAccBalance(iSelectedStartDate, iSelectedEndDate, sSelectedPortfolio, ACC_LEVEL, GEN_SUMMARY, DEL_ZEROS);
				
				tData.addCol("portfolio", COL_TYPE_ENUM.COL_STRING);
				tData.setColValString("portfolio", sSelectedPortfolio);
				
				tTable.select(tData, "account_number,"
						+ "account_name,"
						+ "starting_balance,"
						+ "debit_amount,"
						+ "credit_amount,"
						+ "final_balance,"
						+ "portfolio", "account_number GT 0");
				tData.destroy();
			}
			
		// Display message in case tReturn is empty
			if(tTable.getNumRows()==0){
				Ask.ok("La busqueda no arrojo resultados para los filtros seleccionados");
			}
			
		// Refresh table display info
			UserDataWorksheet.setRefreshDataTable(tTable);
	}

	private void formatData(Table tDisplay) {
		
		// Get rid of portfolio and account number inside account name
		String sPortfolio = "";
		String sAccountNumber = "";
		String sAccountName = "";
		
		int iNumRows = 0;
		
		try {
			iNumRows = tDisplay.getNumRows();
		} catch (OException e1) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to get NumRows for table tDisplay");
		}
		
		for(int iRow = 1; iRow < iNumRows; iRow ++){
			
			try {
				sPortfolio = tDisplay.getString("portfolio", iRow);
				sAccountNumber = tDisplay.getString("account_number", iRow);
				sAccountName = tDisplay.getString("account_name", iRow);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to get portfolio or account_number or account name on formatData() function.");
			}
			
			// Format string to be found
			sPortfolio = sPortfolio+" - ";
			sAccountNumber= " - "+sAccountNumber;
			
			// Get rid of portfolio
			if(sAccountName.contains(sPortfolio)){
				sAccountName = sAccountName.replace(sPortfolio, "");
			}
			
			// Get rid of account number
			if(sAccountName.contains(sAccountNumber)){
				sAccountName = sAccountName.replace(sAccountNumber, "");
			}
			
			// Insert new value for account name
			try {
				tDisplay.setString("account_name", iRow, sAccountName);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set new account_name");
			}
		}
		// This is used to refresh tReturn table in case it is. If it's tDisplay it doesn't do anything
		try {
			UserDataWorksheet.setRefreshDataTable(tDisplay);
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set new refresh table on formatTable()");
		}
	}

	private void setTableProperties() {
		
			try {
				// Initialize properties tables
					Table tblProperties = UserDataWorksheet.initProperties();
					Table tblGlobalProperties		=	UserDataWorksheet.initGlobalProperties();
				
				// Setting col properties
					UserDataWorksheet.setColReadOnly("account_number", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("account_name", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("starting_balance", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("debit_amount", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("credit_amount", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("final_balance", tDisplay, tblProperties);
					UserDataWorksheet.setColReadOnly("portfolio", tDisplay, tblProperties);

				// Setting UDW properties
					UserDataWorksheet.setDisableSave(tblGlobalProperties);
					UserDataWorksheet.setDisableAddRow(tblGlobalProperties);
					UserDataWorksheet.setDisableDelRow(tblGlobalProperties);
					UserDataWorksheet.setDisableReload(tblGlobalProperties);
					UserDataWorksheet.setAllTables(tDisplay, tblProperties, tblGlobalProperties);
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set some of the table properties. " +e.getMessage());
			}

			
	}

	private void addVisualElements() {
		
		Table tPortfolio = Util.NULL_TABLE;
		Table tSendType = Util.NULL_TABLE;
		Table tVersion = Util.NULL_TABLE;
		
		try {
			// Setting portfolio table for combobox
			String sPortafolioQuery = "SELECT name as portfolio FROM portfolio where portfolio_type =0";
			tPortfolio = Table.tableNew();
			DBaseTable.execISql(tPortfolio, sPortafolioQuery);
			int iNewRow = tPortfolio.addRow();
			tPortfolio.setString("portfolio", iNewRow, "TODOS");
			
		// Setting send type table for combobox
			tSendType = Table.tableNew();
			tSendType.addCol("Tipo Envio", COL_TYPE_ENUM.COL_STRING);
			int iRow = tSendType.addRow();
			tSendType.setString("Tipo Envio", iRow, "Normal");
			iRow = tSendType.addRow();
			tSendType.setString("Tipo Envio", iRow, "Complementario");
			
		// Setting version table for combobox
			tVersion = Table.tableNew();
			tVersion.addCol("Version", COL_TYPE_ENUM.COL_STRING);
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.0");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.1");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.2");
			iRow = tVersion.addRow();
			tVersion.setString("Version", iRow, "1.3");

		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set some of the tables user for combobox. " +e.getMessage());
		}
		
		try {
			// Setting visual elements
				tDisplay.scriptDataAddWidget("lblPortfolio", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=50, y=17, h=20, w=50",
						"label=Portfolio:");
				
				tDisplay.scriptDataAddWidget("Portfolio", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
						"x=100, y=20, h=25, w=130",
						"label="+tPortfolio.getString(1, 1), tPortfolio);
						
				tDisplay.scriptDataAddWidget("lblStartDate", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=250, y=17, h=20, w=50",
						"label=Fecha Ini:");
				
				tDisplay.scriptDataAddWidget("StartDate", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_CALENDAR_WIDGET.toInt(), 
						"x=300, y=20, h=25, w=130", 
						"label="+sToday);
				
				tDisplay.scriptDataAddWidget("lblEndDate", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=450, y=17, h=20, w=50",
						"label=Fecha Fin:");
				
				tDisplay.scriptDataAddWidget("EndDate", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_CALENDAR_WIDGET.toInt(), 
						"x=500, y=20, h=25, w=130",
						"label="+sToday);
				
				tDisplay.scriptDataAddWidget("lblTipoEnvio", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=650, y=17, h=20, w=100",
						"label=Tipo Envio:");
				
				tDisplay.scriptDataAddWidget("TipoEnvio", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
						"x=720, y=20, h=25, w=100",
						"label="+tSendType.getString(1, 1), tSendType);
				
				tDisplay.scriptDataAddWidget("lblVersion", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_LABEL_WIDGET.toInt(), 
						"x=840, y=17, h=20, w=50",
						"label=Version:");
				
				tDisplay.scriptDataAddWidget("Version", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_COMBOBOX_WIDGET.toInt(), 
						"x=890, y=20, h=25, w=50",
						"label="+tVersion.getString(1, 1), tVersion);
				
				tDisplay.scriptDataAddWidget("btnConsultar", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
						"x=975, y=15, h=25, w=130", "label=Consultar");
				
				tDisplay.scriptDataAddWidget("btnGenerarReporte", 
						SCRIPT_PANEL_WIDGET_TYPE_ENUM.SCRIPT_PANEL_PUSHBUTTON_WIDGET.toInt(), 
						"x=1120, y=15, h=25, w=130", "label=Generar Reporte");
		
				tDisplay.scriptDataMoveListBox("top=45,left=40,right=50,bottom=40");
		} catch (OException e) {
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to set some of the visual elements. " +e.getMessage());
		}

		// Clean memory
			try {
				tPortfolio.destroy();
				tVersion.destroy();
				tSendType.destroy();
			} catch (OException e) {
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to destroy comobox tables. " +e.getMessage());
			}
	}
}
