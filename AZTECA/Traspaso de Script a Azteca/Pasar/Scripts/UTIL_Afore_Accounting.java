/*$Header: v 1.0, 21/02/2018 $*/
/**
 * File Name:              UTIL_Afore_Accounting.java
 * 
 * Author:                 Jorge Vergara - VMetrix International
 * Creation Date:          Febrero 2018
 * Version:                1.0
 * Description:            Se crea esta clase para agrupar las funcionalidades utilitarias de contabilidad
 *
 ************************************************************************************/

package com.afore.util;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsTranInfoFields;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Str;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;

public class UTIL_Afore_Accounting {
	
	protected UTIL_Afore _Afore = new UTIL_Afore();
	protected String sScriptName = this.getClass().getSimpleName();
	protected UTIL_Log _Log = new UTIL_Log(sScriptName);
	
	public String CUSTOM_WHERE = "";
	
	/**
	 * Obtiene balanza contable segun parametros
	 * @param iJulianFrom: Fecha Desde 
	 * @param iJulianTo: Fecha Hasta
	 * @param sPortfolio: Nombre Corto de Portafolio (Ej. SB1, SB2, etc).
	 * @param iAccLevel: Filtro de Nivel para Resultado.
	 * @param iGenSummary: 1, Genera Niveles Inferiores con Totales; 0, NO Genera Niveles Inferiores con Totales.
	 * @param iDelZeros: 1, Elimina saldos en cero; 0, NO elimina saldos en cero.
	 * @return Table con resultado.
	 * @throws OException
	 */
	public Table getAccBalance(int iJulianFrom, int iJulianTo, String sPortfolio, int iAccLevel, int iGenSummary, int iDelZeros) throws OException{
		Table tRetAccBalance = this.getAccBalance(iJulianFrom, iJulianTo, Ref.getValue(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, sPortfolio), iAccLevel, iGenSummary, iDelZeros);
		return tRetAccBalance;
	}
	
	/**
	 * Obtiene balanza contable segun parametros (Considera transacciones desde Fecha de Inicio en Adelante).
	 * @param iJulianFrom: Fecha Desde 
	 * @param iJulianTo: Fecha Hasta
	 * @param sPortfolio: Nombre Corto de Portafolio (Ej. SB1, SB2, etc).
	 * @param iAccLevel: Filtro de Nivel para Resultado.
	 * @param iGenSummary: 1, Genera Niveles Inferiores con Totales; 0, NO Genera Niveles Inferiores con Totales.
	 * @param iDelZeros: 1, Elimina saldos en cero; 0, NO elimina saldos en cero.
	 * @return Table con resultado.
	 * @throws OException
	 */
	public Table getAccBalanceBetweenDate(int iJulianFrom, int iJulianTo, int iPfolioId, int iAccLevel, int iGenSummary, int iDelZeros) throws OException{
		
		this.CUSTOM_WHERE = "AND je.acs_posting_date >= je.period_start";
		Table tRetAccBalance = this.getAccBalance(iJulianFrom, iJulianTo, iPfolioId, iAccLevel, iGenSummary, iDelZeros);
		this.CUSTOM_WHERE = "";
		return tRetAccBalance;
	}
	
	
	
	/**
	 * Obtiene balanza contable segun parametros (considera transacciones de toda la historia contable)
	 * @param iJulianFrom: Fecha Desde 
	 * @param iJulianTo: Fecha Hasta
	 * @param iPfolioId: ID Portafolio
	 * @param iAccLevel: Filtro de Nivel para Resultado.
	 * @param iGenSummary: 1, Genera Niveles Inferiores con Totales; 0, NO Genera Niveles Inferiores con Totales.
	 * @param iDelZeros: 1, Elimina saldos en cero; 0, NO elimina saldos en cero. 
	 * @return Table con resultado.
	 * @throws OException
	 */
	public Table getAccBalance(int iJulianFrom, int iJulianTo, int iPfolioId, int iAccLevel, int iGenSummary, int iDelZeros) throws OException{
		
		_Log.markStartScript();
		Table tAuxBalanza = Util.NULL_TABLE;
		Table tAuxLevels = Util.NULL_TABLE;
		Table tFinalBalanza = Table.tableNew("FINAL_BALANCE");
		
		//Formatea a DD/MM/YYYY
		String sFromDate = OCalendar.formatDateInt(iJulianFrom, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);
		String sToDate = OCalendar.formatDateInt(iJulianTo, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);	
		int iFinStmt = this.getFinStmtFromPortfolio(iPfolioId);
		
		_Log.printMsg(EnumTypeMessage.INFO, "Obteniendo balanza contable, desde: " + sFromDate + ", hasta: " + sToDate + ", portafolio: "+ Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, iPfolioId));
		
		String sSql = "WITH " + 
				"\n ctg_name AS (" + 
				"\n SELECT	cast(categ_id as INT) as category_id, category_name FROM user_third_categories_deuda" + 
				"\n UNION ALL" + 
				"\n SELECT cast(sector_id as int) , sector_name FROM credit_sector WHERE  sector_id NOT IN (SELECT categ_id FROM user_third_categories_deuda)" + 
				"\n )," + 

				"\n cta_tercer_nivel AS (" + 
				"\n  SELECT acs_rule_id, user_str_value, descripcion as glosa_tercer_nivel" + 
				"\n  FROM acs_rule_info_types_view it " + 
				"\n  JOIN acs_rule_info ri ON it.acs_info_type_id = ri.acs_info_type_id AND lower(it.acs_info_type_name) = 'cuenta tercer nivel'" + 
				"\n  JOIN user_mx_tercer_nivel_derivado ON user_str_value = cuentatercernivel " + 
				"\n )," + 
				
				"\n plz_class AS (" +
				"\n SELECT acs_rule_id, user_str_value " +
				"\n FROM acs_rule_info_types_view it " +
				"\n JOIN acs_rule_info ri ON (it.acs_info_type_id = ri.acs_info_type_id AND LOWER(it.acs_info_type_name) = 'clase de poliza')" +
				"\n )," +

 				"\n ssi AS" + 
				"\n (SELECT * FROM (" + 
				"\n    SELECT tre.tran_num, holder.party_id as holding_bank_id, holder.long_name as holder_long_name" + 
				"\n      , rank() over(partition by tran_num order by tre.event_num, rownum) rnk" + 
				"\n    FROM ab_tran_event tre" + 
				"\n    JOIN event_type et ON (et.id_number = tre.event_type)" + 
				"\n    JOIN ab_tran_event_settle estl ON (tre.event_num=estl.event_num)" + 
				"\n    JOIN account acct ON (estl.int_account_id = acct.account_id)" + 
				"\n    JOIN party holder ON (acct.holder_id = holder.party_id)" + 
				"\n    WHERE et.delivery_class > 0" + 
				"\n  ) WHERE rnk =1)," +

				"\n Cash_TranInfo_Ticker AS(" + 
				"\n SELECT tiv.tran_num, he.ins_num, tiv.value as ticker, tiv.type_name" + 
				"\n FROM ab_tran_info_view tiv" + 
				"\n LEFT JOIN header he ON(he.ticker = tiv.value)" + 
				"\n LEFT JOIN ab_tran ab ON(ab.ins_num = he.ins_num)" + 
				"\n WHERE tiv.type_id = " + EnumsTranInfoFields.MX_CASH_TICKER.toInt()  +
				"\n AND ab.tran_type = " + TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() +
				"\n AND ab.tran_status IN ("+ TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_MATURED.toInt() + ")" +  
				"\n ORDER BY tiv.tran_num" + 
				"\n )" + 
				
				//Grand Total
				"\n SELECT acs_account_id," +
				"\n acs_account_number || '-' || LPAD(third_level_number, 7, '0') || '-' || LPAD(fourth_level_number, 7, '0') as account_number, " + 
				"\n acs_account_number, " + 
				"\n third_level_number, " + 
				"\n fourth_level_number, " + 
				"\n third_level_name, " + 
				"\n fourth_level_name, " + 
				"\n CAST(SUM(ohd_starting_balance) as float) as starting_balance, " + 
				"\n CAST(-1.0 * SUM(CASE WHEN ohd_account_amount < 0.0 THEN ohd_account_amount ELSE 0.0 END) as float) as debit_amount," + 
				"\n CAST(SUM(CASE WHEN ohd_account_amount > 0.0 THEN ohd_account_amount ELSE 0.0 END) as float) as credit_amount," + 
				"\n CAST(SUM (ohd_final_balance) as float) as final_balance" + 
				
				//Netting Criteria
				"\n FROM ( " +
				"\n SELECT acs_posting_date, acs_account_id, acs_account_number, third_level_number, fourth_level_number, " +
				"\n third_level_name, fourth_level_name, poliza_key_rule, poliza_description_rule, netting_rule, poliza_key, poliza_description," + 
				"\n SUM(ohd_starting_balance) ohd_starting_balance, SUM(ohd_final_balance) ohd_final_balance, SUM(ohd_account_amount) ohd_account_amount" +
				
				//Transactions
				"\n FROM (" + 
				"\n SELECT" + 
				"\n     je.acs_posting_date" + 
				"\n   , je.acs_journal_entry_id" + 
				"\n   , je.deal_tracking_num " + 
				"\n   , je.internal_bunit" + 
				"\n   , je.internal_lentity" + 
				"\n   , je.external_bunit" + 
				"\n   , je.external_lentity" + 
				"\n   , je.toolset             entry_toolset" + 
				"\n   , je.ins_type            entry_ins_type" + 
				"\n   , je.ins_num             entry_ins_num" + 
				"\n   , je.acs_posting_currency" + 
				"\n   , je.acs_account_id" + 
				"\n   , je.acs_fin_stmt_id" + 
				"\n   , je.acs_account_name" + 
				"\n   , je.acs_account_number" + 
				"\n   , je.acs_account_type_id" + 
				
				//Datos de Poliza, se utiliza para el primer Netting
				"\n , NVL(plz_cfg.poliza_key_rule, 'DEFAULT') poliza_key_rule" +
				"\n , NVL(plz_cfg.poliza_description_rule, 'DEFAULT') poliza_description_rule" +
				
				"\n , CASE WHEN NVL(net_cfg.netting_rule, 'DEFAULT') IN ('DEAL_NUMBER') THEN je.deal_tracking_num ELSE 0 END as netting_rule " +
				 
				"\n , plz_class.user_str_value || '-' || TO_CHAR(je.acs_posting_date, 'YYYYMMDD') || '-' || LPAD(NVL(plz.poliza_categ_id, 0), 5, '0') || " +
				"\n   CASE WHEN NVL(plz_cfg.poliza_key_rule, 'DEFAULT') IN ('DEFAULT', 'DEAL_NUMBER') THEN '-' || tr.deal_tracking_num " +
				"\n 		WHEN NVL(plz_cfg.poliza_key_rule, 'DEFAULT') IN ('POLIZA_CATEGORY') THEN '-00000'" +
				"\n 		ELSE ''" +
				"\n	END  as poliza_key" +
	
				"\n , CASE WHEN NVL(plz_cfg.poliza_description_rule, 'DEFAULT') IN ('DEFAULT', 'POLIZA_CATEGORY') THEN plz.poliza_categ_name ELSE ''" +
				"\n   END as poliza_description" +
   
				// Obtiene Numero Cuenta de Tercer Nivel
				"\n   , CASE WHEN cfg.third_level_rule = 'INSTRUMENT_ID' THEN" + 
				"\n 			 NVL(CASE WHEN je.deal_tracking_num = 0 THEN  je.ins_num" + 
				"\n 			 ELSE CASE WHEN je.toolset <> " + TOOLSET_ENUM.CASH_TOOLSET.jvsValue()+ " THEN tr.ins_num ELSE cash_ticker.ins_num END" + 
				"\n 			 END, 0)" + 
				"\n 		WHEN  cfg.third_level_rule = 'CURRENCY' THEN je.acs_posting_currency" + 
				"\n 		WHEN  cfg.third_level_rule = 'EXTERNAL_BU' THEN je.external_bunit" + 
				"\n 		WHEN  cfg.third_level_rule = 'SOCIO_LIQ' THEN NVL(sl.id_contabilidad, 0)" + 
				"\n 		WHEN  cfg.third_level_rule = 'CTA_DERIVADOS' THEN NVL(CAST(cta_tercer_nivel.user_str_value as int), 0)" + 
				"\n 		WHEN  cfg.third_level_rule = 'HOLDING_BANK' THEN NVL(hbank.id_contabilidad, 0)" + 
				"\n 		ELSE 0" + 
				"\n 	END as third_level_number" + 

				//Obtiene Nombre Cuenta Tercer Nivel
				"\n    , CASE WHEN cfg.third_level_rule = 'INSTRUMENT_ID' THEN" + 
				"\n 			 NVL(CASE WHEN je.deal_tracking_num = 0 THEN  " + 
				"\n     			SUBSTR(jd.user_general_string, INSTR(jd.user_general_string, '+')+1, LENGTH(jd.user_general_string)-INSTR(jd.user_general_string, '+')+1)" +
				"\n 			 ELSE CASE WHEN je.toolset <> " + TOOLSET_ENUM.CASH_TOOLSET.jvsValue()+ " THEN hdr.ticker ELSE cash_ticker.ticker END" + 
				"\n 			 END, ' ') " + 
				"\n 		WHEN  cfg.third_level_rule = 'CURRENCY' THEN DECODE(ccy.name, 'CAD', 'Dolar Canadiense (CAD)', ccy.name)" + 
				"\n 		WHEN  cfg.third_level_rule = 'EXTERNAL_BU' THEN pty.short_name" + 
				"\n 		WHEN  cfg.third_level_rule = 'SOCIO_LIQ' THEN NVL(sl.nombre_socio_liq, ' ')" + 
				"\n 		WHEN  cfg.third_level_rule = 'CTA_DERIVADOS' THEN NVL(cta_tercer_nivel.glosa_tercer_nivel, ' ')" + 
				"\n 		WHEN  cfg.third_level_rule = 'HOLDING_BANK' THEN NVL(hbank.nombre_holding_bank, ' ')" + 
				"\n 		ELSE ' '" + 
				"\n 	END as third_level_name" + 

				//Obtiene Numero Cuenta de CUARTO Nivel
				"\n   , CASE WHEN cfg.fourth_level_rule = 'INSTRUMENT_ID' THEN" + 
				"\n 			 NVL(CASE WHEN je.deal_tracking_num = 0 THEN  je.ins_num" + 
				"\n 			 ELSE CASE WHEN je.toolset <> " + TOOLSET_ENUM.CASH_TOOLSET.jvsValue()+ " THEN tr.ins_num ELSE cash_ticker.ins_num END" + 
				"\n 			 END , 0)" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'CURRENCY' THEN je.acs_posting_currency" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'EXTERNAL_BU' THEN je.external_bunit" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'SOCIO_LIQ' THEN NVL(sl.id_contabilidad, 0)" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'CTA_DERIVADOS' THEN NVL(CAST(cta_tercer_nivel.user_str_value as int), 0)" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'HOLDING_BANK' THEN NVL(hbank.id_contabilidad, 0)" + 
				"\n 		ELSE 0" + 
				"\n 	END as fourth_level_number" + 

				// Obtiene Nombre Cuenta CUARTO Nivel 
				"\n    , CASE WHEN cfg.fourth_level_rule = 'INSTRUMENT_ID' THEN" + 
				"\n 			 NVL(CASE WHEN je.deal_tracking_num = 0 THEN " + 
				"\n     			SUBSTR(jd.user_general_string, INSTR(jd.user_general_string, '+')+1, LENGTH(jd.user_general_string)-INSTR(jd.user_general_string, '+')+1)" +	
				"\n 			 ELSE CASE WHEN je.toolset <> " + TOOLSET_ENUM.CASH_TOOLSET.jvsValue()+ " THEN hdr.ticker ELSE cash_ticker.ticker END" + 
				"\n 			 END, ' ') " + 
				"\n 		WHEN  cfg.fourth_level_rule = 'CURRENCY' THEN DECODE(ccy.name, 'CAD', 'Dolar Canadiense (CAD)', ccy.name)" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'EXTERNAL_BU' THEN pty.short_name" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'SOCIO_LIQ' THEN NVL(sl.nombre_socio_liq, ' ')" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'CTA_DERIVADOS' THEN NVL(cta_tercer_nivel.glosa_tercer_nivel, ' ')" + 
				"\n 		WHEN  cfg.fourth_level_rule = 'HOLDING_BANK' THEN NVL(hbank.nombre_holding_bank, ' ')" + 
				"\n 		ELSE ' '" + 
				"\n 	END as fourth_level_name" + 

				"\n   , ROUND(je.acs_account_amount * (CASE WHEN je.acs_posting_date < period_start THEN 1 ELSE 0 END ),2) ohd_starting_balance" + 
				"\n   , ROUND(je.acs_account_amount,2) ohd_final_balance" + 
				"\n   , ROUND(je.acs_account_amount * (CASE WHEN je.acs_posting_date >= period_start THEN -1 ELSE 0 END ),2) ohd_account_amount" + 

				"\n FROM " + 
				"\n   (SELECT ajedv.*, " + 
				"\n 	trunc(TO_DATE('" +sFromDate + "', 'DD/MM/YYYY')) period_start, " + 
				"\n 	trunc(TO_DATE('" +sToDate + "', 'DD/MM/YYYY')) period_end " + 
				"\n 	FROM acs_journal_entries_dual_view ajedv" + 
				"\n 	WHERE ajedv.acs_fin_stmt_id = " + iFinStmt +
				"\n 	) je" + 
				"\n   LEFT JOIN acs_journal_userdata jd  ON (je.acs_journal_entry_id = jd.acs_journal_entry_id)" + 
				"\n   LEFT JOIN ab_tran tr  ON (tr.deal_tracking_num = je.deal_tracking_num AND tr.current_flag=1)" + 
				"\n   LEFT JOIN header hdr ON (tr.ins_num = hdr.ins_num)" + 
				"\n   LEFT JOIN cta_tercer_nivel ON (je.acs_rule_id = cta_tercer_nivel.acs_rule_id)" + 
				"\n   LEFT JOIN user_acct_journal_base_cfg cfg ON cfg.account_number = je.acs_account_number" + 
				"\n   LEFT JOIN user_mx_socios_liquidadores sl ON (sl.id_findur = je.external_bunit) " + 
				"\n   LEFT JOIN Cash_TranInfo_Ticker cash_ticker ON (cash_ticker.tran_num = je.tran_num)" + 
				"\n   LEFT JOIN currency ccy ON(ccy.id_number = je.acs_posting_currency)" + 
				"\n   LEFT JOIN party pty ON(pty.party_id = je.external_bunit)" + 
				"\n   LEFT JOIN ssi ON (ssi.tran_num = tr.tran_num)" +
				"\n   LEFT JOIN user_mx_bancos_corresponsales hbank ON (hbank.id_findur = ssi.holding_bank_id)" +			
				"\n   LEFT JOIN user_acct_poliza_categ plz ON (plz.poliza_categ_id = jd.user_document_num)" +
				"\n   LEFT JOIN user_acct_poliza_rule_cfg plz_cfg ON (plz_cfg.poliza_categ_id = plz.poliza_categ_id)" +
				"\n   LEFT JOIN user_acct_netting_rule_cfg net_cfg ON (net_cfg.poliza_categ_id = plz.poliza_categ_id) " +
				"\n   LEFT JOIN plz_class ON (je.acs_rule_id = plz_class.acs_rule_id)" +
				
				//Transactions
				"\n WHERE" + 
				"\n   je.acs_posting_date <= je.period_end " + 
						this.CUSTOM_WHERE +
				"\n ) balanza " + 
				
				//Netting Criteria
				"\n GROUP BY acs_posting_date, acs_account_id, acs_account_number, third_level_number, fourth_level_number, third_level_name, fourth_level_name," +
				"\n poliza_key_rule, poliza_description_rule, netting_rule, poliza_key, poliza_description" +
				"\n )neteo" + 
				
				//Grand TOTAL
				"\n GROUP BY acs_account_number, third_level_number, fourth_level_number, third_level_name, fourth_level_name, acs_account_id" +
				"\n ORDER BY acs_account_number, third_level_number, fourth_level_number, acs_account_id";
		
		tAuxBalanza = _Afore.getTable(sSql, "Balanza_Contable");
		
		tAuxLevels = getAccLowerLevels();
		
		tAuxBalanza.select(tAuxLevels, "first_level_name(1_level_name), second_level_name(2_level_name), flag_join", "acs_account_id EQ $acs_account_id");
		tAuxBalanza.setColName("third_level_name", "3_level_name");
		tAuxBalanza.setColName("fourth_level_name", "4_level_name");
		
		//Elimina Cuentas No Validas
		tAuxBalanza.deleteWhereValue("flag_join", 0);
		
		//Separa niveles de cuentas, obtiene numeros y nombres por cada nivel
		tAuxBalanza.addCol("1_level_number", COL_TYPE_ENUM.COL_INT);
		tAuxBalanza.addCol("2_level_number", COL_TYPE_ENUM.COL_INT);
		tAuxBalanza.addCol("3_level_number", COL_TYPE_ENUM.COL_INT);
		tAuxBalanza.addCol("4_level_number", COL_TYPE_ENUM.COL_INT);
		tAuxBalanza.addCol("account_level", COL_TYPE_ENUM.COL_INT);
		tAuxBalanza.addCol("account_name", COL_TYPE_ENUM.COL_STRING);
		
		for(int i=1; i<= tAuxBalanza.getNumRows(); i++){
			String sAccountFull = tAuxBalanza.getString("account_number", i);
			String[] sArrayAccount = null;
			sArrayAccount = sAccountFull.split("-");
			
			tAuxBalanza.setInt("1_level_number", i, Str.strToInt(sArrayAccount[0]));
			tAuxBalanza.setInt("2_level_number", i, Str.strToInt(sArrayAccount[1]));
			tAuxBalanza.setInt("3_level_number", i, Str.strToInt(sArrayAccount[2]));
			tAuxBalanza.setInt("4_level_number", i, Str.strToInt(sArrayAccount[3]));	
			
			int level = 1;		
			if (!sArrayAccount[3].equals("0000000")) level = 4;
			else if (!sArrayAccount[2].equals("0000000")) level = 3;
			else if (!sArrayAccount[1].equals("0000")) level = 2;

			tAuxBalanza.setInt("account_level", i, level);	
			tAuxBalanza.setString("account_name", i, tAuxBalanza.getString(level + "_level_name", i));

		}
		
		//Totaliza cuentas por niveles

		Table tAux1Level = Table.tableNew("Level_Total");
		tAux1Level.addCol("acs_account_id", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("account_number", COL_TYPE_ENUM.COL_STRING);
		tAux1Level.addCol("1_level_number", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("2_level_number", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("3_level_number", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("4_level_number", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("account_level", COL_TYPE_ENUM.COL_INT);
		tAux1Level.addCol("flag_summary", COL_TYPE_ENUM.COL_INT);
		
		Table tAux2Level = tAux1Level.cloneTable();
		Table tAux3Level = tAux1Level.cloneTable();
		
//		tAuxBalanza.viewTable();
		//Primer Nivel
		tAux1Level.select(tAuxBalanza, "DISTINCT, 1_level_number, 1_level_name(account_name)", "acs_account_id GT 0");
		tAux1Level.select(tAuxBalanza, "SUM, starting_balance, debit_amount, credit_amount, final_balance", "1_level_number EQ $1_level_number");
		tAux1Level.setColValInt("account_level", 1);
		tAux1Level.setColValInt("flag_summary", 1);
//		tAux1Level.viewTable();
		
		//Segundo Nivel
		tAux2Level.select(tAuxBalanza, "DISTINCT, 1_level_number, 2_level_number, 2_level_name(account_name)", "acs_account_id GT 0 AND 3_level_number GT 0");
		tAux2Level.select(tAuxBalanza, "SUM, starting_balance, debit_amount, credit_amount, final_balance", "1_level_number EQ $1_level_number AND 2_level_number EQ $2_level_number");
		tAux2Level.setColValInt("account_level", 2);
		tAux2Level.setColValInt("flag_summary", 1);
//		tAux2Level.viewTable();
		
		//Tercer Nivel
		tAux3Level.select(tAuxBalanza, "DISTINCT, 1_level_number, 2_level_number, 3_level_number, 3_level_name(account_name)", "acs_account_id GT 0 AND 4_level_number GT 0");
		tAux3Level.select(tAuxBalanza, "SUM, starting_balance, debit_amount, credit_amount, final_balance", "1_level_number EQ $1_level_number AND 2_level_number EQ $2_level_number AND 3_level_number EQ $3_level_number");
		tAux3Level.deleteWhereValue("3_level_number", 0);
		tAux3Level.setColValInt("account_level", 3);
		tAux3Level.setColValInt("flag_summary", 1);

//		tAux3Level.viewTable();
		
		//NIVELES
		if(iGenSummary == 1){
			//Reemplaza los niveles calculados por los nuevos calculados
			tAuxBalanza.select(tAux1Level, "flag_summary", "1_level_number EQ $1_level_number AND 2_level_number EQ $2_level_number AND 3_level_number EQ $3_level_number AND 4_level_number EQ $4_level_number");
			tAuxBalanza.select(tAux2Level, "flag_summary", "1_level_number EQ $1_level_number AND 2_level_number EQ $2_level_number AND 3_level_number EQ $3_level_number AND 4_level_number EQ $4_level_number");
			tAuxBalanza.select(tAux3Level, "flag_summary", "1_level_number EQ $1_level_number AND 2_level_number EQ $2_level_number AND 3_level_number EQ $3_level_number AND 4_level_number EQ $4_level_number");
			tAuxBalanza.deleteWhereValue("flag_summary", 1);
			
			//Consolida resultados de todos los niveles
			tFinalBalanza.select(tAux1Level, "acs_account_id, account_level, account_number, account_name, starting_balance, debit_amount, credit_amount, final_balance, 1_level_number, 2_level_number, 3_level_number, 4_level_number", "account_level GT 0");
			tFinalBalanza.select(tAux2Level, "acs_account_id, account_level, account_number, account_name, starting_balance, debit_amount, credit_amount, final_balance, 1_level_number, 2_level_number, 3_level_number, 4_level_number", "account_level GT 0");
			tFinalBalanza.select(tAux3Level, "acs_account_id, account_level, account_number, account_name, starting_balance, debit_amount, credit_amount, final_balance, 1_level_number, 2_level_number, 3_level_number, 4_level_number", "account_level GT 0");
		}
		//TRANSACCIONES 
		tFinalBalanza.select(tAuxBalanza, "acs_account_id, account_level, account_number, account_name, starting_balance, debit_amount, credit_amount, final_balance, 1_level_number, 2_level_number, 3_level_number, 4_level_number", "acs_account_id GT 0");

		//Formatea Numero de Cuenta de Acuerdo al Nivel
		for(int i=1; i<=tFinalBalanza.getNumRows(); i++){
			int iAuxAccLevel=tFinalBalanza.getInt("account_level", i);
			String sAuxAcc ="";
			String sAuxAcc1 =_Afore.formatAsIntForReport("" +tFinalBalanza.getInt("1_level_number", i), 4, 1);
			String sAuxAcc2 =sAuxAcc1 + "-" +_Afore.formatAsIntForReport("" +tFinalBalanza.getInt("2_level_number", i), 4, 1);
			String sAuxAcc3 =sAuxAcc2 + "-" +_Afore.formatAsIntForReport("" +tFinalBalanza.getInt("3_level_number", i), 7, 1);
			String sAuxAcc4 =sAuxAcc3 + "-" +_Afore.formatAsIntForReport("" +tFinalBalanza.getInt("4_level_number", i), 7, 1);
			
			switch (iAuxAccLevel){
			case 1: sAuxAcc = sAuxAcc1;
					break;
			case 2: sAuxAcc = sAuxAcc2;
					break;
			case 3: sAuxAcc = sAuxAcc3;
					break;
			case 4: sAuxAcc = sAuxAcc4;
					break;
			default: break;
			}
			tFinalBalanza.setString("account_number", i, sAuxAcc);
		}		
		//Elimina Duplicados
		tFinalBalanza.group("account_number");
		tFinalBalanza.distinctRows();
		
		//Elimina Saldos en Cero (Solo si parametro esta en 1)
		if (iDelZeros == 1){
			String sFrmCeros = "iif("
					+ "(COL('starting_balance')>-0.01 && COL('starting_balance')<0.01)&&"
					+ "(COL('debit_amount')>-0.01 && COL('debit_amount')<0.01)&&"
					+ "(COL('credit_amount')>-0.01 && COL('credit_amount')<0.01)&&"
					+ "(COL('final_balance')>-0.01 && COL('final_balance')<0.01)"
					+ ", 1, 0)";

			tFinalBalanza.addFormulaColumn(sFrmCeros, COL_TYPE_ENUM.COL_INT.toInt(), "frmSaldosCero");
			tFinalBalanza.deleteWhereValue("frmSaldosCero", 1);
			tFinalBalanza.deleteFormulaColumn("frmSaldosCero");
		}

		//Selecciona resultados de acuerdo al parametro NIVEL
		tFinalBalanza.addFormulaColumn("iif(COL('account_level')>"+iAccLevel+",1,0)", COL_TYPE_ENUM.COL_INT.toInt(), "frmFiltroNivel"); 
		tFinalBalanza.deleteWhereValue("frmFiltroNivel", 1);
		tFinalBalanza.deleteFormulaColumn("frmFiltroNivel");
		
		//Ordena Registros
		tFinalBalanza.addGroupBy("account_number");
		tFinalBalanza.addGroupBy("account_level");
		tFinalBalanza.groupBy();
		
		//libera memoria
		if(Table.isTableValid(tAuxBalanza) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAuxBalanza.destroy();
		if(Table.isTableValid(tAuxLevels) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAuxLevels.destroy();
		if(Table.isTableValid(tAux1Level) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAux1Level.destroy();
		if(Table.isTableValid(tAux2Level) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAux2Level.destroy();
		if(Table.isTableValid(tAux3Level) == OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()) tAux3Level.destroy();
		
		
		//TODO Formatea Resultado a 2 Decimales Previo a la Generacion de Niveles. (starting_balance, debit_amount, credit_amount, final_balance)
		tFinalBalanza.addFormulaColumn("round(COL('starting_balance'),2)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frmStart");
		tFinalBalanza.addFormulaColumn("round(COL('debit_amount'),2)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frmDebit");
		tFinalBalanza.addFormulaColumn("round(COL('credit_amount'),2)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frmCredit");
		tFinalBalanza.addFormulaColumn("round(COL('final_balance'),2)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frmFinal");
		
		tFinalBalanza.copyCol("frmStart", tFinalBalanza, "starting_balance");
		tFinalBalanza.copyCol("frmDebit", tFinalBalanza, "debit_amount");
		tFinalBalanza.copyCol("frmCredit", tFinalBalanza, "credit_amount");
		tFinalBalanza.copyCol("frmFinal", tFinalBalanza, "final_balance");
		
		tFinalBalanza.mathRoundCol("starting_balance", 2);
		tFinalBalanza.mathRoundCol("debit_amount", 2);
		tFinalBalanza.mathRoundCol("credit_amount", 2);
		tFinalBalanza.mathRoundCol("final_balance", 2);
		
		_Log.markEndScript();
		return tFinalBalanza;
	
	}
	/**
	 * Obtiene todas las cuentas de primer y segundo nivel con sus respectivos nombres.
	 * @return 
	 * @throws OException
	 */
	public Table getAccLowerLevels() throws OException{
		
		String sSqlFirstSecondLevels = 
				//First Level Name
				"WITH first_level AS (" + 
				"\n SELECT acs_fin_stmt_id, acs_account_id, acs_account_number, acs_account_name" + 
				"\n FROM acs_account acc" + 
				"\n LEFT JOIN acs_account_type acc_type ON(acc.acs_account_type_id = acc_type.acs_account_type_id)" + 
				"\n WHERE SUBSTR(acs_account_number,  6, 4) = '0000'" + 
				"\n AND acc_type.acs_account_type_name IN ('Standard', 'Summary')" + 
				"\n )," + 
				//Second Level Name 
				"\n second_level AS (" + 
				"\n SELECT acs_fin_stmt_id, acs_account_id, acs_account_number, acs_account_name, acs_summary_account_id" + 
				"\n FROM acs_account acc" + 
				"\n LEFT JOIN acs_account_type acc_type ON(acc.acs_account_type_id = acc_type.acs_account_type_id)" + 
				"\n WHERE SUBSTR(acs_account_number,  6, 4) <> '0000'" + 
				"\n AND acc_type.acs_account_type_name IN ('Standard', 'Summary')" + 
				"\n )" + 
				//Select Names
				"\n SELECT " + 
				"\n  (CASE WHEN second_level.acs_account_id  <> 0 THEN second_level.acs_account_id ELSE first_level.acs_account_id END) AS acs_account_id" + 
				"\n ,(CASE WHEN second_level.acs_account_id  <> 0 THEN second_level.acs_account_number ELSE first_level.acs_account_number END) AS acs_account_number" +
				"\n , first_level.acs_fin_stmt_id" + 
				"\n , first_level.acs_account_name AS first_level_name" + 
				"\n , second_level.acs_account_name AS second_level_name" + 
				"\n , 1 as flag_join" + 
				"\n FROM first_level" + 
				"\n LEFT JOIN second_level ON (first_level.acs_account_id = second_level.acs_summary_account_id)";

		
		return  _Afore.getTable(sSqlFirstSecondLevels, "Niveles_Superiores");

	}
	
	
	/**
	 * Obtiene financial Statement from Party Id
	 * @param iPfolioId: Id Portafolio
	 * @return iFinStmt: Id Financial Statement
	 * @throws OException
	 */
	public int getFinStmtFromPortfolio(int iPfolioId) throws OException{
		Table tFinStmt = Util.NULL_TABLE;
		
		//Obtiene lista de Financial Statements / Portafolios 
		String sSqlFinStmt = "SELECT fin_stmt.acs_fin_stmt_id, fin_stmt.acs_fin_stmt_name,  name as portfolio" +
				"\n FROM acs_financial_stmt fin_stmt " +
				"\n JOIN user_siefore_cfg user_siefore ON (user_siefore.acs_fin_stmt_id = fin_stmt.acs_fin_stmt_id )";

		tFinStmt = _Afore.getTable(sSqlFinStmt);
		
		int iRowFinSt = tFinStmt.unsortedFindString("portfolio", Ref.getName(SHM_USR_TABLES_ENUM.PORTFOLIO_TABLE, iPfolioId), SEARCH_CASE_ENUM.CASE_INSENSITIVE);
		int iFinStmt = tFinStmt.getInt("acs_fin_stmt_id", iRowFinSt);
		
		tFinStmt.destroy();
		return iFinStmt;
	}
	
	/**
	 * Obtiene el signo de la cuenta en funcion de su naturaleza (A: Acreedor -, D:Deudor +)
	 * @return
	 * @throws OException
	 */
	
	public Table getAccountSign() throws OException{
		
		String sSql = "SELECT num_cta as account_number, naturaleza, CAST(CASE WHEN  naturaleza = 'A' THEN -1.0 ELSE 1.0 END as float) AS signo " +
				"\n FROM " + EnumsUserTables.USER_MX_CATALOGO_CUENTAS.toString();		
		
		Table tAccSign = _Afore.getTable(sSql); 
		
		//Formatea las cuentas a 4 Posiciones
		for(int i=1; i<= tAccSign.getNumRows(); i++){
			String sAccountFull = tAccSign.getString("account_number", i);
			String[] sArrayAccount = null;
			sArrayAccount = sAccountFull.split("-");
			
			String sAuxAcc ="";			
			if(sArrayAccount.length>1){
			String sAuxAcc1 =_Afore.formatAsIntForReport("" +Str.strToInt(sArrayAccount[0]), 4, 1);
			String sAuxAcc2 =_Afore.formatAsIntForReport("" +Str.strToInt(sArrayAccount[1]), 4, 1);
			sAuxAcc = sAuxAcc1 + "-" + sAuxAcc2;
			}
			
			tAccSign.setString("account_number", i, sAuxAcc);
			
			//La naturaleza de 5301 (para 1101) debe ser Deudor
			if(Str.strToInt(sArrayAccount[0]) == 5301){
				tAccSign.setString("naturaleza", i, "D");
				tAccSign.setDouble("signo", i, 1.0);
			}
		}
		
			return tAccSign;
	
	}
}
