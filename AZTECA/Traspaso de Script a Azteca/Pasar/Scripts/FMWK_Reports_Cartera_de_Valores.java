/*$Header: v 1.0, 21/02/2018 $*/
/***********************************************************************************
 * File Name:              FMWK_Reports_Cartera_de_Valores.java
 * 
 * Author:                 Jorge Vergara Vera- VMetrix SpA
 * Creation Date:          Febrero 2018
 * Version:                1.0
 * Description:            Genera reporte de cartera de valores de forma Batch, con salidas XLS/PDF
 *         
 ************************************************************************************/
package com.afore.custom_reports;

import com.afore.enums.EnumTypeMessage;
import com.afore.enums.EnumsCuentasNav;
import com.afore.enums.EnumsCurrency;
import com.afore.enums.EnumsIndex;
import com.afore.enums.EnumsInstrumentsMX;
import com.afore.enums.EnumsUserTables;
import com.afore.log.UTIL_Log;
import com.afore.util.UTIL_Afore;
import com.olf.openjvs.Crystal;
import com.olf.openjvs.IContainerContext;
import com.olf.openjvs.IScript;
import com.olf.openjvs.Index;
import com.olf.openjvs.OCalendar;
import com.olf.openjvs.OException;
import com.olf.openjvs.Ref;
import com.olf.openjvs.Table;
import com.olf.openjvs.Util;
import com.olf.openjvs.enums.ASSET_TYPE_ENUM;
import com.olf.openjvs.enums.COL_TYPE_ENUM;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_OPTIONS;
import com.olf.openjvs.enums.CRYSTAL_EXPORT_TYPES;
import com.olf.openjvs.enums.DATE_FORMAT;
import com.olf.openjvs.enums.OLF_RETURN_CODE;
import com.olf.openjvs.enums.SEARCH_CASE_ENUM;
import com.olf.openjvs.enums.SHM_USR_TABLES_ENUM;
import com.olf.openjvs.enums.TOOLSET_ENUM;
import com.olf.openjvs.enums.TRAN_STATUS_ENUM;
import com.olf.openjvs.enums.TRAN_TYPE_ENUM;


public class FMWK_Reports_Cartera_de_Valores implements IScript {

	protected int INT_TODAY = 0;
	protected String STR_TODAY = "";
	protected int INT_PORTFOLIO=0;
	protected String STR_PORTFOLIO = "";
	protected Table tCurveSpot = Util.NULL_TABLE;

	private final String DOC_SCRIPT_NAME = this.getClass().getSimpleName();
	private UTIL_Log LOG = new UTIL_Log(DOC_SCRIPT_NAME); 	
	protected UTIL_Afore _Afore = new UTIL_Afore();

	@Override
	public void execute(IContainerContext context) throws OException {
		// TODO Auto-generated method stub
		LOG.markStartScript();
		this.INT_TODAY = OCalendar.today();	
		this.STR_TODAY = OCalendar.formatDateInt(this.INT_TODAY, DATE_FORMAT.DATE_FORMAT_MDY_SLASH);

		String sSqlPFolio = "SELECT pfolio.id_number, pfolio.name, pty.long_name, padd.irs_terminal_num as rfc" +
				"\n FROM portfolio pfolio" +
				"\n LEFT JOIN party pty ON (pfolio.id_number = pty.default_portfolio_id)" +
				"\n LEFT JOIN party_address padd ON (padd.party_id = pty.party_id)" +
				"\n WHERE pfolio.restricted = 1" +
				"\n AND short_name LIKE 'PROF-%BU'" +
				"\n ORDER BY pfolio.id_number";
		

		Table tPFolios = _Afore.getTable(sSqlPFolio, "Portfolios");
		
		//Tipo Cambio
		Double dUSD =0.0;
		Double dEUR =0.0;
		Double dUDI =0.0;
		Double dJPY =0.0;
		int iAuxRow = 0;
		
		//TODO Tipo Cambio
		LOG.printMsg(EnumTypeMessage.INFO, "Obteniendo Tipo de Cambio para el Dia: " + this.STR_TODAY);
		if(this.getTipoCambio()){
			iAuxRow = tCurveSpot.unsortedFindString("name", EnumsCurrency.MX_CURRENCY_USD.toString(), SEARCH_CASE_ENUM.CASE_INSENSITIVE);
			if(iAuxRow > 0) dUSD = tCurveSpot.getDouble("tipo_cambio", iAuxRow);
			
			iAuxRow = tCurveSpot.unsortedFindString("name", EnumsCurrency.MX_CURRENCY_UDI.toString(), SEARCH_CASE_ENUM.CASE_INSENSITIVE);
			if(iAuxRow > 0) dUDI= tCurveSpot.getDouble("tipo_cambio", iAuxRow);
			
			iAuxRow = tCurveSpot.unsortedFindString("name", EnumsCurrency.MX_CURRENCY_EUR.toString(), SEARCH_CASE_ENUM.CASE_INSENSITIVE);
			if(iAuxRow > 0) dEUR = tCurveSpot.getDouble("tipo_cambio", iAuxRow);
			
			iAuxRow = tCurveSpot.unsortedFindString("name", EnumsCurrency.MX_CURRENCY_JPY.toString(), SEARCH_CASE_ENUM.CASE_INSENSITIVE);
			if(iAuxRow > 0) dJPY = tCurveSpot.getDouble("tipo_cambio", iAuxRow);
		}
		
		//TODO Parametros del Reporte
		Table tParam = Table.tableNew("Crystal_Parm");
		tParam.addCol("parGeneradoPor", COL_TYPE_ENUM.COL_STRING);
		tParam.addCol("parFecha", COL_TYPE_ENUM.COL_STRING);
		tParam.addCol("parSiefore", COL_TYPE_ENUM.COL_STRING);
		tParam.addCol("parSieforeLN", COL_TYPE_ENUM.COL_STRING);
		tParam.addCol("parRFC", COL_TYPE_ENUM.COL_STRING);
		tParam.addCol("parCapitalContable", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parActivoTotal", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parEfectivoValores", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parUSD", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parUDI", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parEUR", COL_TYPE_ENUM.COL_DOUBLE);
		tParam.addCol("parJPY", COL_TYPE_ENUM.COL_DOUBLE);
		
		tParam.addRow();
		tParam.setString("parGeneradoPor", 1, Ref.getUserName());
		tParam.setString("parFecha", 1, this.STR_TODAY);
		tParam.setDouble("parUSD", 1, dUSD);
		tParam.setDouble("parUDI", 1, dUDI);
		tParam.setDouble("parEUR", 1, dEUR);
		tParam.setDouble("parJPY", 1, dJPY);

		//TODO Ejecuta reporte por cada portafolio
		for (int i=1; i<=tPFolios.getNumRows(); i++){
			this.INT_PORTFOLIO = tPFolios.getInt("id_number", i);
			this.STR_PORTFOLIO = tPFolios.getString("name", i);
			

			Table tReportTable = Util.NULL_TABLE;
			Table tAuxParam = tParam.cloneTable();
			tParam.copyRowAddAll(tAuxParam);
			
			tAuxParam.setString("parSiefore", 1, this.STR_PORTFOLIO);
			tAuxParam.setString("parSieforeLN", 1, tPFolios.getString("long_name", i));
			tAuxParam.setString("parRFC", 1, tPFolios.getString("rfc", i));	
			
			
			//TODO Valores obtenidos de la tabla de NAV.
			Table tAuxNAV = this.getNavValues();
			Double dCapitalContable=0.0, dActivoTotal=0.0, dEfectivoValores=0.0;
			int iAuxRowNav=0;
			
			if(Table.isTableValid(tAuxNAV)==1 && tAuxNAV.getNumRows()> 0){
				//Activo Neto (NAV)
				iAuxRowNav = tAuxNAV.unsortedFindInt("nav_id_concepto", EnumsCuentasNav.MX_ACTIVO_NETO.toInt());
				if (iAuxRowNav > 0)dCapitalContable = tAuxNAV.getDouble("nav_value", iAuxRowNav);
				tAuxParam.setDouble("parCapitalContable", 1, dCapitalContable);

				//Activo Total
				iAuxRowNav = tAuxNAV.unsortedFindInt("nav_id_concepto", EnumsCuentasNav.MX_ACTIVO_TOTAL.toInt());
				if (iAuxRowNav > 0)dActivoTotal = tAuxNAV.getDouble("nav_value", iAuxRowNav);
				tAuxParam.setDouble("parActivoTotal", 1, dActivoTotal);

				//Efectivo Valores
				iAuxRowNav = tAuxNAV.unsortedFindInt("nav_id_concepto", EnumsCuentasNav.MX_ACTIVO_BCOS_MONEDAS_EXT.toInt());
				if (iAuxRowNav > 0)dEfectivoValores += tAuxNAV.getDouble("nav_value", iAuxRowNav);
				iAuxRowNav = tAuxNAV.unsortedFindInt("nav_id_concepto", EnumsCuentasNav.MX_ACTIVO_CAJA_Y_BANCOS.toInt());
				if (iAuxRowNav > 0)dEfectivoValores += tAuxNAV.getDouble("nav_value", iAuxRowNav);
				iAuxRowNav = tAuxNAV.unsortedFindInt("nav_id_concepto", EnumsCuentasNav.MX_ACTIVO_DEUDA_Y_CAPITALES.toInt());
				if (iAuxRowNav > 0)dEfectivoValores += tAuxNAV.getDouble("nav_value", iAuxRowNav);
				tAuxParam.setDouble("parEfectivoValores", 1, dEfectivoValores);
			}else
				LOG.printMsg(EnumTypeMessage.WARNING, "No se encontraron registros para NAV, Portfolio: " + this.STR_PORTFOLIO);
			
			LOG.printMsg(EnumTypeMessage.INFO, "Ejecutando Reporte Cartera de Valores para Portfolio: " + this.STR_PORTFOLIO);
			tReportTable = this.getReport();
//			if(this.INT_PORTFOLIO == 20010)tReportTable.viewTable();
			
			if(!this.genCrystal(tReportTable, tAuxParam, ".xls"))continue;
			if(!this.genCrystal(tReportTable, tAuxParam, ".pdf"))continue;
			
			tReportTable.destroy();
			tAuxParam.destroy();
			tAuxNAV.destroy();
			
		}
		
		tPFolios.destroy();
		tCurveSpot.destroy();
		tParam.destroy();
		LOG.markEndScript();
	}
	public Table getReport() throws OException {    	
		Table tReportTable;
		String sSql = "SELECT " + 
				"\n he.ticker," + 
				"\n NVL(acc_num.account_number, ' ') as cuenta_mayor," + 
				"\n UPPER(tset.name) as tipo_instrumento," + 
				"\n CAST(ab.position as float) as nro_titulos," + 
				"\n CAST(NVL(cpp_diario.costo_promedio_ponderado, 0.00) as float) as costo_cpp," + 
				"\n CAST(0.0 as float) as costo_total_inversion," + //Calcular OK
				"\n CAST(NVL(vector_diario.precio_limpio_24h, 0.0) as float) as precio_limpio_24h," + 
				"\n CAST(0.0 as float) as valor_mercado_total," + //Calcular OK
				"\n CAST(0.0 as float) as plusvalia," + //Calcular OK
				"\n CAST(0.0 as float) as porc_act_neto," + //Calcular OK
				"\n CAST((100 * NVL(prf.rate, 0.0)) as float) as tasa," + 
				"\n (ab_hold.maturity_date - TO_DATE('"+this.STR_TODAY+"', 'DD/MM/YYYY')) as dias_vencer_ins," + 
				"\n CAST(0.0 as float) as interes," + //Calcular OK
				//FIN DATOS REPORTE
				"\n CAST(nav.nav_value as float) as nav_diario," +
				"\n ins_grp.ins_group_name,"+
				"\n CAST(vector_diario.factor_interes_24h as float) as factor_interes_24h" + 
//				"\n ab.ins_num," + 
//				"\n ab.deal_tracking_num," + 
//				"\n ab.ins_type" +
				"\n FROM ab_tran ab" + 
				"\n LEFT JOIN ab_tran ab_hold ON (ab_hold.ins_num = ab.ins_num AND ab_hold.tran_type = "+TRAN_TYPE_ENUM.TRAN_TYPE_HOLDING.toInt() +")" + 
				"\n LEFT JOIN header he ON (he.ins_num = ab.ins_num)" + 
				"\n LEFT JOIN toolsets tset ON (tset.id_number = ab.toolset)" + 
				"\n LEFT JOIN parameter par ON (par.ins_num = ab.ins_num)" + 
				"\n LEFT JOIN " + EnumsUserTables.USER_MX_VECTOR_PRECIOS_DIARIO + " vector_diario ON (vector_diario.ticker = he.ticker)" +  

				"\n LEFT JOIN profile prf ON (prf.ins_num = ab.ins_num " +
											" AND prf.param_seq_num = par.param_seq_num" +
											" AND prf.start_date <= TO_DATE('"+this.STR_TODAY+"', 'DD/MM/YYYY')" + 
											" AND prf.end_date > TO_DATE('"+this.STR_TODAY+"', 'DD/MM/YYYY')" + 
											")" +
				"\n LEFT JOIN " + EnumsUserTables.USER_MX_CPP_EQUITY_BOND_DIARIO + " cpp_diario ON (he.ticker = cpp_diario.ticker "+
				"\n         						AND cpp_diario.portfolio = '" + this.STR_PORTFOLIO+ "')" +

				"\n LEFT JOIN ins_groups ins_grp ON (ins_grp.ins_group_id = he.ins_group_id)" +
				"\n LEFT JOIN " + EnumsUserTables.USER_MX_INS_GROUP_ACC_NUM + " acc_num ON (acc_num.ins_group_id = ins_grp.ins_group_id)" +
				"\n LEFT JOIN " + EnumsUserTables.USER_MX_NAV_DIARIO + " nav ON (nav.nav_date = TO_DATE('"+this.STR_TODAY+"', 'DD/MM/YYYY') " +
														" AND nav.nav_portfolio_id ="+ this.INT_PORTFOLIO + 
														" AND nav.nav_concepto = '"+EnumsCuentasNav.MX_ACTIVO_NETO.toString()+"'" +			
														")" +
				"\n WHERE " + 
				"\n ab.internal_portfolio = "+ this.INT_PORTFOLIO+ " AND" + 
				"\n ab.toolset IN ("+ TOOLSET_ENUM.BOND_TOOLSET.jvsValue()+", " + TOOLSET_ENUM.EQUITY_TOOLSET.jvsValue()+ ") AND" + 
				"\n ab.tran_status IN (" + TRAN_STATUS_ENUM.TRAN_STATUS_VALIDATED.toInt() + ", " + TRAN_STATUS_ENUM.TRAN_STATUS_CLOSEOUT.toInt() + ") AND" +
				"\n ab.tran_type = "+TRAN_TYPE_ENUM.TRAN_TYPE_TRADING.toInt() +" AND" + 
				"\n ab.ins_type NOT IN ("+EnumsInstrumentsMX.MX_EQT_SIEFORE.toInt() + ") AND"+	
				"\n ab.asset_type NOT IN (" + ASSET_TYPE_ENUM.ASSET_TYPE_REPO_COLLATERAL.jvsValue() +")" + 
				"\n ORDER BY acc_num.account_number" ;

		tReportTable = _Afore.getTable(sSql, "Cartera_Valores_" + this.STR_PORTFOLIO);	

		if(tReportTable.getNumRows()> 0){
			//TODO Obtiene datos pendientes de calculo.
			//costo_total_inversion
			tReportTable.addFormulaColumn("COL('nro_titulos')*COL('costo_cpp')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frm_costo_total_inversion");
			tReportTable.copyCol("frm_costo_total_inversion", tReportTable, "costo_total_inversion");
			
			//valor_mercado_total
			tReportTable.addFormulaColumn("COL('nro_titulos')*COL('precio_limpio_24h')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frm_valor_mercado_total");
			tReportTable.copyCol("frm_valor_mercado_total", tReportTable, "valor_mercado_total");
			
			//plusvalia
			tReportTable.addFormulaColumn("COL('valor_mercado_total')-COL('costo_total_inversion')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frm_plusvalia");
			tReportTable.copyCol("frm_plusvalia", tReportTable, "plusvalia");
			
			//interes
			tReportTable.addFormulaColumn("COL('nro_titulos')*COL('factor_interes_24h')", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frm_interes");
			tReportTable.copyCol("frm_interes", tReportTable, "interes");
			
			//porc_act_neto
			tReportTable.addFormulaColumn("iif(COL('nav_diario')==0.0, 0.0, COL('valor_mercado_total')/COL('nav_diario')*100)", COL_TYPE_ENUM.COL_DOUBLE.toInt(), "frm_porc_act_neto");
			tReportTable.copyCol("frm_porc_act_neto", tReportTable, "porc_act_neto");
			
			tReportTable.deleteFormulaColumn("frm_costo_total_inversion");
			tReportTable.deleteFormulaColumn("frm_valor_mercado_total");
			tReportTable.deleteFormulaColumn("frm_plusvalia");
			tReportTable.deleteFormulaColumn("frm_porc_act_neto");
			tReportTable.deleteFormulaColumn("frm_interes");
			
			//TODO Totaliza Resultados por Ins_Group + Ticker
			Table tReportTableClone = Util.NULL_TABLE;
			tReportTableClone = tReportTable.copyTable();
			tReportTable.group("ins_group_name, ticker");
			tReportTable.distinctRows();
			
			tReportTable.setColValDouble("nro_titulos", 0.0);
			tReportTable.setColValDouble("costo_total_inversion", 0.0);
			tReportTable.setColValDouble("valor_mercado_total", 0.0);
			tReportTable.setColValDouble("plusvalia", 0.0);
			tReportTable.setColValDouble("porc_act_neto", 0.0);
			tReportTable.setColValDouble("interes", 0.0);	
			
			tReportTable.fillSetSourceTable(tReportTableClone);
			tReportTable.fillAddMatch("ins_group_name", "ins_group_name");
			tReportTable.fillAddMatch("ticker", "ticker");
			
			tReportTable.fillAddData("nro_titulos", "nro_titulos");
			tReportTable.fillAddData("costo_total_inversion", "costo_total_inversion");
			tReportTable.fillAddData("valor_mercado_total", "valor_mercado_total");
			tReportTable.fillAddData("plusvalia", "plusvalia");
			tReportTable.fillAddData("porc_act_neto", "porc_act_neto");
			tReportTable.fillAddData("interes", "interes");
			tReportTable.fillSum();
			
			tReportTableClone.destroy();
		}
		else{
			LOG.printMsg(EnumTypeMessage.WARNING, "No se encontraron resultados para Cartera_Valores_" + this.STR_PORTFOLIO);
		}
		
		return tReportTable;

	}

	@SuppressWarnings("finally")
	public boolean genCrystal(Table data, Table tParam, String sFileExt) throws OException{
		boolean bRet = true;
		int iRet =0;
		
		String[] sDateSplit = this.STR_TODAY.split("\\/");		
		UTIL_Afore util = new UTIL_Afore();
			
		//Ejemplo outputFileName: C:\OpenLink\outdir\Consar\CARTERA_VALORES_$PORTFOLIO_$AAAAMMDD
		String crystalTemplate = util.getVariableGlobal("FINDUR", DOC_SCRIPT_NAME, "crystal_template");
		String sPath = util.getVariableGlobal("FINDUR", DOC_SCRIPT_NAME, "path");
		String sFileName = util.getVariableGlobal("FINDUR", DOC_SCRIPT_NAME, "filename");
			
		String outputFileName = sPath + sFileName + sFileExt;
		
		outputFileName = outputFileName.replace("$AAAAMMDD", sDateSplit[2] + sDateSplit[1] + sDateSplit[0] + "_" + Util.timeGetServerTimeHMS().replaceAll(":", ""));
		outputFileName = outputFileName.replace("$PORTFOLIO", this.STR_PORTFOLIO);
		try {
			
			switch (sFileExt){
			case ".xls": iRet = Crystal.tableExportCrystalReport(data, crystalTemplate, CRYSTAL_EXPORT_TYPES.MS_EXCEL, outputFileName, CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR, tParam);
						break;
			case ".pdf": iRet = Crystal.tableExportCrystalReport(data, crystalTemplate, CRYSTAL_EXPORT_TYPES.PDF, outputFileName, CRYSTAL_EXPORT_OPTIONS.NO_REPORT_DIR, tParam);
						break;

			default: LOG.printMsg(EnumTypeMessage.WARNING, "File Extension  Not Supported " + sFileExt);
						break;
			}
			
			if(iRet != OLF_RETURN_CODE.OLF_RETURN_SUCCEED.toInt()){
				LOG.printMsg(EnumTypeMessage.ERROR, "Unable to generate Crystal Report: " + outputFileName);
				bRet = false;
			}else
				LOG.printMsg(EnumTypeMessage.INFO, "Archivo " + outputFileName + " Generado Exitosamente...");
		}catch(NullPointerException np){
			LOG.printMsg(EnumTypeMessage.ERROR, "NPE::Unable to save output file, please check crystal_template/path/filename in 'user_configurable_variables'");
			bRet = false;
		}catch(OException oe){
			LOG.printMsg(EnumTypeMessage.ERROR, "Unable to generate Crystal Report: " + outputFileName + " ::: " + oe.getMessage());
			bRet = false;
		}finally{
			return bRet;
		}
	}
	

	public boolean getTipoCambio() throws OException {
		int iIndexId = 0;
		boolean bReturn = true;
		LOG.printMsg(EnumTypeMessage.INFO, "--> Getting Exchange Rate for each ccy (TODAY)...");
		
		try{
			iIndexId = Ref.getValue(SHM_USR_TABLES_ENUM.INDEX_TABLE, EnumsIndex.MX_IDX_SPOT_FX_MXN.getName());
			tCurveSpot = Table.tableNew("All_Grid_Points");
			tCurveSpot = Index.loadAllGpts(iIndexId);
			tCurveSpot.sortCol("name");
			tCurveSpot.setColName("input.mid", "tipo_cambio");

		}catch(OException oe){
			LOG.printMsg(EnumTypeMessage.ERROR, "--> Unable to Get Exchange Rate for each ccy...");
			bReturn = false;
		}
		
		return bReturn;
	}
	
	public Table getNavValues() throws OException{
		String sSql = 	"SELECT nav_id_concepto, nav_concepto, nav_value " +
				"\n FROM " + EnumsUserTables.USER_MX_NAV_DIARIO + " nav " +
				"\n WHERE nav.nav_date = TO_DATE('"+this.STR_TODAY+"', 'DD/MM/YYYY') AND" +
				"\n nav.nav_portfolio_id ="+ this.INT_PORTFOLIO + " AND" +
				"\n nav.nav_id_concepto IN ("+EnumsCuentasNav.MX_ACTIVO_NETO.toInt()+"," +
											  EnumsCuentasNav.MX_ACTIVO_TOTAL.toInt()+"," +
											  EnumsCuentasNav.MX_ACTIVO_BCOS_MONEDAS_EXT.toInt()+"," +
											  EnumsCuentasNav.MX_ACTIVO_CAJA_Y_BANCOS.toInt()+"," +
											  EnumsCuentasNav.MX_ACTIVO_DEUDA_Y_CAPITALES.toInt()+")" ;
		
		return _Afore.getTable(sSql, "NAV_DIARIO_" + this.STR_PORTFOLIO);	

	}

}